using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if __UNIFIED__
using Foundation;
using CoreGraphics;
using UIKit;
using ObjCRuntime;
#else
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using MonoTouch.ObjCRuntime;
#endif

namespace MobileUtilities.Multimedia
{
	public enum GetImageLocation
	{
		Camera,
		Library
	}
	
	public class ImageUtilities
	{
		public static bool HasCamera()
		{
			return UIImagePickerController.IsSourceTypeAvailable(UIImagePickerControllerSourceType.Camera);
		}
		
		// Referenced from http://stackoverflow.com/questions/538041/uiimagepickercontroller-camera-preview-is-portrait-in-landscape-app
		public static UIImage ScaleAndRotateImage (UIImage image, int maxResolution)
		{
			float width = image.CGImage.Width;
			float height = image.CGImage.Height;
			
			CGAffineTransform transform = CGAffineTransform.MakeIdentity ();
			RectangleF bounds = new RectangleF (0, 0, width, height);
			if (width > maxResolution || height > maxResolution) {
				float ratio = width / height;
				if (ratio > 1) {
					bounds.Size = new SizeF (maxResolution, Convert.ToSingle (bounds.Size.Width / ratio));

					//bounds.Size.Width = maxResolution;
					//bounds.Size.Height = Convert.ToSingle (bounds.Size.Width / ratio);
				} else {
					bounds.Size = new SizeF (Convert.ToSingle (bounds.Size.Height / ratio), maxResolution);
					//bounds.Size.Height = Convert.ToSingle (bounds.Size.Height / ratio);
					//bounds.Size.Width = maxResolution;
				}
			}
			
			float scaleRatio = bounds.Size.Width / width;
			SizeF imageSize = new SizeF (image.CGImage.Width, image.CGImage.Height);
			float boundHeight;
			UIImageOrientation orient = image.Orientation;
			
			return null;
			
//			switch (orient)
//			{
//			case UIImageOrientation.Up:
//				transform = CGAffineTransform.MakeIdentity;
//				break;
//			case UIImageOrientation.UpMirrored:
//				transform = CGAffineTransform.MakeTranslation(imageSize.Width, 0.0);
//				transform = CGAffineTransform.MakeScale(
//				
		}
		
		// Taken from http://www.fastchicken.co.nz/2011/03/28/scaling-and-rotating-an-image-in-monotouch/
		// Looks like a working version of the ObjC code I was tryng to port
		public static UIImage ScaleImage (UIImage image, int maxSize)
		{
			UIImage res;

			using (CGImage imageRef = image.CGImage) {
				CGImageAlphaInfo alphaInfo = imageRef.AlphaInfo;
				CGColorSpace colorSpaceInfo = CGColorSpace.CreateDeviceRGB ();
				if (alphaInfo == CGImageAlphaInfo.None) {
					alphaInfo = CGImageAlphaInfo.NoneSkipLast;
				}

				nint width, height;

				width = imageRef.Width;
				height = imageRef.Height;

				if (height >= width) {
					width = (int)Math.Floor ((double)width * ((double)maxSize / (double)height));
					height = maxSize;
				} else {
					height = (int)Math.Floor ((double)height * ((double)maxSize / (double)width));
					width = maxSize;
				}

				CGBitmapContext bitmap;

				if (image.Orientation == UIImageOrientation.Up || image.Orientation == UIImageOrientation.Down) {
					bitmap = new CGBitmapContext (IntPtr.Zero, width, height, imageRef.BitsPerComponent, imageRef.BytesPerRow, colorSpaceInfo, alphaInfo);
				} else {
					bitmap = new CGBitmapContext (IntPtr.Zero, height, width, imageRef.BitsPerComponent, imageRef.BytesPerRow, colorSpaceInfo, alphaInfo);
				}

				switch (image.Orientation) {
				case UIImageOrientation.Left:
					bitmap.RotateCTM ((float)Math.PI / 2);
					bitmap.TranslateCTM (0, -height);
					break;
				case UIImageOrientation.Right:
					bitmap.RotateCTM (-((float)Math.PI / 2));
					bitmap.TranslateCTM (-width, 0);
					break;
				case UIImageOrientation.Up:
					break;
				case UIImageOrientation.Down:
					bitmap.TranslateCTM (width, height);
					bitmap.RotateCTM (-(float)Math.PI);
					break;
				}

				bitmap.DrawImage (new Rectangle (0, 0, (int)width, (int)height), imageRef);

				res = UIImage.FromImage (bitmap.ToImage ());
				bitmap = null;

			}

			return res;
		}
	}
}


