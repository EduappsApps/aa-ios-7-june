using System;

namespace MobileUtilities.Reporting
{
	public interface IGetReportValue
	{
		string GetReportValue (string column);
	}
}

