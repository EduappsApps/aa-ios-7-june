using System;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting
{
	public class TiledPdfView : UIView
	{
		public TiledPdfView (RectangleF frame, nfloat scale)
			: base (frame)
		{
			CATiledLayer tiledLayer = Layer as CATiledLayer;
			tiledLayer.LevelsOfDetail = 4;
			tiledLayer.LevelsOfDetailBias = 4;
			tiledLayer.TileSize = new SizeF (512, 512);
			// here we still need to implement the delegate
			tiledLayer.Delegate = new TiledLayerDelegate (this);
			Scale = scale;
		}
		
		public CGPDFPage Page { get; set; }
		
		public nfloat Scale { get; set; }
		
		public override void Draw (RectangleF rect)
		{
			// empty (on purpose so the delegate will draw)
		}

		[Export ("layerClass")]
		public static Class LayerClass ()
		{
			// instruct that we want a CATileLayer (not the default CALayer) for the Layer property
			return new Class (typeof(CATiledLayer));
		}
	}
	
	class TiledLayerDelegate : CALayerDelegate
	{
		TiledPdfView view;
		
		public TiledLayerDelegate (TiledPdfView view)
		{
			this.view = view;
		}
		
		public override void DrawLayer (CALayer layer, CGContext context)
		{
			// keep a copy since (a) it's a _virtual_ property and (b) it could change between filling and flipping
			RectangleF bounds = view.Bounds;
			
			// fill with white background
			context.SetFillColor (1.0f, 1.0f, 1.0f, 1.0f);
			context.FillRect (bounds);
			context.SaveState ();

			// flip page so we render it as it's meant to be read
			context.TranslateCTM (0.0f, bounds.Height);
			context.ScaleCTM (1.0f, -1.0f);
	
			// scale page at the view-zoom level
			context.ScaleCTM (view.Scale, view.Scale);
			context.DrawPDFPage (view.Page);
			context.RestoreState ();
		}
	}
}

