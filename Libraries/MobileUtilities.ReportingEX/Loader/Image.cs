using System;
using System.Drawing;
using System.Xml;
using MobileUtilities.XML;
using UIKit;
using CoreGraphics;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting.Loader
{
	public class PrintImage : DrawableObject
	{
		#region Support for calculated image locations
				
		private ExpressionProcessor expressionParser = null;
		
		public void SetExpressionProcessor (ExpressionProcessor processor)
		{
			expressionParser = processor;
		}
		
		private string GetRealImagePath ()
		{
			ExpressionProcessor parser = GetExpressionParser ();
			if (parser != null) {
				return parser.EvaluateString (_path);
			} else 
				return "";
		}
		public ExpressionProcessor GetExpressionParser ()
		{
			return expressionParser;
		}
		
		#endregion
		
		private bool _stretch = true;

		public bool Stretch
		{
			get
			{
				return _stretch;
			}
			set
			{
				_stretch = value;
				RedrawDesigner();
			}
		}

		private string _path = "";
		private UIImage _image = null;
		public string Path {
			get {
				return _path;
			}
			set {
				if (_path != value) {
					_path = value;
				}
			}
		}

		public override void LoadFromXML (XmlElement e)
		{
			base.LoadFromXML (e);

			Path = XMLHelper.ReadXMLText (e, "Path", Path);
			Stretch = XMLHelper.ReadXMLBool (e, "Stretch", Stretch);
		}
		
		public override void Draw (CGContext context)
		{
			var actualPath = GetRealImagePath ();
			
			if (System.IO.File.Exists (actualPath))
				_image = UIImage.FromFile (actualPath);
			else
				_image = null;
					
			if (_image != null) {
				//TODO: Need to scale this image to proportianate dimensions
				_image.Draw (GetRectangle ());
			} else
				SetNoDimensions ();
		}
		
		private RectangleF GetImageScaleRectangle ()
		{
			RectangleF outerBounds = new RectangleF (Left, Top, Width, Height);
			
			nfloat imageRatio = _image.Size.Height / _image.Size.Width;
			nfloat scale = 1.0f;
			
			nfloat rectRatio = Height / Width;
			if (imageRatio < rectRatio) {
				// based on the widths
				scale = outerBounds.Width / _image.Size.Width;
			} else if (rectRatio < imageRatio) {
				scale = outerBounds.Height / _image.Size.Height;
			} else {
				return outerBounds;				
			}
			
			nfloat newWidth = _image.Size.Width * scale;
			nfloat newHeight = _image.Size.Height * scale;
			
			nfloat xOffset = (outerBounds.Width - newWidth) / 2;
			nfloat yOffset = (outerBounds.Height - newHeight) / 2;
			
			return new RectangleF (xOffset, yOffset, newWidth, newHeight);
		}
	}
}
