﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MobileUtilities.XML;
using CoreGraphics;
using Foundation;

namespace MobileUtilities.Reporting.Loader
{
    public class DataLabel : BaseLabel
    {
        protected override string DoGetText ()
		{
			ExpressionProcessor parser = GetExpressionParser ();
			if (parser != null)
				return parser.EvaluateString (ColumnName);
			else
				return "";
		}

        private string _ColumnName = "";

        public string ColumnName
        {
            get
            {
                return _ColumnName;
            }
            set
            {
                _ColumnName = value;
                RedrawDesigner();
            }
        }

        public override void LoadFromXML (System.Xml.XmlElement e)
		{
			base.LoadFromXML (e);

			ColumnName = XMLHelper.ReadXMLText (e, "ColumnName", ColumnName);
		}
		
		public ExpressionProcessor expressionParser = null;
		
		public ExpressionProcessor GetExpressionParser ()
		{
			return expressionParser;
		}
    }
	
	public class PageNumberLabel : BaseLabel
	{
		public PDFDocument document;
		
		public string DisplayFormat = "Page {0} of {1}";
		
		protected override string DoGetText ()
		{
			return String.Format (DisplayFormat, document.CurrentPage, document.pages.Count);
		}
	}
	
	public class HyperLinkText : PrintLabel
	{
		public string Url;
		
		public override void Draw (CGContext context)
		{
			base.Draw (context);
			
			if (context is CGContextPDF) {
				CGContextPDF pdfCtx = context as CGContextPDF;
				pdfCtx.SetUrl (new NSUrl (Url), GetRectangle());
			}
		}
	}
}
