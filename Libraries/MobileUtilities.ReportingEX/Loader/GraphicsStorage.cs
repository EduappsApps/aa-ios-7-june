using System;
using System.Drawing;
using System.Xml;
using UIKit;
using MobileUtilities.XML;

namespace MobileUtilities.Reporting.Loader
{
	public sealed class GraphicsStorage
	{
		public static UIColor ReadColor (XmlElement e, string name, UIColor defaultColor)
		{
			XmlElement colorNode = XMLHelper.GetChildNode (e, name);
			if (colorNode == null)
				return defaultColor;
			else {
				int a = Convert.ToInt32 (colorNode.GetAttribute ("a"));
				int r = Convert.ToInt32 (colorNode.GetAttribute ("r"));
				int g = Convert.ToInt32 (colorNode.GetAttribute ("g"));
				int b = Convert.ToInt32 (colorNode.GetAttribute ("b"));
				
				return UIColor.FromRGBA(r, g, b, a);
			}
		}
	}
}
