using System;
using System.Drawing;
using System.Xml;
using MobileUtilities.XML;
using UIKit;
using CoreGraphics;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting.Loader
{
	public abstract class BaseLine : DrawableObject
	{
		private int _penwidth = 1;

		public int LineWidth {
			get {
				return _penwidth;
			}
			set {
				if (_penwidth != value) {
					_penwidth = value;
					RedrawDesigner ();
				}
			}
		}
		
		private UIColor _lineColor = UIColor.Black;

		public UIColor LineColor {
			get {
				return _lineColor;
			}
			set {
				if (_lineColor != value) {
					_lineColor = value;
					RedrawDesigner ();
				}
			}
		}
		
		protected abstract void GetPoints(out PointF point1, out PointF point2);
		
		public override void Draw (CGContext context)
		{
			PointF p1, p2;
			GetPoints (out p1, out p2);
			
			LineColor.SetFill ();
			LineColor.SetStroke ();
			context.SetLineWidth (LineWidth);
			
			CGPath path = new CGPath ();
			path.AddLines (new PointF[] { p1, p2 });
			path.CloseSubpath ();
  
			context.AddPath (path);
			context.DrawPath (CGPathDrawingMode.FillStroke);
		}
		
		public override void LoadFromXML (XmlElement e)
		{
			base.LoadFromXML (e);
			
			_lineColor = GraphicsStorage.ReadColor (e, "Color", _lineColor);
			_penwidth = XMLHelper.ReadXMLInt (e, "LineWidth", _penwidth);
			RedrawDesigner ();
		}
	}

	public class HorizontalLine : BaseLine
	{
		protected override void GetPoints (out PointF point1, out PointF point2)
		{
			point1 = new PointF (Left, Top + Height / 2);
			point2 = new PointF(Left + Width, Top + Height / 2);
		}
	}

	public class VerticalLine : BaseLine
	{
		protected override void GetPoints (out PointF point1, out PointF point2)
		{
			point1 = new PointF (Left + Width / 2, Top);
			point2 = new PointF (Left + Width / 2, Top + Height);
		}
	}

	public class TopLeftToBottomRightLine : BaseLine
	{
		protected override void GetPoints (out PointF point1, out PointF point2)
		{
			point1 = new PointF (Left, Top);
			point2 = new PointF (Left + Width, Top + Height);
		}		
	}

	public class TopRightToBottomLeftLine : BaseLine
	{
		protected override void GetPoints (out PointF point1, out PointF point2)
		{
			point1 = new PointF (Left + Width, Top);
			point2 = new PointF (Left, Top + Height);
		}
	}
}
