using System;

namespace MobileUtilities.Reporting.Loader
{
	public sealed class PrintingIds
	{
		public const int Label = 300;
		public const int Image = 301;
		public const int BarCode = 302;
		public const int Shape = 303;
		public const int PageBreak = 304;
		public const int SystemLabel = 305;
		public const int Rectangle = 307;
		public const int Ellipse = 308;
		public const int HorizontalLine = 309;
		public const int VerticalLine = 310;
		public const int TopLeftToBottomRightLine = 311;
		public const int TopRightToBottomLeftLine = 312;
        public const int DataLabel = 313;
		
		public const int RepeatibleSection = 333;
	}
}
