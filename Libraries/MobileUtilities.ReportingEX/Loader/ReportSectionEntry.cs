using System;

namespace MobileUtilities.Reporting
{
	public class ReportSectionEntry
	{
		public string Name;
		public int Position;
		public bool IsRepeatible = false;
	}
}

