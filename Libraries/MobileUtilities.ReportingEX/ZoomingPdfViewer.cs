using UIKit;
using System.Drawing;
using System;
using Foundation;
using MobileUtilities.Forms;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting
{
	public partial class ZoomingPdfViewerViewController : BasePdfViewController
	{
		PdfScrollView _view;
		
		public ZoomingPdfViewerViewController (string filename, string title) : base ()
		{
			_filename = filename;
			
			RectangleF updatedBounds = new RectangleF (View.Bounds.Left, View.Bounds.Top, 
			                                           View.Bounds.Width, View.Bounds.Height - ControlHeights.ToolBarHeight); // was - 30
			
			_view = new PdfScrollView (updatedBounds, filename);
			_view.AlwaysBounceHorizontal = true;
			_view.AlwaysBounceVertical = true;
			
			View.AddSubview (_view);
			
			Title = title;
			
			UpdateButtonStates ();
		}
		
		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}
		
		protected override void DoNextPage ()
		{
			_view.NextPage();
		}
		
		protected override void DoPreviousPage ()
		{
			_view.PreviousPage ();
		}
		
		protected override void UpdateButtonStates ()
		{
			if (_view == null) {
				previousPage.Enabled = false;
				nextPage.Enabled = false;
			} else {
				previousPage.Enabled = _view.CurrentPage > 1;
				nextPage.Enabled = _view.CurrentPage < _view.PageCount;
			}
		}
	}
}

