using System;
using System.Collections.Generic;
using System.Drawing;
using UIKit;
using CoreGraphics;
using MobileUtilities.XML;
using System.Xml;
using MobileUtilities.Reporting.Loader;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Reporting
{
	/// <summary>
	/// A class that represents an item that will be drawn in the scene
	/// </summary>
	public abstract class Item : MobileUtilities.XML.XmlStoredItem
	{
		public abstract void Draw(CGContext context);
		
		// This is a read-only system. No storing will be done in this
		public override void StoreInXML (XmlElement e) { }
	}
	
	/// <summary>
	/// A class that represents drawing on a context. Can be used for view display or creating PDFs/PNGs
	/// </summary>
	public class Scene
	{
		public Scene ()
		{
		}
		
		public List<Item> Items = new List<Item>();
		public UIColor backgroundColor = UIColor.White;
		public bool DrawBackground = true;
		
		protected void DoBeforeDraw (CGContext context, RectangleF bounds)
		{
			if (OnBeforeDrawItems != null)
				OnBeforeDrawItems (this, context, bounds);	
		}
		
		protected void DoAfterDraw (CGContext context, RectangleF bounds)
		{
			if (OnAfterDrawItems != null)
				OnAfterDrawItems (this, context, bounds);
		}
		
		public virtual void Draw (CGContext context, RectangleF bounds)
		{
			if (DrawBackground) {
				backgroundColor.SetColor ();
				context.FillRect (bounds);
			}
			
			DoBeforeDraw(context, bounds);
			
			foreach (Item i in Items)
				i.Draw (context);
			
			DoAfterDraw(context, bounds);
		}
		
		// Events for various aspects
		public event SceneHandler OnBeforeDrawItems = null;
		public event SceneHandler OnAfterDrawItems = null;
	}
	
	
	public delegate void SceneHandler(object sender, CGContext context, RectangleF bounds);
	
	public class ItemTopDownSorter : Comparer<Item>
	{
		public override int Compare (Item x, Item y)
		{
			DrawableObject object1 = x as DrawableObject;
			DrawableObject object2 = y as DrawableObject;
			
			nfloat result = object1.Top - object2.Top;
			if (result == 0) {
				result = object1.Left - object2.Left;
			}
			
			return Convert.ToInt32(result);
		}
	}
}

