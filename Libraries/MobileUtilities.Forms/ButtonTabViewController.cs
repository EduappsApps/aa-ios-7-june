using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public class ButtonTabViewController : UITabBarController
	{
		public virtual UIImage GetButtonImage()
		{
			return null;
		}

		public virtual UIImage GetBackgroundImage()
		{
			return null;
		}

		public virtual string GetTitle()
		{
			return "";
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			AddCentreButtonWithOptions ();
		}

		public void AddCentreButtonWithOptions ()
		{
			UIButton button = UIButton.FromType(UIButtonType.Custom);
			button.AutoresizingMask = UIViewAutoresizing.FlexibleRightMargin | UIViewAutoresizing.FlexibleLeftMargin |
				UIViewAutoresizing.FlexibleTopMargin | UIViewAutoresizing.FlexibleBottomMargin;

			UIImage img = GetBackgroundImage();

			if (img != null) {
				button.Frame = new System.Drawing.RectangleF (0, 0, img.Size.Width, img.Size.Height);
				button.SetBackgroundImage (img, UIControlState.Normal);
				button.SetBackgroundImage (img, UIControlState.Highlighted);
			}

			button.TouchUpInside += HandleTouchUpInside;

			float heightDifference = img.Size.Height - this.TabBar.Frame.Size.Height;
			if (heightDifference < 0)
				button.Center = this.TabBar.Center;
			else {
				PointF center = this.TabBar.Center;
				center.Y = center.Y - heightDifference / 2.0f;
				button.Center = center;
			}
    
			UILabel lblName = new UILabel(new RectangleF(3, img.Size.Height-14, img.Size.Width - 6, 14));
			lblName.Text = GetTitle();
			lblName.Font = UIFont.BoldSystemFontOfSize(10.0f);
			lblName.TextAlignment = UITextAlignment.Center;
			lblName.BackgroundColor = UIColor.Clear;
			button.AddSubview(lblName);

			UIImageView imgIcon = new UIImageView(new RectangleF(3, 3, img.Size.Width - 6, 
			                                                     img.Size.Height - lblName.Bounds.Size.Height - 3));
    		imgIcon.ContentMode = UIViewContentMode.Center;
			imgIcon.Image = GetButtonImage();

			button.AddSubview(imgIcon);

			this.View.AddSubview(button);
		}

		void HandleTouchUpInside (object sender, EventArgs e)
		{
			if (ChangeIndexOnButtonSelect)
				this.SelectedIndex = IndexOnButtonSelect;

			if (OnButtonSelect != null)
				OnButtonSelect(this, EventArgs.Empty);
		}

		public bool ChangeIndexOnButtonSelect = true;
		public int IndexOnButtonSelect = 1;
		public event EventHandler OnButtonSelect = null;

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return true;
		}
	}
}

