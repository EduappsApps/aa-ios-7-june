using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public class ClickableImage : UIImageView
	{
		public ClickableImage (RectangleF bounds) : base(bounds)
		{
		}
		
		public event EventHandler OnTouchBegin = null;
		
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			
			if (OnTouchBegin != null)
				OnTouchBegin(this, EventArgs.Empty);
		}
	}
	
	public class ImageClickDismissViewController : UIViewController
	{
		public ImageClickDismissViewController (string filename) : base()
		{
			imageToView = filename;
		}
		
		string imageToView = "";
		
		ClickableImage v = null;
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			v = new ClickableImage (new RectangleF (0, 0, ControlHeights.FullScreenWidth, 
				ControlHeights.FullScreenHeight));
			v.UserInteractionEnabled = true;
			v.Image = UIImage.FromFile (imageToView);
			v.OnTouchBegin += ClickForm;
			View = v;
		}
		
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			
			UITouch t = evt.AllTouches.AnyObject as UITouch;
			PointF p = t.LocationInView (this.View);
			
			foreach (ClickRect r in rects) {
				if (p.X >= r.bounds.Left && p.X <= r.bounds.Right && p.Y >= r.bounds.Top && p.Y <= r.bounds.Bottom) {
					r.handler (this, EventArgs.Empty);
					return;
				}
			}
			
			DismissModalViewControllerAnimated (true);
		}
		
		public void AddClickRect (RectangleF bounds, EventHandler handler)
		{
			ClickRect entry = new ClickRect ();
			entry.bounds = bounds;
			entry.handler = handler;
			rects.Add(entry);
		}
		
		List<ClickRect> rects = new List<ClickRect>();
		
		public class ClickRect
		{
			public RectangleF bounds;
			public EventHandler handler;
			
		}
		public void ClickForm (object sender, EventArgs e)
		{
			DismissModalViewControllerAnimated (true);
		}
	}
}

