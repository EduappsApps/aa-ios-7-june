
using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MobileUtilities.Data;

namespace MobileUtilities.Forms
{
	public class GroupListSettings
	{
		public static bool ShouldSortGroupNames = false;
	}

	public abstract class AbstractGroupList<T> where T : class
	{
		Dictionary<string, GroupEntry<T>> hashList = new Dictionary<string, GroupEntry<T>> ();
		Dictionary<string, string> sectionIndexToTitle = new Dictionary<string, string>();
		
		protected abstract string GetGroupForItem(T item);
		
		public void AddItem (T item)
		{
			// Find the group name
			string groupName = GetGroupForItem (item);
				
			GroupEntry<T> g;
			if (hashList.ContainsKey (groupName)) {
				g = hashList [groupName];
			} else {
				g = new GroupEntry<T> (groupName);
				hashList [groupName] = g;
			}
			g.Items.Add(item);
		}

		public void RemoveItem (T item)
		{
			// Find the group name
			string groupName = GetGroupForItem (item);
			
			GroupEntry<T> g;
			if (hashList.ContainsKey (groupName)) {
				g = hashList [groupName];
				g.Items.Remove(item);
			} 
		}
			
		public List<string > GroupNames = new List<string> ();

		public string[] GetGroupNames ()
		{
			GroupNames.Clear ();
				
			foreach (string key in hashList.Keys)
				GroupNames.Add (key);
				
			if (GroupListSettings.ShouldSortGroupNames)
				GroupNames.Sort ();
			
			// Setup the alphabetical sorting
			foreach (string s in GroupNames) {
				
				var charString = s.Substring (0, 1).ToUpper ();
				
				if (!sectionIndexToTitle.ContainsKey (charString)) {
					sectionIndexToTitle [charString] = s;
				}
			}
			
			return GroupNames.ToArray ();
		}
			
		public GroupEntry<T> GetGroupForName (string name)
		{
			return hashList [name];
		}
			
		public void Clear ()
		{
			hashList.Clear ();
		}
	}
	
	public class GroupList<T> : AbstractGroupList<T> where T : class
	{
		SearchViewController<T> _svc;
			
		public GroupList (SearchViewController<T> svc)
		{
			_svc = svc;
		}
		
		protected override string GetGroupForItem (T item)
		{
			return _svc.GetGroupForItem (item);
		}
	}
	
	public class PaneledGroupList<T> : AbstractGroupList<T> where T : class
	{
		PaneledViewController<T> _pvc;
		
		public PaneledGroupList (PaneledViewController<T> pvc)
		{
			_pvc = pvc;
		}
		
		protected override string GetGroupForItem (T item)
		{
			return _pvc.GetGroupForItem (item);
		}
	}
}
