using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public abstract class AbstractHolder
	{
		protected RectangleF _bounds;
		protected Entry _entry;
		
		public AbstractHolder (RectangleF bounds, Entry entry)
		{
			_bounds = bounds;
			_entry = entry;
		}
		
		public abstract UIView[] GetViews ();
		
		public void ShowInCell (UITableViewCell cell)
		{
			cell.AddSubviews (GetViews ());
		}
		
		public abstract void SetupControls();

		public virtual void SetFont(UIFont font)
		{

		}
	}
}
