using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class DialogHelpers
	{
		public static void ShowMessage(string title, string message)
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			UIAlertView view = new UIAlertView(title, message, null, null, "Close");
			view.Show();			
		}
				
		public static string ReadInput(string title, string message, EventHandler OnSuccess, EventHandler OnFailure)
		{
			UITextField tf = new UITextField (new System.Drawing.RectangleF (12f, 45f, 260f, 25f));
			tf.BackgroundColor = UIColor.White;
			tf.UserInteractionEnabled = true;
			tf.AutocorrectionType = UITextAutocorrectionType.No;
			tf.AutocapitalizationType = UITextAutocapitalizationType.None;
			tf.ReturnKeyType = UIReturnKeyType.Done;
			tf.SecureTextEntry = false;  // Set this to true if you want a password-style text masking

			UIAlertView myAlertView = new UIAlertView()
			{
				Title = title,
				Message = message
			};

			myAlertView.AddButton("Cancel");
			myAlertView.AddButton("Ok");
			myAlertView.AddSubview(tf);

			myAlertView.Transform = MonoTouch.CoreGraphics.CGAffineTransform.MakeTranslation (0f, 110f);
			
			string resultValue = "";
			myAlertView.Canceled += delegate(object sender, EventArgs e) {
				resultValue = "";
				OnFailure(sender, e);
			};
			myAlertView.Clicked += delegate(object sender, UIButtonEventArgs e) {
				resultValue = tf.Text;
				OnSuccess(tf.Text, EventArgs.Empty);
			};
			
			myAlertView.Show();	
			
			return resultValue;
		}

		public static void AskYesNo(string title, string message, EventHandler OnYes, EventHandler OnNo)
		{
			UIAlertView myAlertView = new UIAlertView()
			{
				Title = title,
				Message = message
			};

			myAlertView.AddButton("Yes");
			myAlertView.AddButton("No");

			myAlertView.Transform = MonoTouch.CoreGraphics.CGAffineTransform.MakeTranslation (0f, 110f);

			myAlertView.Canceled += delegate(object sender, EventArgs e) {
				OnNo(sender, e);
			};
			myAlertView.Clicked += delegate(object sender, UIButtonEventArgs e) {
				if (e.ButtonIndex == 0)
					OnYes(sender, e);
				else
					OnNo(sender, e);
			};

			myAlertView.Show();	
		}
	}
	
	// Taken from https://github.com/kevinmcmahon/CustomUITextFieldAlertView/blob/master/CustomTextFieldAlertView/TextFieldAlertView.cs
	public class TextFieldAlertView : UIAlertView
	{
		private UITextField _tf;
		private bool _secureTextEntry;

		public TextFieldAlertView() : this(false) {}

		public TextFieldAlertView(bool secureTextEntry, string title, string message, UIAlertViewDelegate alertViewDelegate, string cancelBtnTitle, params string[] otherButtons)
			: base(title, message, alertViewDelegate, cancelBtnTitle, otherButtons)
		{
			InitializeControl(secureTextEntry);
		}

		public TextFieldAlertView(bool secureTextEntry)
		{	
			InitializeControl(secureTextEntry);
		}

		private void InitializeControl(bool secureTextEntry)
		{
			_secureTextEntry = secureTextEntry;
			this.AddButton("Cancel");
			this.AddButton("Ok");
			// shift the control up so the keyboard won't hide the control when activated.
			this.Transform = MonoTouch.CoreGraphics.CGAffineTransform.MakeTranslation(0f, 110f);
		}

		public string EnteredText { get { return _tf.Text; } }

		public override void LayoutSubviews ()
		{
			// layout the stock UIAlertView control
			base.LayoutSubviews ();

			// build out the text field
			_tf = ComposeTextFieldControl(_secureTextEntry);

			// add the text field to the alert view
			this.AddSubview(_tf);

			// shift the contents of the alert view around to accomodate the extra text field
			AdjustControlSize();
		}

		private UITextField ComposeTextFieldControl(bool secureTextEntry)
		{
			UITextField textField = new UITextField (new System.Drawing.RectangleF (12f, 45f, 260f, 25f));
			textField.BackgroundColor = UIColor.White;
			textField.UserInteractionEnabled = true;
			textField.AutocorrectionType = UITextAutocorrectionType.No;
			textField.AutocapitalizationType = UITextAutocapitalizationType.None;
			textField.ReturnKeyType = UIReturnKeyType.Done;
			textField.SecureTextEntry = secureTextEntry;
			return textField;
		}

		private void AdjustControlSize()
		{
			float tfExtH = _tf.Frame.Size.Height + 16.0f;

			RectangleF frame = new RectangleF(this.Frame.X, 
			                                  this.Frame.Y - tfExtH/2,
			                                  this.Frame.Size.Width,
			                                  this.Frame.Size.Height + tfExtH);
			this.Frame = frame;

			foreach(var view in this.Subviews)
			{
				if(view is UIControl)
				{
					view.Frame = new RectangleF(view.Frame.X, 
					                            view.Frame.Y + tfExtH,
					                            view.Frame.Size.Width, 
					                            view.Frame.Size.Height);
				}
			}
		}
	}
}

