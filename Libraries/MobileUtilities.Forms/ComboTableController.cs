using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class ComboTableController : UITableViewController
	{
		static NSString kCellIdentifierSubtitle = new NSString ("FormsComboEntrtySubItemIdent");
		
		public event EventHandler OnValueChange = null;
		public event EventHandler OnClearValue = null;
		
		public string[] values = new string[] { };
		public string[] descriptions = new string[] { };
		public string[] images = new string[] { };
		
		public string SelectedValue = "";
		string _title = "Select";
		
		public ComboTableController (IntPtr p) : base(p)
		{

		}

		public ComboTableController(string title) : base()
		{
			this._title = title;
		}
			
		
		class DataSource : UITableViewDataSource 
		{
			ComboTableController tvc;

			public DataSource(ComboTableController tvc)
			{
				this.tvc = tvc;
			}
			
			public override int RowsInSection (UITableView tableView, int section)
			{
				return tvc.values.Length;
			}
			
			public override int NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				UITableViewCell cell = tableView.DequeueReusableCell (kCellIdentifierSubtitle);
				if (cell == null)
					cell = new UITableViewCell (UITableViewCellStyle.Subtitle, kCellIdentifierSubtitle);
				
				cell.TextLabel.Text = tvc.values [indexPath.Row];
				
				if (tvc.descriptions != null && tvc.descriptions.Length > 0)
					cell.DetailTextLabel.Text = tvc.descriptions [indexPath.Row];
				else
					cell.DetailTextLabel.Text = "";
				
				cell.Accessory = UITableViewCellAccessory.None;
				if (tvc.images != null && tvc.images.Length > 0) {
					string filename = tvc.images [indexPath.Row];
					if (filename.Length > 0) {
						cell.ImageView.Image = UIImage.FromFile (tvc.images [indexPath.Row]);
						cell.ImageView.HighlightedImage = UIImage.FromFile (tvc.images [indexPath.Row]);
					}
				}
				
				// Highlight the selected value 
				if (tvc.values [indexPath.Row] == tvc.SelectedValue) {
					cell.Accessory = UITableViewCellAccessory.Checkmark;
				} else {
					cell.Accessory = UITableViewCellAccessory.None;
				}
				
				return cell;
			}
		}
		
		class TableDelegate : UITableViewDelegate
		{
			ComboTableController tvc;
			
			public TableDelegate(ComboTableController tvc)
			{
				this.tvc = tvc;
			}
			
			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				// Store the value
				tvc.SelectedValue = tvc.values[indexPath.Row];
				if (tvc.OnValueChange != null)
					tvc.OnValueChange(tvc, EventArgs.Empty);
				this.tvc.NavigationController.PopViewControllerAnimated(true);
			}
		}
		
		private void ClearComboSelection(object sender, EventArgs e)
		{
			// Clear the state value
			SelectedValue = "";
			if (OnValueChange != null)
				OnValueChange(this, EventArgs.Empty);
			
			NavigationController.PopViewControllerAnimated(true);
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			Title = _title;
			TableView.Delegate = new TableDelegate(this);
			TableView.DataSource = new DataSource(this);
			
			NavigationItem.RightBarButtonItem = 
				new UIBarButtonItem("Clear", PlatformButtonStyle.Bordered, 
				                    new EventHandler(ClearComboSelection));
		}
	}
}