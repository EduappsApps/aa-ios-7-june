using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;

namespace MobileUtilities.Forms
{
	public enum DateTimeViewControllerMode
	{
		Date,
		DateTime,
		Time
	}
	
	public enum SelectDateFormPart
	{
		Date,
		Time
	}	
}

