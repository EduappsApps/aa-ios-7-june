using System;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class PickerDataModel : UIPickerViewModel
	{
		Entry _entry;
		
		public PickerDataModel (Entry entry)
		{
			_entry = entry;
			_items.AddRange(entry.Options);
		}
		
		public event EventHandler<EventArgs> ValueChanged;
		
		public List<string> Items {
			get { return this._items; }
			set { this._items = value; }
		}

		List<string> _items = new List<string> ();
		
		public string SelectedItem {
			get { return this._items [this._selectedIndex]; }
		}

		protected int _selectedIndex = 0;
		
		public PickerDataModel ()
		{
		}
		
		public override int GetRowsInComponent (UIPickerView picker, int component)
		{
			return this._items.Count;
		}

		public override string GetTitle (UIPickerView picker, int row, int component)
		{
			return this._items [row];
		}

		public override int GetComponentCount (UIPickerView picker)
		{
			return 1;
		}
		
		public override void Selected (UIPickerView picker, int row, int component)
		{
			this._selectedIndex = row;
			if (this.ValueChanged != null) {
				this.ValueChanged (this, new EventArgs ());
			}
		}
		
		public void WriteChanges ()
		{
			_entry.Value = SelectedItem;
		}
	}
}

