using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class WebNavigateSection : UIViewController
	{
		private string _url;
		private string _title;
		private string _content;

		public WebNavigateSection (string url, string title) : base()
		{
			_url = url;

			if (!_url.StartsWith("http"))
				_url = "http://" + _url;



			_title = title;
		}

		public WebNavigateSection(string content) : base()
		{
			_url = "";
			_content = content;
			_title = "";
		}
		
		UIWebView webView = null;
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			Title = _title;
			
			webView = new UIWebView (this.View.Bounds);
			webView.ScalesPageToFit = true;
			webView.LoadStarted += (sender, e) => {
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			};
			webView.LoadFinished += (sender, e) => {
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			};
			View.Add (webView);

			if (_url != null && _url != "") {
				NSUrlRequest r = NSUrlRequest.FromUrl (new NSUrl (_url));
				webView.LoadRequest (r);

				NavigationItem.SetRightBarButtonItem(new UIBarButtonItem (UIBarButtonSystemItem.Action, SelectWebPageOptions), true);
			} else {
				webView.LoadHtmlString(_content, null);
			}
		}

		UIActionSheet _sheet;

		void SelectWebPageOptions(object sender, EventArgs e)
		{
			// Display the popup which should be "Open in Safari" and Cancel
			_sheet = new UIActionSheet ("Actions", null, "Cancel", null, "Open in Safari");
			_sheet.Canceled += (sender2, e2) => {

			};
			_sheet.Clicked += (sender3, eventDetails) => {
				switch (eventDetails.ButtonIndex)
				{
					case 0:
						// Open the url in Safari
						var url = new NSUrl(_url);
						UIApplication.SharedApplication.OpenUrl(url);
						break;
				}
			};
			_sheet.ShowInView (NavigationController.View);
		}
	}
}

