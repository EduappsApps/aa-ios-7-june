using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace MobileUtilities.Forms
{
	public class UIGradientView : UIView 
	{
		public UIColor startColor = UIColor.FromRGBA(0.816f, 0.835f, 0.839f, 1.000f);
		public UIColor endColor = UIColor.FromRGBA(0.910f, 0.922f, 0.925f, 1.000f);

		public UIGradientView(RectangleF rect) : base(rect)
		{

		}

		public override void Draw (RectangleF rect)
		{
			var colorSpace = CGColorSpace.CreateDeviceRGB();
			var context = UIGraphics.GetCurrentContext();
			//// Gradient Declarations
			var gradientColors = new CGColor [] { startColor.CGColor, endColor.CGColor };
			var gradientLocations = new float [] {0, 1};
			var gradient = new CGGradient(colorSpace, gradientColors, gradientLocations);
			
			var rectanglePath = UIBezierPath.FromRect(rect);
			context.SaveState();
			rectanglePath.AddClip();
			context.DrawLinearGradient(gradient, new PointF(rect.Left + rect.Width / 2, rect.Top), 
			                           new PointF(rect.Left + rect.Width / 2, rect.Bottom), 0);
			context.RestoreState();

		}
	}
}
