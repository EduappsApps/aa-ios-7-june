using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;

namespace MobileUtilities.Forms
{
	public class FontedTitleView
	{
		public static UILabel TitleView (string Text, string FontName, float fontSize)
		{
			var mainLabel = new UILabel (new RectangleF (0, 4, 180, 40));
			mainLabel.BackgroundColor = UIColor.Clear;
			mainLabel.Text = Text;
			mainLabel.Font = UIFont.FromName (FontName, fontSize);
			mainLabel.TextColor = UIColor.White;
			mainLabel.LineBreakMode = UILineBreakMode.WordWrap;
			mainLabel.Lines = 0;
			mainLabel.ShadowColor = UIColor.Gray;
			mainLabel.TextAlignment = UITextAlignment.Center;
				
			return mainLabel;
		}
	}

}

