using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MobileUtilities.Data;
using MobileUtilities.Network;

namespace MobileUtilities.Forms
{
	public class SearchableTableViewController<T> : UITableViewController where T : class
	{
		public SearchableTableViewController (IntPtr p) : base(p)
		{

		}

		public SearchViewController<T> searchViewController = null;
		
		public SearchableTableViewController(SearchViewController<T> svc) : base()
		{
			this.searchViewController = svc;
		}
		
		public List<T> Items
		{
			get
			{
				return searchViewController.CurrentItems; 
			}
		}
				
		class DataSource : UITableViewDataSource 
		{
			SearchableTableViewController<T> tvc;
			
			public DataSource(SearchableTableViewController<T> tvc)
			{
				this.tvc = tvc;
			}

			private int LoadMoreOffset()
			{
				if (tvc.searchViewController.CanLoadMore)
					return 1;
				else
					return 0;
			}

			public override int RowsInSection (UITableView tableView, int section)
			{
				if (!tvc.searchViewController.IsGrouped)
					return tvc.Items.Count + LoadMoreOffset();
				else {
					string groupName = tvc.searchViewController.CurrentGroup.GroupNames [section];
					GroupEntry<T> e = tvc.searchViewController.CurrentGroup.GetGroupForName(groupName);
					return e.Items.Count;
				}
			}
			
			public override int NumberOfSections (UITableView tableView)
			{
				if (!tvc.searchViewController.IsGrouped)
					return 1;
				else {
					return tvc.searchViewController.CurrentGroup.GroupNames.Count;
				}
			}
			
			#region Methods for displaying the index
			
			public override string TitleForHeader (UITableView tableView, int section)
			{
				if (!tvc.searchViewController.IsGrouped)
					return "";
				else {
					return tvc.searchViewController.CurrentGroup.GroupNames [section];
				}
			}
			
			public override string[] SectionIndexTitles (UITableView tableView)
			{
				if (!tvc.searchViewController.IsGrouped)
					return new string[] { };
				else {
					if (tvc.searchViewController.ShowGroupIndex)
						return GroupIndexHelper.indexEntries;
					else
						return new string[] { };
				}
			}

//			NSArray array;
//
//        	[Export ("sectionIndexTitlesForTableView:")]
//			public NSArray SectionTitles (UITableView tableview)
//			{                   
//				if (array == null) {           
//					array = NSArray.FromStrings (SectionIndexTitles (tableview));
//				}
//
//				return array;
//			}
			
			public override int SectionFor (UITableView tableView, string title, int atIndex)
			{
				if (!tvc.searchViewController.IsGrouped)
					return 0;
				else {
					// Need to find the first group that starts with it
					int foundIndex = -1;
					
					int closestDistance = 1000;
					int closestIndex = -1;
					
					var titleCharacter = GroupingHelper.GetFirstLetter(title);
					var titleByte = Encoding.ASCII.GetBytes (titleCharacter)[0];

					for (int i=0; i < GroupIndexHelper.indexEntries.Length; i++) {
						var s = GroupIndexHelper.indexEntries [i];
						if (s == titleCharacter) {
							// We have a match
							return i;
						} else {
							byte[] asciiBytes = Encoding.ASCII.GetBytes (s);
							
							if (asciiBytes.Length > 0) {
								// See if its a closer match
								var firstByte = asciiBytes [0];
								
								int distance = Math.Abs (firstByte - titleByte);
								if (distance < closestDistance) {
									closestDistance = distance;
									closestIndex = i;
								}
							}
						}
					}
					
					return closestIndex;
					//return tvc.searchViewController.CurrentGroup.GroupNames.IndexOf (title);
				}
			}
			
			#endregion
			
			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				var cell = tableView.DequeueReusableCell(tvc.searchViewController.GetCellIdentifier());
				if (cell == null)
					cell = new UITableViewCell(tvc.searchViewController.drawStyle, 
						tvc.searchViewController.GetCellIdentifier());
				
				return tvc.searchViewController.InternalDrawCell(tvc.searchViewController.GetItemAtIndex(indexPath), tableView, indexPath);
			}

			public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
			{
				if (editingStyle == UITableViewCellEditingStyle.Delete) {
					// Delete the item from the main controller
					var item = tvc.searchViewController.GetItemAtIndex (indexPath);
					
					tvc.searchViewController.RemoveItem (item);
					tvc.searchViewController.CurrentItems.Remove (item);
					tvc.searchViewController.CurrentGroup.RemoveItem (item);

					if (tvc.searchViewController.IsGrouped)
					{
						if (tvc.searchViewController.CurrentMode == SeachViewControllerMode.Data)
							tvc.searchViewController.ConvertItemsToGroup(tvc.searchViewController.Items, tvc.searchViewController.GroupedItems);
						else
							tvc.searchViewController.ConvertItemsToGroup(tvc.searchViewController.SearchItems, tvc.searchViewController.GroupedSearchItems);
					}

					tvc.TableView.ReloadData ();
				}
			}

			public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
			{
				var item = tvc.searchViewController.GetItemAtIndex (indexPath);
				return tvc.searchViewController.CanEditItem(item);
			}
		}
		
		public class TableDelegate : UITableViewDelegate
		{
			SearchableTableViewController<T> tvc;
			
			public TableDelegate(SearchableTableViewController<T> tvc)
			{
				this.tvc = tvc;
			}

			public void RunLoadMore()
			{
				tvc.RunLoadMore ();
			}

			public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				return tvc.searchViewController.GetCellHeight ();
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				// Only do this if we are not loading.
				if (tvc.TableView.DataSource is DisplayStatusMessageTableViewDataSource)
					return;

				if (!tvc.searchViewController.CanLoadMore)
					tvc.searchViewController.DoRowSelected(tvc.searchViewController.GetItemAtIndex (indexPath));
				else {
					if (indexPath.Row == tvc.searchViewController.CurrentItems.Count)
					{
						RunLoadMore();
					} else {
						tvc.searchViewController.DoRowSelected(tvc.searchViewController.GetItemAtIndex (indexPath));
					}
				}
			}
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			TableView.Delegate = new TableDelegate (this);
			TableView.DataSource = new DataSource (this);
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
		}
		
		public virtual bool ShowFormToolbar()
		{
			return false;
		}
		
		public void RefreshTableDisplay()
		{
			TableView.DataSource = new DataSource(this);
			TableView.Delegate = new TableDelegate(this);
			TableView.ReloadData();
		}
		
		public void DisplayNoFoundSearchResults()
		{
			TableView.DataSource = new DisplayNoResultsDataSource("No results found");
			TableView.Delegate = new DoNothingTableDelegate(this);
			TableView.ReloadData();
		}

		public void InternalLoadMore ()
		{
			bool populateData;
			
			T[] items;
			if (searchViewController.CurrentMode == SeachViewControllerMode.Data) {
				items = searchViewController.LoadMoreItems ();
				populateData = true;
			} else {
				items = searchViewController.LoadMoreSearchItems ();
				populateData = false;
			}
			
			// Update the display with the list
			searchViewController.InvokeOnMainThread(delegate {
				if (populateData)
					searchViewController.Items.AddRange(items);
				else
					searchViewController.SearchItems.AddRange(items);
				
				searchViewController.IsLoadingMore = false;
				
				// Update the data with the Grouping
				if (searchViewController.IsGrouped) {
					if (searchViewController.CurrentMode == SeachViewControllerMode.Data)
						searchViewController.ConvertItemsToGroup (searchViewController.Items, searchViewController.GroupedItems);
					else
						searchViewController.ConvertItemsToGroup (searchViewController.SearchItems, searchViewController.GroupedSearchItems);
				}
				
				TableView.ReloadData();
			});
		}

		public void RunLoadMore ()
		{
			searchViewController.IsLoadingMore = true;
			TableView.ReloadData();
			
			ThreadStart job = new ThreadStart(InternalLoadMore);
			Thread thread = new Thread(job);
			thread.Start();
		}
	}
	
	public enum SeachViewControllerMode
	{
		Data,
		Search
	}
	
	/// <summary>
	/// Class that manages the display of the searches and handles them appropriatly
	/// </summary>
	public abstract class SearchViewController<T> : UIViewController, IDataObjectUpdate, IShowOrHideAd where T : class
	{
		string _title = "";
		public SearchableTableViewController<T> tvc;
		
		public T GetItemAtIndex (NSIndexPath indexPath)
		{
			if (!IsGrouped)
			{
				if (indexPath.Row == CurrentItems.Count)
					return null;
				else
					return CurrentItems [indexPath.Row];
			} else {
				if (indexPath.Section >= CurrentGroup.GroupNames.Count)
					return null;

				string groupName = CurrentGroup.GroupNames [indexPath.Section];
				var g = CurrentGroup.GetGroupForName (groupName);
				if (g == null)
					return null;
				else 
				{
					if (indexPath.Row < g.Items.Count)
						return g.Items [indexPath.Row];
					else
						return null;
				}
			}
		}

		UIRefreshControl _refreshControl = null;

		public void RunLoadMore ()
		{
			tvc.RunLoadMore ();
		}

		public void SetupPullToRefresh ()
		{
			// Its not working yet
			return;

			if (UIDevice.CurrentDevice.CheckSystemVersion (6, 0)) {
				_refreshControl = new UIRefreshControl ();
				this.tvc.RefreshControl = _refreshControl;
				_refreshControl.ValueChanged += HandleOnRefreshDataLoad;
			}
		}

		public void RefreshData ()
		{
			if (_currentMode == SeachViewControllerMode.Data)
				StartAsyncDataRetrieve ();
			else
				SearchFor (SearchText);
		}

		void HandleOnRefreshDataLoad (object sender, EventArgs e)
		{
			RefreshData ();

			InvokeOnMainThread (() => { 
				_refreshControl.EndRefreshing (); 
			});
		}

		public SearchViewController (string title) : base()
		{
			// Update the view so we can get finer control over laying out the objects
			_title = title;
			NotifyList = new DataObjectObservers (this);
			
			GroupedItems = new GroupList<T> (this);
			GroupedSearchItems = new GroupList<T> (this);
		}
		
		public void AddModalClose (string text)
		{
			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (text, UIBarButtonItemStyle.Done, 
				delegate(object sender, EventArgs e)
			{
				this.DismissModalViewControllerAnimated (true);
			});
		}

		public bool EditButtonOnRight = true;

		private void PlaceEditButton(UIBarButtonItem button)
		{
			if (EditButtonOnRight)
				NavigationItem.SetRightBarButtonItem(button, true);
			else
				NavigationItem.SetLeftBarButtonItem(button, true);
		}
		
		public void AddEditButton ()
		{
			PlaceEditButton(PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit, 
				StartTheEditMode));
		}

		private void StartTheEditMode (object sender, EventArgs e)
		{
			tvc.SetEditing (true, true);
			
			PlaceEditButton (
				PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Done, new EventHandler (FinishEditing)));
		}

		public virtual bool CanEditItem(T item)
		{
			return true;
		}
		
		private void FinishEditing (object sender, EventArgs e)
		{
			tvc.SetEditing (false, true);
			PlaceEditButton(PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit,
				new EventHandler (StartTheEditMode)));
		}
		
		public string SearchBarPlaceholder = "";
		
		public bool ShowSearchOption = true;
		
		public FormHeightMode HeightMode = FormHeightMode.NavigationController;

		public bool AutoLoadMore = false;
		
		#region Code for animation. Mainly used for Ads
		
		protected virtual void GetAnimateBelowBounds (UIView view, out RectangleF tableBounds, out RectangleF vBounds)
		{
			if (ShowSearchOption) {
				vBounds = new RectangleF (0, 41, 320, view.Bounds.Height);
				tableBounds = new RectangleF (0, 41 + view.Bounds.Height, 320, 439 - view.Bounds.Height - 110);
			} else {
				vBounds = RectangleF.Empty;
				tableBounds = new RectangleF (0, view.Bounds.Height, 320, 480 - view.Bounds.Height - 110);
			}
		}
		
		public void AnimateBelowSearchBar (UIView view)
		{
			RectangleF tableBounds, vBounds;
			
			GetAnimateBelowBounds (view, out tableBounds, out vBounds);
						
			view.Alpha = 0.0f;
			view.Frame = vBounds;
				
			UIView.BeginAnimations ("AnimateBelowSearch"); 
			UIView.SetAnimationDuration (0.5);
			
			tvc.TableView.Frame = tableBounds;
			
			if (view != null)
				view.Alpha = 1.0f;

			UIView.CommitAnimations ();
		}
		
		public void RevertAnimation (UIView view)
		{
			RectangleF tableBounds;
			
			if (ShowSearchOption) {
				tableBounds = new RectangleF (0, 41, 320, 439);
			} else {
				tableBounds = new RectangleF (0, 0, 320, 480);
			}
			
			UIView.BeginAnimations ("AnimateBelowSearch"); 
			UIView.SetAnimationDuration (0.5);
			
			tvc.TableView.Frame = tableBounds;
			
			if (view != null)
				view.Alpha = 0.0f;

			UIView.CommitAnimations ();
		}
		
		#endregion
		
		public virtual void GetBounds (out RectangleF searchBarBounds, out RectangleF tableBounds)
		{
			if (ShowSearchOption) {
				searchBarBounds = new RectangleF (0, 0, 320, 41);
				tableBounds = new RectangleF (0, 41, 320, 439);
			} else {
				searchBarBounds = new RectangleF (0, 0, 320, 41);
				tableBounds = new RectangleF (0, 0, 320, 480);
			}
			
			// Substract the height of the various controls where required
			if (HeightMode == FormHeightMode.NavigationControllerInTabBar) 
				tableBounds.Height = tableBounds.Height - ControlHeights.TabBarHeight / 2 + 10;

		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			Title = _title;
		
			this.View = new SearchViewControllerView<T> (this);
			
			SetupDisplay ();
		}
		
		private void SetupDisplay ()
		{
			RectangleF searchBarBounds, tableBounds;
			
			// The following call allows for other dimension based systems to be drawn
			// such as for iPad split view screens and popups
			GetBounds (out searchBarBounds, out tableBounds);
			
			if (ShowSearchOption) {
				// Setup the Search Controller
				searchBar = new UISearchBar (searchBarBounds);
				searchBar.Placeholder = SearchBarPlaceholder;
				Theme.ApplyThemeToSearchBar(searchBar);

				View.AddSubview (searchBar);
			}
			
			// Setup the display of the view. We need the Search Bar and also the Table View Controller
			tvc = new SearchableTableViewController<T> (this);
			
			View.AddSubview (tvc.View);
			tvc.View.Frame = tableBounds;
			
			if (ShowSearchOption) {
				searchBar.Delegate = new SearchBarDelegate (tvc);
				
				// Setup the search fade screen for searching mode
				disableViewOverlay = new UIView (tableBounds);
				disableViewOverlay.BackgroundColor = UIColor.Black;
				disableViewOverlay.Alpha = 0;
			}
			
			View.SetNeedsLayout();
		}
						
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			
			if (tvc != null)
				tvc.TableView.ReloadData ();
		}
		
		public void AddItemToList (T item)
		{
			Items.Add (item);
			
			if (IsGrouped) {
				ConvertItemsToGroup (Items, GroupedItems);
			}
			
			tvc.TableView.ReloadData();
		}
		
		internal UIView disableViewOverlay = null;
			
		public bool ShowGroupIndex = true;

		class SearchBarDelegate : UISearchBarDelegate
		{
			SearchableTableViewController<T> tvc;
			
			public SearchBarDelegate(SearchableTableViewController<T> tvc)
			{
				this.tvc = tvc;
			}
			
			public override bool ShouldBeginEditing (UISearchBar searchBar)
			{
				tvc.Editing = false;
				tvc.searchViewController.CurrentMode = SeachViewControllerMode.Search;
				
				tvc.searchViewController.searchBar.SetShowsCancelButton (true, true);
				tvc.TableView.AllowsSelection = false;
				tvc.TableView.ScrollEnabled = false;
				
				// Fade in the disabled overlay
				tvc.searchViewController.disableViewOverlay.Alpha = 0.0f;
				tvc.searchViewController.View.AddSubview(tvc.searchViewController.disableViewOverlay);
				
				UIView.BeginAnimations("FadeIn");
				UIView.SetAnimationDuration(0.5);
				tvc.searchViewController.disableViewOverlay.Alpha = 0.6f;
				UIView.CommitAnimations();
				
				return true;
			}
			
			public override bool ShouldEndEditing (UISearchBar searchBar)
			{
				UIView.BeginAnimations("FadeOut");
				UIView.SetAnimationDuration(0.5);
				tvc.searchViewController.disableViewOverlay.Alpha = 0.0f;
				UIView.CommitAnimations();
				
				return true;
			}
			
			// A lot of this comes from 
		    //http://jduff.github.com/2010/03/01/building-a-searchview-with-uisearchbar-and-uitableview/
			public override void TextChanged (UISearchBar searchBar, string searchText)
			{
			}
			
			public override void SearchButtonClicked (UISearchBar searchBar)
			{
				tvc.searchViewController.searchBar.SetShowsCancelButton (false, true);
				tvc.TableView.AllowsSelection = true;
				tvc.TableView.ScrollEnabled = true;
				
				tvc.searchViewController.SearchItems.Clear ();
				
				// Perform the search and update the data
				tvc.searchViewController.SearchFor (searchBar.Text);
				
				// Reload the data from the system
				tvc.TableView.ReloadData ();	
				
				tvc.searchViewController.disableViewOverlay.RemoveFromSuperview ();
				
				searchBar.ResignFirstResponder();
			}
			
			private void ClearSearchSelection ()
			{
				tvc.searchViewController.searchBar.SetShowsCancelButton (false, true);
				tvc.TableView.AllowsSelection = true;
				tvc.TableView.ScrollEnabled = true;
				
				tvc.searchViewController.searchBar.ResignFirstResponder ();
				
				// Reload the data from the system
				// Need to change back to the original view of data
				tvc.RefreshTableDisplay ();
				
				tvc.searchViewController.disableViewOverlay.RemoveFromSuperview ();
			}
			
			public override void CancelButtonClicked (UISearchBar searchBar)
			{
				tvc.searchViewController.CurrentMode = SeachViewControllerMode.Data;
				ClearSearchSelection();
			}
		}
		
		// Contains the items that will be displayed
		public List<T> Items = new List<T>();
		public List<T> SearchItems = new List<T>();
		
		// The grouping of the items
		public GroupList<T> GroupedItems = null;
		public GroupList<T> GroupedSearchItems = null;
		
		public List<T> CurrentItems {
			get {
				if (CurrentMode == SeachViewControllerMode.Data)
					return Items;
				else
					return SearchItems;
			}
		}
		
		public GroupList<T> CurrentGroup {
			get {
				if (CurrentMode == SeachViewControllerMode.Data)
					return GroupedItems;
				else
					return GroupedSearchItems;
			}
		}
		
		// Caused by the delete action when editing the list
		public virtual void RemoveItem (T item)
		{
			
		}
		
		private void DoForceRemoveList (List<T> list, T item)
		{
			T foundItem = null;
			
			foreach (T counterItem in list) {
				if (counterItem.Equals (item)) {
					foundItem = counterItem;
					break;
				}
			}
			
			if (foundItem != null)
				list.Remove (foundItem);
		}
		
		// Does a very good force remove of the items
		public void ForceRemove (T item)
		{
			DoForceRemoveList (Items, item);
			DoForceRemoveList (SearchItems, item);
			
			foreach (string s in GroupedItems.GroupNames) {
				GroupEntry<T> currentGroup = GroupedItems.GetGroupForName (s);
				DoForceRemoveList (currentGroup.Items, item);
			}
			
			foreach (string s in GroupedSearchItems.GroupNames) {
				GroupEntry<T> currentGroup = GroupedItems.GetGroupForName (s);
				DoForceRemoveList (currentGroup.Items, item);
			}
			
			tvc.TableView.ReloadData();
		}

		#region Load More option

		public bool CanLoadMoreData { get; set; }
		public bool CanLoadMoreSearch { get; set; }

		public bool CanLoadMore { 
			get
			{
				return CurrentMode == SeachViewControllerMode.Data ? CanLoadMoreData : CanLoadMoreSearch;
			}
			set
			{
				if (CurrentMode == SeachViewControllerMode.Data)
				{
					CanLoadMoreData = value;
				} else {
					CanLoadMoreSearch = value;
				}
			}
		}

		public string LoadMoreText = "Load More...";
		public string IsLoadingMoreText = "Loading...";

		internal bool IsLoadingMore = false;

		public virtual T[] LoadMoreItems()
		{
			return null;
		}

		public virtual T[] LoadMoreSearchItems()
		{
			return null;
		}
		

		
		#endregion

		#region Code for Managing Save if required

		public bool IsShowingDialog = false;
		public bool CheckOnBackButton = false;

		public virtual void SaveData()
		{

		}

		public virtual void DoBackButton()
		{
			
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

			if (_currentBannerAd != null)
				HideAd(_currentBannerAd);

			if (IsShowingDialog)
				return;
			
			// Save the changes if the item is not null
			if (!CheckOnBackButton) {
				SaveData ();
			} else {
				bool hasController = false;
				if (NavigationController == null) {
					return;
				}
				
				foreach (UIViewController c in NavigationController.ViewControllers) {
					if (c == this) {
						hasController = true;
						break;
					}
				}
				
				if (!hasController)
					DoBackButton ();
			}
		}

		#endregion

		// Details for the Search Bar
		internal UISearchBar searchBar = null;
		
		public abstract NSString GetCellIdentifier();
		public UITableViewCellStyle drawStyle = UITableViewCellStyle.Default;
		
		public abstract UITableViewCell DrawCell(T item, UITableView tableView, NSIndexPath indexPath);

		public virtual void ApplyThemeToCell(T item, UITableViewCell cell)
		{

		}

		internal UITableViewCell InternalDrawCell (T item, UITableView tableView, NSIndexPath indexPath)
		{
			// For those that can load more, we do that otherwise we just pass it on
			if (!CanLoadMore) {
				var i = DrawCell (item, tableView, indexPath);
				ApplyThemeToCell(item, i);
				return i;
			} else {
				if (indexPath.Row == CurrentItems.Count)
				{
					// We are on the load more items
					UITableViewCell cell;

					if (IsLoadingMore)
					{
						cell = tableView.DequeueReusableCell(LoadingMoreCell.CellID);
						if (cell == null)
							cell = new LoadingMoreCell(IsLoadingMoreText);
						else
							cell.TextLabel.Text = IsLoadingMoreText;
					} else {
						cell = tableView.DequeueReusableCell(LoadMoreCell.CellID);
						if (cell == null) {
							cell = new LoadMoreCell(LoadMoreText);

							(cell as LoadMoreCell).OnTap += (sender, e) => {
								(this.tvc.TableView.Delegate as SearchableTableViewController<T>.TableDelegate).RunLoadMore();
							};
						} else
							cell.TextLabel.Text = LoadMoreText;

					}

					ApplyThemeToCell(null, cell);

					return cell;
				} else {
					var i = DrawCell(item, tableView, indexPath);
					ApplyThemeToCell(item, i);
					return i;
				}
			}
		}

		public virtual void DoRowSelected(T item)
		{
			// Load the Core Details

		}

		public virtual float GetCellHeight()
		{
			return 44.0f;
		}
		
		public bool UseNetworkIndicator = false;
		
		//private UITableViewDataSource _originalSource = null;

		private UITableViewDataSource _dataSource = null;
		private UITableViewDataSource _loadingSource = null;

		public void RestoreTableView()
		{
			tvc.TableView.DataSource = _dataSource;
			tvc.TableView.ReloadData();
		}
		
		public void StartAsyncDataRetrieve(string loadingText = "Loading", string loadingSubtext = "Please wait...")
		{
			if (_dataSource == null)
				_dataSource = tvc.TableView.DataSource;

			_loadingSource = new DisplayStatusMessageTableViewDataSource(loadingText, loadingSubtext);
			tvc.TableView.DataSource = _loadingSource;
			tvc.TableView.ReloadData ();

			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			
			ThreadStart job = new ThreadStart(InternalDataRetrieve);
	        Thread thread = new Thread(job);
	        thread.Start();
		}


		public void Reload()
		{
			StartAsyncDataRetrieve();
		}
		
		public virtual void DoDataRetrieve()
		{
			
		}
	
		private SeachViewControllerMode _currentMode = SeachViewControllerMode.Data;
		public event EventHandler OnModeChange = null;
		
		public SeachViewControllerMode CurrentMode
		{
			get
			{
				return _currentMode;
			}
			set
			{
				if (_currentMode != value)
				{
					// Change the mode
					_currentMode = value;
					
					if (OnModeChange != null)
						OnModeChange(this, EventArgs.Empty);
				}
			}
		}
		
		public void InternalDataRetrieve ()
		{
			using (var pool = new NSAutoreleasePool()) {
				try {
					CurrentMode = SeachViewControllerMode.Data;
				
					DoDataRetrieve ();
				
					if (IsGrouped)
						ConvertItemsToGroup (Items, GroupedItems);
				
					// Process the response here
					this.InvokeOnMainThread (new NSAction (UpdateUI));

				} catch (Exception e) {
					ErrorMessageDescription = e.Message;
					// Need to display an error message
#if DEBUG
					ErrorMessageDescription = e.StackTrace;
#endif
					this.InvokeOnMainThread(new NSAction (DisplayProblemLoading));  
				}
			}
		}
		
		public string ErrorMessageTitle = "Loading Issue";
		public string ErrorMessageDescription = "Error Loading the data";
		
		public void DisplayProblemLoading()
		{
			OutputLogFile.Log(ErrorMessageTitle + Environment.NewLine + Environment.NewLine + ErrorMessageDescription);

			DialogHelpers.ShowMessage(ErrorMessageTitle, ErrorMessageDescription);
		}
		
		public void UpdateUI()
		{
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			RestoreTableView();

			if (AutoLoadMore && CanLoadMore)
				RunLoadMore ();
		}

		protected void LoadNewSearchItems ()
		{
			if (AutoLoadMore && CanLoadMore) {
				(this.tvc.TableView.Delegate as SearchableTableViewController<T>.TableDelegate).RunLoadMore ();
			}
		}
		
		public void SearchFor(string text)
		{
			// Set the Search Loading Message 
			tvc.TableView.DataSource = new DisplayStatusMessageTableViewDataSource("Searching...", "for '" + text + "'");
			
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			
			SearchText = text;
			
			ThreadStart job = new ThreadStart(InternalSearchDataRetrieve);
	        Thread thread = new Thread(job);
	        thread.Start();
		}
		
		public string SearchText = "";
		
		public virtual void DoSearchDataRetrieve()
		{
			
		}
		
		public void InternalSearchDataRetrieve ()
		{
			using (var pool = new NSAutoreleasePool()) {
				CurrentMode = SeachViewControllerMode.Search;
				DoSearchDataRetrieve ();
				
				if (IsGrouped)
					ConvertItemsToGroup (SearchItems, GroupedSearchItems);

				// Process the response here
				this.InvokeOnMainThread (new NSAction (UpdateUIFromSearch));
			}
		}
		
		public void UpdateUIFromSearch ()
		{
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			
			if (SearchItems.Count > 0)
				tvc.RefreshTableDisplay ();
			else
				tvc.DisplayNoFoundSearchResults ();
		}
		
		public virtual void DataObjectNotify (object sender, DataObjectUpdateArgs args)
		{
			// For notifications, we can directly deal with the object that we are working with in the collection
			T affected = args.Item as T;
			
			if (affected == null) {
				// Its not something we are interested in
				return;
			}
			
			
			bool isAffected = false;
			
			switch (args.Operation) {
			case DataObjectOperation.Insert:
				tvc.Items.Add (affected);
				isAffected = true;
				
				if (IsGrouped) {
					ConvertItemsToGroup (Items, GroupedItems);
				}
				
				break;
			case DataObjectOperation.Delete:
				int index = tvc.Items.IndexOf (affected);
				if (index >= 0) {
					tvc.Items.RemoveAt (index);
					isAffected = true;
				}
				
				if (isAffected) {
					if (IsGrouped) {
						
					}
				}
				
				break;
			case DataObjectOperation.Update:
				int indexU = tvc.Items.IndexOf (affected);
				if (indexU >= 0) {
					isAffected = true;
				} else {
					// Insert the item into the list
					tvc.Items.Add (affected);
					isAffected = true;
				
					if (IsGrouped) {
						ConvertItemsToGroup (Items, GroupedItems);
					}
				}
				break;
			}
			
			if (isAffected)
				tvc.TableView.ReloadData ();
		}
		
		public DataObjectObservers NotifyList;

		public virtual bool ShowFormToolbar ()
		{
			return false;
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			if (NavigationController != null)
				NavigationController.SetToolbarHidden (!ShowFormToolbar (), animated);
		}
		
		#region Grouping Support for the table view
		
		public bool IsGrouped = false;
		
		public virtual string GetGroupForItem (T item)
		{
			return item.ToString().Substring(0, 1);
		}
		
		public void ConvertItemsToGroup (List<T> items, GroupList<T> g)
		{
			g.Clear ();
			
			foreach (T item in items) {
				g.AddItem (item);
			}

			g.GetGroupNames();
		}
		
		#endregion

		#region IShowOrHideAd implementation
		
		internal MonoTouch.iAd.ADBannerView _currentBannerAd = null;
		
		public void ShowAd (MonoTouch.iAd.ADBannerView view)
		{
			_currentBannerAd = view;
			
			AnimateBelowSearchBar (view);
		}

		public void HideAd (MonoTouch.iAd.ADBannerView view)
		{
			RevertAnimation (view);
			
			_currentBannerAd = null;
		}
		
		#endregion
	}
	
	public abstract class SearchViewControlleriPad<T> : SearchViewController<T>, IDataObjectUpdate, IShowOrHideAd where T : class
	{
		public SearchViewControlleriPad (string title) : base(title)
		{
			
		}
		
		public override void GetBounds (out RectangleF searchBarBounds, out RectangleF tableBounds)
		{
			// Get new dimensions for the popup display
			if (ShowSearchOption) {
				searchBarBounds = new RectangleF (View.Bounds.Left, View.Bounds.Top, 
				                                  320f, 50f);
				tableBounds = new RectangleF (
					View.Bounds.Left, View.Bounds.Top + 65f, 
					View.Bounds.Width, View.Bounds.Height - searchBarBounds.Height);
			} else {
				searchBarBounds = new RectangleF (0, 0, 320, 41);
				tableBounds = new RectangleF (0, 0, 320, 480);
			}
			
			// Substract the height of the various controls where required
			if (HeightMode == FormHeightMode.NavigationControllerInTabBar) 
				tableBounds.Height = tableBounds.Height - ControlHeights.TabBarHeight / 2 + 10;
		}
		
		protected override void GetAnimateBelowBounds (UIView view, out RectangleF tableBounds, out RectangleF vBounds)
		{
			tableBounds = new RectangleF (
				View.Bounds.Left, View.Bounds.Top + 51f, 
					View.Bounds.Width, View.Bounds.Height - 41);
			
			vBounds = new RectangleF (
				View.Bounds.Left, View.Bounds.Top - 20f, 
				View.Bounds.Width, View.Bounds.Height// - 90f
				);
			
//			if (ShowSearchOption) {
//				vBounds = new RectangleF (0, 41, 320, view.Bounds.Height);
//				tableBounds = new RectangleF (0, 41 + view.Bounds.Height, 320, 439 - view.Bounds.Height - 110);
//			} else {
//				tableBounds = new RectangleF (0, view.Bounds.Height, 320, 480 - view.Bounds.Height - 110);
//			}
		}
	}
	
	// Subclass used to properly layout the items in the controller. Works a treat
	public class SearchViewControllerView<T> : UIView where T : class
	{
		SearchViewController<T> _svc = null;
		
		public SearchViewControllerView (SearchViewController<T> svc) : base()
		{
			_svc = svc;
		}
		
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			
			// Layout the items based on the current view
			var c = this.Frame;
			
			if (_svc.tvc != null) {
				// Layout the Search Controller at the top
				_svc.searchBar.Frame = new RectangleF (0, 0, c.Width, 41);
			
				if (_svc._currentBannerAd != null) {
					_svc.tvc.TableView.Frame = new RectangleF (0, 41, 
					                                           c.Width, c.Height - 41 - _svc._currentBannerAd.Frame.Height);
					_svc._currentBannerAd.Frame = new RectangleF (0, 41 + _svc.tvc.TableView.Frame.Height, 
					                                              c.Width, _svc._currentBannerAd.Frame.Height);

				} else {
					// Layout the Table View in the middle
					_svc.tvc.TableView.Frame = new RectangleF (0, 41, c.Width, c.Height - 41);
				}
				
				// Give the overlay the same view as the table view
				_svc.disableViewOverlay.Frame = _svc.tvc.TableView.Frame;
			}
		}
	}
}