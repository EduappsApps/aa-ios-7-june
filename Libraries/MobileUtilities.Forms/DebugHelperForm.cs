using System;

namespace MobileUtilities.Forms
{
	public class DebugHelperForm : ViewEditFormController<object>
	{
		public DebugHelperForm () : base()
		{
			Title = "Debugger";
		}
		
		Section _main;
		Entry _button;
		
		public event EventHandler OnClick = null;
		
		public override void SetupForm ()
		{
			base.SetupForm ();
			
			_main = new Section ("Debug");
			_main.AddInfoText ("Debug Utility", "The Application only has 17 seconds to start. Sometimes that is not enough time. " +
			                  "This form lets you delay the start so you step through the app.");
			_button = _main.AddButton ("Start App");
			_button.OnClick += (sender, e) => OnClick (sender, e);
			sections.Add(_main);
		}
	}
}

