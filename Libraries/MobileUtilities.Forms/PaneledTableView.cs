using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Text;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MobileUtilities.Data;

namespace MobileUtilities.Forms
{
	public class PaneledTableViewController <T> : UITableViewController where T : class
	{
		public PaneledTableViewController (IntPtr p) : base(p)
		{

		}
		
		PaneledViewController<T> paneledController = null;
		
		public PaneledTableViewController (PaneledViewController<T> pvc) : base()
		{
			this.paneledController = pvc;
		}
		
		
		public List<T> Items {
			get {
				return paneledController.Items; 
			}
		}
		
		class DataSource : UITableViewDataSource 
		{
			PaneledTableViewController<T> tvc;
			
			public DataSource(PaneledTableViewController<T> tvc)
			{
				this.tvc = tvc;
			}
			
			public override int RowsInSection (UITableView tableView, int section)
			{
				if (!tvc.paneledController.IsGrouped)
					return tvc.Items.Count;
				else {
					string groupName = tvc.paneledController.GroupedItems.GroupNames [section];
					GroupEntry<T> e = tvc.paneledController.GroupedItems.GetGroupForName(groupName);
					return e.Items.Count;
				}
			}
			
			public override int NumberOfSections (UITableView tableView)
			{
				if (!tvc.paneledController.IsGrouped)
					return 1;
				else {
					return tvc.paneledController.GroupedItems.GroupNames.Count;
				}
			}
			
			#region Methods for displaying the index
			
			public override string TitleForHeader (UITableView tableView, int section)
			{
				if (!tvc.paneledController.IsGrouped)
					return "";
				else {
					return tvc.paneledController.GroupedItems.GroupNames [section];
				}
			}
			/*
			public override string[] SectionIndexTitles (UITableView tableView)
			{
				if (!tvc.paneledController.IsGrouped)
					return new string[] { };
				else {
					return GroupIndexHelper.indexEntries;
				}
			}*/
			
			NSArray array;

        	[Export ("sectionIndexTitlesForTableView:")]
			public NSArray SectionTitles (UITableView tableview)
			{                   
				if (array == null) {           
					array = NSArray.FromStrings (SectionIndexTitles (tableview));
				}

				return array;
			}
			
			public override int SectionFor (UITableView tableView, string title, int atIndex)
			{
				if (!tvc.paneledController.IsGrouped)
					return 0;
				else {
					// Need to find the first group that starts with it
					int foundIndex = -1;
					
					int closestDistance = 1000;
					int closestIndex = -1;
					
					var titleByte = Encoding.ASCII.GetBytes (title) [0];
					
					for (int i=0; i < tvc.paneledController.GroupedItems.GroupNames.Count; i++) {
						var s = tvc.paneledController.GroupedItems.GroupNames [i];
						if (s.ToUpper ().StartsWith (title)) {
							// We have a match
							return foundIndex;
						} else {
							byte[] asciiBytes = Encoding.ASCII.GetBytes (s.ToUpper ());
							
							if (asciiBytes.Length > 0) {
								// See if its a closer match
								var firstByte = asciiBytes [0];
								
								int distance = Math.Abs (firstByte - titleByte);
								if (distance < closestDistance) {
									closestDistance = distance;
									closestIndex = i;
								}
							}
						}
					}
					
					return closestIndex;
				}
			}
			
			#endregion
			
			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				var cell = tableView.DequeueReusableCell (tvc.paneledController.GetCellIdentifier ());
				if (cell == null)
					cell = new UITableViewCell (tvc.paneledController.drawStyle, 
						tvc.paneledController.GetCellIdentifier ());
				
				return tvc.paneledController.DrawCell (tvc.paneledController.GetItemAtIndex (indexPath), tableView, indexPath);
			}
			
			public override void CommitEditingStyle (UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
			{
				if (editingStyle == UITableViewCellEditingStyle.Delete) {
					// Delete the item from the main controller
					var item = tvc.paneledController.GetItemAtIndex (indexPath);
					
					tvc.paneledController.RemoveItem (item);
					tvc.paneledController.Items.Remove (item);
					
					tvc.TableView.ReloadData ();
				}
			}
		}
		
		class TableDelegate : UITableViewDelegate
		{
			PaneledTableViewController<T> tvc;
			
			public TableDelegate(PaneledTableViewController<T> tvc)
			{
				this.tvc = tvc;
			}
			
			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				tvc.paneledController.DoRowSelected(tvc.paneledController.GetItemAtIndex (indexPath));
			}
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			TableView.Delegate = new TableDelegate(this);
			TableView.DataSource = new DataSource(this);
		}
		
		public virtual bool ShowFormToolbar()
		{
			return false;
		}
		
		public void RefreshTableDisplay()
		{
			TableView.DataSource = new DataSource(this);
			TableView.Delegate = new TableDelegate(this);
			TableView.ReloadData();
		}
		
		public void DisplayNoFoundSearchResults()
		{
			TableView.DataSource = new DisplayNoResultsDataSource("No results found");
			TableView.Delegate = new DoNothingTableDelegate(this);
			TableView.ReloadData();
		}
	}
	
	/// <summary>
	/// Class that manages the display of the searches and handles them appropriatly
	/// </summary>
	public abstract class PaneledViewController<T> : UIViewController, IDataObjectUpdate where T : class
	{
		string _title = "";
		public PaneledTableViewController<T> tvc;
		
		public T GetItemAtIndex (NSIndexPath indexPath)
		{
			if (!IsGrouped)
				return Items [indexPath.Row];
			else {
				string groupName = GroupedItems.GroupNames [indexPath.Section];
				return GroupedItems.GetGroupForName(groupName).Items [indexPath.Row];
			}
		}
		
		public PaneledViewController (string title) : base()
		{
			_title = title;
			NotifyList = new DataObjectObservers (this);
			
			GroupedItems = new PaneledGroupList<T>(this);
		}
		
		public void AddModalClose (string text)
		{
			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (text, UIBarButtonItemStyle.Done, 
				delegate(object sender, EventArgs e)
			{
				this.DismissModalViewControllerAnimated (true);
			});
		}
		
		public void AddEditButton ()
		{
			this.NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit, 
				StartTheEditMode);
		}
		
		private void StartTheEditMode (object sender, EventArgs e)
		{
			tvc.SetEditing (true, true);
			
			NavigationItem.SetRightBarButtonItem (
				PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Done, new EventHandler (FinishEditing)), true);
		}
		
		private void FinishEditing (object sender, EventArgs e)
		{
			tvc.SetEditing (false, true);
			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit,
				new EventHandler (StartTheEditMode));
		}
		
		// Access information for the Panelled control
		
		protected RectangleF panelBounds, tableBounds;
		
		protected UIView _control = null;
		protected abstract UIView DoCreateControl ();
		public abstract int GetControlHeight();
		
		protected virtual void DoAfterControlCreate ()
		{
			// Do nothing. Some other controls may want to use this
		}
		
		public UIView GetControl ()
		{
			if (_control == null)
				_control = DoCreateControl ();
			
			return _control;
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			Title = _title;
		
			int panelHeight = this.GetControlHeight ();
			int totalHeight = 480 - panelHeight;
			
			panelBounds = new RectangleF (0, 0, 320, panelHeight);
			tableBounds = new RectangleF (0, panelHeight, 320, totalHeight - panelHeight - 20);  
			
			_control = DoCreateControl ();
			
			if (_control != null) {
				View.AddSubview (_control);
			}
			
			// Setup the display of the view. We need the Search Bar and also the Table View Controller
			tvc = new PaneledTableViewController<T> (this);
			
			View.AddSubview (tvc.View);
			tvc.View.Frame = tableBounds;
			
			DoAfterControlCreate ();
		}
		
		// Contains the items that will be displayed
		public List<T> Items = new List<T>();
		
		// The grouping of the items
		public PaneledGroupList<T> GroupedItems = null;
		
		// Caused by the delete action when editing the list
		public virtual void RemoveItem(T item)
		{
			
		}
		
		public abstract NSString GetCellIdentifier();
		
		public abstract UITableViewCell DrawCell(T item, UITableView tableView, NSIndexPath indexPath);
		
		public virtual void DoRowSelected(T item)
		{
			
		}
		
		public bool UseNetworkIndicator = false;
		
		private UITableViewDataSource _originalSource = null;
		
		public void StoreCurrentTableView()
		{
			_originalSource = tvc.TableView.DataSource;
		}
		
		public void RestoreTableView()
		{
			tvc.TableView.DataSource = _originalSource;
			tvc.TableView.ReloadData();
		}
		
		public void StartAsyncDataRetrieve()
		{
			StoreCurrentTableView();
			tvc.TableView.DataSource = new DisplayStatusMessageTableViewDataSource("Loading", "Please wait...");
			
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			
			ThreadStart job = new ThreadStart(InternalDataRetrieve);
	        Thread thread = new Thread(job);
	        thread.Start();
		}
		
		public virtual void DoDataRetrieve()
		{
			
		}
			
		public void InternalDataRetrieve ()
		{
			using (var pool = new NSAutoreleasePool()) {
				try {
					DoDataRetrieve ();
				
					if (IsGrouped)
						ConvertItemsToGroup (Items, GroupedItems);
				
					// Process the response here
					View.InvokeOnMainThread (new NSAction (UpdateUI));

				} catch (Exception e) {
					// Need to display an error message
					ErrorMessageDescription = e.Message;
					View.InvokeOnMainThread (new NSAction (DisplayProblemLoading));
				}
			}
		}
		
		public string ErrorMessageTitle = "Loading Issue";
		public string ErrorMessageDescription = "Error Loading the data";
		
		public void DisplayProblemLoading()
		{
			DialogHelpers.ShowMessage(ErrorMessageTitle, ErrorMessageDescription);
		}
		
		public void UpdateUI()
		{
			if (UseNetworkIndicator)
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
			RestoreTableView();
		}
		
		public virtual void DataObjectNotify (object sender, DataObjectUpdateArgs args)
		{
			// For notifications, we can directly deal with the object that we are working with in the collection
			T affected = args.Item as T;
			
			if (affected != null) {
				bool isAffected = false;
			
				switch (args.Operation) {
				case DataObjectOperation.Insert:
					if (tvc.Items.Contains (affected) == false)
						tvc.Items.Add (affected);
					isAffected = true;
					break;
				case DataObjectOperation.Delete:
					int index = tvc.Items.IndexOf (affected);
					if (index >= 0) {
						tvc.Items.RemoveAt (index);
						isAffected = true;
					}
					break;
				case DataObjectOperation.Update:
					int indexU = tvc.Items.IndexOf (affected);
					if (indexU >= 0) {
						isAffected = true;
					} else {
						// We may need to add the item
						tvc.Items.Add (affected);
						isAffected = true;
					}
					break;
				}
			
				if (isAffected)
					tvc.TableView.ReloadData ();
			}
		}
		
		public DataObjectObservers NotifyList;

		public virtual bool ShowFormToolbar ()
		{
			return false;
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			NavigationController.SetToolbarHidden (!ShowFormToolbar (), animated);
		}
		
		public UITableViewCellStyle drawStyle = UITableViewCellStyle.Default;
		
		#region Grouping Support for the table view
		
		public bool IsGrouped = false;
		
		public virtual string GetGroupForItem (T item)
		{
			return item.ToString().Substring(0, 1);
		}
		
		public void ConvertItemsToGroup (List<T> items, PaneledGroupList<T> g)
		{
			g.Clear ();
			
			foreach (T item in items) {
				g.AddItem (item);
			}
			
			g.GetGroupNames();
		}
		
		#endregion
	}
}

