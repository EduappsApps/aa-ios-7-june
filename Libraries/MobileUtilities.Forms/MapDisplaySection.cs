using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using MonoTouch.MapKit;
using MonoTouch.CoreLocation;
using MobileUtilities.Mapping;
using MobileUtilities.Mapping.Yahoo;
using System.Threading;

namespace MobileUtilities.Forms
{
	public class MapDisplaySection : UIViewController
	{
		private string _title;
		private AddressDetailsArgs _args;
		
		public MapDisplaySection (AddressDetailsArgs args, string title) : base()
		{
			_args = args;
			_title = title;
		}
		
		MKMapView mapView = null;
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			Title = _title;
			
			mapView = new MKMapView(); 
			mapView.Frame = View.Bounds;
			//mapView.ShowsUserLocation = true;
			//mapView.Bounds = this.View.Bounds;
			mapView.SetNeedsLayout();
			View.Add(mapView);

			var parts = _args.Location.Split (new char[] { ',' });

			if (parts.Length == 2) {
				CLLocationCoordinate2D c = new CLLocationCoordinate2D (Convert.ToDouble(parts[0]), 
					Convert.ToDouble(parts[1]));
				mapView.AddAnnotation (new CustomAnnotation (c, "Location", _title));  
				mapView.SetCenterCoordinate (c, true);
			}
		}
	}
	
	public class CustomAnnotation : MKAnnotation
	{
		private CLLocationCoordinate2D _coordinate;
		private string _title, _subtitle;
		
		public override string Title
		{
			get { return _title; }
		}
		

		public override string Subtitle
		{
			get { return _subtitle; }
		}
			
		public CustomAnnotation (CLLocationCoordinate2D c, string title, string subtitle)
		{
			_coordinate = c;
			_title = title;
			_subtitle = subtitle;
		}
		
		public override CLLocationCoordinate2D Coordinate
		{
			get { return _coordinate; }
			set { _coordinate = value; } // required for iOS 3.2/4.0 
		}
	}
}
