using System;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class Theme
	{
		public static bool ApplyTheme = false;
		public static string TextFontName = "Baskerville-Bold";
		public static float TextFontSize = 16.0f;

		public static string SubTextFontName = "Baskerville";
		public static float SubTextFontSize = 13.0f;

		public static string ControlFontName = "Baskerville";
		public static float ControlFontSize = 13.0f;

		public static void ApplyThemeToCell(UITableViewCell cell, Entry e)
		{
			if (!ApplyTheme)
				return;

			ApplyThemeToCell(cell);
			ApplyThemeToEntryControl(e);
		}

		public static void ApplyThemeToCell(UITableViewCell cell)
		{
			if (!ApplyTheme)
				return;

			cell.TextLabel.Font = UIFont.FromName(TextFontName, TextFontSize);

			if (cell.DetailTextLabel != null)
				cell.DetailTextLabel.Font = UIFont.FromName(SubTextFontName, SubTextFontSize);
		}

		public static void ApplyThemeNavigation(UINavigationItem item)
		{
			if (!ApplyTheme)
				return;

			item.TitleView = FontedTitleView.TitleView(item.Title, TextFontName, TextFontSize);
		}

		public static void ApplyThemeToSearchBar(UISearchBar searcher)
		{
			if (!ApplyTheme)
				return;

			var searchField = searcher.ValueForKey(new MonoTouch.Foundation.NSString("_searchField")) as UITextField;
			searchField.Font = UIFont.FromName(SubTextFontName, SubTextFontSize);
		}

		public static void ApplyThemeToEntryControl(Entry e)
		{
			if (!ApplyTheme)
				return;

			if (e.control != null)
			{
				UIFont font = UIFont.FromName(TextFontName, TextFontSize);

				if (e.control is UITextField)
				{
					(e.control as UITextField).Font = font;
				} else if (e.control is UIPlaceholderTextView)
				{
					(e.control as UIPlaceholderTextView).Font = font;
				} else if (e.control is GlassButtonHolder)
				{
					var gbh = e.control as GlassButtonHolder;
					gbh.SetFont(UIFont.FromName(TextFontName, TextFontSize));
				}
				else if (e.control is AbstractHolder)
				{
					(e.control as AbstractHolder).SetFont(font);
				} else if (e.control is UILabel)
				{
					(e.control as UILabel).Font = font;
				} 
			}
		}
	}

	public class FontNames
	{

	}
}

