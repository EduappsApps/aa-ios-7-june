using System;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class PlatformButtonStyle
	{
		public static UIBarButtonItemStyle Bordered
		{
			get {
				if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () >= 7.0)
					return UIBarButtonItemStyle.Plain;
				else
					return UIBarButtonItemStyle.Bordered;
			}
		}

		public static UIBarButtonItemStyle Done {
			get {
				return UIBarButtonItemStyle.Done;
			}
		}

		public static void ChangeNavigationItemFont(UINavigationItem item, UIColor color)
		{
			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () >= 7.0) {
				if (item.TitleView is UILabel)
					(item.TitleView as UILabel).TextColor = color;
				else if (item.TitleView == null) {
					float fontSize = 18.0f;
					var f = UIFont.BoldSystemFontOfSize (fontSize);
					var v = FontedTitleView.TitleView (item.Title, f.FamilyName, fontSize);
					v.TextColor = color;
					item.TitleView = v;
				}
			}
		}

		public static UIBarButtonItem MakeTextButton(UIBarButtonSystemItem item, EventHandler handler)
		{
			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () < 7.0)
				return new UIBarButtonItem(item, handler);
			else
			{
				string text = "";
				switch (item)
				{
					case UIBarButtonSystemItem.Add:
						return new UIBarButtonItem("Add", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Cancel:
						return new UIBarButtonItem("Cancel", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Compose:
						return new UIBarButtonItem("Compose", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Done:
						return new UIBarButtonItem("Done", UIBarButtonItemStyle.Done, handler);
					case UIBarButtonSystemItem.Edit:
						return new UIBarButtonItem("Edit", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Refresh:
						return new UIBarButtonItem("Refresh", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Reply:
						return new UIBarButtonItem("Reply", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Save:
						return new UIBarButtonItem("Save", UIBarButtonItemStyle.Plain, handler);
					case UIBarButtonSystemItem.Search:
						return new UIBarButtonItem("Search", UIBarButtonItemStyle.Plain, handler);
					default:
						return new UIBarButtonItem(item, handler);
				}
			}
		}
	}
}

