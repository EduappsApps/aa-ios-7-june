using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Collections.Generic;

namespace MobileUtilities.Forms
{
	public partial class ContentTutorialSlidesController : UIViewController
	{
		public ContentTutorialSlidesController (string[] images) : base ()
		{
			imageFiles.AddRange(images);
		}
		
		private UIPageViewController pageController;
		
		public int TotalPages
		{
			get
			{
				return imageFiles.Count;
			}
		}
		
		public List<string> imageFiles = new List<string>();
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			SinglePageController firstPageController = new SinglePageController(imageFiles[0], 0);
			
			this.pageController = new UIPageViewController(UIPageViewControllerTransitionStyle.Scroll, 
			                                               UIPageViewControllerNavigationOrientation.Horizontal, 
			                                               UIPageViewControllerSpineLocation.Min);
			
			this.pageController.SetViewControllers(new UIViewController[] { firstPageController }, UIPageViewControllerNavigationDirection.Forward, 
				true, s => { });
			
			this.pageController.DataSource = new PageDataSource(this);
			
			this.pageController.View.Frame = this.View.Bounds;
			this.View.AddSubview(this.pageController.View);

			if (NavigationItem != null)
			{
				this.NavigationItem.RightBarButtonItem = new UIBarButtonItem("Close", PlatformButtonStyle.Bordered, 
				                                                             (sender, e) => DismissModalViewControllerAnimated(true));
			}
		}

		public static UINavigationController CreateNavBar(string[] images, string Title)
		{
			UINavigationController c = new UINavigationController();

			ContentTutorialSlidesController slides = new ContentTutorialSlidesController(images);
			slides.Title = Title;
			c.PushViewController(slides, false);

			return c;
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			return (toInterfaceOrientation != UIInterfaceOrientation.PortraitUpsideDown);
		}
		
		private class PageDataSource : UIPageViewControllerDataSource
		{
			public PageDataSource(ContentTutorialSlidesController parentController)
			{
				this.parentController = parentController;
			}
			
			private ContentTutorialSlidesController parentController;
			
			public override UIViewController GetPreviousViewController (UIPageViewController pageViewController, UIViewController referenceViewController)
			{
				SinglePageController currentPageController = referenceViewController as SinglePageController;
				
				if (currentPageController.PageIndex <= 0)
					return null;
				else {
					int previousPageIndex = currentPageController.PageIndex - 1;
					
					return new SinglePageController(parentController.imageFiles[previousPageIndex], previousPageIndex);
				}				
			}
			
			public override UIViewController GetNextViewController (UIPageViewController pageViewController, UIViewController referenceViewController)
			{
				SinglePageController currentPageController = referenceViewController as SinglePageController;
				if (currentPageController.PageIndex >= (this.parentController.TotalPages - 1))
					return null;
				else
				{
					int nextPageIndex = currentPageController.PageIndex + 1;
					return new SinglePageController(parentController.imageFiles[nextPageIndex], nextPageIndex);
				}
			}
			
			public override int GetPresentationCount (UIPageViewController pageViewController)
			{
				return parentController.imageFiles.Count;
			}
			
			public override int GetPresentationIndex (UIPageViewController pageViewController)
			{
				return 0;
			}
		}
		
		private class SinglePageController : UIViewController
		{
			string imageFile;
			
			public SinglePageController (string imageFile, int index) : base ()
			{
				this.imageFile = imageFile;
				PageIndex = index;
			}
			
			public int PageIndex
			{
				get;
				private set;
			}
			
			private UIImageView img;
			
			public override void ViewDidLoad ()
			{
				base.ViewDidLoad ();
				
				img = new UIImageView(this.View.Bounds);
				img.Image = UIImage.FromFile(this.imageFile);
				
				this.View.AddSubview(img);
			}
		}
	}
}
