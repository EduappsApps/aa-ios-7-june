using System;
using MonoTouch.UIKit;

namespace MobileUtilities.Forms
{
	public class ColoredNavTheme
	{
		public static UIColor NavColor = null;
		public static UIColor ToolbarColor = null;
		public static UIColor NavTextColor = null;
		public static UIColor NavShadowColor = null;

		public static void Setup ()
		{
			if (NavColor != null) {
				UINavigationBar.Appearance.BackgroundColor = NavColor;
				UISearchBar.Appearance.BackgroundColor = NavColor;
			}

			if (ToolbarColor != null)
				UIToolbar.Appearance.BackgroundColor = ToolbarColor;
		}
	}
}

