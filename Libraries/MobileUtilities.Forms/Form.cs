using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;
using MonoTouch.UIKit;
using MonoTouch.AddressBookUI;
using MonoTouch.AddressBook;
using System.Web;
using MobileUtilities.Data;
using MonoTouch.Dialog;
using MobileUtilities.Multimedia;
using System.Threading;
using MonoTouch.iAd;

namespace MobileUtilities.Forms
{
	public class ViewEditFormController<T> : UITableViewController, IDataObjectUpdate where T : class
	{
		#region Contructor Details
		
		public ViewEditFormController (UITableViewStyle style) : base(style)
		{
			NotifyList = new DataObjectObservers (this);
			
			if (font == null)
				font = UIFont.FromName ("Helvetica", 14.0f);
		}
		
		public ViewEditFormController () : base(UITableViewStyle.Plain)
		{
			NotifyList = new DataObjectObservers (this);
			
			if (font == null)
				font = UIFont.FromName ("Helvetica", 14.0f);
		}
		
		public void AddModalClose (string text)
		{
			NavigationItem.LeftBarButtonItem = new UIBarButtonItem (text, UIBarButtonItemStyle.Done, 
				delegate(object sender, EventArgs e)
			{
				this.DismissModalViewControllerAnimated (true);
			});
		}
		
		public void AddEditButton ()
		{
			this.NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit, 
				StartTheEditMode);
		}
		
		private void StartTheEditMode (object sender, EventArgs e)
		{
			this.SetEditing (true, true);
			TableView.SetEditing (true, true);
			
			NavigationItem.SetRightBarButtonItem (
				PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Done, new EventHandler (FinishEditing)), true);
			
			DoStartEditMode ();
		}
		
		protected virtual void DoAfterDelete ()
		{
			// Subclasses to use	
		}
		
		protected virtual void DoStartEditMode ()
		{
			// Subclasses to use
		}
		
		private void FinishEditing (object sender, EventArgs e)
		{
			SetEditing (false, true);
			TableView.SetEditing (false, true);
			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Edit,
				new EventHandler (StartTheEditMode));
			DoEndEditMode ();
		}
		
		protected void DoEndEditMode ()
		{
			// Subclasses to use
		}
		
		protected virtual void RemoveEntry (Entry entry)
		{
			entry.section.entries.Remove(entry);
		}
		
		public List<Section> sections = new List<Section>();
		
		public bool EditImageAfterCapture = false;
		
		public DataObjectObservers NotifyList;
		
		public virtual void SetupForm ()
		{
		}
		
		public void LoadAndSetup (T item)
		{
			_item = item;
			SetupForm ();
		}
		
		public virtual void DataObjectNotify (object sender, DataObjectUpdateArgs args)
		{
			// In form based items, subclasses need to handle this
		}
		
		public virtual bool ShowFormToolbar ()
		{
			return false;
		}
		
		#endregion

		#region Multimedia aspects
		
		public void PerformAudioRecord (Entry entry)
		{
			if (MobileUtilities.Multimedia.AudioRecording.CanRecordAudio ()) {
					
				AudioRecordDisplayViewController recordIt = new AudioRecordDisplayViewController ();
				recordIt.CreateFileNameForFolder (entry.Value);
				PresentModalViewController (recordIt, true);
					
				if (recordIt.DidFailToRecord) {
					DismissModalViewControllerAnimated (true);
					UIAlertView v2 = new UIAlertView ("Could not record", "Recording failed to work. Please ensure that your recording is working.", null, null, "Close");
					v2.Show ();
				}
					
				TableView.ReloadData ();
			} else {
				ShowAudioFromFolder audioFolder = new ShowAudioFromFolder ("Audio Notes", entry.Value);
				NavigationController.PushViewController (audioFolder, true);
			}
		}
		
		UIImagePickerController imagePicker = null;
			
		public void AddImageFromType (Entry entry)
		{
			bool hasCamera = UIImagePickerController.IsSourceTypeAvailable (UIImagePickerControllerSourceType.Camera);
					
			if (!hasCamera) {
				// We just select the image from the library
				AddImageFromType (entry, UIImagePickerControllerSourceType.PhotoLibrary);
			} else {
				var _pictureActionSheet = new UIActionSheet ("Add Image from", null, 
							"Cancel", null, "Camera", "Image Library");
			
				_pictureActionSheet.Clicked += delegate(object a, UIButtonEventArgs b) {
					// Based on the selection add the item
					switch (b.ButtonIndex) {
					case 0:
									// Select an image from the Camera
						AddImageFromType (entry, UIImagePickerControllerSourceType.Camera);
						break;
					case 1:
									// Select an image from the library
						AddImageFromType (entry, UIImagePickerControllerSourceType.PhotoLibrary);
						break;
					}
				};
						
				_pictureActionSheet.ShowInView (NavigationController.View); 	
			}	
		}
		
		public void AddImageFromType (Entry entry, UIImagePickerControllerSourceType type)
		{
			if (UIImagePickerController.IsSourceTypeAvailable (type)) {
				imagePicker = new UIImagePickerController ();
				imagePicker.SourceType = type;
				imagePicker.AllowsEditing = EditImageAfterCapture;

				imagePicker.Delegate = new ImagePickerDelegate (this, entry.Value, entry.OnImageCapture); 
					
				NavigationController.PresentModalViewController (imagePicker, true);
			}
		}

		public void AddImageFromType(string folder, UIImagePickerControllerSourceType type, ImageCapture capture)
		{
			if (UIImagePickerController.IsSourceTypeAvailable (type)) {
				imagePicker = new UIImagePickerController ();
				imagePicker.SourceType = type;
				imagePicker.AllowsEditing = EditImageAfterCapture;
				imagePicker.Delegate = new ImagePickerDelegate (this, folder, capture); 
				
				NavigationController.PresentModalViewController (imagePicker, true);
			}
		}
		
		#endregion
		
		#region Table View Details for the various areas
		
		class DataSource : UITableViewDataSource 
		{
			ViewEditFormController<T> tvc;	

			public DataSource(ViewEditFormController<T> tvc)
			{
				this.tvc = tvc;
			}
			
			public override int RowsInSection (UITableView tableView, int section)
			{
				return tvc.sections[section].entries.Count;
			}
			
			public override int NumberOfSections (UITableView tableView)
			{
				return tvc.sections.Count;
			}
			
			public override string TitleForHeader (UITableView tableView, int section)
			{
				return tvc.sections[section].Header;
			}
			
			public override string TitleForFooter (UITableView tableView, int section)
			{
				return tvc.sections[section].Footer;
			}
					
			public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
			{
				Section s = tvc.sections [indexPath.Section];
				Entry e = s.entries [indexPath.Row];
				return e.CanDelete; 
			}
			
			public override void CommitEditingStyle (UITableView tableView, 
				UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
			{
				Section section = tvc.sections [indexPath.Section];
				Entry entry = section.entries [indexPath.Row];
				
				// Fire off something to remove it
				bool didRemove = false;
				try {
					tvc.RemoveEntry (entry);
					didRemove = true;
				} catch (Exception ae) {
					//DialogHelpers.ShowMessage ("Could not remove", ae.Message);
				}
				
				if (didRemove) {
					// Remove the culprit row
					tableView.DeleteRows (new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
					tvc.DoAfterDelete();
				}
			}
			
			private void SetupKeyboardAutoHide (UITextField tf)
			{
				tf.EditingDidEndOnExit += delegate
				{
					tf.ResignFirstResponder ();
				};
			}

			private OnOffHolder SetupOnOffCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				
				OnOffHolder holder;
				if (entry.control == null) {
					// Create the holder
					holder = new OnOffHolder (new Rectangle (200, 7, 60, 30), entry);
					
					holder.SetupControls ();
					holder.control.On = entry.AsBoolean;
					
					entry.control = holder;
					
					_createdControls.AddRange (holder.GetViews ());
				} else {
					holder = entry.control as OnOffHolder;
				}
				
				holder.ShowInCell (cell);
				
				return holder;
			}
			
			private UpDownHolder SetupUpDownCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				
				UpDownHolder holder;
				if (entry.control == null) {
					// Create the holder
					holder = new UpDownHolder (tvc.GetNonEditInnerControlBounds (), entry);
					entry.control = holder;
					holder.SetupControls ();
					
					_createdControls.AddRange(holder.GetViews());
				} else {
					holder = entry.control as UpDownHolder;
				}
				
				holder.ShowInCell (cell);
				return holder;
			}
			
			private GlassButtonHolder SetupGlassButton (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = "";
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				
				GlassButtonHolder holder;
				if (entry.control == null) {
					// Create the holder
					holder = new GlassButtonHolder (tvc.GetFullWidthBounds (), entry);
					holder.SetupControls ();
					entry.control = holder;

					holder.mainButton.Enabled = entry.InitialButtonState;
					
					_createdControls.AddRange (holder.GetViews ());
				} else {
					holder = entry.control as GlassButtonHolder;
				}
				
				holder.ShowInCell (cell);
				
				return holder;
			}

			private UITextField SetupStringCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				
				UITextField tf;
				if (entry.control == null) {
					tf = new UITextField (tvc.GetInnerControlBounds ());
					tf.AdjustsFontSizeToFitWidth = true;
					tf.TextColor = new UIColor (0, 0, 0, 1);
					tf.BackgroundColor = new UIColor (1, 1, 1, 1);
					tf.ReturnKeyType = UIReturnKeyType.Next;
					
					//tf.AutocorrectionType = UITextAutocorrectionType.Default;
					//tf.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
					tf.TextAlignment = UITextAlignment.Left;
					tf.ClearButtonMode = UITextFieldViewMode.Never;
					tf.Placeholder = entry.Placeholder;
					SetupKeyboardAutoHide (tf);
					tf.KeyboardType = UIKeyboardType.ASCIICapable;
					tf.Text = entry.Value;
					
					if (entry.IsReadOnly)
						tf.UserInteractionEnabled = false;
					
					entry.control = tf;
					entry.SetupControlChangeEvent ();
					
					_createdControls.Add(tf);
				} else {
					tf = entry.control as UITextField;
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				cell.AddSubview(tf);
				
				return tf;
			}
			
			private void SetupMemoCell (UITableViewCell cell, Entry entry)
			{
				UITextView tv;
				if (entry.control == null) {
					tv = new UIPlaceholderTextView (new RectangleF (10, 8, 300, tvc.TableView.RowHeight * entry.RowHeight - 15), 
						entry.Placeholder);
					tv.TextColor = new UIColor (0, 0, 0, 1);
					tv.BackgroundColor = new UIColor (1, 1, 1, 1);
					tv.ReturnKeyType = UIReturnKeyType.Next;
					tv.Font = UIFont.FromName (Theme.TextFontName, 16.0f);
					
					tv.TextAlignment = UITextAlignment.Left;
					tv.KeyboardType = UIKeyboardType.ASCIICapable;
					tv.Text = entry.Value;
					
					if (entry.IsReadOnly)
						tv.UserInteractionEnabled = false;

					//tv.Changed += (sender, e) => 
					tv.Ended += delegate(object sender, EventArgs e) 
					{
						tv.ResignFirstResponder ();
						entry.DoValueChange();
					};
					entry.control = tv;
					_createdControls.Add(tv);
				} else {
					tv = entry.control as UITextView;
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				cell.AddSubview(tv);
				cell.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
			}
			
			private UITextField SetupComboCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				
				UITextField lbl;
				if (entry.control == null) {
					lbl = new UITextField (tvc.GetInnerControlBounds ());
					lbl.TextAlignment = UITextAlignment.Left;
					lbl.Placeholder = entry.Placeholder;
					lbl.Enabled = false;
					lbl.AdjustsFontSizeToFitWidth = true;
					lbl.TextColor = new UIColor (0, 0, 0, 1);
					lbl.BackgroundColor = new UIColor (1, 1, 1, 1);
					lbl.Text = entry.Value;
					
					entry.control = lbl;
					_createdControls.Add (lbl);
				} else {
					lbl = entry.control as UITextField;
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				cell.AddSubview (lbl);
				
				// Check to see if we have an image
				if (entry.OptionImages == null)
					cell.ImageView.Image = null;
				else {
					if (entry.OptionImages.Length == 0) {
						cell.ImageView.Image = null;
					} else {
					// Get the index of the combo option
						List<string> details = new List<string> (entry.Options);
						int i = details.IndexOf (entry.Value);
						if (i >= 0)
							cell.ImageView.Image = UIImage.FromFile (entry.OptionImages [i]);
						else
							cell.ImageView.Image = null;
					}	
				}
				
				return lbl;
			}
			
			private void SetupInfoTextCell (UITableViewCell cell, Entry entry)
			{
				cell.BackgroundColor = UIColor.White;
				cell.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				cell.TextLabel.Lines = 0;
				cell.DetailTextLabel.LineBreakMode = UILineBreakMode.WordWrap;
				cell.DetailTextLabel.Lines = 0;
				
				cell.TextLabel.Text = entry.Label;
				cell.DetailTextLabel.Text = entry.Value;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				
				cell.TextLabel.Font = tvc.font;
				cell.DetailTextLabel.Font = tvc.font;
				
				if (entry.HasClickHandler())
					cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				else
					cell.Accessory = UITableViewCellAccessory.None;
				
				// Sizing the LabelTitle.
				cell.TextLabel.Frame = new RectangleF(
					new PointF(cell.TextLabel.Frame.X, cell.TextLabel.Frame.Y),
					entry.InfoTextTitleSize);
				
				// Sizing the LabelDescription
				cell.DetailTextLabel.Frame = new RectangleF(
					new PointF(
					cell.DetailTextLabel.Frame.X,
					entry.InfoTextTitleSize.Height + 10),
					entry.InfoTextDescriptionSize);
			}
			
			private void SetupAudioCell (UITableViewCell cell, Entry entry)
			{
				if (entry.Label.Length == 0)
					cell.TextLabel.Text = "Audio Recording";
				else 
					cell.TextLabel.Text = entry.Label;
				cell.DetailTextLabel.Text = "Recorded: " + System.IO.File.GetCreationTime (entry.Value).ToString ();
				
				if (AudioPlayingSingleton.Instance.IsPlayingSound (entry.Value)) {
					cell.ImageView.Image = UIImage.FromFile (ImageConstants.StopSound);
					cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.StopSound);
					cell.TextLabel.Text += " (Playing)";
				} else {
					cell.ImageView.Image = UIImage.FromFile (ImageConstants.PlaySound);
					cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.PlaySound);					
				}
			}
			
			private ADBannerView _currentBannerAd = null;
			
			private void SetupiAdCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = "";
				cell.DetailTextLabel.Text = "";
				
				MonoTouch.iAd.ADBannerView bv;
					
				if (entry.control == null) {
					RectangleF adBounds = new RectangleF (0, 41, 320, 50);
					
					bv = new ADBannerView (adBounds);
					entry.AsBoolean = false;
					entry.control = bv;
					_createdControls.Add (bv);
				} else {
					bv = entry.control as ADBannerView;
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				cell.AddSubview (bv);
				cell.AutoresizingMask = UIViewAutoresizing.FlexibleHeight;
			}
			
			private void SetupRollupCell (UITableViewCell cell, Entry entry)
			{
				SetupComboCell (cell, entry);
				cell.Accessory = UITableViewCellAccessory.None;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			}
			
			private UISegmentedControl SetupSegmentCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = "";
				
				UISegmentedControl segment;
				if (entry.control == null) {
					
					RectangleF newBounds = new RectangleF (cell.Bounds.Left, cell.Bounds.Top, cell.Bounds.Width, cell.Bounds.Height);
					newBounds.Inflate (-16, -4);
					
					segment = new UISegmentedControl (newBounds);
					segment.ControlStyle = UISegmentedControlStyle.Bar;
					
					int currentIndex = 0;
					int selectedIndex = -1;
					foreach (string segmentItem in entry.Options)
					{
						if (segmentItem == entry.AsString)
							selectedIndex = currentIndex;
						
						segment.InsertSegment (segmentItem, currentIndex, false);
						currentIndex++;
					}
					
					if (selectedIndex != -1)
						segment.SelectedSegment = selectedIndex;
					
					segment.ValueChanged += new EventHandler(PerformSegmentChanged);
					
					entry.control = segment;
					_createdControls.Add (segment);
				} else {
					segment = entry.control as UISegmentedControl;
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				
				cell.AddSubview (segment);
				
				return segment;
			}
			
			private void PerformSegmentChanged (object sender, EventArgs e)
			{
				// Get the entry value
				UISegmentedControl control = sender as UISegmentedControl;
				
				Entry entry = tvc.FindEmbeddedControl (control);
				if (entry != null)
					entry.DoClick();
			}
			
			private UILabel SetupNameValueCell(UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				
				UILabel lbl;
				if (entry.control == null) {
					lbl = new UILabel (tvc.GetInnerControlBounds());
					lbl.TextAlignment = UITextAlignment.Left;
					lbl.Enabled = false;
					lbl.AdjustsFontSizeToFitWidth = true;
					lbl.TextColor = new UIColor (0, 0, 1, 1);
					lbl.BackgroundColor = new UIColor (1, 1, 1, 1);
					lbl.Text = entry.Value;
					
					entry.control = lbl;
					_createdControls.Add (lbl);
				} else {
					lbl = entry.control as UILabel;
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
				cell.AddSubview (lbl);
				
				return lbl;
			}
			
			private void SetupMapCell(UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			
			private UIButton SetupButtonCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = "";
				cell.DetailTextLabel.Text = "";
				
				if (entry.button == null) {
					entry.button = new GlassButton (tvc.GetFullWidthBounds ());
					entry.button.SetTitle (entry.Label, UIControlState.Normal);
					entry.control = entry.button;
					entry.button.TouchDown -= HandleButtonTapped;
					entry.button.TouchDown += HandleButtonTapped;
					
					entry.button.NormalColor = entry.ButtonColor;
					//entry.button.HighlightedColor = entry.ButtonColor;

					entry.button.Enabled = entry.InitialButtonState;

					_createdControls.Add (entry.button);
				} 
				
				cell.Accessory = UITableViewCellAccessory.None;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.AddSubview (entry.button);
				
				return entry.button;
			}

			void HandleButtonTapped (object sender, EventArgs e)
			{
				Entry foundEntry = tvc.FindEmbeddedControl (sender);
				if (foundEntry == null) {
					DialogHelpers.ShowMessage ("Not found", "The Entry for this button was not found");
				} else {
					foundEntry.DoClick ();
				}
			}
			
			private void SetupCollapsibleCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				
				if (entry.SubEntries.Count > 0) {
					if (!entry.IsExpanded)
						cell.ImageView.Image = UIImage.FromFile (ImageConstants.RightArrow);
					else
						cell.ImageView.Image = UIImage.FromFile (ImageConstants.DownArrow);
					
					cell.ImageView.Alpha = 1.0f;
				} else {
					cell.ImageView.Image = UIImage.FromFile (ImageConstants.RightArrow);
					cell.ImageView.Alpha = 0.4f;
				}
				
				cell.TextLabel.TextAlignment = UITextAlignment.Left;
				
				if (entry.ShowSubItemCount) {
					if (entry.SubItemFormat.Length == 0)
						cell.DetailTextLabel.Text = entry.SubEntries.Count.ToString ();
					else
						cell.DetailTextLabel.Text = String.Format (entry.SubItemFormat, entry.SubEntries.Count);
				}
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				
				if (entry.Placeholder.Equals ("tick"))
					cell.Accessory = UITableViewCellAccessory.Checkmark;
				else {
					cell.Accessory = UITableViewCellAccessory.None;
					
					// Add the add button if required
					if (entry.HasAddButton) {
						
						ButtonHolder holder;
						
						if (entry.control == null) {
							holder = new ButtonHolder (new RectangleF (250, 0, 60, 45), entry);
							holder.SetupControls ();
							holder.mainButton.SetImage (UIImage.FromFile (ImageConstants.CollapsibleAddButton), UIControlState.Normal);
							entry.control = holder;
							
							_createdControls.AddRange (holder.GetViews ());
						} else {
							holder = entry.control as ButtonHolder;
						}
						holder.ShowInCell (cell);
					} else if (entry.ShowDetailDisclosure) {
						cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
					}
				}
			}
			
			private void SetupDecimalCell(UITableViewCell cell, Entry entry)
			{
				UITextField tf = SetupStringCell(cell, entry);
				tf.KeyboardType = UIKeyboardType.DecimalPad;
			}
			
			private void SetupCustomCell(UITableViewCell cell, Entry entry)
			{
				// To implement here
			}
			
			private void SetupCurrencyCell(UITableViewCell cell, Entry entry)
			{
				SetupDecimalCell(cell, entry);
			}
			
			private void SetupIntegerCell (UITableViewCell cell, Entry entry)
			{
				UITextField tf = SetupStringCell (cell, entry);
				tf.KeyboardType = UIKeyboardType.NumberPad;
			}
			
			private void SetupPasswordCell (UITableViewCell cell, Entry entry)
			{
				UITextField tf = SetupStringCell (cell, entry);
				tf.SecureTextEntry = true;
			}
			
			private void SetupEmailCell(UITableViewCell cell, Entry entry)
			{
				UITextField tf = SetupStringCell(cell, entry);
				tf.KeyboardType = UIKeyboardType.EmailAddress;
			}
			
			private void SetupURLCell(UITableViewCell cell, Entry entry)
			{
				UITextField tf = SetupStringCell(cell, entry);
				tf.KeyboardType = UIKeyboardType.Url;
			}
			
			private void SetupSingleLineCell (UITableViewCell cell, Entry entry)
			{
				if (entry.CustomFontSize != -1) {
					cell.TextLabel.Font = tvc.font;
					cell.DetailTextLabel.Font = tvc.font;
				}
				
				cell.TextLabel.Text = entry.Label;
				cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;				
			}
			
			private void SetupNavigateCell (UITableViewCell cell, Entry entry)
			{
				SetupSingleLineCell (cell, entry);
				
				if (cell.TextLabel.Text.Trim ().Length == 0) {
					cell.TextLabel.Text = entry.Placeholder;
					cell.TextLabel.TextColor = UIColor.Gray;
					cell.TextLabel.HighlightedTextColor = UIColor.Gray;
				} else {
					cell.TextLabel.TextColor = UIColor.DarkTextColor;
					cell.TextLabel.HighlightedTextColor = UIColor.DarkTextColor;
				}
				
				cell.DetailTextLabel.Text = entry.Value;
				
				if (entry.NavigateImage != null) {
					cell.ImageView.Image = UIImage.FromFile (entry.NavigateImage);
					cell.ImageView.HighlightedImage = UIImage.FromFile (entry.NavigateImage);
				}
				
				if (entry.HideNavigation) {
					cell.SelectionStyle = UITableViewCellSelectionStyle.Gray;
					cell.Accessory = UITableViewCellAccessory.None;				
				}
				
				if (entry.IsReadOnly) {
					cell.TextLabel.Alpha = 0.5f;
					cell.ImageView.Alpha = 0.5f;
					cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				} else {
					cell.TextLabel.Alpha = 1f;
					cell.ImageView.Alpha = 1f;	

					if (entry.NoSelect)
					{
						cell.SelectionStyle = UITableViewCellSelectionStyle.None;
					}
				}
			}
			
			private void SetupCheckNavigateCell (UITableViewCell cell, Entry entry)
			{
				SetupNavigateCell (cell, entry);
				
				if (entry.IsChecked ()) {
					cell.Accessory = UITableViewCellAccessory.Checkmark;				
				} else {
					cell.Accessory = UITableViewCellAccessory.None;
				}
			}
			
			private void SetupDocumentCell (UITableViewCell cell, Entry entry)
			{
				// This should be a UIDocumentInteractionController link
				// Checkout http://stackoverflow.com/questions/4460667/view-pdfs-and-office-documents-from-within-own-app
				SetupNavigateCell(cell, entry);	
			}
			
			private void SetupDateCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				if (entry.AsDateTime == DateTime.MinValue)
					cell.DetailTextLabel.Text = "(none)";
				else {
					switch (entry.entryType) {
					case EntryType.Date:
						cell.DetailTextLabel.Text = entry.AsDateTime.ToShortDateString ();
						break;
					case EntryType.Time:
						cell.DetailTextLabel.Text = entry.AsDateTime.ToShortTimeString ();
						break;
					case EntryType.DateTime:
						cell.DetailTextLabel.Text = entry.AsDateTime.ToShortDateString () + " " + 
							entry.AsDateTime.ToShortTimeString ();
						break;
					}
				}
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				cell.Accessory = UITableViewCellAccessory.None;
			}
			
			private void SetupDateTimeCell (UITableViewCell cell, Entry entry)
			{
				SetupNameValueCell (cell, entry);
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			
			private void SetupTimeCell (UITableViewCell cell, Entry entry)
			{
				SetupNameValueCell (cell, entry);
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			
			private void SetupImageCell(UITableViewCell cell, Entry entry)
			{
				// Do nothing for now	
				SetupStringCell(cell, entry);
			}
			
			private void SetupImagesFolderCell (UITableViewCell cell, Entry entry)
			{
				List<CapturedImage > images = CapturedImageUtil.ReadFromFolder (entry.Value);
				
				// Get all the files for the folder
				cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				
				cell.TextLabel.Font = tvc.font;
				cell.DetailTextLabel.Font = tvc.font;
				
				// Add the ability to insert an image into the folder
				cell.TextLabel.Text = String.Format (entry.Label, images.Count);
				
				if (!entry.IsReadOnly)
					cell.DetailTextLabel.Text = "Tap to add image";
				else {
					cell.DetailTextLabel.Text = "";
				}
				
				if (images.Count > 0) {
					cell.ImageView.Image = UIImage.FromFile (images [0].ThumbnailFilename);
					cell.ImageView.HighlightedImage = UIImage.FromFile (images [0].ThumbnailFilename);
					cell.ImageView.ContentMode = UIViewContentMode.ScaleToFill;
				} else {
					cell.ImageView.Image = UIImage.FromFile (ImageConstants.CameraIcon);
					cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.CameraIcon);	
					cell.ImageView.ContentMode = UIViewContentMode.Center;
				}
			}
			
			private void SetupAudioFolderCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Font = tvc.font;
				cell.DetailTextLabel.Font = tvc.font;

				List<RecordedAudioFile > recordings = RecordedAudioUtil.ReadFromFolder (entry.Value);
				
				// Get all the files for the folder
				
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				
				// Add the ability to insert an audio note into the folder
				string subTitleText = recordings.Count.ToString () + " ";
				if (recordings.Count == 1)
					subTitleText += "note";
				else
					subTitleText += "notes";
				cell.TextLabel.Text = "Audio Notes (" + subTitleText + ")";
				
				if (MobileUtilities.Multimedia.AudioRecording.CanRecordAudio ()) {
					cell.DetailTextLabel.Text = "Tap to record audio";
					cell.Accessory = UITableViewCellAccessory.DetailDisclosureButton;
				} else {
					cell.DetailTextLabel.Text = "";
					cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				}
				
				cell.ImageView.Image = UIImage.FromFile (ImageConstants.MicrophoneIcon);
				cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.MicrophoneIcon);
			}
			
			private void SetupBooleanCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				
				if (entry.AsBoolean)
					cell.Accessory = UITableViewCellAccessory.Checkmark;
				else
					cell.Accessory = UITableViewCellAccessory.None;
				
				if (entry.NavigateImage != null) {
					if (entry.NavigateImage.Length > 0) {
						cell.ImageView.Image = UIImage.FromFile (entry.NavigateImage);
						cell.ImageView.HighlightedImage = UIImage.FromFile (entry.NavigateImage);
					}
				}				
	
				cell.DetailTextLabel.Text = entry.Placeholder;
			}
			
			private void SetupWebLinkCell(UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				cell.DetailTextLabel.Text = entry.DefaultValue;

				cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			}
			
			private void SetupContactCell (UITableViewCell cell, Entry entry)
			{
				cell.TextLabel.Text = entry.Label;
				cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				
				cell.Accessory = UITableViewCellAccessory.None;	
				
				// GS: At the moment the user image is screwing up. Reverting to just the item
				cell.ImageView.Image = UIImage.FromFile (ImageConstants.UserIcon);
				cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.UserIcon);

//				ABAddressBook ab = new ABAddressBook ();
//				ABPerson person = ab.GetPerson (Convert.ToInt32 (entry.AsInteger));
//				UIImage personImage = null;
//				
//				if (person != null && person.HasImage) {
//					personImage = UIImage.LoadFromData (person.Image);
//				}
//				
//				if (personImage == null) {
//					cell.ImageView.Image = UIImage.FromFile (ImageConstants.UserIcon);
//					cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.UserIcon);
//				} else {
//					cell.ImageView.Image = personImage;
//					cell.ImageView.HighlightedImage = personImage;
//				}
			}
			
			const float FONT_SIZE = 14.0f;
			const float CELL_CONTENT_WIDTH = 320.0f;
			const float CELL_CONTENT_MARGIN = 10.0f;

			float HeightForRowAtIndex(UITableViewCell cell, UITextView tv)
			{
				SizeF constraint = new SizeF(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0f);
				SizeF size = tvc.TableView.StringSize(tv.Text, tv.Font, constraint, UILineBreakMode.WordWrap);
				return Math.Max(size.Height, 44.0f);
			}   
			
			NSString kCellIdentifier = new NSString("FormSectionEntryIdent");
			NSString kCellIdentifierSubtitle = new NSString("FormSectionEntryIdentSubtitle");
			NSString kCellIdentifierSingleline = new NSString("FormSectionEntrySingleLine");
			
			NSString kCellIdentifierCollapsible = new NSString("FormSectionEntryCollapsible");
			
			NSString kCellIdentifierSegment = new NSString("FormSectionEntrySegmentIdent");
			
			NSString kCellIdentifierDate = new NSString("FormSectionEntrySegmentDate");
			
			NSString kCellIdentifierButton = new NSString("FormSectionEntryButton");
			
			NSString kCellIdentifierAudio = new NSString("audioCell");
			
			NSString kCellIdentifierInfoText = new NSString("FormSectionEntryInforText");
			
			private List<object> _createdControls = new List<object>();
			
			private bool IsCreatedControl (object o)
			{
				return _createdControls.Contains (o);
			}
			
			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				Entry e = tvc.sections [indexPath.Section].entries [indexPath.Row];
				
				NSString cellType;
				
				if (e.entryType == EntryType.Navigate || e.entryType == EntryType.CheckNavigate)
					cellType = kCellIdentifierSingleline;
				else if (e.entryType == EntryType.Combo)
					cellType = kCellIdentifierCollapsible;
				else if (e.entryType == EntryType.Collapsible)
					cellType = kCellIdentifierCollapsible;
				else if (e.entryType == EntryType.Segment)
					cellType = kCellIdentifierSegment;
				else if (e.entryType == EntryType.Date || e.entryType == EntryType.DateTime || e.entryType == EntryType.Time)
					cellType = kCellIdentifierDate;
				else if (e.entryType == EntryType.Button)
					cellType = kCellIdentifierButton;
				else if (e.entryType == EntryType.InfoText)
					cellType = kCellIdentifierInfoText;
				else
					cellType = kCellIdentifier;
				
				var cell = tableView.DequeueReusableCell (cellType);
				if (cell == null) {
					if (e.entryType == EntryType.Combo)
						cell = new UITableViewCell(UITableViewCellStyle.Value1, cellType);
					else if (e.entryType == EntryType.Collapsible)
						cell = new UITableViewCell (UITableViewCellStyle.Value1, cellType);
					else if (e.entryType == EntryType.Date || e.entryType == EntryType.DateTime || e.entryType == EntryType.Time)
						cell = new UITableViewCell (UITableViewCellStyle.Value1, cellType);
					else 
						cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellType);
				} else {
					if (cell.Subviews.Length > 0) {
						// Clear all the subview for the details
						for (int i = cell.Subviews.Length - 1; i >= 0; i--)
							if (IsCreatedControl (cell.Subviews [i]))
								cell.Subviews [i].RemoveFromSuperview ();
					} 
				}
				
				if (e.IsReadOnly) {
					cell.BackgroundColor = UIColor.LightGray;
				}
				
				cell.TextLabel.Text = "";
				cell.TextLabel.TextAlignment = UITextAlignment.Left;
				cell.DetailTextLabel.Text = "";
				cell.ImageView.Image = null;
				cell.ImageView.HighlightedImage = null;

				cell.BackgroundColor = UIColor.Clear;

				// Depending on the entry type, setup the appropriate cell
				switch (e.entryType) {
				case EntryType.String:
				default:
					SetupStringCell (cell, e);
					break;
				case EntryType.Date:
				case EntryType.DateTime:
				case EntryType.Time:
					SetupDateCell (cell, e);
					break;
				case EntryType.Boolean:
					SetupBooleanCell (cell, e);
					break;
				case EntryType.Combo:
					SetupComboCell (cell, e);
					break;
				case EntryType.Currency:
					SetupCurrencyCell (cell, e);
					break;
				case EntryType.Custom:
					SetupCustomCell (cell, e);
					break;
				case EntryType.Decimal:
					SetupDecimalCell (cell, e);
					break;
				case EntryType.Email:
					SetupEmailCell (cell, e);
					break;
				case EntryType.Integer:
					SetupIntegerCell (cell, e);
					break;
				case EntryType.UpDown:
					SetupUpDownCell (cell, e);
					break;
				case EntryType.OnOff:
					SetupOnOffCell (cell, e);
					break;
				case EntryType.Memo:
					SetupMemoCell (cell, e);
					break;
				case EntryType.URL:
					SetupURLCell (cell, e);
					break;
				case EntryType.WebLink:
					SetupWebLinkCell (cell, e);
					break;
				case EntryType.Map:
					SetupMapCell (cell, e);
					break;
				case EntryType.Button:
					SetupGlassButton (cell, e);
					break;
				case EntryType.Navigate:
					SetupNavigateCell (cell, e);
					break;
				case EntryType.CheckNavigate:
					SetupCheckNavigateCell (cell, e);
					break;
				case EntryType.Document:
					SetupDocumentCell (cell, e);
					break;
				case EntryType.Contact:
					SetupContactCell (cell, e);
					break;
				case EntryType.NameValue:
					SetupNameValueCell (cell, e);
					break;
				case EntryType.Collapsible:
					SetupCollapsibleCell (cell, e);
					break;
				case EntryType.Password:
					SetupPasswordCell (cell, e);
					break;
				case EntryType.ImageFolder:
					SetupImagesFolderCell (cell, e);
					break;
				case EntryType.AudioFolder:
					SetupAudioFolderCell (cell, e);
					break;
				case EntryType.Segment:
					SetupSegmentCell (cell, e);
					break;
				case EntryType.Rollup:
					SetupRollupCell (cell, e);
					break;
				case EntryType.AudioFile:
					SetupAudioCell (cell, e);
					break;
				case EntryType.InfoText:
					SetupInfoTextCell (cell, e);
					break;
				}

				tvc.ApplyThemeToCell(cell, e);

				return cell;
			}
		}

		protected virtual void ApplyThemeToCell(UITableViewCell cell, Entry e)
		{
			// Base Classes to override

		}



		class TableDelegate : UITableViewDelegate
		{
			ViewEditFormController<T> tvc;
			
			public TableDelegate (ViewEditFormController<T> tvc)
			{
				this.tvc = tvc;
			}
			
			public override UITableViewCellEditingStyle EditingStyleForRow (UITableView tableView, NSIndexPath indexPath)
			{
				Section section = tvc.sections [indexPath.Section];
				Entry entry = section.entries [indexPath.Row];
				
				if (entry.CanDelete) {
					return UITableViewCellEditingStyle.Delete;
				} else
					return UITableViewCellEditingStyle.None;
			}
			
			ComboTableController currentComboController = null;
			WebNavigateSection webView = null;
			MapDisplaySection mapShow = null;
			
			private Entry GetEntry (NSIndexPath indexPath)
			{
				Section section = tvc.sections [indexPath.Section];
				return section.entries [indexPath.Row];
			}
			
			public override void AccessoryButtonTapped (UITableView tableView, NSIndexPath indexPath)
			{
				Entry entry = GetEntry (indexPath);
				
				switch (entry.entryType) {
				case EntryType.ImageFolder:
					// Show the folder list
					ShowFilesFromFolder folder = new ShowFilesFromFolder ("Images", entry.Value);
					folder.IsReadOnly = entry.IsReadOnly;
					tvc.NavigationController.PushViewController (folder, true);
					break;
				case EntryType.AudioFolder:
					// Show all the audio notes
					ShowAudioFromFolder audioFolder = new ShowAudioFromFolder ("Audio Notes", entry.Value);
					tvc.NavigationController.PushViewController (audioFolder, true);
					break;
				case EntryType.InfoText:
				case EntryType.Collapsible:
					// Perform the click event
					entry.DoClick ();
					break;
				}
			}
			
			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				Section section = tvc.sections [indexPath.Section];
				Entry entry = GetEntry (indexPath);
				
				if (entry.IsReadOnly) {
					switch (entry.entryType) {
					case EntryType.AudioFile:
					case EntryType.WebLink:
					case EntryType.Navigate:
					case EntryType.CheckNavigate:
					case EntryType.Button:
					case EntryType.Map:
					case EntryType.AudioFolder:
					case EntryType.ImageFolder:
					case EntryType.InfoText:
						break;

					default:
						return;
					}
				}
				
				// Depending on the entry type, setup the appropriate cell
				switch (entry.entryType) {
				default:
					// Do nothing for string cells
					break;
				case EntryType.Boolean:
					// Potentially change the value
					entry.AsBoolean = !entry.AsBoolean;
					tvc.TableView.ReloadData ();
					
					break;
				case EntryType.Combo:
					
					// Create the Table View combo and the selection
					currentComboController = new ComboTableController (entry.Label);
					currentComboController.values = entry.Options;
					currentComboController.descriptions = entry.Descriptions;
					currentComboController.images = entry.OptionImages;
					currentComboController.SelectedValue = entry.Value;
					
					currentComboController.OnValueChange += delegate(object sender, EventArgs ee) 
					{
							// Update the value from the entry
						entry.UpdateValue (currentComboController.SelectedValue);
						tvc.TableView.ReloadData ();
					};
					currentComboController.OnClearValue += delegate(object sender, EventArgs eee)
					{
						entry.ClearValue ();
						tvc.TableView.ReloadData ();
					};
					
					tvc.NavigationController.PushViewController (currentComboController, true);
					break;
				case EntryType.Custom:
					break;
				case EntryType.Date:
				case EntryType.DateTime:
				case EntryType.Time:
					// Popup the Date view controller
					rollupMenu = new UIActionSheet ("Select Date" + entry.Label, 
						null, "Cancel", null, "Select", "Clear");
					dateEntry = entry;
					
					rollupMenu.Clicked += delegate(object sender, UIButtonEventArgs e) {
						//Console.WriteLine("Button Index: " + e.ButtonIndex);
						if (e.ButtonIndex == 0) {
							// Update the date view
							dateEntry.AsDateTime = DateLocalisation.ConvertToStorageDate (datePicker.Date);
							this.tvc.TableView.ReloadData ();
						} else if (e.ButtonIndex == 1) {
							dateEntry.AsDateTime = DateTime.MinValue;
							this.tvc.TableView.ReloadData ();
						}
					};
					
					datePicker = new UIDatePicker (new RectangleF (0, 230, 0, 0));
					switch (entry.entryType) {
					case EntryType.Date:
						datePicker.Mode = UIDatePickerMode.Date;
						break;
					case EntryType.DateTime:
						datePicker.Mode = UIDatePickerMode.DateAndTime;
						break;
					case EntryType.Time:
						datePicker.Mode = UIDatePickerMode.Time;
						break;
					}
					datePicker.TimeZone = NSTimeZone.LocalTimeZone;
					
					if (entry.AsDateTime != DateTime.MinValue)
						datePicker.SetDate (DateLocalisation.ConvertToDisplayDate (entry.AsDateTime), true);
					else
						datePicker.SetDate (DateLocalisation.StripSeconds (DateTime.Now), true);
					
					rollupMenu.InsertSubview (datePicker, 1);
					rollupMenu.ShowInView (tvc.NavigationController.View);
					rollupMenu.Bounds = new RectangleF (0, 0, 320, 700);
					
					break;
				case EntryType.String:	
				case EntryType.Currency:
				case EntryType.Decimal:
				case EntryType.Email:
				case EntryType.Integer:
				case EntryType.Memo:
					
				case EntryType.URL:
					break;
				case EntryType.WebLink:
					// View the web page
					webView = new WebNavigateSection (entry.DefaultValue, entry.Label);
					tvc.NavigationController.PushViewController (webView, true);
					break;
				case EntryType.Navigate:
				case EntryType.CheckNavigate:
				case EntryType.InfoText:
					// Display the Navigate option
					entry.DoClick ();
					break;
				case EntryType.Contact:
					// Display the Contact selection dialog
					tvc.ShowContactDetails (entry, false);
					break;
				case EntryType.Button:
					break;
				case EntryType.NameValue:
					// Fire the event on the button
					entry.DoClick ();
					break;
				case EntryType.Collapsible:
					if (entry.SubEntries.Count > 0) {
						if (entry.IsExpanded) {
							entry.Collapse ();	
						} else {
							Expand (entry);
						}
						tvc.TableView.ReloadData ();
					}
					break;
				case EntryType.AudioFile:
					if (!AudioPlayingSingleton.Instance.IsPlayingSound (entry.Value)) {
						AudioPlayingSingleton.Instance.PlayFile (entry.Value);
					} else {
						AudioPlayingSingleton.Instance.Stop ();
					}
					tvc.TableView.ReloadData ();
					break;
				case EntryType.Map:
					// Fire the view off to display the map
					AddressDetailsArgs details = new AddressDetailsArgs ();
					entry.DoRetrieveAddress (details);
					
					if (entry.runiOSMaps) {
						string appPath = "http://maps.google.com/maps?q=" + HttpUtility.UrlEncode (details.Location);
						UIApplication.SharedApplication.OpenUrl (new NSUrl (appPath));
					} else {
						
						mapShow = new MapDisplaySection (details, entry.Label);
						tvc.NavigationController.PushViewController (mapShow, true);
					}
					break;
				case EntryType.Rollup:
					rollupMenu = new UIActionSheet ("Select " + entry.Label, 
						null, "Cancel", null, "Select");
					
					rollupMenu.Clicked += delegate(object sender, UIButtonEventArgs e) {
						if (e.ButtonIndex == 0) {
							rollupModel.WriteChanges ();
							this.tvc.TableView.ReloadData ();
						}
					};

					;

					rollupPicker = new UIPickerView (new RectangleF (0, 185, 0, 0));
					rollupModel = new PickerDataModel (entry);
					rollupPicker.Source = rollupModel;
					rollupPicker.ShowSelectionIndicator = true;
					
					for (int o = 0; o < entry.Options.Length; o++)
						if (entry.Options [o] == entry.Value) {
							rollupPicker.Select (o, 0, false);
							break;
						}
					
					rollupMenu.AddSubview (rollupPicker);
					rollupMenu.ShowInView (tvc.NavigationController.View);

					float top = tvc.NavigationController.View.Bounds.Height;
					rollupMenu.Bounds = new RectangleF (0, 0, 320, 628);

					break;
				case EntryType.AudioFolder:
					// Start the recording process
					tvc.PerformAudioRecord (entry);
					
					break;
				case EntryType.ImageFolder:
					if (entry.IsReadOnly) {
						// Go to the list
						ShowFilesFromFolder folder = new ShowFilesFromFolder ("Images", entry.Value);
						folder.IsReadOnly = true;
						tvc.NavigationController.PushViewController (folder, true);
					} else {
						// Display a dialog for selecting which one that we are 
						bool hasCamera = UIImagePickerController.IsSourceTypeAvailable (UIImagePickerControllerSourceType.Camera);
					
						if (!hasCamera) {
						// We just select the image from the library
							tvc.AddImageFromType (entry, UIImagePickerControllerSourceType.PhotoLibrary);
						} else {
							this._pictureActionSheet = new UIActionSheet ("Add Image from", null, 
							"Cancel", null, "Camera", "Image Library");
			
							this._pictureActionSheet.Clicked += delegate(object a, UIButtonEventArgs b) {
							// Based on the selection add the item
								switch (b.ButtonIndex) {
								case 0:
									// Select an image from the Camera
									tvc.AddImageFromType (entry, UIImagePickerControllerSourceType.Camera);
									break;
								case 1:
									// Select an image from the library
									tvc.AddImageFromType (entry, UIImagePickerControllerSourceType.PhotoLibrary);
									break;
								}
							};
						
							this._pictureActionSheet.ShowInView (tvc.NavigationController.View); 	
						}
					}
					break;
				}
			}
			
			public void Expand (Entry entry)
			{
				List<Entry > onesToCollapse = new List<Entry> ();
				
				foreach (Section s in tvc.sections) {
					foreach (Entry e in s.entries)
						if (e.entryType == EntryType.Collapsible) {
							if (entry != e)
								onesToCollapse.Add (e);
						}
				}
				
				while (onesToCollapse.Count > 0) {
					onesToCollapse [onesToCollapse.Count - 1].Collapse ();
					onesToCollapse.RemoveAt (onesToCollapse.Count - 1);
				}
				
				entry.Expand();
			}
				
			#region Item delegates for the picker control
		
			UIActionSheet rollupMenu;
			UIPickerView rollupPicker;
			PickerDataModel rollupModel;
			UIDatePicker datePicker;
			Entry dateEntry;
			
			#endregion
			
			private UIActionSheet _pictureActionSheet = null;
				
			private void ReloadTable(object sender, EventArgs e)
			{
				tvc.TableView.ReloadData();	
			}
					
			public override float GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				Entry e = tvc.sections [indexPath.Section].entries [indexPath.Row];
				
				if (e.entryType == EntryType.Memo)
					return tableView.RowHeight * e.RowHeight;
				else if (e.entryType == EntryType.InfoText) {
					// We need to cache the cell size
					if (!e.CellSizeSet) {
						var label = e.Label.Trim ();
						var text = e.Value.Trim ();
					
						float widthToUse;
						if (!e.HasClickHandler ())
							widthToUse = 300f;
						else
							widthToUse = 270f;
						e.InfoTextTitleSize = tableView.StringSize (label, tvc.font, new SizeF (widthToUse, 1000.0f), 
							UILineBreakMode.WordWrap);
						e.InfoTextDescriptionSize = tableView.StringSize (text, tvc.font, new SizeF (widthToUse, 1000.0f), 
							UILineBreakMode.WordWrap);
					
						SizeF sizeTotal = new SizeF (widthToUse, e.InfoTextTitleSize.Height + e.InfoTextDescriptionSize.Height + 20);
						e.CellSize = sizeTotal;
						
						e.CellSizeSet = true;
					}
					
					return e.CellSize.Height;
				} else if (e.entryType == EntryType.Ad) {
					if (e.AsBoolean) {
						e.CellSize = new SizeF (0.0f, 0.0f);
						e.CellSizeSet = true;
					} else {
						e.CellSize = new SizeF (50.0f, 0.0f);
						e.CellSizeSet = true;
					}				
					return e.CellSize.Height;
				} else
					return tableView.RowHeight;	
			}
		}
		
		public class ImagePickerDelegate : UIImagePickerControllerDelegate
		{
			ViewEditFormController<T> tvc;
			//Entry _entry;
			string _folder;
			ImageCapture _OnImageCapture;
			
			public ImagePickerDelegate (ViewEditFormController<T> tvc, string folder, ImageCapture OnImageCapture)
			{
				this.tvc = tvc;
				this._folder = folder;
				//this._entry = entry;
				_OnImageCapture = OnImageCapture;
			}
			
			public override void Canceled (UIImagePickerController picker)
			{
				tvc.DismissModalViewControllerAnimated (true); 
			}
			
			ShowProcessingAlertView _alertView;
			UIImagePickerController _picker;
			NSDictionary _info;
			
			private void ProcessImage ()
			{
				DateTime startTime, midTime, saveTime, thumbnailTime, endTime;
				
				startTime = DateTime.Now;
				
				using (var pool = new NSAutoreleasePool()) {
				// Get the image
					var editedImage = new NSString ("UIImagePickerControllerEditedImage");
					var originalImage = new NSString ("UIImagePickerControllerOriginalImage");
				
					UIImage image = (UIImage)_info [editedImage];
					if (image == null)
						image = (UIImage)_info [originalImage];
				
				// Convert the image as a result of the camera rotation
					if (image.Orientation != UIImageOrientation.Up) {
						bool isSideways = image.Orientation == UIImageOrientation.Left || 
						image.Orientation == UIImageOrientation.Right;
						image = MobileUtilities.Multimedia.ImageUtilities.ScaleImage (image, 
						isSideways ? Convert.ToInt32 (image.Size.Height) : Convert.ToInt32 (image.Size.Width));
						
						midTime = DateTime.Now;
					}
				
					string filename = "";
					foreach (char c in Guid.NewGuid ().ToString ())
						if (Char.IsLetterOrDigit (c))
							filename += c;
					string imageFilename = "image" + filename + ".jpg";
					string thumbnailFilename = "thumb" + filename + ".jpg";
				
					imageFilename = System.IO.Path.Combine (_folder, imageFilename);
					thumbnailFilename = System.IO.Path.Combine (_folder, thumbnailFilename);
				
					if (!System.IO.Directory.Exists (_folder))
						System.IO.Directory.CreateDirectory (_folder);
				
					
					NSData jpegImage = image.AsJPEG ();
					NSError err;
					jpegImage.Save (imageFilename, NSDataWritingOptions.Atomic, out err);
				
					saveTime = DateTime.Now;
					
					float THUMBWIDTH = 40;
					float THUMBHEIGHT = 40;
				
					if (image.Size.Width == image.Size.Height) {
						THUMBWIDTH = 40f;
						THUMBHEIGHT = 40f;				
					} else if (image.Size.Width > image.Size.Height) {
						THUMBWIDTH = 40f;
						THUMBHEIGHT = image.Size.Height * 40f / image.Size.Width; 
					} else {
						THUMBHEIGHT = 40f;
						THUMBWIDTH = image.Size.Width * 40f / image.Size.Height;
					}
				
					THUMBWIDTH = Convert.ToInt32 (THUMBWIDTH);
					THUMBHEIGHT = Convert.ToInt32 (THUMBHEIGHT);
				
					IntPtr data = IntPtr.Zero;
					SizeF bitmapSize = new SizeF (THUMBWIDTH, THUMBHEIGHT);
					int bitsPerComponent = 8;
					int bytesPerRow = (int)(4 * bitmapSize.Width);
					CGColorSpace colorSpace = CGColorSpace.CreateDeviceRGB ();
					CGImageAlphaInfo alphaType = CGImageAlphaInfo.PremultipliedFirst;
				
					using (CGBitmapContext context = new CGBitmapContext (data, (int)bitmapSize.Width, (int)bitmapSize.Height, bitsPerComponent, bytesPerRow, colorSpace, alphaType)) {
					// Draw the thumbnail
						context.DrawImage (new Rectangle (0, 0, Convert.ToInt32 (THUMBWIDTH), Convert.ToInt32 (THUMBHEIGHT)), image.CGImage);
						CGImage i = context.ToImage ();
						UIImage thumb = UIImage.FromImage (i);
					
						NSData thumbImg = thumb.AsJPEG ();
						NSError err2;
						thumbImg.Save (thumbnailFilename, NSDataWritingOptions.Atomic, out err2);
					}
					
					thumbnailTime = DateTime.Now;
				
//					DialogHelpers.ShowMessage("Timings are: ",
//						string.Join(Environment.NewLine, 
//						new string[] {
//							"The timings are:",
//							"Start: " + startTime.ToString(),
//							"Mid: " + midTime.ToString(),
//							"Save: " + saveTime.ToString(),
//							"Thumb Save: " + thumbnailTime.ToString()
//					}));
						
					CapturedImage eventArgs = new CapturedImage (imageFilename);
				
					if (_OnImageCapture != null)
						_OnImageCapture (this, eventArgs);		
				
				#region Code that should be in a main thred call. It still works, but is slightly scary
				// should be: View.InvokeOnMainThread (new NSAction (ProcessImageFinished));
						
					InvokeOnMainThread (delegate {
						_alertView.Close ();
				
					// Update the files from the folder
						tvc.TableView.ReloadData ();
						tvc.DismissModalViewControllerAnimated (true);
				
						_picker = null;
						_info = null;
						_alertView = null;
				
					});
				}
				
				#endregion
			}
						
			public override void FinishedPickingMedia (UIImagePickerController picker, NSDictionary info)
			{
				_picker = picker;
				_info = info;
				
				_alertView = new ShowProcessingAlertView ("Adding Image", "Please wait...");
				_alertView.Show ();
				
				ThreadStart job = new ThreadStart (ProcessImage);
				Thread thread = new Thread (job);
				thread.Start ();
			}
		}
		
		ABPeoplePickerNavigationController peoplePicker = null;
		
		private void UpdateEntryWithContact (Entry entry, ABPerson person)
		{
			string name = person.FirstName + " " + person.LastName;
			string id = person.Id.ToString ();
			
			// See if we already have this person in the section. (No duplicates)
			foreach (Entry e in entry.section.entries)
				if (e.entryType == EntryType.Contact) {
					if (e.Value == id) {
						return;
					}
				}
			
			entry.Value = id;
			entry.Label = name;
			
			DoContactAdded (entry);
		}
		
		UILabel _updatedTitleLabel = null;
		
		public void SetLongishTitle (string value)
		{
			if (_updatedTitleLabel == null) {
				_updatedTitleLabel = new UILabel (new RectangleF (0, 4, 180, 40));
				_updatedTitleLabel.BackgroundColor = UIColor.Clear;
			
				_updatedTitleLabel.Font = UIFont.FromName ("Helvetica-Bold", 16.0f);
				_updatedTitleLabel.TextColor = UIColor.White;
				_updatedTitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
				_updatedTitleLabel.Lines = 0;
				_updatedTitleLabel.ShadowColor = UIColor.Gray;
				_updatedTitleLabel.TextAlignment = UITextAlignment.Center;
				
				NavigationItem.TitleView = _updatedTitleLabel;
			}
			_updatedTitleLabel.Text = value;
		}
		
		public void PerformContactSearch (Entry entry)
		{
			// Serach for the contact based on the string that contains a contact identifier
			peoplePicker = new ABPeoplePickerNavigationController ();
			peoplePicker.Cancelled += delegate(object sender, EventArgs e) {
				NavigationController.DismissModalViewControllerAnimated (true);
				DoContactCancelled (entry);
			};
			
			peoplePicker.SelectPerson += delegate(object sender, ABPeoplePickerSelectPersonEventArgs e) {
				UpdateEntryWithContact (entry, e.Person);
				NavigationController.DismissModalViewControllerAnimated (true);
			};
			
			NavigationController.PresentModalViewController (peoplePicker, true);
		}
		
		public event EventHandler ContactAdded = null;
		
		public virtual void DoContactAdded (Entry entry)
		{
			if (ContactAdded != null)
				ContactAdded (entry, EventArgs.Empty);
			
			TableView.ReloadData ();
		}
		
		public event EventHandler ContactCancelled = null;
		
		public virtual void DoContactCancelled (Entry entry)
		{
			if (ContactCancelled != null)
				ContactCancelled (entry, EventArgs.Empty);
			
			TableView.ReloadData();
		}
				
		UIActionSheet _contactActionSheet = null;
		
		public void PopupContactSelection (Entry entry, UIView view)
		{
			// Give the user the option to select
			_contactActionSheet = new UIActionSheet ("Add Contact", null, null, null, "As a new Contact", 
					"From Existing Contact", "Cancel");
			_contactActionSheet.CancelButtonIndex = 2;
				
			_contactActionSheet.Clicked += delegate(object sender2, UIButtonEventArgs buttonArgs) {
				switch (buttonArgs.ButtonIndex) {
				case 0: 
					CreateNewContactEntry (entry);	
					break;
				case 1:
					ShowContactDetails (entry, true);
					break;
				case 2:
					// We have cancelled
					entry.section.entries.Remove(entry);
					break;
				}
			};
			
			_contactActionSheet.ShowInView (NavigationController.View);
		}
		
		public void ShowContactDetails(Entry entry, bool removeOnCancel)
		{
			ABAddressBook ab = new ABAddressBook();
			int personId = 0;
			if (Int32.TryParse(entry.Value, out personId))
			{
				ABPerson person = ab.GetPerson(Convert.ToInt32(entry.Value));
				
				ABPersonViewController personVC = new ABPersonViewController();
				personVC.DisplayedPerson = person;
				personVC.AllowsEditing = true;
				
				personVC.PerformDefaultAction += delegate(object sender, ABPersonViewPerformDefaultActionEventArgs e) {
					UpdateEntryWithContact(entry, e.Person);
				};
				
				NavigationController.PushViewController(personVC, true);
			} else {
				PerformContactSearch(entry);
			}
		}
			
		ABNewPersonViewController _addressBookNewPerson;
		
		public void CreateNewContactEntry (Entry entry)
		{
			this._addressBookNewPerson = new ABNewPersonViewController (); 
			
			this._addressBookNewPerson.NewPersonComplete += (object sender, ABNewPersonCompleteEventArgs args) => 
			{
				if (args.Completed) {
					UpdateEntryWithContact (entry, args.Person);
				} else {
					DoContactCancelled (entry);
				}
				this.NavigationController.PopViewControllerAnimated (true);
			};
			
			this.NavigationController.PushViewController (this._addressBookNewPerson, true);
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			TableView.Delegate = new TableDelegate(this);
			TableView.DataSource = new DataSource(this);
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			if (NavigationController != null)
				NavigationController.SetToolbarHidden (!ShowFormToolbar (), animated);
		}
		
		public bool SaveSuspended = false;
		
		public bool CheckOnBackButton = false;
		public bool IsShowingDialog = false;
		
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			
			if (IsShowingDialog)
				return;
			
			// Save the changes if the item is not null
			if (_item != null && !CheckOnBackButton) {
				SaveData ();
			} else {
				bool hasController = false;
				if (NavigationController == null) {
					return;
				}
				
				foreach (UIViewController c in NavigationController.ViewControllers) {
					if (c == this) {
						hasController = true;
						break;
					}
				}
				
				if (!hasController)
					DoBackButton ();
			}
		}
		
		public virtual void DoBackButton()
		{
			
		}
		
		public event EventHandler OnCancel = null;
		
		protected void DoCancel()
		{
			if (OnCancel != null)
				OnCancel(this, EventArgs.Empty);
		}
		
		protected void AddCancelButton()
		{
			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Cancel, 
			                                                        new EventHandler(CancelButtonClick));
		}
		
		protected void ProcessBackButton(object sender, EventArgs e)
		{
			SaveData();
		}
		
		public virtual void SaveData()
		{
			
		}
		
		public Entry FindEmbeddedControl (object control)
		{
			foreach (Section s in sections)
				foreach (Entry e in s.entries) {
					if (e.control == control)
						return e;
				
					foreach (Entry e2 in e.SubEntries)
						if (e2.control == control)
							return e2;
				}
			
			return null;
		}
		
		public void ShowMap (Entry entry, string address)
		{
			if (entry.runiOSMaps) {
				string appPath = "http://maps.google.com/maps?q=" + HttpUtility.UrlEncode (address);
				UIApplication.SharedApplication.OpenUrl (new NSUrl (appPath));
			} else {
				AddressDetailsArgs details = new AddressDetailsArgs ();
				details.Location = address;
				details.addrType = AddressType.StreetAddress;
				details.Note = address;
				var mapShow = new MapDisplaySection (details, entry.Label);
				NavigationController.PushViewController (mapShow, true);
			}
		}
		
		public int InnerControlOffset = 110;
		
		public RectangleF GetInnerControlBounds ()
		{
			return new RectangleF (InnerControlOffset, 10, 255 - InnerControlOffset, 30);
		}
		
		public RectangleF GetNonEditInnerControlBounds ()
		{
			return new RectangleF (InnerControlOffset, 10, 280 - InnerControlOffset, 30);
		}
		
		public RectangleF GetFullWidthBounds()
		{
			return new RectangleF (10, 2, View.Bounds.Width - 20, 38);
			//return new RectangleF (10, 2, 300, 38);
		}
		
		protected T _item = null;
		
		public virtual void LoadData(T item)
		{
			_item = item;
		}
		
		protected void ClearBackButtonEvent()
		{
			NavigationItem.BackBarButtonItem.Clicked -= new EventHandler(ProcessBackButton);	
		}
		
		public void CancelButtonClick(object sender, EventArgs e)
		{
			DoCancel();
			NavigationController.PopViewControllerAnimated(true);	
		}
		
		#endregion
		
		#region Area for determining the deletion of rows
			
		public void DeleteEntry(Section s, Entry e)
		{
			// Subclasses to implement
		}
		
		#endregion
		
		#region Code used for the presentation of fonts for InfoText
		
		public UIFont font;
		
		#endregion
		
	}
}
