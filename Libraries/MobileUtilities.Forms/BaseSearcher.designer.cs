// WARNING
//
// This file has been generated automatically by MonoDevelop to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace MobileUtilities.Forms
{
	[Register ("BaseSearcher")]
	partial class BaseSearcher
	{
		[Outlet]
		MonoTouch.UIKit.UISearchBar searcher { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableView tableview { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIView overlayView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (searcher != null) {
				searcher.Dispose ();
				searcher = null;
			}

			if (tableview != null) {
				tableview.Dispose ();
				tableview = null;
			}

			if (overlayView != null) {
				overlayView.Dispose ();
				overlayView = null;
			}
		}
	}
}
