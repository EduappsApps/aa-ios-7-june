using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public class UpDownHolder : AbstractHolder
	{
		public UpDownHolder (RectangleF bounds, Entry entry) : base(bounds, entry)
		{
		}
		
		public UIButton subtractButton = null;
		public UIButton addButton = null;
		public UILabel valueLabel = null;
		
		public override void SetupControls ()
		{
			// We need the button, label button combination
			float buttonSize = 30;
			
			float labelWidth = _bounds.Width - buttonSize * 2;
			
			subtractButton = new UIButton (new RectangleF (_bounds.Left, _bounds.Top, buttonSize, _bounds.Height));
			valueLabel = new UILabel (new RectangleF (_bounds.Left + buttonSize, _bounds.Top, labelWidth, _bounds.Height));
			addButton = new UIButton (new RectangleF (_bounds.Left + buttonSize + labelWidth, _bounds.Top, buttonSize, _bounds.Height));
			
			valueLabel.TextAlignment = UITextAlignment.Center;
			valueLabel.BackgroundColor = UIColor.Clear;
			
			addButton.SetImage (UIImage.FromFile (ImageConstants.PlusIcon), UIControlState.Normal);
			subtractButton.SetImage (UIImage.FromFile (ImageConstants.MinusIcon), UIControlState.Normal);
			
			subtractButton.TouchDown += SubtractClick;
			addButton.TouchDown += AddClick;
			UpdateDisplay ();
		}
		
		private void SubtractClick (object sender, EventArgs e)
		{
			_entry.AsInteger = _entry.AsInteger - 1;
			if (_entry.AsInteger < _entry.MinimumValue)
				_entry.AsInteger = _entry.MinimumValue;
			UpdateDisplay ();
		}
		
		private void AddClick (object sender, EventArgs e)
		{
			_entry.AsInteger = _entry.AsInteger + 1;
			if (_entry.AsInteger > _entry.MaximumValue)
				_entry.AsInteger = _entry.MaximumValue;
			UpdateDisplay ();
		}
		
		private void UpdateDisplay ()
		{
			valueLabel.Text = _entry.AsInteger.ToString ();
			subtractButton.Enabled = _entry.AsInteger > _entry.MinimumValue;
			addButton.Enabled = _entry.AsInteger < _entry.MaximumValue;
			_entry.DoValueChange ();
		}
		
		public override UIView[] GetViews ()
		{
			return new UIView[] { subtractButton, valueLabel, addButton };
		}
	}
}

