using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	// http://stackoverflow.com/questions/1328638/placeholder-in-uitextview
	public class UIPlaceholderTextView : UITextView
	{
		UILabel _placeholderLabel;
		string _placeholder = "";
		UIColor _placeholderColor = UIColor.LightGray;
		
		public UIPlaceholderTextView (RectangleF f, string placeholderText) : base(f)
		{
			_placeholder = placeholderText;
		}

		public override bool ShouldChangeTextInRange (UITextRange inRange, string replacementText)
		{
			if (MaxChars == 0)
				return true;

			int oldLength = Text.Length;
			int replacementLength = replacementText.Length;
			var rangeLength = this.TextInRange (inRange).Length;

			int newLength = oldLength - rangeLength + replacementLength;

			bool returnKey = replacementText.Contains("\n");

			return newLength <= MaxChars || returnKey;
		}
				
		public int MaxChars = 0;

		public void DoUpdate ()
		{
			if (_placeholder.Length == 0)
				return;
		
			if (Text.Length == 0)
				GetPlaceholderLabel ().Alpha = 1;
			else
				GetPlaceholderLabel ().Alpha = 0;
		}
		
		private void UpdateChange(object sender, EventArgs e)
		{
			DoUpdate();
		}
		
		private UILabel GetPlaceholderLabel ()
		{
			if (_placeholderLabel == null)
			{
				_placeholderLabel = new UILabel (new RectangleF (8, 8, this.Bounds.Size.Width - 16, 0));
				_placeholderLabel.LineBreakMode = UILineBreakMode.WordWrap;
				_placeholderLabel.Lines = 0;
				_placeholderLabel.Font = this.Font;
				_placeholderLabel.BackgroundColor = UIColor.Clear;
				_placeholderLabel.TextColor = _placeholderColor;
				_placeholderLabel.Alpha = 0;
				_placeholderLabel.Tag = 999;
				_placeholderLabel.Text = _placeholder;
				
				AddSubview (_placeholderLabel);
				
				_placeholderLabel.SizeToFit ();
		
				this.Changed += new EventHandler(UpdateChange);
			}
			
			return _placeholderLabel;
		}
		
		protected override void Dispose (bool disposing)
		{
			_placeholderLabel = null;
			
			base.Dispose (disposing);
		}
		
		public override void Draw (RectangleF rect)
		{
			if (_placeholder.Length > 0)
    		{		
		        if (_placeholderLabel == null)
					GetPlaceholderLabel();
		
		        _placeholderLabel.Text = _placeholder;
				_placeholderLabel.SizeToFit();
				
				this.SendSubviewToBack(_placeholderLabel);
		    }
		
			if (Text.Length == 0 && _placeholder.Length > 0)
				GetPlaceholderLabel().Alpha = 1;
			
			base.Draw (rect);
		}
	}
}

