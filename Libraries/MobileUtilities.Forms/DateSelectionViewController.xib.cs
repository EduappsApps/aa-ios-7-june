
using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public partial class DateSelectionViewController : UIViewController
	{
		#region Constructors

		// The IntPtr and initWithCoder constructors are required for items that need 
		// to be able to be created from a xib rather than from managed code

		public DateSelectionViewController (IntPtr handle) : base(handle)
		{
			Initialize ();
		}

		[Export("initWithCoder:")]
		public DateSelectionViewController (NSCoder coder) : base(coder)
		{
			Initialize ();
		}

		public DateSelectionViewController () : base("DateSelectionViewController", null)
		{
			Initialize ();
		}
		
		Entry _entry;
		DateTimeViewControllerMode _mode;

		public void UpdateEntryDisplay ()
		{
			DateTime dtValue;
			
			if (_entry.Value.Length == 0) {
				dtValue = DateTime.Now;
			} else {
				dtValue = _entry.AsDateTime;
			}
			
			dtValue = DateLocalisation.ConvertToDisplayDate(dtValue);
			selectedDate = dtValue;
			selectedTime = dtValue;
		}
		
		public DateSelectionViewController (Entry entry, DateTimeViewControllerMode mode) : base("DateSelectionViewController", null)
		{
			_entry = entry;
			_mode = mode;
			
			UpdateEntryDisplay ();
		}
		
		DateAndTimeDisplayController dtView = null;
		
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			
			// Update the Date/Time value of the main entry
			DateTime resultDate = new DateTime (selectedDate.Year, selectedDate.Month, selectedDate.Day, 
				selectedTime.Hour, selectedTime.Minute, selectedTime.Millisecond);
			
			_entry.AsDateTime = resultDate;
		}
		
		public DateTime selectedDate;
		public DateTime selectedTime;
		
		public override void ViewDidLoad ()
		{
			// Setup the Table View Controller
			base.ViewDidLoad ();
			
			if (_entry != null)
				Title = _entry.Label;
			
			dtView = new DateAndTimeDisplayController ();
			dtView.TableView = this.tableView;
			dtView.SetupDetails (_entry, _mode);
			dtView.OnSelectionModeChange += delegate(object sender, EventArgs e) {
				// Update what we will update
				switch (dtView.SelectedPart) {
				case SelectDateFormPart.Date:
					picker.Mode = UIDatePickerMode.Date;
					break;
				case SelectDateFormPart.Time:
					picker.Mode = UIDatePickerMode.Time;
					break;
				}
			};
			
			if (_entry.Value.Length > 0)
				dtView.UpdateDisplay (_entry.AsDateTime);
			else
				dtView.UpdateDisplay (DateTime.Now);
			
			// Add a clear button
			NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Clear", UIBarButtonItemStyle.Plain, new EventHandler (DoClear));
		}
		
		private void DoClear (object sender, EventArgs e)
		{
			_entry.Value = "";
			NavigationController.PopViewControllerAnimated(true);
		}
		
		void Initialize ()
		{
			picker.TimeZone = NSTimeZone.LocalTimeZone;
		}
		
		partial void dateValueChange (UIDatePicker sender)
		{
			// Implement what needs to happen here to update the value
			if (dtView != null)
				dtView.UpdateDisplay(sender.Date);
		}
		
		#endregion
	}
	
	[MonoTouch.Foundation.Register("DateAndTimeDisplayController")]
	public class DateAndTimeDisplayController : UITableViewController
	{
		static NSString kCellIdentifier = new NSString("DateAndTimeDisplayControllerIdent");
		
		public DateAndTimeDisplayController (IntPtr p) : base(p)
		{

		}
		
		public DateAndTimeDisplayController() : base()
		{

		}

		class DataSource : UITableViewDataSource 
		{
			DateAndTimeDisplayController tvc;

			public DataSource(DateAndTimeDisplayController tvc)
			{
				this.tvc = tvc;
			}
			
			public override int RowsInSection (UITableView tableView, int section)
			{
				if (tvc._mode == DateTimeViewControllerMode.Date || tvc._mode == DateTimeViewControllerMode.Time)
					return 1;
				else
					return 2;
			}
			
			public override int NumberOfSections (UITableView tableView)
			{
				return 1;
			}
			
			public override string[] SectionIndexTitles (UITableView tableView)
			{
				return new string[] { };
			}
			
			Dictionary<int, object> controls = new Dictionary<int, object> ();
			
			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				var cell = tableView.DequeueReusableCell (kCellIdentifier);
				if (cell == null)
					cell = new UITableViewCell (UITableViewCellStyle.Subtitle, kCellIdentifier);
				
				string displayText = "";
				string labelText = "";
				
				if (tvc._mode == DateTimeViewControllerMode.Date) {
					labelText = "Date";
					displayText = tvc.dateDisplay;
				} else if (tvc._mode == DateTimeViewControllerMode.Time) {
					labelText = "Time";
					displayText = tvc.timeDisplay;
				} else if (tvc._mode == DateTimeViewControllerMode.DateTime) {
					if (indexPath.Row == 0) {
						labelText = "Date";
						displayText = tvc.dateDisplay;
					} else {
						labelText = "Time";
						displayText = tvc.timeDisplay;
					}
				}
				
				cell.TextLabel.Text = labelText;
				
				UILabel lbl;
				if (!controls.ContainsKey (indexPath.Row)) {
					lbl = new UILabel (new RectangleF (110, 10, 145, 30));
					lbl.TextAlignment = UITextAlignment.Left;
					lbl.Enabled = false;
					lbl.AdjustsFontSizeToFitWidth = true;
					lbl.TextColor = new UIColor (0, 0, 1, 1);
					lbl.BackgroundColor = new UIColor (1, 1, 1, 1);
					
					controls[indexPath.Row] = lbl;
				} else {
					lbl = controls[indexPath.Row] as UILabel;
				}
				
				lbl.Text = displayText;
				cell.SelectionStyle = UITableViewCellSelectionStyle.Blue;
				cell.Accessory = UITableViewCellAccessory.None;
				cell.AddSubview (lbl);
				
				return cell;
			}
		}
		
		class TableDelegate : UITableViewDelegate
		{
			DateAndTimeDisplayController tvc;
			
			public TableDelegate(DateAndTimeDisplayController tvc)
			{
				this.tvc = tvc;
			}
			
			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				if (tvc._mode == DateTimeViewControllerMode.Date)
				{
					tvc.SelectedPart = SelectDateFormPart.Date;

				} else if (tvc._mode == DateTimeViewControllerMode.Time)
				{
					tvc.SelectedPart = SelectDateFormPart.Time;

				} else if (tvc._mode == DateTimeViewControllerMode.DateTime)
				{
					if (indexPath.Row == 0)
						tvc.SelectedPart = SelectDateFormPart.Date;
					else
						tvc.SelectedPart = SelectDateFormPart.Time;
				}

				tvc.DoSelectionModeChange();
			}
		}
		
		public SelectDateFormPart SelectedPart = SelectDateFormPart.Date;
		Entry _entry = null;
		DateTimeViewControllerMode _mode = DateTimeViewControllerMode.Date;
		
		public void SetupDetails (Entry entry, DateTimeViewControllerMode mode)
		{
			_entry = entry;
			_mode = mode;
			
			TableView.Delegate = new TableDelegate (this);
			TableView.DataSource = new DataSource(this);
		}
		
		public event EventHandler OnSelectionModeChange = null;
		
		protected void DoSelectionModeChange ()
		{
			if (OnSelectionModeChange != null)
				OnSelectionModeChange (this, EventArgs.Empty);
		}
		
		
		public void UpdateDisplay (DateTime newDate)
		{
			newDate = DateLocalisation.ConvertToStorageDate(newDate);
			
			switch (_mode) {
			case DateTimeViewControllerMode.Date:
				dateDisplay = newDate.Date.ToString ("ddd d MMM yyyy");
				break;
			case DateTimeViewControllerMode.DateTime:
				dateDisplay = newDate.Date.ToString ("ddd d MMM yyyy");
				timeDisplay = newDate.ToString ("hh:mm:ss AM/PM");
				break;
			case DateTimeViewControllerMode.Time:
				timeDisplay = newDate.ToString ("hh:mm:ss AM/PM");
				break;
			}
			
			TableView.ReloadData ();
		}
		
		private string timeDisplay = "";
		private string dateDisplay = "";

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			TableView.Delegate = new TableDelegate (this);
			TableView.DataSource = new DataSource (this);
		}
	}
	
	public class DateLocalisation
	{
		public static DateTime ConvertToStorageDate (DateTime value)
		{
			try {
				// Taken from: https://gist.github.com/826443
				// Was an absolute pain in the ass to resolve this stupid issue
				var utcOffset = TimeZone.CurrentTimeZone.GetUtcOffset (value);
				return value.Add (utcOffset);
			} catch {
				return value;
			}
		}
		
		public static DateTime ConvertToDisplayDate (DateTime value)
		{
			return value;
		}
		
		public static DateTime StripSeconds (DateTime value)
		{
			return new DateTime(value.Year, value.Month, value.Day, value.Hour, value.Minute, 0);
		}
	}
}

