using System;

namespace MobileUtilities.Forms
{
	public enum FormHeightMode
	{
		NavigationController,
		NavigationControllerInTabBar
	}
	
	public class ControlHeights
	{
		public const int NavigationItemHeight = 44;
		public const int TabBarHeight = 49;
		public const int ToolBarHeight = 44;
		public const int SearchBarHeight = 41;
			
		public const int FullScreenHeight = 460;
		public const int FullScreenWidth = 320;
	}
}

