using System;
using UIKit;
using Foundation;
using ObjCRuntime;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Forms
{
	public enum PopupNotifierLocation
	{
		Top,
		Bottom
	}

	public class PopupNotifierController : UIViewController
	{
		UIView _parentView;
		string _Text;
		string _SubText;

		public PopupNotifierController (UIView ParentView, string Text, string SubText) 
		{
			_parentView = ParentView;
			_Text = Text;
			_SubText = SubText;
		}

		public event EventHandler AnimStart = null;
		public event EventHandler AnimStop = null;

		public PopupNotifierLocation Location = PopupNotifierLocation.Top;

		public UIImage Image = null;

		public UIColor BackgroundColor = null;

		private UIView _view = null;

		public bool AutomaticTiming = true;

		public bool UseSystemFont = true;
		public string CustomFontName = "TrebuchetMS";
		public string CustomBoldFontName = "TrebuchetMS-Bold";

		public bool UseSystemFontSize = true;
		public float CustomFontSize = 10.0f; 

		UIFont GetBoldFont(nfloat size)
		{
			if (UseSystemFont)
				return UIFont.BoldSystemFontOfSize(size);
			else
				return UIFont.FromName(CustomBoldFontName, size);
		}

		UIFont GetNormalFont(nfloat size)
		{
			if (UseSystemFont)
				return UIFont.SystemFontOfSize(size);
			else
				return UIFont.FromName(CustomFontName, size);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			SetupView();
		}

		public override void ViewWillAppear (bool animated) 
		{
			SetupView();
		}

		public int DelayBeforeShowingInSeconds = 1;

		public void Popup ()
		{
			if (AutomaticTiming) {
				// Delay for two seconds with the timer
				var userInfo = new NSString ("MyUserInfo");
				var timer = NSTimer.CreateScheduledTimer (DelayBeforeShowingInSeconds, this, 
			                                         new Selector ("DoWaitForABit"), userInfo, false);
			} else {
				Animate();
			}
		}

		[Export("DoWaitForABit")]
		void DoWaitForABit ()
		{
			InvokeOnMainThread(delegate { Animate(); });
		}

		private RectangleF _startRect, _endRect;

		private void SetupView()
		{
			nfloat top, left, width, height;
			width = _parentView.Frame.Width;
			left = 0;
			height = 18.0f + UIFont.SystemFontSize * 1.5f + UIFont.SmallSystemFontSize * 1.5f;

			// Measure the font height
			if (this.Location == PopupNotifierLocation.Top)
				top = 0 - height;
			else
				top = _parentView.Frame.Bottom;

			_startRect = new RectangleF(left, top, width, height);

			if (this.Location == PopupNotifierLocation.Top)
				_endRect = new RectangleF(0, 0, width, height);
			else
				_endRect = new RectangleF(0, _parentView.Frame.Bottom - height, width, height);

			_view = new UIView(_startRect);

			// Add the items
			if (BackgroundColor != null)
				_view.BackgroundColor = this.BackgroundColor;
			else
				_view.BackgroundColor = new UIColor(0f, 0f, 1f, 0.5f);

			RectangleF textRect = new RectangleF(8, 8, width - 16.0f, UIFont.SystemFontSize * 1.5f);
			_textLabel = new UILabel(textRect);
			_textLabel.Font = GetBoldFont(UIFont.SystemFontSize);
			_textLabel.Text = _Text;
			_textLabel.BackgroundColor = UIColor.Clear;
			_textLabel.TextColor = UIColor.White;
			_textLabel.TextAlignment = UITextAlignment.Center;
			_view.AddSubview(_textLabel);

			RectangleF subTextRect = new RectangleF(8, textRect.Bottom + 2, width - 16.0f, UIFont.SmallSystemFontSize * 1.5f);
			_subtextLabel = new UILabel(subTextRect);
			_subtextLabel.Font = GetNormalFont(UseSystemFontSize ? UIFont.SmallSystemFontSize : CustomFontSize);
			_subtextLabel.Text = _SubText;
			_subtextLabel.BackgroundColor = UIColor.Clear;
			_subtextLabel.TextColor = UIColor.White;
			_subtextLabel.TextAlignment = UITextAlignment.Center;
			_view.AddSubview(_subtextLabel);

			_parentView.AddSubview(_view);

			_view.Alpha = 1f;

			this.View = _view;
		}

		UILabel _textLabel;
		UILabel _subtextLabel;

		public void UpdateSubText(string value)
		{
			_subtextLabel.Text = value;
			_subtextLabel.SetNeedsDisplay();
		}

		public void UpdateText(string value)
		{
			_textLabel.Text = value;
			_textLabel.SetNeedsDisplay();
		}

		private void Animate ()
		{
			if (_view == null)
				SetupView ();

			UIView.BeginAnimations ("MyAnimations");
			UIView.SetAnimationDuration (0.3f);
			UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);

			_view.Alpha = 1.0f;
			_view.Frame = _endRect;

			if (AutomaticTiming) {
				UIView.SetAnimationDidStopSelector (new Selector ("didFinishAnimation:"));
				UIView.SetAnimationDelegate (this);
			}

			if (this.AnimStart != null)
				AnimStart(this, EventArgs.Empty);

			UIView.CommitAnimations();
		}

		public int SecondsToShow = 2;

		[Export("didFinishAnimation:")]
		void DidFinishAnimation (Selector sel)
		{
			// Delay for two seconds with the timer
			var userInfo = new NSString("MyUserInfo");
			NSTimer.CreateScheduledTimer(SecondsToShow, this, new Selector("DoFadeOut"), userInfo, false);
		}

		public void Hide()
		{
			UIView.BeginAnimations("MyAnimations");
			UIView.SetAnimationDuration (0.5f);
			UIView.SetAnimationCurve (UIViewAnimationCurve.EaseInOut);
			
			_view.Alpha = 0.0f;

			if (this.AnimStop != null)
				AnimStop(this, EventArgs.Empty);

			UIView.CommitAnimations();
		}

		[Export("DoFadeOut")]
		void DoFadeOut()
		{
			InvokeOnMainThread(delegate {
				Hide();
			});
		}

		public override void ViewWillLayoutSubviews ()
		{
			base.ViewWillLayoutSubviews ();
		}
	}
}

