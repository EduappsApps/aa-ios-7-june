using System;
using UIKit;
using Foundation;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Forms
{
	public class LoadingMoreCell : UITableViewCell
	{
		public static NSString CellID = new NSString("LoadingMoreTextTableCellid");

		UIActivityIndicatorView _activity;

		public LoadingMoreCell(string text) : base(UITableViewCellStyle.Subtitle, CellID)
		{
			// Load the animation in the same spot as the UIImage
			_activity = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
			_activity.Frame = new System.Drawing.RectangleF(60, 2, 40, 40);
			this.AddSubview(_activity);

			_activity.StartAnimating();

			TextLabel.Frame = new RectangleF(110f, TextLabel.Frame.Top, 210f, TextLabel.Frame.Height);
			TextLabel.Text = text;

			DetailTextLabel.Frame = new RectangleF(110f, DetailTextLabel.Frame.Top, 210f, DetailTextLabel.Frame.Height);

			if (Theme.ApplyTheme)
			{
				TextLabel.Font = UIFont.FromName(Theme.TextFontName, Theme.TextFontSize);
				DetailTextLabel.Font = UIFont.FromName(Theme.SubTextFontName, Theme.SubTextFontSize);
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			_activity.Frame = new RectangleF(10, 2, 40, 40);
			TextLabel.Frame = new RectangleF(60f, TextLabel.Frame.Top, 210f, TextLabel.Frame.Height);
			DetailTextLabel.Frame = new RectangleF(60f, DetailTextLabel.Frame.Top, 210f, DetailTextLabel.Frame.Height);
		}
	}
	
}
