using System;
using UIKit;
using Foundation;
using MonoTouch.Dialog;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;

namespace MobileUtilities.Forms
{

	public class LoadMoreCell : UITableViewCell
	{
		public static NSString CellID = new NSString("LoadMoreTextTableCellid");

		GlassButton _button;

		public LoadMoreCell(string text) : base(UITableViewCellStyle.Subtitle, CellID)
		{
			RectangleF b = this.Bounds;
			b.Inflate(-4, -2);

			_button = new GlassButton(b);
			_button.SetTitle(text, UIControlState.Normal);
			_button.TouchDown += DoLoadMoreTouch;
			AddSubview(_button);
		}

		void DoLoadMoreTouch (object sender, EventArgs e)
		{
			if (OnTap != null)
				OnTap(this, EventArgs.Empty);
		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);

			DoLoadMoreTouch(this, EventArgs.Empty);
		}

		public event EventHandler OnTap = null;
	}
}
