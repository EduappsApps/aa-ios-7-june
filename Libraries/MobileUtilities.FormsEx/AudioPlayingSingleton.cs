using System;
#if __UNIFIED__
using AudioToolbox;
using Foundation;
using AVFoundation;
#else
using MonoTouch.AudioToolbox;
using MonoTouch.Foundation;
using MonoTouch.AVFoundation;
#endif

namespace MobileUtilities.Forms
{
	public class AudioPlayingSingleton
	{
		static readonly AudioPlayingSingleton instance=new AudioPlayingSingleton();

	    // Explicit static constructor to tell C# compiler
	    // not to mark type as beforefieldinit
	    static AudioPlayingSingleton()
	    {
	    }
	
	    AudioPlayingSingleton()
	    {
	    }
	
	    public static AudioPlayingSingleton Instance {
			get {
				return instance;
			}
		}
		
		public bool IsPlayingSound (string filename)
		{
			return _currentPlayingFile.Equals (filename);
		}
		
		private string _currentPlayingFile = "";
		
		public string CurrentPlayingFile {
			get {
				return _currentPlayingFile;
			}
			set {
				_currentPlayingFile = value;
			}
		}
		
		public void PlayFile (string filename)
		{
			CurrentPlayingFile = filename;

			NSError error = new NSError ();

			player = new AVAudioPlayer (new NSUrl (filename, false), "mp3", out error);
			
			player.FinishedPlaying += PlayingStopped;
			player.PrepareToPlay ();
			player.Play ();
		}
		
		private void PlayingStopped (object sender, AVStatusEventArgs e)
		{
			player = null;
			CurrentPlayingFile = "";
			
			DoOnAudioFileStopped ();
		}
		
		public event EventHandler OnAudioFileStopped = null;
		
		private void DoOnAudioFileStopped ()
		{
			if (OnAudioFileStopped != null)
				OnAudioFileStopped(this, EventArgs.Empty);
		}
		
		AVAudioPlayer player = null;
		
		public void Stop ()
		{
			if (player != null) {
				player.Stop ();
				CurrentPlayingFile = "";
			}
			
			DoOnAudioFileStopped ();
		}
		
		public bool IsPlaying {
			get {
				if (player == null) {
					return false;
				} else {
					return player.Playing;
				}
			}
		}
	}
}

