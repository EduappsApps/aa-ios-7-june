using System;
using System.Collections.Generic;

namespace MobileUtilities.Forms
{
	public delegate void ImageCapture(object sender, CapturedImage image);
	
	public class CapturedImage
	{
		public CapturedImage (string filename)
		{
			this.Filename = filename;
		}
		
		public string Filename { get; set; }
		
		public DateTime CapturedDate {
			get
			{
				return System.IO.File.GetCreationTime (Filename);
			}
		}
		
		public string ThumbnailFilename {
			get
			{
				string path = System.IO.Path.GetDirectoryName (Filename);
				string filename = System.IO.Path.GetFileName (Filename);
				
				filename = filename.Replace ("image", "thumb");
				return System.IO.Path.Combine(path, filename);
			}
		}
	}
	
	public class CapturedImageUtil
	{
		public static List<CapturedImage> ReadFromFolder (string folderName)
		{
			List<CapturedImage> result = new List<CapturedImage> ();
			
			if (System.IO.Directory.Exists (folderName))
			{
				List<string> allFiles = new List<string> (System.IO.Directory.GetFiles (folderName));
				foreach (string filename in allFiles)
				{
					string filenameOnly = System.IO.Path.GetFileName (filename);
					if (filenameOnly.StartsWith ("image"))
					{
						CapturedImage img = new CapturedImage (filename);
						result.Add (img);
					}
				}
			
				result.Sort (new NewestToOldestFileComparer ());
			}
			return result;
		}
		
		private class NewestToOldestFileComparer : IComparer<CapturedImage>
		{
			public int Compare (CapturedImage x, CapturedImage y)
			{
				return DateTime.Compare(y.CapturedDate, x.CapturedDate);
			}
		}
	}
}

