using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using System.Threading;
using MobileUtilities.Data;

namespace MobileUtilities.Forms
{
	public class ShowFilesFromFolder : SearchViewController<CapturedImage>
	{
		public string FolderName = "";
		public bool IsReadOnly = false;
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			StartAsyncDataRetrieve ();
			
			if (!IsReadOnly)
				AddEditButton ();
		}
		
		public ShowFilesFromFolder (string title, string folder) : base(title)
		{
			this.FolderName = folder;
			this.ShowSearchOption = false;
		}
		
		public NSString cellIdent = new NSString("ShowFilesFromFolderCellIdent");
		
		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}
		
		public override UITableViewCell DrawCell (CapturedImage item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);
			if (cell == null)
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
					
			cell.TextLabel.Text = "Image File";
			cell.DetailTextLabel.Text = "Taken: " + item.CapturedDate.ToString ();
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			
			cell.ImageView.Image = UIImage.FromFile (item.ThumbnailFilename);
			cell.ImageView.HighlightedImage = UIImage.FromFile (item.ThumbnailFilename);
			cell.ImageView.ContentMode = UIViewContentMode.ScaleAspectFill;
			
			return cell; 
		}
		
		public override void DoDataRetrieve ()
		{
			Items = CapturedImageUtil.ReadFromFolder (FolderName);
		}
		
		public override void DoSearchDataRetrieve ()
		{
			SearchItems.Clear ();
			foreach (CapturedImage image in Items)
				if (image.CapturedDate.ToString ().Contains (SearchText))
					SearchItems.Add (image);
		}
		
		public override void RemoveItem (CapturedImage item)
		{
			// Remove the file and preview image as well
			string filename = item.Filename;
			string thumbnail = item.ThumbnailFilename;
			
			Items.Remove (item);
			if (SearchItems.IndexOf (item) >= 0)
				SearchItems.Remove (item);
			
			System.IO.File.Delete (filename);
			System.IO.File.Delete (thumbnail);
			
			tvc.TableView.ReloadData ();
		}
		
		public override void DoRowSelected (CapturedImage item)
		{
			ZoomableImageView zoomie = new ZoomableImageView (item.Filename, "Preview");
			NavigationController.PushViewController(zoomie, true);
			
			// Show a preview image of the picture modally
//			PreviewImageViewController preview = new PreviewImageViewController ();
//			preview.SetImageFile (item.Filename);
//			this.NavigationController.PushViewController (preview, true);
		}
	}
}

