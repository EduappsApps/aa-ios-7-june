using System;
using UIKit;
using Foundation;

namespace MobileUtilities.Forms
{
	public class DisplayStatusMessageTableViewDataSource : UITableViewDataSource
	{
		string _title;
		string _description;
			
		public DisplayStatusMessageTableViewDataSource (string title, string description)
		{
			this._title = title;
			this._description = description;
		}
		
		public override nint RowsInSection (UITableView tableView, nint section)
		{
			return 1;
		}
		
		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}
			
		static NSString cellIdentifier = new NSString("LoadingDetailsCellIdent");
		
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell(LoadingMoreCell.CellID);
			if (cell == null)
				cell = new LoadingMoreCell(_title); 
			
			cell.TextLabel.Text = _title;
			cell.DetailTextLabel.Text = _description;
			cell.Accessory = UITableViewCellAccessory.None;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			
			return cell;
		}
	}
}

