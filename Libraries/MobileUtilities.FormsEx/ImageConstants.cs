using System;

namespace MobileUtilities.Forms
{
	public class ImageConstants
	{
		public static string StopSound = "images/38-circle-stop.png";
		public static string PlaySound = "images/36-circle-play.png";
		
		public static string RightArrow = "images/rightarrow.png";
		public static string DownArrow = "images/downarrow.png";
		
		public static string CollapsibleAddButton = "images/50-plus.png";
		public static string CameraIcon = "images/86-camera.png";
		public static string MicrophoneIcon = "images/66-microphone.png";
		
		public static string UserIcon = "images/111-user.png";
		
		public static string PlusIcon = "images/50-plus.png";
		public static string MinusIcon = "images/45-minus.png";
	}
}

