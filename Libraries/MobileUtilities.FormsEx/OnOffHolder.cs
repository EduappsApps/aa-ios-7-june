using System;
using UIKit;
using System.Drawing;

namespace MobileUtilities.Forms
{
	public class OnOffHolder : AbstractHolder
	{
		public OnOffHolder (RectangleF bounds, Entry entry) : base(bounds, entry)
		{
		}
		
		public UISwitch control = null;
		
		public override void SetupControls ()
		{
			control = new UISwitch (this._bounds);
			control.On = _entry.AsBoolean;
			control.ValueChanged += delegate(object sender, EventArgs e) {
				_entry.DoValueChange();
			};
		}
		
		public override UIView[] GetViews ()
		{
			return new UIView[] { control };
		}
	}
}
