using System;
using UIKit;
using Foundation;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Forms
{
	public class ZoomableImageView : UIViewController
	{
		string _filename;
		string _title;
		
		public ZoomableImageView (string filename, string title) : base()
		{
			_filename = filename;
			_title = title;
			
			Title = title;
		}
		
		TapZoomScrollView _scrollView;
		UIImageView _imageView;
		
		public class TapZoomScrollView : UIScrollView
		{
			public TapZoomScrollView (IntPtr handle) : base(handle)
			{
			}

			[Export("initWithCoder:")]
			public TapZoomScrollView (NSCoder coder) : base(coder)
			{
			}

			public TapZoomScrollView ()
			{
			}

			public TapZoomScrollView (RectangleF frame, UIImageView image) : base(frame)
			{
				_image = image;
			}
			
			UIImageView _image;
			
			public override void LayoutSubviews ()
			{
				base.LayoutSubviews ();
				
				// center the image as it becomes smaller than the size of the screen
				SizeF boundsSize = Bounds.Size;
				RectangleF frameToCenter = _image.Frame;

				// center horizontally
				nfloat newLeft = 0;
				nfloat newTop = 0;
				if (frameToCenter.Size.Width < boundsSize.Width)
					newLeft = (boundsSize.Width - frameToCenter.Size.Width) / 2;

				// center vertically
				if (frameToCenter.Size.Height < boundsSize.Height)
					newTop = (boundsSize.Height - frameToCenter.Size.Height) / 2;

				_image.Frame = new RectangleF(newLeft, newTop, _image.Frame.Width, _image.Frame.Height);
			}

			public override void TouchesBegan (Foundation.NSSet touches, UIEvent evt)
			{
				base.TouchesBegan (touches, evt);
				UITouch touch = touches.AnyObject as UITouch;
				if (touch.TapCount == 2) {
					if (this.ZoomScale >= 2) {
						this.SetZoomScale (1, true);
					} else {
						this.SetZoomScale (3, true);
					}
				}
			}
		}
		
		public override void LoadView ()
		{
			base.LoadView ();
			
			this._imageView = new UIImageView (UIImage.FromFile (_filename));
			
			this._scrollView = new TapZoomScrollView (
				new RectangleF (0, 0, this.View.Frame.Width, 
				this.View.Frame.Height - this.NavigationController.NavigationBar.Frame.Height), _imageView);
			this.View.AddSubview (this._scrollView);
			_scrollView.BackgroundColor = UIColor.Black;
			
			//---- create our image view
			
			this._scrollView.ContentSize = this._imageView.Image.Size;
			this._scrollView.AddSubview (this._imageView);
			
			this._scrollView.ViewForZoomingInScrollView += delegate(UIScrollView scrollView) {
				return this._imageView;
			};
			
			this._scrollView.MaximumZoomScale = 3f;
			this._scrollView.MinimumZoomScale = .1f;
			this._scrollView.PagingEnabled = true;
			
			nfloat scaleAmount = View.Bounds.Size.Width / _imageView.Frame.Width;
			if (scaleAmount < 0.1f)
				scaleAmount = 0.1f;
			
			_scrollView.SetZoomScale (scaleAmount, false);
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
		}
	}
}

