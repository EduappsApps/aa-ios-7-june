using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using System.Threading;
using MobileUtilities.Data;

namespace MobileUtilities.Forms
{
	public class RecordedAudioFile
	{
		public RecordedAudioFile ()
		{
		}
		
		public string Filename = "";
		
		public DateTime CreationDate {
			get {
				return System.IO.File.GetCreationTime(Filename);
			}
		}
	}
	
	public class ShowAudioFromFolder : SearchViewController<RecordedAudioFile>
	{
		public ShowAudioFromFolder (string title, string folder) : base(title)
		{
			this.FolderName = folder;
			this.ShowSearchOption = false;
		}
		
		public string FolderName = "";
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			StartAsyncDataRetrieve ();
			AddEditButton ();
			
			AudioPlayingSingleton.Instance.OnAudioFileStopped += HandleAudioPlayingSingletonInstanceOnAudioFileStopped;
		}
		
		void HandleAudioPlayingSingletonInstanceOnAudioFileStopped (object sender, EventArgs e)
		{
			tvc.TableView.ReloadData();	
		}
		
		public NSString cellIdent = new NSString ("ShowAudioFromFolderCellIdent");
		
		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}
		
		public override UITableViewCell DrawCell (RecordedAudioFile item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);
			if (cell == null)
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
					
			cell.TextLabel.Text = "Audio Recording";
			cell.DetailTextLabel.Text = "Recorded: " + item.CreationDate.ToString ();
			cell.Accessory = UITableViewCellAccessory.None;
			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			
			if (AudioPlayingSingleton.Instance.IsPlayingSound (item.Filename)) {
				cell.ImageView.Image = UIImage.FromFile (ImageConstants.StopSound);
				cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.StopSound);
				cell.TextLabel.Text += " (Playing)";
			} else {
				cell.ImageView.Image = UIImage.FromFile (ImageConstants.PlaySound);
				cell.ImageView.HighlightedImage = UIImage.FromFile (ImageConstants.PlaySound);					
			}
			
			return cell; 
		}
		
		public override void DoDataRetrieve ()
		{
			Items = RecordedAudioUtil.ReadFromFolder (FolderName);
		}
		
		public override void DoSearchDataRetrieve ()
		{
			SearchItems.Clear ();
			foreach (RecordedAudioFile image in Items)
				if (image.CreationDate.ToString ().Contains (SearchText))
					SearchItems.Add (image);
		}
		
		public override void RemoveItem (RecordedAudioFile item)
		{
			// Remove the file and preview image as well
			string filename = item.Filename;
			
			Items.Remove (item);
			if (SearchItems.IndexOf (item) >= 0)
				SearchItems.Remove (item);
			
			System.IO.File.Delete (filename);
			
			tvc.TableView.ReloadData ();
		}
		
		public override void DoRowSelected (RecordedAudioFile item)
		{
			// Play the audio or stop it
			if (AudioPlayingSingleton.Instance.IsPlayingSound (item.Filename)) {
				AudioPlayingSingleton.Instance.Stop ();
			} else {
				AudioPlayingSingleton.Instance.PlayFile (item.Filename);
			}

			tvc.TableView.ReloadData ();
		}
	}

	public class RecordedAudioUtil
	{
		public static List<RecordedAudioFile> ReadFromFolder (string folderName)
		{
			List<RecordedAudioFile > result = new List<RecordedAudioFile> ();
			
			if (System.IO.Directory.Exists (folderName)) {
				List<string > allFiles = new List<string> (System.IO.Directory.GetFiles (folderName));
				foreach (string filename in allFiles) {
					//string filenameOnly = System.IO.Path.GetFileName (filename);
					// Apply any filters below
					RecordedAudioFile file = new RecordedAudioFile ();
					file.Filename = filename;
					result.Add (file);
				}
			
				result.Sort (new NewestToOldestFileComparer ());
			}
			return result;
		}
		
		private class NewestToOldestFileComparer : IComparer<RecordedAudioFile>
		{
			public int Compare (RecordedAudioFile x, RecordedAudioFile y)
			{
				return DateTime.Compare (y.CreationDate, x.CreationDate);
			}
		}
	}

}

