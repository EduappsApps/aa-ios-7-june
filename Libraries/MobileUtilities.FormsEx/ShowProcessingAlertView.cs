using System;
using UIKit;
using System.Drawing;
using RectangleF = global::CoreGraphics.CGRect;
using PointF = global::CoreGraphics.CGPoint;
using SizeF = global::CoreGraphics.CGSize;

namespace MobileUtilities.Forms
{
	public class AbstractProcessingView
	{
		public AbstractProcessingView (string title, string message)
		{
			_title = title;
			_message = message;
		}
		
		protected UIAlertView alert;
		
		protected string _title, _message;
		protected int _position = 0;
		protected int _maximum = 0;
		
		public void Show ()
		{
			alert = new UIAlertView (_title, _message, null, null);
			alert.Show ();
			
			SetupAdditionalControls ();
		}
		
		protected virtual void SetupAdditionalControls ()
		{
		}
		
		public void Close ()
		{
			alert.DismissWithClickedButtonIndex (0, true);
		}
		
		public void UpdateMessage (string message)
		{
			_message = message;
			alert.Message = message;
			alert.SetNeedsLayout();
			alert.SetNeedsDisplay ();
		}
		
		public void UpdateTitle (string title)
		{
			_title = title;
			alert.Title = _title;
		}
		
		public virtual void UpdateProgress (int position, int maximum)
		{
			_position = position;
			_maximum = maximum;
			
			alert.Message = _message.Trim () + " (" + position + " of " + maximum + ")";
			alert.SetNeedsDisplay();
		}
	}
	
	// Borrowed from http://iphonedevelopertips.com/user-interface/uialertview-without-buttons-please-wait-dialog.html
	public class ShowProcessingAlertView : AbstractProcessingView
	{
		public ShowProcessingAlertView (string title, string message) : base(title, message)
		{
		}
		
		protected UIActivityIndicatorView indicator;
		
		protected override void SetupAdditionalControls ()
		{
			indicator = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.WhiteLarge);
			
			indicator.Center = new PointF (alert.Bounds.Width / 2, alert.Bounds.Height - 50);
			indicator.StartAnimating ();
			
			alert.AddSubview (indicator);			
		}
	}
}

