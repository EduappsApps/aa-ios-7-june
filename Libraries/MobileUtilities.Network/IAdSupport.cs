using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.iAd;
using MonoTouch.ObjCRuntime;
using MonoTouch.Foundation;

namespace MobileUtilities.Network
{
	public interface IShowOrHideAd
	{
		void ShowAd(ADBannerView view);
		void HideAd(ADBannerView view);
	}
	
	/// <summary>
	/// Support for an iAd system in the application. Needs to be instatiated, but once there it should be ok
	/// 
	/// http://stackoverflow.com/questions/5819952/monotouch-app-with-iads-runs-on-simulator-crashes-on-device
	/// </summary>
	public class IAdSupport
	{
		#region Helper Methods
		
		public static bool IsOnIOS4 ()
		{
			return UIDevice.CurrentDevice.RespondsToSelector (new Selector ("isMultitaskingSupported")); 
		}
		
		public static bool IsMultitaskingAvailable ()
		{
			if (IsOnIOS4 ()) {
				return UIDevice.CurrentDevice.IsMultitaskingSupported;
			} else {
				return false;
			}
		}
		
		public static bool IsCapable ()
		{
			return IsMultitaskingAvailable ();
		}
		
		public static RectangleF GetBottomBoundsInView (UIView view)
		{
			return new RectangleF (0, view.Frame.Height - 50, 320, 50);
		}
		
		public static RectangleF GetTopBoundsInView (UIView view)
		{
			return new RectangleF (0, 0, 320, 50);			
		}
		
		#endregion
		
		public ADBannerView adBannerView = null;
		private bool _hasAd = false;
		
		/// <summary>
		/// Setupis the ad.
		/// </summary>
		/// <param name='bounds'>
		/// Bounds should be something like new RectangleF (0, m_uivInner.Frame.Bottom + 9, 320, 50)
		/// </param>
		public void SetupiAd (UIView view, RectangleF bounds, IShowOrHideAd access)
		{
			if (IsCapable ()) {
				
				if (adBannerView == null) {
					adBannerView = new ADBannerView ();
					NSMutableSet nsM = new NSMutableSet (); 
					nsM.Add (new NSString ("ADBannerContentSize320x50")); 
			
					adBannerView.RequiredContentSizeIdentifiers = nsM; 
			
					adBannerView.AdLoaded += delegate(object sender, EventArgs e) {
						_hasAd = true;
						if (_currentInterface != null)
							_currentInterface.ShowAd (adBannerView);
					};
				
					adBannerView.FailedToReceiveAd += delegate(object sender, AdErrorEventArgs e) {
						_hasAd = false;
						if (_currentInterface != null)
							_currentInterface.HideAd (adBannerView);
					};
				}
				
				adBannerView.Frame = bounds; 
				adBannerView.Alpha = 0.0f;
				_currentInterface = access;
				adBannerView.RemoveFromSuperview ();
				
				view.AddSubview (adBannerView);
				
				if (_hasAd) {
					if (_currentInterface != null)
						_currentInterface.ShowAd (adBannerView);
				}
			}
		}
		
		IShowOrHideAd _currentInterface = null;
	}
}

