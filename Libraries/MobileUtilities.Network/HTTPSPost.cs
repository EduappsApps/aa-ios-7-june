using System;
using MonoTouch.Foundation; 
using MonoTouch.CoreFoundation; 
using MonoTouch.UIKit; 
using MonoTouch.ObjCRuntime; 
using System.Net;

namespace MobileUtilities.Network
{ 
	public class Http 
	{ 
		public static string HttpsPost2 (string url, string postData, string username, string password)
		{
			System.Net.WebRequest req = System.Net.WebRequest.Create (url);
			req.ContentType = "application/x-www-form-urlencoded";
			req.Method = "POST";
			
			if (username.Length > 0)
				req.Credentials = new NetworkCredential (username, password);
			
			byte[] bytes = System.Text.Encoding.ASCII.GetBytes (postData);
			req.ContentLength = bytes.Length;
			System.IO.Stream os = req.GetRequestStream ();
			os.Write (bytes, 0, bytes.Length); 
			os.Close ();
			
			System.Net.WebResponse resp = req.GetResponse ();
			if (resp == null)
				return null;
			
			System.IO.StreamReader sr = new System.IO.StreamReader (resp.GetResponseStream ());
			return sr.ReadToEnd ().Trim ();
		}
		
    	public static string HttpsPost (string url, string postData, string username, string password)
		{ 
			//Instantiate a NSMutableURLRequest 
			IntPtr nsMutableRequestPtr = Messaging.IntPtr_objc_msgSend (new Class ("NSMutableURLRequest").Handle, 
        new Selector ("new").Handle); 

			//Since NSMutableURLRequest subclasses NSUrlRequest, we can use NSURLRequest to work with 
			NSUrlRequest req = (NSUrlRequest)Runtime.GetNSObject (nsMutableRequestPtr); 

			//Set the url of the request 
			Messaging.void_objc_msgSend_IntPtr (req.Handle, new Selector ("setURL:").Handle,
        new NSUrl (url).Handle); 

			//Set the HTTP Method (POST) 
			Messaging.void_objc_msgSend_IntPtr (req.Handle, 
        new Selector ("setHTTPMethod:").Handle, 
        new NSString ("POST").Handle); 

			//Make a selector to be used twice 
			Selector selSetValueForHttpHeaderField = new Selector ("setValue:forHTTPHeaderField:"); 

			//Set the Content-Length HTTP Header 
			Messaging.void_objc_msgSend_IntPtr_IntPtr (req.Handle, 
        selSetValueForHttpHeaderField.Handle, 
        new NSString (postData.Length.ToString ()).Handle, 
        new NSString ("Content-Length").Handle); 

			//Set the Content-Type HTTP Header 
			Messaging.void_objc_msgSend_IntPtr_IntPtr (req.Handle, 
        selSetValueForHttpHeaderField.Handle, 
        new NSString ("application/x-www-form-urlencoded").Handle, 
        new NSString ("Content-Type").Handle); 

			//Make our c# string into a NSString of our post data 
			NSString sData = new NSString (postData); 

			//Now get NSData from that string using ASCII Encoding 
			NSData pData = new NSData (Messaging.IntPtr_objc_msgSend_int_int (sData.Handle, 
        new Selector ("dataUsingEncoding:allowLossyConversion:").Handle, 1, 1)); 

			//Set the HTTPBody, which is our POST data 
			Messaging.void_objc_msgSend_IntPtr (req.Handle, 
        new Selector ("setHTTPBody:").Handle, 
        pData.Handle); 

			//Need to pass in a reference to the urlResponse object for the next method 
			IntPtr urlRespHandle = IntPtr.Zero; 

			//Send our request Synchronously 
			NSData dataResult = new NSData (Messaging.IntPtr_objc_msgSend_IntPtr_IntPtr_IntPtr (
        new Class ("NSURLConnection").Handle, 
        new Selector ("sendSynchronousRequest:returningResponse:error:").Handle, 
        req.Handle, urlRespHandle, IntPtr.Zero)); 

			//Get the urlResponse object 
			// Get this if you need it 
			//NSUrlResponse urlResp = (NSUrlResponse)Runtime.GetNSObject(urlRespHandle); 

			//Get ourselves a new NSString alloc'd, but not init'd 
			IntPtr resultStrHandle = Messaging.IntPtr_objc_msgSend (new Class ("NSString").Handle, 
        new Selector ("alloc").Handle); 

			//Init the NSString with our response data, and UTF8 encoding 
			resultStrHandle = Messaging.IntPtr_objc_msgSend_IntPtr_int (resultStrHandle, 
        new Selector ("initWithData:encoding:").Handle, dataResult.Handle, 4); 

			//Finally, get our string result 

			return new NSString (resultStrHandle); 
		}
	}
}