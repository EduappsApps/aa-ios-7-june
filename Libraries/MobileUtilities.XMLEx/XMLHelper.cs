using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace MobileUtilities.XML
{
	/// <summary>
	/// Class used for manipulation of the XML. This is used quite heavily
	/// </summary>
	public class XMLHelper 
	{
		public static void StoreXMLText(XmlElement e, string ElementName, string ElementValue)
		{
			//See if the node name already exists and use that one if it does

			XmlNodeList nodes = e.GetElementsByTagName(ElementName);
			if (nodes.Count > 0)
			{
				nodes[0].InnerText = ElementValue;
			} 
			else
			{
				XmlElement Child = e.OwnerDocument.CreateElement(ElementName);
				Child.InnerText = ElementValue;
				e.AppendChild(Child);
			}
		}

        public static void StoreXMLCData(XmlElement e, string ElementName, string ElementValue)
        {
            XmlNodeList nodes = e.GetElementsByTagName(ElementName);
            if (nodes.Count > 0)
            {
                nodes[0].RemoveAll();

                XmlCDataSection cChild = e.OwnerDocument.CreateCDataSection(ElementName);
                nodes[0].AppendChild(cChild);
            }
            else
            {
                XmlElement Child = e.OwnerDocument.CreateElement(ElementName);
                Child.InnerText = ElementValue;
                e.AppendChild(Child);

                XmlCDataSection cChild = e.OwnerDocument.CreateCDataSection(ElementName);
                Child.AppendChild(cChild);
                e.AppendChild(Child);
            }
        }

		public static void StoreXMLBoolean(XmlElement e, string ElementName, bool BoolValue)
		{
			if (BoolValue)
				StoreXMLText(e, ElementName, "Yes");
			else
				StoreXMLText(e, ElementName, "No");
		}

		public static XmlElement GetChildNode(XmlElement e, string ElementName)
		{
			XmlNodeList nodes = e.GetElementsByTagName(ElementName);
			if (nodes.Count > 0)
			{
				return (XmlElement)(nodes[0]);
			} 
			else
			{
				return null;
			}
		}

		public static XmlElement CreateNewChildElement(XmlElement e, string ElementName)
		{
			XmlElement Child = e.OwnerDocument.CreateElement(ElementName);
			e.AppendChild(Child);
			return Child;
		}

		//Loading Methods
		public static string ReadXMLText(XmlElement parent, string ElementName, string DefaultValue)
		{
			if (!parent.HasChildNodes)
				return DefaultValue;
			else 
			{
				//Load the value from the item
				XmlNodeList nodes = parent.GetElementsByTagName(ElementName);
				if (nodes.Count > 0)
				{
					return nodes[0].InnerText;
				} 
				else
				{
					return DefaultValue;
				}
			}
		}

		public static int ReadXMLInt(XmlElement parent, string ElementName, int Default)
		{
			try
			{
				return Convert.ToInt32(ReadXMLText(parent, ElementName, Default.ToString()));
			} 
			catch 
			{
				return Default;
			}
		}

		public static bool ReadXMLBool(XmlElement parent, string ElementName, bool Default)
		{
			string DefaultAsString;
			if (Default)
				DefaultAsString = "Yes";
			else
				DefaultAsString = "No";
			string inText = ReadXMLText(parent, ElementName, DefaultAsString);
			switch (inText.ToLower())
			{
				case "yes":
				case "y":
				case "true":
				case "t":
				case "1":
					return true;
				default:
					return false;
			}
		}

        public static void WriteSeries(XmlElement parent, string CollectionName, string EntryName, XmlStoredItem[] items)
        {
            XmlElement e = GetChildNode(parent, CollectionName);
            if (e != null)
                e.RemoveAll();
            else
                e = CreateNewChildElement(parent, CollectionName);

            //Add the items
            foreach (XmlStoredItem item in items)
                item.StoreInXML(CreateNewChildElement(e, EntryName));
        }

        public static XmlStoredItem[] ReadSeries(XmlElement parent, string CollectionName, string EntryName, Type itemItem)
        {
            List<XmlStoredItem> result = new List<XmlStoredItem>();

            XmlElement e = GetChildNode(parent, CollectionName);
            if (e != null)
            {
                foreach (XmlElement entry in e.GetElementsByTagName(EntryName))
                {
                    XmlStoredItem item = (XmlStoredItem)Activator.CreateInstance(itemItem);
                    item.LoadFromXML(entry);
                    result.Add(item);
                }
            }

            return result.ToArray();
        }

        public static XmlDocument LoadXml(string xmlContent)
        {
            //if (!xmlContent.StartsWith("<xml"))
            //    xmlContent = "<?xml version=\"1.0\" encoding=\"UTF-16\"?>" + Environment.NewLine + xmlContent;

            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            doc.LoadXml(xmlContent.Trim());

            return doc;

            //// Encode the XML string in a UTF-8 byte array
            //byte[] encodedString = Encoding.Unicode.GetBytes(xmlContent);

            //// Put the byte array into a stream and rewind it to the beginning
            //MemoryStream ms = new MemoryStream(encodedString);
            //ms.Flush();
            //ms.Position = 0;

            //// Build the XmlDocument from the MemorySteam of UTF-8 encoded bytes
            //XmlDocument xmlDoc = new XmlDocument();
            //xmlDoc.Load(ms);

            //return xmlDoc;
        }
	}
}
