using System;

namespace MobileUtilities.XML
{
	public class DataTypeStorage
	{
		public static string WriteXmlString(string value)
		{
			return value;
		}

		public static string WriteXmlInteger(int EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlInt16(Int16 EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlInt64(Int64 EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlUInt32(uint EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlUInt16(UInt16 EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlUInt64(UInt64 EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlCurrency(decimal EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlDecimal(decimal EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlDate(DateTime EntryValue)
		{
			return EntryValue.ToString("yyyy-MM-dd");
		}

		public static string WriteXmlTime(DateTime EntryValue)
		{
			return EntryValue.ToString("hh:mm:ss");
		}

		public static string WriteXmlDateTime(DateTime EntryValue)
		{
			return EntryValue.ToString("yyyy-MM-ddThh:mm:ss");
		}

		public static string WriteXmlDouble(double EntryValue)
		{
			return EntryValue.ToString();
		}

		public static string WriteXmlLongText(string EntryValue)
		{
			return EntryValue;
		}

		public static string WriteXmlBinaryData(byte[] EntryValue)
		{
			///TODO: Needs work - See the XML Book
			//System.IO.MemoryStream ms = new System.IO.MemoryStream();
			//System.Xml.XmlTextWriter w = new System.Xml.XmlTextWriter(ms);
			//w.WriteBinHex(EntryValue, 0, EntryValue.Length);
			return "";
		}

		public static string WriteXmlBoolean(bool EntryValue)
		{
			return EntryValue ? "1" : "0";
		}

		public static string ReadXmlString(string value)
		{
			return value;
		}

		public static int ReadXmlInteger(string value)
		{
			return ReadXmlInt32(value);
		}

		public static int ReadXmlInt32(string value)
		{
			return Convert.ToInt32(value);
		}

		public static Int16 ReadXmlInt16(string value)
		{
			return Convert.ToInt16(value);
		}

		public static Int64 ReadXmlInt64(string value)
		{
			return Convert.ToInt64(value);
		}

		public static uint ReadXmlUInt32(string value)
		{
			return Convert.ToUInt32(value);
		}

		public static UInt16 ReadXmlUInt16(string value)
		{
			return Convert.ToUInt16(value);
		}

		public static UInt64 ReadXmlUInt64(string value)
		{
			return Convert.ToUInt64(value);
		}

		public static decimal ReadXmlCurrency(string value)
		{
			return Convert.ToDecimal(value);
		}

		public static decimal ReadXmlDecimal(string value)
		{
			return ReadXmlCurrency(value);
		}

		private static void GetTime(string value, out int hours, out int minutes, out int seconds)
		{
			//Should be the format hh:mm:ss
			hours = Convert.ToInt32(value.Substring(0, 2));
			minutes = Convert.ToInt32(value.Substring(3, 2));
			seconds = Convert.ToInt32(value.Substring(6, 2));
		}

		private static void GetDate(string value, out int years, out int months, out int days)
		{
			//Should be the format yyyy-MM-dd
			years = Convert.ToInt32(value.Substring(0, 4));
			months = Convert.ToInt32(value.Substring(5, 2));
			days = Convert.ToInt32(value.Substring(8, 2));
		}

		public static DateTime ReadXmlDate(string value)
		{
			int years = 0;
			int months = 0;
			int days = 0;
			try
			{
				GetDate(value, out years, out months, out days);
				return new DateTime(years, months, days);
			}
			catch 
			{
				return DateTime.Now;
			}
		}

		public static DateTime ReadXmlTime(string value, DateTime DefaultValue)
		{
			int hours = 0;
			int minutes = 0;
			int seconds = 0;
			DateTime current = DateTime.Now;
			try
			{
				GetTime(value, out hours, out minutes, out seconds);
				return new DateTime(current.Year, current.Month, current.Day,
					hours, minutes, seconds);
			}
			catch 
			{
				return DefaultValue;
			}
		}

		public static DateTime ReadXmlDateTime(string value, DateTime DefaultValue)
		{
			int hours = 0;
			int minutes = 0;
			int seconds = 0;

			int years = 0;
			int months = 0;
			int days = 0;

			try
			{
				GetDate(value.Substring(0, 10), out years, out months, out days);
				GetTime(value.Substring(11), out hours, out minutes, out seconds);

				return new DateTime(years, months, days, hours, minutes, seconds);
			} 
			catch 
			{
				return DefaultValue;
			}
		}

		public static double ReadXmlDouble(string value)
		{
			return Convert.ToDouble(value);
		}

		public static string ReadXmlLongText(string value)
		{
			return value;
		}

		public static byte[] ReadXmlBinaryData(string value)
		{
			///TODO: Needs work - See the XML Book
			//System.IO.MemoryStream ms = new System.IO.MemoryStream();
			//System.Xml.XmlTextWriter w = new System.Xml.XmlTextWriter(ms);
			//w.WriteBinHex(EntryValue, 0, EntryValue.Length);
			return new byte[] {};
		}

		public static bool ReadXmlBoolean(string value)
		{
			return value == "1";
		}
	}
}

