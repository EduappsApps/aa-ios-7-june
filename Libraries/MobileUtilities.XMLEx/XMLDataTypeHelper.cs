using System;
using System.Xml;
using System.IO;

namespace MobileUtilities.XML
{
	/// <summary>
	/// This class manages the process of writing the correct data types for
	/// XSD based systems
	/// </summary>
	public sealed class XMLDataTypeHelper
	{
		private XMLDataTypeHelper()
		{
		}

		#region Methods for Writing the Data

		public static void WriteXmlString(XmlElement e, string entryName, string EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue);
		}

		public static void WriteXmlInteger(XmlElement e, string entryName, int EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlInt32(XmlElement e, string entryName, int EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlInt16(XmlElement e, string entryName, Int16 EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlInt64(XmlElement e, string entryName, Int64 EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlUInt32(XmlElement e, string entryName, uint EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlUInt16(XmlElement e, string entryName, UInt16 EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlUInt64(XmlElement e, string entryName, UInt64 EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlCurrency(XmlElement e, string entryName, decimal EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlDecimal(XmlElement e, string entryName, decimal EntryValue)
		{
			WriteXmlCurrency(e, entryName, EntryValue);
		}

		public static void WriteXmlDate(XmlElement e, string entryName, DateTime EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString("yyyy-MM-dd"));
		}

		public static void WriteXmlTime(XmlElement e, string entryName, DateTime EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString("hh:mm:ss"));
		}

		public static void WriteXmlDateTime(XmlElement e, string entryName, DateTime EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString("yyyy-MM-ddThh:mm:ss"));
		}

		public static void WriteXmlDouble(XmlElement e, string entryName, double EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue.ToString());
		}

		public static void WriteXmlLongText(XmlElement e, string entryName, string EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue);
		}

		public static void WriteXmlBinaryData(XmlElement e, string entryName, byte[] EntryValue)
		{
			///TODO: Needs work - See the XML Book
			//System.IO.MemoryStream ms = new System.IO.MemoryStream();
			//System.Xml.XmlTextWriter w = new System.Xml.XmlTextWriter(ms);
			//w.WriteBinHex(EntryValue, 0, EntryValue.Length);
		}

		public static void WriteXmlBoolean(XmlElement e, string entryName, bool EntryValue)
		{
			XMLHelper.StoreXMLText(e, entryName, EntryValue ? "1" : "0");
		}

		#endregion

		#region Methods for Reading the Data

		public static string ReadXmlString(XmlElement e, string entryName, string DefaultValue)
		{
			return XMLHelper.ReadXMLText(e, entryName, DefaultValue);
		}

		public static int ReadXmlInteger(XmlElement e, string entryName, int DefaultValue)
		{
			return ReadXmlInt32(e, entryName, DefaultValue);
		}

		public static int ReadXmlInt32(XmlElement e, string entryName, int DefaultValue)
		{
			return XMLHelper.ReadXMLInt(e, entryName, DefaultValue);
		}

		public static Int16 ReadXmlInt16(XmlElement e, string entryName, Int16 DefaultValue)
		{
			return Convert.ToInt16(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static Int64 ReadXmlInt64(XmlElement e, string entryName, Int64 DefaultValue)
		{
			return Convert.ToInt64(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static uint ReadXmlUInt32(XmlElement e, string entryName, uint DefaultValue)
		{
			return Convert.ToUInt32(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static UInt16 ReadXmlUInt16(XmlElement e, string entryName, UInt16 DefaultValue)
		{
			return Convert.ToUInt16(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static UInt64 ReadXmlUInt64(XmlElement e, string entryName, UInt64 DefaultValue)
		{
			return Convert.ToUInt64(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static decimal ReadXmlCurrency(XmlElement e, string entryName, decimal DefaultValue)
		{
			return Convert.ToDecimal(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static decimal ReadXmlDecimal(XmlElement e, string entryName, decimal DefaultValue)
		{
			return ReadXmlCurrency(e, entryName, DefaultValue);
		}

		#region Date/Time routines

		private static void GetTime(string value, out int hours, out int minutes, out int seconds)
		{
			//Should be the format hh:mm:ss
			hours = Convert.ToInt32(value.Substring(0, 2));
			minutes = Convert.ToInt32(value.Substring(3, 2));
			seconds = Convert.ToInt32(value.Substring(6, 2));
		}

		private static void GetDate(string value, out int years, out int months, out int days)
		{
			//Should be the format yyyy-MM-dd
			years = Convert.ToInt32(value.Substring(0, 4));
			months = Convert.ToInt32(value.Substring(5, 2));
			days = Convert.ToInt32(value.Substring(8, 2));
		}

		public static DateTime ReadXmlDate(XmlElement e, string entryName, DateTime DefaultValue)
		{
			string value = XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString("yyyy-MM-dd"));

			int years = 0;
			int months = 0;
			int days = 0;
			try
			{
				GetDate(value, out years, out months, out days);
				return new DateTime(years, months, days);
			}
			catch 
			{
				return DefaultValue;
			}
		}

		public static DateTime ReadXmlTime(XmlElement e, string entryName, DateTime DefaultValue)
		{
			string value = XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString("hh:mm:ss"));

			int hours = 0;
			int minutes = 0;
			int seconds = 0;
			DateTime current = DateTime.Now;
			try
			{
				GetTime(value, out hours, out minutes, out seconds);
				return new DateTime(current.Year, current.Month, current.Day,
					hours, minutes, seconds);
			}
			catch 
			{
				return DefaultValue;
			}
		}

		public static DateTime ReadXmlDateTime(XmlElement e, string entryName, DateTime DefaultValue)
		{
			string value = XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString("yyyy-MM-ddThh:mm:ss"));

			int hours = 0;
			int minutes = 0;
			int seconds = 0;

			int years = 0;
			int months = 0;
			int days = 0;

			try
			{
				GetDate(value.Substring(0, 10), out years, out months, out days);
				GetTime(value.Substring(11), out hours, out minutes, out seconds);

				return new DateTime(years, months, days, hours, minutes, seconds);
			} 
			catch 
			{
				return DefaultValue;
			}
		}

		#endregion

		public static double ReadXmlDouble(XmlElement e, string entryName, double DefaultValue)
		{
			return Convert.ToDouble(XMLHelper.ReadXMLText(e, entryName, DefaultValue.ToString()));
		}

		public static string ReadXmlLongText(XmlElement e, string entryName, string DefaultValue)
		{
			return XMLHelper.ReadXMLText(e, entryName, DefaultValue);
		}

		public static byte[] ReadXmlBinaryData(XmlElement e, string entryName)
		{
			///TODO: Needs work - See the XML Book
			//System.IO.MemoryStream ms = new System.IO.MemoryStream();
			//System.Xml.XmlTextWriter w = new System.Xml.XmlTextWriter(ms);
			//w.WriteBinHex(EntryValue, 0, EntryValue.Length);
			return new byte[] {};
		}

		public static bool ReadXmlBoolean(XmlElement e, string entryName, bool DefaultValue)
		{
			return XMLHelper.ReadXMLText(e, entryName, "0") == "1";
		}

		#endregion

	}
}
