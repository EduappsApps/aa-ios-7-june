using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using System.IO;
using MonoTouch.MessageUI;

namespace MobileUtilities.Reporting
{
	public abstract class BasePdfViewController : UIViewController
	{
		protected string _filename = "";
		
		public BasePdfViewController () : base()
		{
		}
		
		MFMailComposeViewController _mail;
		public string EmailSubject = "";
		public string EmailBodyContent = "";
		public string[] SendToRecipiants = new string[] { };
		public string[] SendCCRecipiants = new string[] { };
		public string[] SendBCCRecipiants = new string[] { };
		
		public void DoForwardDocument (object sender, EventArgs e)
		{
			// Construct a new email message, attach the file and show the email dialog
			_mail = new MFMailComposeViewController ();
			_mail.SetMessageBody (EmailBodyContent, false);
			_mail.SetSubject (EmailSubject);
			
			if (SendToRecipiants.Length > 0)
				_mail.SetToRecipients (SendToRecipiants);
			if (SendCCRecipiants.Length > 0)
				_mail.SetCcRecipients (SendCCRecipiants);
			if (SendBCCRecipiants.Length > 0)
				_mail.SetBccRecipients (SendBCCRecipiants);
			
			// Determine the mime type
			_mail.AddAttachmentData (NSData.FromFile (_filename), "application/pdf", System.IO.Path.GetFileName (_filename));
			_mail.Finished += HandleMailFinished;
			
			this.PresentModalViewController (_mail, true);
		}
		
		private void HandleMailFinished (object sender, MFComposeResultEventArgs e)
		{
			switch (e.Result) {
			case MFMailComposeResult.Sent:
				UIAlertView alert = new UIAlertView ("Mail Sent", "The mail and attached PDF document were successfully sent", 
						null, "Close", null);
				alert.Show ();
				e.Controller.DismissModalViewControllerAnimated (true);
				break;
			case MFMailComposeResult.Saved:
				UIAlertView alertSaved = new UIAlertView ("Mail Saved", "The mail and attached PDF document were saved for later transmission", null, "Close", null);
				alertSaved.Show ();
				e.Controller.DismissModalViewControllerAnimated (true);
				break;
			case MFMailComposeResult.Cancelled:
				e.Controller.DismissModalViewControllerAnimated (true);
				break;
			case MFMailComposeResult.Failed:
				UIAlertView alertFailed = new UIAlertView ("Mail delivery failed", "The mail and attached PDF document failed. Please ensure you have " +
					"an email account set up and appropriate network connectivity. ", null, "Close", null);
				alertFailed.Show ();
				
				break;
			}
		}
		
		#region Support for the Toolbar
		
		protected UIBarButtonItem previousPage;
		protected UIBarButtonItem nextPage;
		protected UIBarButtonItem printPage;
		protected UIBarButtonItem forwardDocument;
		
		public static string PreviousPageImage = "images/arrowleft.png";
		public static string NextPageImage = "images/arrowright.png";
		public static string EmailImage = "images/18-envelope.png";
		public static string PrintDocumentImage = "images/185-printer.png";
		
		protected abstract void DoNextPage();
		protected abstract void DoPreviousPage();
		
		private void PreviousPage (object sender, EventArgs e)
		{
			DoPreviousPage ();
			UpdateButtonStates ();
		}
		
		private void NextPage (object sender, EventArgs e)
		{
			DoNextPage ();	
			UpdateButtonStates ();
		}
		
		private void PrintDocument(object sender, EventArgs e)
		{
			var printInfo = UIPrintInfo.PrintInfo;
			printInfo.OutputType = UIPrintInfoOutputType.General;
			printInfo.JobName = "ProperSight Report";

			string pdfFileName = _filename;
			NSUrl url = NSUrl.FromFilename (pdfFileName);           

			var printer = UIPrintInteractionController.SharedPrintController;
			printer.PrintInfo = printInfo;
			printer.PrintingItem = url;
			printer.ShowsPageRange = true;

			printer.Present (true, (handler, completed, err) => {
				if (!completed && err != null) {
					MobileUtilities.Forms.DialogHelpers.ShowMessage("Problem Printing...", "A problem occurred while printing the document.");
				}
			});
		}
		
		protected abstract void UpdateButtonStates();
		
		public void SetupToolbar ()
		{
			previousPage = new UIBarButtonItem (UIImage.FromFile (PreviousPageImage), UIBarButtonItemStyle.Plain, new EventHandler (PreviousPage));
			nextPage = new UIBarButtonItem (UIImage.FromFile (NextPageImage), UIBarButtonItemStyle.Plain, new EventHandler (NextPage));
			printPage = new UIBarButtonItem (UIImage.FromFile (PrintDocumentImage), UIBarButtonItemStyle.Plain, new EventHandler (PrintDocument));
			forwardDocument = new UIBarButtonItem (UIImage.FromFile (EmailImage), UIBarButtonItemStyle.Plain, new EventHandler (DoForwardDocument));
		
			if (MFMailComposeViewController.CanSendMail) {
				ToolbarItems = new UIBarButtonItem[] {
					previousPage, nextPage,
					new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace, null),
					printPage,
					forwardDocument				
				};
			} else {
				ToolbarItems = new UIBarButtonItem[] { previousPage, nextPage };
			}
			
			UpdateButtonStates ();
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			SetupToolbar ();
		}
		
		#endregion
	}
}

