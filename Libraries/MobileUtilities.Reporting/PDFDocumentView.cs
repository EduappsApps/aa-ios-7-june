using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using System.IO;
using MonoTouch.MessageUI;
using MobileUtilities.Forms;

namespace MobileUtilities.Reporting
{
    public class PDFDocumentView : UIView
    {
        int _pageNumber = 1;
        CGPDFDocument _pdf;
		string _filename;
		
		public string Filename
		{
			get
			{
				return _filename;
			}
		}
		
        public int PageNumber {
            get { return this._pageNumber; }
            set {
                if (value >= 0 && value < _pdf.Pages) {
                    _pageNumber = value;
                    this.SetNeedsDisplay ();
                }
            }
        }
		
		public bool CanMoveNext ()
		{
			return _pageNumber < _pdf.Pages - 1;
		}
		
		public bool CanMoveBack ()
		{
			return _pageNumber > 1;
		}
		
        public PDFDocumentView (RectangleF frame, string filename) : base(frame)
        {
        	_pageNumber = 1;
			_filename = filename;
            _pdf = CGPDFDocument.FromFile (_filename);
        }

        public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
            
			CGContext gctx = UIGraphics.GetCurrentContext ();
			
			// Fill the background
			gctx.SetFillColor (1, 1, 1, 1);
			gctx.FillRect (Bounds);
			
			gctx.TranslateCTM (0, Bounds.Height);
			gctx.ScaleCTM (1, -1);
            
			using (CGPDFPage pdfPg = _pdf.GetPage (PageNumber)) {

				gctx.SaveState ();       
                
				RectangleF r = new RectangleF (Bounds.Left, Bounds.Top, Bounds.Width, Bounds.Height); 
				                               //+ 44f);
				CGAffineTransform tf = pdfPg.GetDrawingTransform (CGPDFBox.Crop, r, 0, true);
				gctx.ConcatCTM (tf);
				gctx.DrawPDFPage (pdfPg);
                
				gctx.RestoreState ();
				gctx.TranslateCTM (0, Bounds.Height - 25);
			}
		}

        protected override void Dispose (bool disposing)
        {
            _pdf.Dispose ();           
            base.Dispose (disposing);
        }
    }
	
	public class PDFDocumentViewController : BasePdfViewController
	{
		public PDFDocumentViewController (string filename) : base()
		{
			_filename = filename;
		}
		
		
		private PDFDocumentView GetPdfView ()
		{
			return (View as PDFDocumentView);
		}
		
		protected override void UpdateButtonStates ()
		{
			previousPage.Enabled = GetPdfView ().CanMoveBack ();
			nextPage.Enabled = GetPdfView ().CanMoveNext ();
		}
		
		protected override void DoPreviousPage ()
		{
			GetPdfView ().PageNumber--;
			UpdateButtonStates ();
		}
		
		protected override void DoNextPage ()
		{
			GetPdfView().PageNumber++;
			UpdateButtonStates ();
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			NavigationController.SetToolbarHidden(false, animated);
		}
		
		public override void LoadView ()
		{
			base.LoadView ();
			
			RectangleF newBounds = new RectangleF (View.Bounds.Left, 
			                                       View.Bounds.Top, 
			                                       View.Bounds.Width, 
			                                       View.Bounds.Height);// - ControlHeights.ToolBarHeight);
			
			this.View = new PDFDocumentView (newBounds, _filename);
			
			SetupToolbar ();
			
			Title = System.IO.Path.GetFileName (_filename);
		}
	}
}
