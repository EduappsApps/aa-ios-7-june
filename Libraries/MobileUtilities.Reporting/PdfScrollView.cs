using System;
using MonoTouch.CoreAnimation;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using System.Drawing;

namespace MobileUtilities.Reporting
{
	public class PdfScrollView : UIScrollView
	{
		// main, visible, pdf view
		TiledPdfView pdfView;
		// temporary, while zooming, view
		TiledPdfView oldPdfView;
		// low resolution bitmap using until 'pdfView' is ready
		UIImageView backgroundImageView;

		// global reference to the PDF document/page
		CGPDFDocument pdf;
		CGPDFPage page;
		float scale;
		
		public int PageCount;
		public int CurrentPage;
		
		RectangleF _frame;
		string _filename;
		
		public void DisplayPage (int pageNo)
		{
			page = pdf.GetPage (pageNo);
			pdfView.Page = page;
			CurrentPage = pageNo;
			
			pdfView.SetNeedsDisplay ();
		}
		
		public void NextPage ()
		{
			DisplayPage (CurrentPage + 1);
		}
		
		public void PreviousPage ()
		{
			DisplayPage(CurrentPage - 1);
		}
		
		public PdfScrollView (RectangleF frame, string filename)
			: base (frame)
		{
			this._filename = filename;
			_frame = frame;
			
			ShowsVerticalScrollIndicator = false;
			ShowsHorizontalScrollIndicator = false;
			BouncesZoom = true;
			DecelerationRate = UIScrollView.DecelerationRateFast;
			BackgroundColor = UIColor.Gray;
			MaximumZoomScale = 5.0f;
			MinimumZoomScale = 0.25f;
			ScrollEnabled = true;
			
			// open the PDF file (default directory is the bundle path)
			pdf = CGPDFDocument.FromFile (filename);
			PageCount = pdf.Pages;
			
			// select the first page (the only one we'll use)
			page = pdf.GetPage (1);
			CurrentPage = 1;
			
			// make the initial view 'fit to width'
			RectangleF pageRect = page.GetBoxRect (CGPDFBox.Media);
			scale = Frame.Width / pageRect.Width;
			pageRect.Size = new SizeF (pageRect.Width * scale, pageRect.Height * scale);
			
			// create bitmap version of the PDF page, to be used (scaled)
			// when no other (tiled) view are visible
			UIGraphics.BeginImageContext (pageRect.Size);
			CGContext context = UIGraphics.GetCurrentContext ();

			// fill with white background
			context.SetFillColor (1.0f, 1.0f, 1.0f, 1.0f);
			context.FillRect (pageRect);
			context.SaveState ();
			
			// flip page so we render it as it's meant to be read
			context.TranslateCTM (0.0f, pageRect.Height);
			context.ScaleCTM (1.0f, -1.0f);

			// scale page at the view-zoom level
			context.ScaleCTM (scale, scale);
			context.DrawPDFPage (page);
			context.RestoreState ();
			
			UIImage backgroundImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			backgroundImageView = new UIImageView (backgroundImage);
			backgroundImageView.Frame = pageRect;
			backgroundImageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			AddSubview (backgroundImageView);
			SendSubviewToBack (backgroundImageView);
			
			// Create the TiledPDFView based on the size of the PDF page and scale it to fit the view.
			pdfView = new TiledPdfView (pageRect, scale);
			pdfView.Page = page;
			AddSubview (pdfView);
			
			// no need to have (or set) a UIScrollViewDelegate with MonoTouch
			
			this.ViewForZoomingInScrollView = delegate {
				// return the view we'll be using while zooming
				return pdfView;
			};
			
			// when zooming starts we remove (from view) and dispose any 
			// oldPdfView and set pdfView as our 'new' oldPdfView, it will
			// stay there until a new view is available (when zooming ends)
			this.ZoomingStarted += delegate {
				if (oldPdfView != null) {
					oldPdfView.RemoveFromSuperview ();
					oldPdfView.Dispose ();
				}
				oldPdfView = pdfView;
				AddSubview (oldPdfView);
			};
			
			// when zooming ends a new TiledPdfView is created (and shown)
			// based on the updated 'scale' and 'frame'
			ZoomingEnded += delegate (object sender, ZoomingEndedEventArgs e) {
				scale *= e.AtScale;

				RectangleF rect = pdfView.Page.GetBoxRect (CGPDFBox.Media);
				rect.Size = new SizeF (rect.Width * scale, rect.Height * scale);
				
				pdfView = new TiledPdfView (rect, scale);
				pdfView.Page = page;
				AddSubview (pdfView);
			};
		}
		
		public override void TouchesBegan (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			
//			UITouch touch = touches.AnyObject as UITouch;
//			if (touch.TapCount == 2) {
//				if (this.ZoomScale >= 2.0f) {
//					this.SetZoomScale (0.5f, true);
//					ZoomScale = 0.5f;
//				} else {
//					this.SetZoomScale (2.0f, true);
//					ZoomScale = 2.0f;
//				}
//			}
		}
		
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			
			// if the page becomes smaller than the view's bounds then we
			// center it in the screen
			SizeF boundsSize = Bounds.Size;
			RectangleF frameToCenter = pdfView.Frame;

			if (frameToCenter.Width < boundsSize.Width)
				frameToCenter.X = (boundsSize.Width - frameToCenter.Width) / 2;
			else
				frameToCenter.X = 0;
    
			if (frameToCenter.Height < boundsSize.Height)
				frameToCenter.Y = (boundsSize.Height - frameToCenter.Height) / 2;
			else
				frameToCenter.Y = 0;

			// adjust the pdf and the bitmap views to the new, centered, frame
			pdfView.Frame = frameToCenter;
			backgroundImageView.Frame = frameToCenter;
    
			// this is important wrt high resolution screen to ensure 
			// CATiledLayer will ask proper tile sizes 
			pdfView.ContentScaleFactor = 1.0f;
		}
	}
}

