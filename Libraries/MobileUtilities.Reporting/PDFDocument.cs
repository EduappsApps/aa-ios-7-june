using System;
using MonoTouch.UIKit;
using System.Collections.Generic;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreFoundation;
using MonoTouch.Foundation;
using MobileUtilities.Reporting.Loader;
using System.Xml;
using MobileUtilities.XML;

// The PDF document code examples to create a PDF file using the utility is displayed below

// string fullPath = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), "NewbiePDF3.pdf");
// PDFDocument doc = new PDFDocument (fullPath, 800, 1200);
// Scene page1 = new Scene ();
// page1.Items.Add (new Text ("Hello World", "Helvetica", 18.5f, UIColor.Black, new RectangleF (20, 20, 120, 120)));
// doc.pages.Add (page1);
//
// Scene page2 = new Scene ();
// page2.Items.Add (new Line (420f, 420f, 480f, 480f, UIColor.Black, 5f));
// page2.Items.Add (new Image ("Strawberry.gif", new RectangleF (320f, 0f, 120f, 120f)));
// doc.pages.Add (page2);
//
// doc.HeaderFooter.Items.Add(new PageNumberText(doc, "Page {0} of {1}.", "Helvetica", 16.0f, UIColor.Blue, 
//		new RectangleF(700, 20, 100, 60)));
//
// doc.CreateFile ();

namespace MobileUtilities.Reporting
{
	public class PDFDocument
	{
		string _filename;
		RectangleF _bounds;
		float _width, _height;
		
		public float Width {
			get {
				return _width;		
			}
		}
		
		public float Height {
			get {
				return _height;
			}
		}
		
		public PDFDocument (string filename, float width, float height)
		{
			_filename = filename;
			_width = width;
			_height = height;
			_bounds = new RectangleF (0, 0, width, height);
			
			Header = new PdfScene (this);
			Footer = new PdfScene (this);
			Watermark = new PdfScene(this);
		}
		
		public List<Scene> pages = new List<Scene>();
		
		public PdfScene Header = null;
		public PdfScene Footer = null;
		public PdfScene Watermark = null;
		
		public int CurrentPage = 1;
		
		public int PageCount {
			get {
				return pages.Count;
			}
		}
		
		/// <summary>
		/// Details of the Current Y location, used for repeatible sections
		/// </summary>
		public float CurrentY;
		
		public event GetPDFInfo OnGetPDFInfo = null;
		
		public event GetColumnValue OnGetColumnValue = null;
		
		public PdfScene NewPage ()
		{
			PdfScene result = new PdfScene (this);
			pages.Add (result);
			return result;
		}
		
		public Dictionary<string, PdfScene> repeatibleSections = new Dictionary<string, PdfScene>();
		
		public PdfScene CreateRepeatibleSection (string sectionName)
		{
			PdfScene scene = new PdfScene (this);
			repeatibleSections [sectionName] = scene;
			return scene;
		}
		
		public PdfScene GetGroupBySection (string sectionName, out string groupByExpression)
		{
			string lookFor = sectionName + ".GroupBy.";
			
			foreach (string key in repeatibleSections.Keys) {
				if (key.StartsWith (lookFor)) {
					groupByExpression = "[Item." + key.Substring (lookFor.Length) + "]";
					return repeatibleSections [key];
				}
			}
			
			groupByExpression = "";
			return null;
		}
		
		public float GetHeaderHeight ()
		{
			float maxy = 0;
			
			foreach (DrawableObject o in Header.Items) {
				if (o.Top + o.Height > maxy)
					maxy = o.Top + o.Height;
			}
			
			return maxy;
		}
		
		public float GetFooterTop ()
		{
			float miny = _height;
			
			foreach (DrawableObject o in Footer.Items) {
				if (o.Top < miny)
					miny = o.Top;
			}
			
			return miny;
		}
		
		public void Scale (float factor)
		{
			List<Scene> scenes = new List<Scene> (pages);
			scenes.AddRange (new Scene[] { Header, Footer, Watermark });
			
			foreach (Scene s in scenes) {
				foreach (DrawableObject obj in s.Items) {
					obj.Left *= factor;
					obj.Top *= factor;
					obj.Width *= factor;
					obj.Height *= factor;
					if (obj is BaseLabel) {
						(obj as BaseLabel).fontSize *= factor;
					}
				}
			}
		}
		
		private ExpressionProcessor expressionParser = null;
		
		public void SetExpressionProcessor (ExpressionProcessor processor)
		{
			expressionParser = processor;
		}
		
		public string GetExpressionValue (string value)
		{
			if (expressionParser != null) {
				return expressionParser.EvaluateString (value.ToLower());
			} else 
				return "";
		}
		
		public void CreateFile ()
		{
			CurrentY = 0;
			
			Header.backgroundColor = UIColor.Clear;
			Header.DrawBackground = false;
			Footer.DrawBackground = false;
			Watermark.DrawBackground = false;
			
			CGPDFInfo info = new CGPDFInfo ();
			if (OnGetPDFInfo != null)
				OnGetPDFInfo (this, info);
			
			UIGraphics.BeginPDFContext (_filename, _bounds, info);
			
			// Draw each page
			CurrentPage = 0;
			
			foreach (PdfScene page in pages) {
				// See if we should add the page in first
				if (page.PageCondition.Length > 0) {
					string result = GetExpressionValue ("[datacheck." + page.PageCondition + "]");
					if (result == "0") {
						// We should not be displaying this page
						continue;
					}
				}
				
				MakeNewPage ();
				page.Draw (currentContext, _bounds);
			}
			
			UIGraphics.EndPDFContent ();
		}
		
		public CGContext currentContext = null;
		
		public void MakeNewPage ()
		{
			UIGraphics.BeginPDFPage ();
			currentContext = UIGraphics.GetCurrentContext ();
			
			CurrentPage ++;
			
			Watermark.Draw (currentContext, _bounds);
			Header.Draw (currentContext, _bounds);
			Footer.Draw (currentContext, _bounds);	
		}
		
		#region Loading the report from a file
		
		public void LoadFromFile (string reportFilename)
		{
			// Load the details of the report from the file, creating the items as needed
			pages.Clear ();

			XmlDocument doc = new XmlDocument ();
			doc.Load (reportFilename);
			
			Dictionary<string, ReportSectionEntry> sectionEntries = new Dictionary<string, ReportSectionEntry> ();
			
			// Load the section details so we know where to put the appropriate bits of information
			XmlElement flow = XMLHelper.GetChildNode (doc.DocumentElement, "display");
			foreach (XmlElement p in flow.GetElementsByTagName("page")) {
				ReportSectionEntry newEntry = new ReportSectionEntry ();
				
				newEntry.Name = XMLHelper.ReadXMLText (p, "PageName", "");
				newEntry.Position = XMLHelper.ReadXMLInt (p, "Position", 0);
				newEntry.IsRepeatible = XMLHelper.ReadXMLBool (p, "DoesRepeat", false);
				
				sectionEntries [newEntry.Name] = newEntry;
			}
			
			// Go through them all
			XmlElement allPages = XMLHelper.GetChildNode (doc.DocumentElement, "pages");
			
			foreach (XmlElement page in allPages.GetElementsByTagName("page")) {
				// Load the details of the page and create the tab
				string pageName = page.GetAttribute ("pageName");
				
				// Based on the details of the page, store the details in the appropriate page
				ReportSectionEntry currentEntry = sectionEntries [pageName];
				
				PdfScene currentScene;
				bool isRepeatible = false;
				
				if (currentEntry.Name.ToLower ().Equals ("footer")) {
					currentScene = Footer;	
				} else if (currentEntry.Name.ToLower ().Equals ("header")) {
					currentScene = Header;
				} else if (currentEntry.Name.ToLower ().Equals ("watermark")) {
					currentScene = Watermark;
				} else {
					// Determine if it is a page or a section
					if (currentEntry.IsRepeatible) {
						currentScene = CreateRepeatibleSection (currentEntry.Name);
						isRepeatible = true;
					} else {
						currentScene = NewPage ();
						currentScene.DrawBackground = false;
						currentScene.SetPageName(pageName);
					}
				}
				
				// Get the elements
				foreach (XmlElement item in page.GetElementsByTagName("item")) {
					int typeId = Convert.ToInt32 (item.GetAttribute ("typeID"));
					
					DrawableObject newItem;
					
					switch (typeId) {
					case PrintingIds.Label:
						newItem = new PrintLabel ();
						break;
					case PrintingIds.Ellipse:
						newItem = new EllipseShape ();
						break;
					case PrintingIds.Image:
						newItem = new PrintImage ();
						break;
					case PrintingIds.SystemLabel:
						newItem = new SystemLabel ();
						break;
					case PrintingIds.Rectangle:
						newItem = new RectangleShape ();
						break;
					case PrintingIds.HorizontalLine:
						newItem = new HorizontalLine ();
						break;
					case PrintingIds.VerticalLine:
						newItem = new VerticalLine ();
						break;
					case PrintingIds.TopLeftToBottomRightLine:
						newItem = new TopLeftToBottomRightLine ();
						break;
					case PrintingIds.TopRightToBottomLeftLine:
						newItem = new TopRightToBottomLeftLine ();
						break;
					case PrintingIds.DataLabel:
						newItem = new DataLabel ();
						break;
					case PrintingIds.RepeatibleSection:
						newItem = new RepeatingSection ();
						break;
					default:
						throw new ApplicationException ("The item added does not exist in the system.");
					}
				
					newItem.LoadFromXML (item);
					currentScene.Items.Add (newItem);
				}
				
				if (isRepeatible)
					currentScene.ConvertToOffsets ();
			}
			
			//Scale (2.0f);
		}
		
		#endregion
	
		public delegate void GetPDFInfo(object sender, CGPDFInfo info);
		
		#region Code to manage the data columns
		
		public class ReportColumnArgs
		{
			public string repeatibleSection;
			public string item;
			public string columnname;
		}
		
		public delegate void GetColumnValue(object sender, ReportColumnArgs e);
		
		#endregion 
		
		#region Support for the custom iteration over a datasource
		
		List<IGetReportValue> currentIterator = new List<IGetReportValue>();
		int currentIteratorPosition = 0;
		
		public class ReportIteratorArgs
		{
			public List<IGetReportValue> values;	
			public string DataSource;
			public string Arguments;
		}
		
		public string GetIteratorItemValue (string column)
		{
			return currentIterator[currentIteratorPosition].GetReportValue(column);
		}
		
		public delegate void GetReportIterator(object sender, ReportIteratorArgs e);
		
		public event GetReportIterator OnGetReportIterator = null;
		
		public void SetupItemDataSource (string datasource, string arguments)
		{
			currentIterator.Clear ();
			currentIteratorPosition = -1;
			
			ReportIteratorArgs args = new ReportIteratorArgs ();
			args.values = currentIterator;
			args.DataSource = datasource;
			args.Arguments = arguments;
				
			if (OnGetReportIterator != null)
				OnGetReportIterator (this, args);
		}
		
		public bool ReadDataSourceItem ()
		{
			int newPosition = currentIteratorPosition + 1;
			if (newPosition > currentIterator.Count - 1) {
				return false;
			}
			
			currentIteratorPosition = newPosition;
			return true;
		}
		
		#endregion
	}
}