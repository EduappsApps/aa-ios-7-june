using System;
using System.Drawing;
using System.Xml;
using MobileUtilities.XML;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;

namespace MobileUtilities.Reporting.Loader
{
	public abstract class BaseLabel : DrawableObject
	{
		protected UITextAlignment _horizontalAlignment = UITextAlignment.Left;
		protected UITextAlignment _verticalAlignment = UITextAlignment.Left;

		public UITextAlignment HorizontalAlignment 
		{
			get
			{
				return _horizontalAlignment;
			}
			set
			{
				if (_horizontalAlignment != value)
				{
					_horizontalAlignment = value;
					RedrawDesigner();
				}
			}
		}

		public UITextAlignment VerticalAlignment 
		{
			get
			{
				return _verticalAlignment;
			}
			set
			{
				if (_verticalAlignment != value)
				{
					_verticalAlignment = value;
					RedrawDesigner();
				}
			}
		}

		public override Size GetDefaultSize ()
		{
			return new Size (144, 16);
		}
		
		private UIColor _foreColor = UIColor.Black;
		public string fontName = "Helvetica";
		public float fontSize = 12.0f;
		public bool fontIsBold = false;
		public bool fontIsUnderlined = false;
		public bool fontIsItalic = false;
		public bool fontIsStrikeOut = false;
		
		public override void LoadFromXML (XmlElement e)
		{
			base.LoadFromXML (e);
			
			//TODO: Correct this
//			_horizontalAlignment = (UITextAlignment)XMLHelper.ReadXMLInt(e, "Horz", (int)_horizontalAlignment);
//			_verticalAlignment = (UITextAlignment)XMLHelper.ReadXMLInt(e, "Vert", (int)_verticalAlignment);

			fontName = XMLHelper.ReadXMLText (e, "FontName", "Segoe UI");
			fontSize = Convert.ToSingle (XMLHelper.ReadXMLText (e, "FontSize", "8"));
			fontIsBold = XMLHelper.ReadXMLBool (e, "FontBold", false);
			//fontIsUnderlined = XMLHelper.ReadXMLBool (e, "FontUnderline", false);
			fontIsItalic = XMLHelper.ReadXMLBool (e, "FontItalic", false);
			//fontIsStrikeOut = XMLHelper.ReadXMLBool (e, "FontStrikeOut", false);

			int a = Convert.ToInt32 (XMLHelper.ReadXMLText (e, "FontAlpha", "0"));
			int r = Convert.ToInt32 (XMLHelper.ReadXMLText (e, "FontRed", "0"));
			int g = Convert.ToInt32 (XMLHelper.ReadXMLText (e, "FontGreen", "0"));
			int b = Convert.ToInt32 (XMLHelper.ReadXMLText (e, "FontBlue", "0"));

			_foreColor = UIColor.FromRGBA(r, g, b, a);

//			FontStyle style = FontStyle.Regular;
//			if (fontIsBold)
//				style = style | FontStyle.Bold;
//			if (fontIsUnderlined)
//				style = style | FontStyle.Underline;
//			if (fontIsItalic)
//				style = style | FontStyle.Italic;
//			if (fontIsStrikeOut)
//				style = style | FontStyle.Strikeout;
			
			// TODO: Set the format
		}

		protected abstract string DoGetText();
		
		private string GetFontName ()
		{
			if (fontIsBold && fontIsItalic) {
				return "Helvetica-BoldOblique";
			} else if (fontIsBold) {
				return "Helvetica-Bold";
			} else if (fontIsItalic) {
				return "Helvetica-Oblique";
			} else
				return "Helvetica";
		}
		
		public override void Draw (CGContext context)
		{
			_foreColor.SetFill ();
			_foreColor.SetStroke ();
			NSString display = new NSString (DoGetText ());
			
			using (UIFont currentFont = UIFont.FromName (GetFontName (), fontSize)) {
				
				SizeF constraint = new SizeF(Width, 20000.0f);
				SizeF dimensions = display.StringSize (currentFont, constraint, UILineBreakMode.WordWrap);
				this.Height = dimensions.Height;
				display.DrawString (GetRectangle (), currentFont, UILineBreakMode.WordWrap);
			}
		}
	}
}
