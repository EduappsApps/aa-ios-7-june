using System;
using MobileUtilities.XML;
using MobileUtilities.Reporting.Loader;
using System.Xml;

namespace MobileUtilities.Reporting
{
	public class RepeatingSection : DrawableObject
	{
		public RepeatingSection ()
		{
		}
		
		string _sectionName = "";
		
		int _horizontalPadding = 8;
		int _verticalPadding = 8;
		int _columns = 1;
			
		string _datasource = "";
		string _argument = "";
		
		public string SectionName {
			get { return _sectionName; }
			set { _sectionName = value; }
		}
		
		public int Columns {
			get { return _columns; }
			set { _columns = value; }
		}

		
		public int HorizontalPadding {
			get { return _horizontalPadding; }
			set { _horizontalPadding = value; }	
		}
		
		public int VerticalPadding {
			get { return _verticalPadding; }
			set { _verticalPadding = value; }
		}
		
		public string DataSource {
			get {
				return _datasource;
			}
			set {
				_datasource = value;
			}
		}
		
		public string Argument {
			get {
				return _argument;
			}
			set {
				_argument = value;
			}
		}
		
		public override void LoadFromXML (XmlElement e)
		{
			base.LoadFromXML (e);
			
			SectionName = XMLHelper.ReadXMLText (e, "SectionName", "");
			Columns = XMLHelper.ReadXMLInt (e, "Columns", 1);
			HorizontalPadding = XMLHelper.ReadXMLInt (e, "HorizontalPadding", _horizontalPadding);
			VerticalPadding = XMLHelper.ReadXMLInt (e, "VerticalPadding", _verticalPadding);
			DataSource = XMLHelper.ReadXMLText (e, "DataSource", _datasource);
			Argument = XMLHelper.ReadXMLText(e, "Arguments", "");
		}
		
		public override void Draw (MonoTouch.CoreGraphics.CGContext context)
		{
			// Need to manage the appropriate looping. The PdfScene would really need to do this
		}
	}
}

