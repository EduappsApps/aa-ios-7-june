using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using MobileUtilities.XML;

namespace MobileUtilities.Reporting.Loader
{
	/// <summary>
	/// Base class for objects that can be drawn
	/// </summary>
	public abstract class DrawableObject : Item
	{
		private float CurrLeft, CurrTop, CurrWidth, CurrHeight;

		public float Left {
			get
			{
				return CurrLeft;
			}
			set
			{
				CurrLeft = value;
				RedrawDesigner();
			}
		}

		public float Top {
			get
			{
				return CurrTop;
			}
			set
			{
				CurrTop = value;
				RedrawDesigner();
			}
		}

		public float Width {
			get
			{
				return CurrWidth;
			}
			set
			{
				CurrWidth = value;
				RedrawDesigner();
			}
		}

		public float Height {
			get {
				return CurrHeight;
			}
			set {
				CurrHeight = value;
				RedrawDesigner ();
			}
		}
		
		public bool HasDimensions ()
		{
			return Width > 0 && Height > 0;
		}
		
		public void SetNoDimensions ()
		{
			Width = 0;
			Height = 0;
		}
		
		public void RestoreDimensions (RectangleF bounds)
		{
			Left = bounds.Left;
			Top = bounds.Top;
			Width = bounds.Width;
			Height = bounds.Height;
		}
			
		protected string DrawableObjectName = "";
		
		public string Name
		{
			get
			{
				return DrawableObjectName;
			}
			set
			{
				DrawableObjectName = value;
				RedrawDesigner();
			}
		}

        protected bool _locked = false;

        public bool Locked
        {
            get
            {
                return _locked;
            }
            set
            {
                if (_locked != value)
                {
                    _locked = value;
                    RedrawDesigner();
                }
            }
        }

		public bool IsNew = false;
		public bool Deleted = false;
		public bool Visible = true;
		public bool HasChanged = false;
		public bool ReadOnly = false;

		protected void RedrawDesigner() 
		{
//			if (Owner != null)
//			{
//				Owner.RedrawDesigner();
//			}
		}

		public RectangleF GetRectangle ()
		{
			return new RectangleF (Left, Top, Width, Height);
		}

        public virtual bool CanResize()
        {
            return true;
        }

        public virtual bool DrawResizePoints()
        {
            return true;
        }

		protected virtual bool ShouldDraw()
		{
			return (Width > 0 && Height > 0);
		}

		public RectangleF ClientRectangle()
		{
			return new RectangleF(Left, Top, Width, Height);
		}

		public void SetDefaultSize()
		{
			Size s = GetDefaultSize();
			this.Width = s.Width;
			this.Height = s.Height;
		}

		public virtual Size GetDefaultSize()
		{
			return new Size(16, 16);
		}

		public override void LoadFromXML(XmlElement e)
		{
			//Load the items in the details
			Left = Convert.ToInt32(ReadXMLText(e, "Left", Left.ToString()));
			Top = Convert.ToInt32(ReadXMLText(e, "Top", Top.ToString()));
			Height = Convert.ToInt32(ReadXMLText(e, "Height", Height.ToString()));
			Width = Convert.ToInt32(ReadXMLText(e, "Width", Width.ToString()));
			ReadOnly = Convert.ToBoolean(ReadXMLText(e, "ReadOnly", ReadOnly.ToString()));
			Visible = Convert.ToBoolean(ReadXMLText(e, "Visible", Visible.ToString()));
			Name = ReadXMLText(e, "Name", Name);
		}
	}

	public class DrawableObjectList : ArrayList
	{
		public void Add(DrawableObject o)
		{
			base.Add(o);
		}

		public void Remove(DrawableObject o)
		{
			base.Remove(o);
		}

		public new DrawableObject this[int index]
		{
			get
			{
                return (DrawableObject)(base[index]);
			}
			set
			{
            	base[index] = value;
			}
		}

		public DrawableObjectList GetObjectsOfType(Type t)
		{
			DrawableObjectList ReturnList = new DrawableObjectList();
			foreach (DrawableObject d in this)
			{
				if (d.GetType() == t || d.GetType().IsSubclassOf(t))
				{
					ReturnList.Add(d);
				}
			}
			return ReturnList;
		}

		public void Exchange(int index1, int index2)
		{
			DrawableObject item1 = this[index1];
			DrawableObject item2 = this[index2];

			this[index1] = item2;
			this[index2] = item1;
		}

		public override object Clone()
		{
			DrawableObjectList NewList = new DrawableObjectList();
			foreach (DrawableObject d in this)
				NewList.Add(d);
			return NewList;
		}

		public void CopyElementsTo(DrawableObjectList list)
		{
			list.Clear();
			foreach (DrawableObject d in this)
				list.Add(d);
		}
	}
}
