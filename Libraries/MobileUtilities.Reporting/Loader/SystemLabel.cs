using System;
using MobileUtilities.XML;

namespace MobileUtilities.Reporting.Loader
{
	public enum SystemLabelType
	{
		PageNumber = 1,
		PageCount = 2,
		Date,
		Time,
		DateTime
	}

	public class SystemLabel : BaseLabel
	{
		private SystemLabelType _labelType = SystemLabelType.PageNumber;

		public SystemLabelType Label
		{
			get
			{
				return _labelType;
			}
			set
			{
				if (_labelType != value)
				{
					_labelType = value;
					RedrawDesigner();
				}
			}
		}

		private string _postText = "";
		private string _preText = "";

		public string PostText
		{
			get
			{
				return _postText;
			}
			set
			{
				if (_postText != value)
				{
					_postText = value;
					RedrawDesigner();
				}
			}
		}

		public string PreText
		{
			get
			{
				return _preText;
			}
			set
			{
				if (_preText != value)
				{
					_preText = value;
					RedrawDesigner();
				}
			}
		}

		private string GetTypeText()
		{
			switch (_labelType)
			{
				case SystemLabelType.PageNumber:
					return "#Page No#";
				case SystemLabelType.PageCount:
					return "#Page Count#";
				case SystemLabelType.Date:
					return "#Date#";
				case SystemLabelType.Time:
					return "#Time#";
				case SystemLabelType.DateTime:
					return "#Date/Time#";
				default:
					return "";
			}
		}

		protected override string DoGetText()
		{
			return PreText + GetTypeText() + PostText;
		}

		public override void StoreInXML(System.Xml.XmlElement e)
		{
			base.StoreInXML(e);

			XMLHelper.StoreXMLText(e, "PreText", PreText);
			XMLHelper.StoreXMLText(e, "PostText", PostText);
			XMLHelper.StoreXMLText(e, "Type", Convert.ToInt32(_labelType).ToString());
		}

		public override void LoadFromXML(System.Xml.XmlElement e)
		{
			base.LoadFromXML(e);

			PreText = XMLHelper.ReadXMLText(e, "PreText", PreText);
			PostText = XMLHelper.ReadXMLText(e, "PostText", PostText);
			_labelType = (SystemLabelType)(XMLHelper.ReadXMLInt(e, "Type", 1));
		}
	}
}
