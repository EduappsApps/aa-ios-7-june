using System;
using System.Drawing;
using System.Xml;
using MobileUtilities.XML;

namespace MobileUtilities.Reporting.Loader
{
    public class PrintLabel : BaseLabel
    {
        protected string __Text = "";

        public string Text
        {
            get
            {
                return __Text;
            }
            set
            {
                if (!__Text.Equals(value))
                {
                    __Text = value;
                    RedrawDesigner();
                }
            }
        }

        protected override string DoGetText()
        {
            return __Text;
        }

        public override void LoadFromXML(XmlElement e)
        {
            base.LoadFromXML(e);

            __Text = ReadXMLText(e, "Text", "");
//            _horizontalAlignment = (StringAlignment)XMLHelper.ReadXMLInt(e, "horz", (int)StringAlignment.Near);
//            _verticalAlignment = (StringAlignment)XMLHelper.ReadXMLInt(e, "vert", (int)StringAlignment.Near);
        }

        public override void StoreInXML(XmlElement e)
        {
            base.StoreInXML(e);

            XMLHelper.StoreXMLText(e, "Text", __Text);
            XMLHelper.StoreXMLText(e, "horz", ((int)_horizontalAlignment).ToString());
            XMLHelper.StoreXMLText(e, "vert", ((int)_verticalAlignment).ToString());
        }
    }
}
