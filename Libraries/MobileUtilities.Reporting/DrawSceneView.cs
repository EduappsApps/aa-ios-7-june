using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;

namespace MobileUtilities.Reporting
{
	// Add items to the scene to allow for drawing. Access the DrawingView.Scene reference to work with the objects
	//
	// private void SetupScene ()
	// 	{
	// 		scene.Items.Add(new Text("Hello World", "Helvetica", 18.5f, UIColor.Black, new RectangleF(20, 20, 120, 120)));	
	//		scene.Items.Add(new Line(420f, 420f, 480f, 480f, UIColor.Black, 5f));
	//		scene.Items.Add(new Image("Strawberry.gif", new RectangleF(320f, 0f, 120f, 120f)));
	//	}
	
	[Register("DrawingView")]
	public class DrawingView : UIView
	{
		public DrawingView (IntPtr p) : base(p)
		{
		}
		
		public DrawingView(RectangleF b) : base(b)
		{
		}
		
		private Scene scene = new Scene();
		
		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
			
			using (CGContext context = UIGraphics.GetCurrentContext ())
			{
				context.SetAllowsAntialiasing (true);
				
				scene.Draw (context, this.Bounds);
			}
		}
	}
	
	public class DrawingViewController : UIViewController
	{
		public DrawingViewController () : base()
		
		{
		}
		
		public override void LoadView ()
		{
			this.View = new DrawingView(UIScreen.MainScreen.ApplicationFrame);
		}
	}
}

