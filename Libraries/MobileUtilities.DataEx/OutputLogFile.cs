using System;

namespace MobileUtilities.Data
{
	public class OutputLogFile
	{
		public static void Log (string msg)
		{
			string last = "";
			foreach (char c in Guid.NewGuid().ToString())
				if (Char.IsLetterOrDigit(c))
					last += c;
			last += ".log";
			
			string filename = System.IO.Path.Combine(
				Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
				last
				);
			System.IO.File.WriteAllText(filename, msg);
		}

		public static void Log(string msg, Exception e)
		{
			Log (msg + Environment.NewLine + e.Message + Environment.NewLine + e.StackTrace);
		}
	}
}

