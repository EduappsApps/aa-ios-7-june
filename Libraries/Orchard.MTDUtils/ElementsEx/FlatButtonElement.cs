using System;
using MonoTouch.Dialog;
using System.Drawing;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;

namespace Orchard.MTDUtils
{
	public class FlatButton : UIButton
	{
		bool pressed;
		public UIColor NormalColor, HighlightedColor, DisabledColor;

		public event Action<FlatButton> Tapped;

		public FlatButton (RectangleF frame) : base (frame)
		{
			NormalColor = new UIColor (0.00f, 0.00f, 0.91f, 1);
			HighlightedColor = new UIColor (0.00f, 0.45f, 0.91f, 1); //UIColor.Black;
			DisabledColor = UIColor.Gray;
			BackgroundColor = UIColor.Clear;
		}

		public override bool Enabled { 
			get {
				return base.Enabled;
			}
			set {
				base.Enabled = value;
				SetNeedsDisplay ();
			}
		}

		public override bool BeginTracking (UITouch uitouch, UIEvent uievent)
		{
			SetNeedsDisplay ();
			pressed = true;
			return base.BeginTracking (uitouch, uievent);
		}

		public override void EndTracking (UITouch uitouch, UIEvent uievent)
		{
			if (pressed && Enabled) {
				if (Tapped != null)
					Tapped (this);
			}
			pressed = false;
			SetNeedsDisplay ();
			base.EndTracking (uitouch, uievent);
		}

		public override bool ContinueTracking (UITouch uitouch, UIEvent uievent)
		{
			var touch = uievent.AllTouches.AnyObject as UITouch;
			if (Bounds.Contains (touch.LocationInView (this)))
				pressed = true;
			else
				pressed = false;
			return base.ContinueTracking (uitouch, uievent);
		}

		public override void Draw (RectangleF rect)
		{
			var context = UIGraphics.GetCurrentContext ();
			var bounds = Bounds;

			UIColor background = Enabled ? pressed ? HighlightedColor : NormalColor : DisabledColor;
			float alpha = 1;

			CGPath container;
			container = GraphicsUtil.MakeRoundedRectPath (bounds, 14);
			context.AddPath (container);
			context.Clip ();

			using (var cs = CGColorSpace.CreateDeviceRGB ()) {
				var topCenter = new PointF (bounds.GetMidX (), 0);
				var midCenter = new PointF (bounds.GetMidX (), bounds.GetMidY ());
				var bottomCenter = new PointF (bounds.GetMidX (), bounds.GetMaxY ());

				container = GraphicsUtil.MakeRoundedRectPath (bounds.Inset (1, 1), 8);
				context.AddPath (container);
				context.Clip ();

				var nb = bounds.Inset (4, 4);
				container = GraphicsUtil.MakeRoundedRectPath (nb, 8);
				context.AddPath (container);
				context.Clip ();

				background.SetFill ();
				context.FillRect (nb);
			}
		}
	}

	public class FlatButtonElement : UIViewElement
	{
		public FlatButton button;

		public FlatButtonElement(String caption, Action Handler)
			: base("", null, false)
		{
			RectangleF rec = new RectangleF(0, 0, 320, 50);
			UIView container = new UIView(rec);
			SizeF size = new NSString(caption).StringSize(UIFont.SystemFontOfSize(18), UIScreen.MainScreen.Bounds.Width, UILineBreakMode.TailTruncation);

			button = new FlatButton(new RectangleF(rec.Left + 8, rec.Top + 4, rec.Width - 16, rec.Height - 8)) 
			{
				Font = UIFont.SystemFontOfSize(18),
				NormalColor = new UIColor(0f, 0f, 1.0f, 1.0f)
			};

			button.Tapped += delegate
			{
				Handler.Invoke();
			};

			button.SetTitle(caption, UIControlState.Normal);
			container.AddSubview(button);
			View = container;
		}

		public override UITableViewCell GetCell (UITableView tv)
		{
			var result = base.GetCell (tv);
			result.SelectionStyle = UITableViewCellSelectionStyle.None;
			return result;
		}
	}
}

