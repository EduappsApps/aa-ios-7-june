using System;
using MonoTouch.Dialog;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;

namespace Orchard.MTDUtils
{
	public class SizeableEntryElement : EntryElement, IElementSizing
	{
		public SizeableEntryElement (string caption, string placeholder, string value, bool isPassword) : base(caption, placeholder, value, isPassword)
		{
		}

		public SizeableEntryElement (string caption, string placeholder, string value) : base(caption, placeholder, value)
		{
		}

		private static readonly NSString passwordKey = new NSString ("SizableEntryElement+Password");

		private static NSString entryKey = new NSString ("SizableEntryElement");

		private static readonly NSString cellkey = new NSString ("SizableEntryElement");

		protected override MonoTouch.Foundation.NSString CellKey {
			get {
				return entryKey;
			}
		}
		/// <summary>
		/// Number of Rows in the Edit Control
		/// </summary>
		public int RowCount = 1;

		/// <summary>
		/// The maximum width of the caption part of the entry 
		/// </summary>
		public float MaxCaptionWidth = 250f;

		/// <summary>
		/// The distance allocated for additional rows in the edit control
		/// </summary>
		public float PerRowBuffer = 8f;

		public UIFont CaptionFont = UIFont.SystemFontOfSize(17f);

		public UIFont EditFont = UIFont.SystemFontOfSize(17f);

		private float GetEditHeight()
		{
			return RowCount * GetSizeOfText (EditFont, "Aq").Height + PerRowBuffer * (RowCount - 1);
		}

		protected override MonoTouch.UIKit.UITextField CreateTextField (System.Drawing.RectangleF frame)
		{
			// Get the current cell
			var cell = GetContainerTableView ().CellAt (this.IndexPath);

			var height = RowCount * GetSizeOfText (EditFont, "Aq").Height + PerRowBuffer * (RowCount - 1);
			var width = _tableView.Frame.Width - MaxCaptionWidth - 24f;

			var rect = new RectangleF (MaxCaptionWidth + 16f, 4, width, height);
			var result = base.CreateTextField (rect);

			result.Font = EditFont;

			result.EnablesReturnKeyAutomatically = RowCount > 1;
			result.BorderStyle = UITextBorderStyle.RoundedRect;
			result.TextAlignment = UITextAlignment.Left;
			result.VerticalAlignment = UIControlContentVerticalAlignment.Top;

			return result;
		}

		UITableView _tableView;

		public override UITableViewCell GetCell (UITableView tv)
		{
			_tableView = tv;

			var result = base.GetCell (tv);

			result.TextLabel.Font = this.CaptionFont;

			// Update the dimensions of the Cell
			var size = GetSizeOfText (CaptionFont, this.Caption, MaxCaptionWidth);

			result.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
			result.TextLabel.Frame = new RectangleF (result.TextLabel.Bounds.Location, size);
			result.TextLabel.TextAlignment = UITextAlignment.Left;
			//result.TextLabel.VerticalAlignment = UIControlContentVerticalAlignment.Top;
			result.TextLabel.Lines = 0;
			result.TextLabel.SetNeedsLayout ();

			return result;
		}

		public float GetHeight (MonoTouch.UIKit.UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			_tableView = tableView;

			var editHeight = GetEditHeight () + 8f;
			var captionHeight = GetSizeOfText (CaptionFont, this.Caption, MaxCaptionWidth).Height + 8f;

			return Math.Max (editHeight, captionHeight);
		}

		public SizeF GetSizeOfText (UIFont font, string text, float maxWidth = 300f)
		{
			return _tableView.StringSize (text, font, maxWidth, UILineBreakMode.WordWrap);
		}
	}
}

