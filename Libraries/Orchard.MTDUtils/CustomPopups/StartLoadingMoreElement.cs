﻿using System;
using MonoTouch.Dialog;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;

namespace Orchard.MTDUtils
{
	public class WatchedUIActivityIndicatorView: UIActivityIndicatorView
	{
		public WatchedUIActivityIndicatorView(UIActivityIndicatorViewStyle style) : base(style)
		{

		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);

			// Fire the event
			if (DidDraw != null)
				DidDraw ();
		}

		public event Action DidDraw = null;
	}

	/// <summary>
	/// Based on the Activity Element but is used to load more items
	/// </summary>
	public class StartLoadingMoreElement : UIViewElement, IElementSizing
	{
		public bool Animating {
			get {
				return ((UIActivityIndicatorView)this.View).IsAnimating;
			}
			set {
				UIActivityIndicatorView uIActivityIndicatorView = this.View as UIActivityIndicatorView;
				if (value) {
					uIActivityIndicatorView.StartAnimating ();
				}
				else {
					uIActivityIndicatorView.StopAnimating ();
				}
			}
		}

		bool isDownloading = false;

		void StartTheNextDownload ()
		{
			if (isDownloading)
				return;

			isDownloading = true;

			if (OnStartNextDownload != null)
				OnStartNextDownload ();
		}

		public event Action OnStartNextDownload = null;

		NSTimer _timer;

		public StartLoadingMoreElement () : base (string.Empty, new WatchedUIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray), false)
		{
			var sbounds = UIScreen.MainScreen.Bounds;                        
			var uia = View as WatchedUIActivityIndicatorView;
			uia.StartAnimating ();
			uia.BackgroundColor = UIColor.Clear;

			var vbounds = View.Bounds;
			View.Frame = new RectangleF ((sbounds.Width-vbounds.Width) / 2, 4, vbounds.Width, vbounds.Height + 0);
			View.AutoresizingMask = UIViewAutoresizing.FlexibleLeftMargin | UIViewAutoresizing.FlexibleRightMargin;

			uia.DidDraw += delegate() {
				StartTheNextDownload ();
			};
		}

		float IElementSizing.GetHeight (UITableView tableView, NSIndexPath indexPath)
		{
			return base.GetHeight (tableView, indexPath) + 8;
		}
	}
}

