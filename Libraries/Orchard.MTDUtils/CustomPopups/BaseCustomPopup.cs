using System;
using MonoTouch.UIKit;

namespace Orchard.MTDUtils
{
	/// <summary>
	/// A base class used for pop-up values where a callback can be used to get the apprpriate information
	/// </summary>
	public abstract class BaseCustomPopup : UIViewController
	{
		string _InitialValue;
		public BaseCustomPopup (string InitialValue) : base()
		{
			_InitialValue = InitialValue;
		}

		public abstract UIView DoGetView();

		public UIView GetView()
		{
			if (_control == null) {
				_control = DoGetView ();
				Title = GetTitle ();
			}

			return _control;
		}

		protected UIView _control;

		protected abstract string GetValue();

		protected virtual string GetTitle()
		{
			return "Select";
		}

		public event Action<string> OnSetValue = null;
		public event Action OnClearValue = null;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationItem.RightBarButtonItem = new UIBarButtonItem (UIBarButtonSystemItem.Done, (sender, e) => {
				// Set the value and close the box
				if (OnSetValue != null)
					OnSetValue(GetValue());
			});

			NavigationItem.LeftBarButtonItem = new UIBarButtonItem ("Clear", UIBarButtonItemStyle.Plain, (sender, e) => {
				// Set the value and close the box
				if (OnClearValue != null)
					OnClearValue();
			});

			Add (GetView());
		}
	}
}

