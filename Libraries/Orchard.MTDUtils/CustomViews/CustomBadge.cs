using System;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.CoreGraphics;

namespace Orchard.MTDUtils
{
	// Monotouch port of https://github.com/ckteebe/CustomBadge/tree/master/Classes
	public class CustomBadge : UIView
	{
		public CustomBadge (RectangleF rect, string content, float scale, bool shines) : base(rect)
		{
			BackgroundColor = UIColor.Clear;
			ContentScaleFactor = UIScreen.MainScreen.Scale;
			BadgeText = content;
			TextColor = UIColor.White;
			FrameBadge = true;
			FrameColor = UIColor.White;
			InsetColor = UIColor.Red;
			CornerRoundness = 0.4f;
			ScaleFactor = scale;
			Shining = shines;

			AutosizeBadge ();
		}

		public string BadgeText;
		public UIColor TextColor;
		public UIColor InsetColor;
		public UIColor FrameColor;
		public float CornerRoundness;
		public float ScaleFactor;
		public bool Shining;
		public bool FrameBadge;

		private float mPi = Convert.ToSingle (Math.PI);

		void DrawRoundedRectWithContext (CGContext context, RectangleF rect)
		{
			context.SaveState ();

			float radius = rect.Bottom * CornerRoundness;
			float puffer = rect.Bottom * 0.10f;
			float maxX = rect.Right - puffer;
			float maxY = rect.Bottom - puffer;
			float minX = rect.Left + puffer;
			float minY = rect.Top + puffer;

			context.BeginPath ();
			context.SetFillColorWithColor(InsetColor.CGColor);
			context.AddArc(maxX-radius, minY+radius, radius, mPi +(mPi/2), 0f, false);
			context.AddArc(maxX-radius, maxY-radius, radius, 0, mPi/2, false);
			context.AddArc(minX+radius, maxY-radius, radius, mPi/2, mPi, false);
			context.AddArc(minX+radius, minY+radius, radius, mPi, mPi+mPi/2, false);
			context.SetShadowWithColor(new SizeF(1.0f, 1.0f), 3f, UIColor.Black.CGColor);
			context.FillPath();

			context.RestoreState ();
		}

		void DrawShineWithContext (CGContext context, RectangleF rect)
		{
			context.SaveState();

			float radius = rect.Bottom * CornerRoundness;
			float puffer = rect.Bottom * 0.10f;
			float maxX = rect.Right - puffer;
			float maxY = rect.Bottom - puffer;
			float minX = rect.Left + puffer;
			float minY = rect.Top + puffer;

			context.BeginPath ();
			context.AddArc(maxX-radius, minY+radius, radius, mPi+(mPi/2), 0, false);
			context.AddArc(maxX-radius, maxY-radius, radius, 0, mPi/2, false);
			context.AddArc(minX+radius, maxY-radius, radius, mPi/2, mPi, false);
			context.AddArc(minX+radius, minY+radius, radius, mPi, mPi+mPi/2, false);
			context.Clip();

			float[] locations = new float[] { 0.0f, 0.4f };
			float[] components = new float[ ] {  0.92f, 0.92f, 0.92f, 1.0f, 0.82f, 0.82f, 0.82f, 0.4f };

			using (CGColorSpace cspace = CGColorSpace.CreateDeviceRGB()) {
				using (CGGradient gradient = new CGGradient(cspace, components, locations)) {
					PointF sPoint = new PointF (0f, 0f);
					PointF ePoint = new PointF (0f, maxY);

					CGGradientDrawingOptions options = new CGGradientDrawingOptions ();
					context.DrawLinearGradient(gradient, sPoint, ePoint, options);
				}
			}
		}

		void DrawFrameWithContext (CGContext context, RectangleF rect)
		{
			float radius = rect.Bottom * CornerRoundness;
			float puffer = rect.Bottom * 0.10f;

			float maxX = rect.Right - puffer;
			float maxY = rect.Bottom - puffer;
			float minX = rect.Left + puffer;
			float minY = rect.Top + puffer;

			context.BeginPath ();
			float lineSize = 2;
			if(ScaleFactor > 1) {
				lineSize += ScaleFactor * 0.25f;
			}

			context.SetLineWidth(lineSize);
			context.SetStrokeColorWithColor(FrameColor.CGColor);
			context.AddArc(maxX-radius, minY+radius, radius, mPi+(mPi/2), 0, false);
			context.AddArc(maxX-radius, maxY-radius, radius, 0, mPi/2, false);
			context.AddArc(minX+radius, maxY-radius, radius, mPi/2, mPi, false);
			context.AddArc(minX+radius, minY+radius, radius, mPi, mPi+mPi/2, false);

			context.ClosePath();
			context.StrokePath();
		}

		public override void Draw (System.Drawing.RectangleF rect)
		{
			base.Draw (rect);

			var context = UIGraphics.GetCurrentContext ();

			DrawRoundedRectWithContext(context, rect);

			if (Shining) {
				DrawShineWithContext (context, rect);
			}

			if (FrameBadge)  {
				DrawFrameWithContext(context, rect);
			}

			if (BadgeText.Length > 0) {
				TextColor.SetColor ();
				float sizeOfFont = 13.5f * ScaleFactor;
				if (BadgeText.Length < 2) 
				{
					sizeOfFont += sizeOfFont * 0.20f;
				}
				UIFont textFont = UIFont.BoldSystemFontOfSize(sizeOfFont);

				var bt = new NSString (BadgeText);
				SizeF textSize = bt.StringSize(textFont);
				bt.DrawString(new PointF((rect.Size.Width / 2 - textSize.Width / 2), 
				                         (rect.Size.Height / 2 - textSize.Height / 2)), textFont);
			}
		}

		void AutosizeBadge ()
		{
			SizeF retValue;
			float rectWidth, rectHeight;
			SizeF stringSize = new NSString (BadgeText).StringSize (UIFont.BoldSystemFontOfSize (12f));
			float flexSpace;
			if (BadgeText.Length >= 2) 
			{
				flexSpace = BadgeText.Length;
				rectWidth = 25 + (stringSize.Width + flexSpace); 
				rectHeight = 25;
				retValue = new SizeF(rectWidth * ScaleFactor, rectHeight * ScaleFactor);
			} else {
				retValue = new SizeF(25 * ScaleFactor, 25 * ScaleFactor);
			}
			this.Frame = new RectangleF(this.Frame.Location.X, this.Frame.Location.Y, retValue.Width, retValue.Height);

			SetNeedsDisplay ();
		}
	}
}

