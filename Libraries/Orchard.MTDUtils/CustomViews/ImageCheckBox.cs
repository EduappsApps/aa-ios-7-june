using System;
using MonoTouch.UIKit;
using System.Drawing;

namespace Orchard.MTDUtils
{
	public class ImageCheckBox : UIImageView
	{
		bool _checked = false;
		UIImage _checkedImage;
		UIImage _uncheckedImage;

		public bool Checked
		{
			get
			{
				return _checked;
			}
			set
			{
				if (_checked != value)
				{
					_checked = value;
					Image = _checked ? _checkedImage : _uncheckedImage;

					SetNeedsDisplay ();
				}
			}
		}

		public ImageCheckBox (RectangleF frame, UIImage checkedImage, UIImage uncheckedImage) : base(frame)
		{
			UserInteractionEnabled = true;
			BackgroundColor = UIColor.Clear;
			_checkedImage = checkedImage;
			_uncheckedImage = uncheckedImage;
			Image = _uncheckedImage;
		}

		public override void TouchesBegan (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

			ToggleState ();
		}

		public void ToggleState()
		{
			Checked = !Checked;
		}
	}

	public class TappableLabel : UILabel
	{
		public TappableLabel (RectangleF frame) : base(frame)
		{
			UserInteractionEnabled = true;
			BackgroundColor = UIColor.Clear;
		}

		public event Action Tapped = null;

		public override void TouchesBegan (MonoTouch.Foundation.NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);

			if (Tapped != null)
				Tapped ();
		}
	}

}

