using System;
using System.Xml;

namespace MobileUtilities.XML
{
	public abstract class XmlStoredItem
	{
		public XmlStoredItem()
		{
		}
	
		public abstract void StoreInXML(XmlElement e);
		public abstract void LoadFromXML(XmlElement e);

		protected void StoreXMLText(XmlElement e, string ElementName, string ElementValue)
		{
			XMLHelper.StoreXMLText(e, ElementName, ElementValue);
		}
	
		protected string ReadXMLText(XmlElement parent, string ElementName, string DefaultValue)
		{
			return XMLHelper.ReadXMLText(parent, ElementName, DefaultValue);
		}
		
	}
}
