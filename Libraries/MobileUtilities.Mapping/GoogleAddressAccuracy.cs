using System;

namespace MobileUtilities.Mapping.Google
{
	public enum GoogleAddressAccuracy
	{
		UnknownLocation,
		CountryLevel,
		RegionLevel,
		SubRegionLevel,
		TownLevel,
		ZipCodeLevel,
		StreetLevel,
		IntersectionLevel,
		AddressLevel,
		PremiseLevel
	}
}