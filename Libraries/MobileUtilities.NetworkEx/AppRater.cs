using System;

#if __UNIFIED__
using Foundation;
using UIKit;
#else
using MonoTouch.Foundation;
using MonoTouch.UIKit;
#endif



namespace MobileUtilities.Network
{
	public static class AppRater
	{
		static NSUserDefaults settings = new NSUserDefaults ("appRater");
		public static int RunCountNeeded = 5;
		public static int DaysInstalledCountNeeded = 3;
		static string url = ""; // Should be something like the following @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=" + AppId;
		static string AppId = "";

		public static void AppLaunched (string appId)
		{
			AppId = appId;
			var version = NSBundle.MainBundle.InfoDictionary.ObjectForKey (new NSString ("CFBundleVersion")).ToString ();
			if (settings.StringForKey ("lastInstalledVersion") != version) {
				ResetWarningIndicators ();
				settings.SetString (version, "lastInstalledVersion");
				settings.Synchronize ();
			}
			TryToRate ();
			RunCount += 1;
		}

		static void ResetWarningIndicators ()
		{
			RunCount = 0;
			DateVersionInstalled = DateTime.UtcNow;
			ShouldRateThisVersion = true;
			DidRate = false;
		}

		static nint RunCount {
			get{
				return settings.IntForKey ("runCount");}
			set {
				settings.SetInt (value, "runCount");
				settings.Synchronize ();
			}
		}

		static DateTime DateVersionInstalled {
			get{ return DateTime.Parse (settings.StringForKey ("dateInstalled"));}
			set {
				settings.SetString (value.ToString (), "dateInstalled");
				settings.Synchronize ();
			}
		}

		static bool ShouldRateThisVersion {
			get{ return settings.BoolForKey ("shouldRate");}
			set{ settings.SetBool (value, "shouldRate");}
		}

		static void TryToRate ()
		{
			if (ShouldRate ())
				Rate ();
		}

		public static bool DidRate {
			get{ return settings.BoolForKey ("didRateVersion");}
			set {
				settings.SetBool (value, "didRateVersion");
				settings.Synchronize ();
			}
		}

		public static void Rate ()
		{
			var version = NSBundle.MainBundle.InfoDictionary.ObjectForKey (new NSString ("CFBundleVersion"));
		
			var name = NSBundle.MainBundle.InfoDictionary.ObjectForKey (new NSString ("CFBundleName")).ToString ();
			var alert = new UIAlertView ("Rate " + name, "If you enjoyed using " + name + ". Will you please take a moment to rate it? Thanks for your support", null, "No, Thanks", "Rate " + name, "Remind Me Later");
			alert.Clicked += delegate(object sender, UIButtonEventArgs e) {
				if (e.ButtonIndex == 0)
					ShouldRateThisVersion = false;
				else if (e.ButtonIndex == 1) {
					DidRate = true;
					UIApplication.SharedApplication.OpenUrl (new NSUrl (url));
				} else if (e.ButtonIndex == 2)
					ResetWarningIndicators ();
			};
			alert.Show ();
		}

		static bool ShouldRate ()
		{
			if (DidRate)
				return false;
			if (!ShouldRateThisVersion)
				return false;
			if ((DateTime.UtcNow - DateVersionInstalled).Days < DaysInstalledCountNeeded)
				return false;
			if (RunCount < RunCountNeeded)
				return false;
			return true;
		}
	}
}
