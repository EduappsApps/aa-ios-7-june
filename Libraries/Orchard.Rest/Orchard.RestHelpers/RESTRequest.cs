﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace Orchard.Utils.REST
{
    public class RESTRequest
    {
        public Dictionary<string, string> parameters = new Dictionary<string, string>();
        public string Method;
        public HttpContext context;

        public string this[string name]
        {
            get
            {
                return parameters[name.ToLower()];
            }
        }

        public T GetClientPOSTObject<T>(bool unEncode = false)
        {
            var rdr = new StreamReader(context.Request.InputStream);
            string content = rdr.ReadToEnd();
            if (unEncode)
                content = HttpUtility.UrlDecode(content);
            return JsonHelper.JsonDeserialize<T>(content);
        }

        public void UpdateParameters()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (string k in parameters.Keys)
                result[k] = HttpContext.Current.Server.UrlDecode(parameters[k]);
            parameters = result;
        }
    }
}