using System;
using System.Runtime.InteropServices;
using MonoTouch.UIKit;
using System.IO;
using System.Drawing;
using MonoTouch.Foundation;

namespace MobileUtilities.Multimedia
{
	/// <summary>
	/// Device hardware. Built from information from a variety of sources
	/// http://stackoverflow.com/questions/1108859/detect-the-specific-iphone-ipod-touch-model and 
	/// http://snippets.dzone.com/posts/show/9963
	/// </summary>
	public class DeviceHardware
	{
		public const string HardwareProperty = "hw.machine";

		public enum HardwareVersion
		{
			iPhone1G,
			iPhone2G,
			iPhone3G,
			iPhone3GS,
			iPhone4,
			iPad1,
			iPad2,
			iPod1G,
			iPod2G,
			iPod3G,
			Simulator,
			Unknown
		}

		public static float GetOSVersion()
		{
			string version = UIDevice.CurrentDevice.SystemVersion;

			string result = "";
			bool foundDecimal = false;
			foreach (char c in version)
			{
				if (Char.IsNumber(c))
					result += c;
				if (c == '.')
				{
					if (!foundDecimal)
					{
						result += ".";
						foundDecimal = true;
					}
				}
			}

			return Convert.ToSingle(result);
		}

		public static bool IsOnIPad ()
		{
			return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad; 
		}
		
		public static bool IsOnIPhone()
		{
			return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; 	
		}

		#region Detecting the device on an iPad mini

//		public static bool IsOniPadMini()
//		{
//			if (!IsOnIPad())
//				return false;
//
//			RectangleF screenBounds = UIScreen.MainScreen.Bounds;
//			float screenScale = UIScreen.MainScreen.Scale;
//
//			SizeF screenSize = new SizeF(screenBounds.Size.Width * screenScale, screenBounds.Size.Height * screenScale);
//
//			retu
//
//		}

		#endregion

		#region Support for iPhone 5 larger display

		public static bool IsTall()
		{
			return UIDevice.CurrentDevice.UserInterfaceIdiom 
				== UIUserInterfaceIdiom.Phone 
					&& UIScreen.MainScreen.Bounds.Height * UIScreen.MainScreen.Scale >= 1136;
		}

		private static string tallMagic = "-568h@2x";


		public static UIImage DeviceImage(string path)
		{
			if (IsTall())
			{
				var imagePath = Path.GetDirectoryName(path.ToString());
				var imageFile = Path.GetFileNameWithoutExtension(path.ToString());
				var imageExt = Path.GetExtension(path.ToString());

				imageFile = imageFile + tallMagic + imageExt;
				return UIImage.FromFile(Path.Combine(imagePath,imageFile));
			}
			else 
			{
				return UIImage.FromBundle(path.ToString());
			}
		}

		public static string DeviceImagePath(string path)
		{
			if (IsTall())
			{
				var imagePath = Path.GetDirectoryName(path.ToString());
				var imageFile = Path.GetFileNameWithoutExtension(path.ToString());
				var imageExt = Path.GetExtension(path.ToString());
				
				imageFile = imageFile + tallMagic + imageExt;
				return Path.Combine(imagePath,imageFile);
			}
			else 
			{
				return path.ToString();
			}
		}
		#endregion

		[DllImport(MonoTouch.Constants.SystemLibrary)]
		internal static extern int sysctlbyname ([MarshalAs(UnmanagedType.LPStr)] string property, // name of the property
	                                        IntPtr output, // output
	                                        IntPtr oldLen, // IntPtr.Zero
	                                        IntPtr newp, // IntPtr.Zero
	                                        uint newlen // 0
	                                       );
		
		public static string GetHardwareString ()
		{
			// get the length of the string that will be returned
			var pLen = Marshal.AllocHGlobal (sizeof(int));
			sysctlbyname (DeviceHardware.HardwareProperty, IntPtr.Zero, pLen, IntPtr.Zero, 0);

			var length = Marshal.ReadInt32 (pLen);

			// check to see if we got a length
			if (length == 0) {
				Marshal.FreeHGlobal (pLen);
				return "Unknown";
			}

			// get the hardware string
			var pStr = Marshal.AllocHGlobal (length);
			sysctlbyname (DeviceHardware.HardwareProperty, pStr, pLen, IntPtr.Zero, 0);

			// convert the native string into a C# string
			var hardwareStr = Marshal.PtrToStringAnsi (pStr);
			
			Marshal.FreeHGlobal (pLen);
			Marshal.FreeHGlobal (pStr);
			
			return hardwareStr;
		}
		
		public static HardwareVersion Version {
			get {
				// determine which hardware we are running
				switch (GetHardwareString ()) {
				// iPhone Devices
				case "iPhone1,1":
					return HardwareVersion.iPhone1G;
				case "iPhone1,2":
					return HardwareVersion.iPhone2G;
				case "iPhone2,1":
					return HardwareVersion.iPhone3G;
				case "iPhone3,1":
					return HardwareVersion.iPhone4;
					
				// iPad devices
				case "iPad1,1":
					return HardwareVersion.iPad1;
					
				// iPod devices
				case "iPod1,1":
					return HardwareVersion.iPod1G;
				case "iPod2,1":
					return HardwareVersion.iPod2G;
				case "iPod3,1":
					return HardwareVersion.iPod3G;
					
				// Simulator
				case "i386":
					return HardwareVersion.Simulator;
					break;
				case "Unknown":
				default:
					return HardwareVersion.Unknown;
				}
			}
		}

		public static ulong GetAppFolderSize()
		{
			return NSFileManager.DefaultManager.GetFileSystemAttributes (
				Environment.GetFolderPath (Environment.SpecialFolder.Personal)).Size;
		}

		/// <summary>
		/// Returns the amount of disk space available to you
		/// </summary>
		/// <returns>The free space.</returns>
		public static ulong GetFreeSpace()
		{
			return NSFileManager.DefaultManager.GetFileSystemAttributes (
				Environment.GetFolderPath (Environment.SpecialFolder.Personal)).FreeSize;
		}
	}
}

