
using System;
using System.Collections.Generic;
using System.Linq;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.Drawing;
using MonoTouch.ObjCRuntime;

namespace MobileUtilities.Multimedia
{
	public partial class AudioRecordViewController : UIViewController
	{
		#region Constructors

		// The IntPtr and initWithCoder constructors are required for items that need 
		// to be able to be created from a xib rather than from managed code

		public AudioRecordViewController (IntPtr handle) : base(handle)
		{
			Initialize ();
		}

		[Export("initWithCoder:")]
		public AudioRecordViewController (NSCoder coder) : base(coder)
		{
			Initialize ();
		}

		public AudioRecordViewController () : base("AudioRecordViewController", null)
		{
			Initialize ();
		}

		void Initialize ()
		{
		}
		
		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			
			StopRecording ();
			DoRecordingDone ();
			this.DismissModalViewControllerAnimated (true);
		}
		
		private void DoRecordingDone ()
		{
			if (OnRecordingDone != null)
				OnRecordingDone (this, EventArgs.Empty);
		}
		
		public event EventHandler OnRecordingDone = null;
		
		private void StopRecording ()
		{
			if (IsRecording) {
				IsRecording = false;
				recording.Stop ();
			}
		}
		
		private AudioRecording recording = null;
		private bool IsRecording = false;
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		
			Title = "Recording";
			
			StartTheTimer ();
			
			try {
				
				recording = new AudioRecording ();
				recording.StartRecording (Filename);
				IsRecording = true;
			
				NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, 
				new EventHandler (CancelRecording));
			} catch {
				// Show a message that the recording could not occur
				DidFailToRecord = true;
			}
			
			if (DidFailToRecord) {
				NSTimer.CreateTimer (TimeSpan.FromMilliseconds (200), ShutItDown);
			}
		}
		
		public void ShutItDown ()
		{
			DismissModalViewControllerAnimated (true);	
		}
		
		public bool DidFailToRecord = false;
		
		public string Filename = "";
		
		public void CreateFileNameForFolder (string folder)
		{
			string file = "";
			foreach (char c in Guid.NewGuid().ToString())
				if (Char.IsLetterOrDigit (c))
					file += c;
			file += ".aif";
			
			Filename = System.IO.Path.Combine (folder, file);
		}
			
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			
			StopRecording ();
		}
		
		public void CancelRecording (object sender, EventArgs e)
		{
			StopRecording ();
			
			if (System.IO.File.Exists (Filename)) {
				System.IO.File.Delete (Filename);
			}
			
			if (timer != null) {
				timer.Invalidate ();
				timer = null;
			}
			
			DismissModalViewControllerAnimated (true);
		}
		
		private void StartTheTimer ()
		{
			timer = NSTimer.CreateScheduledTimer (1, this, new Selector ("OneSecondTimer"), null, true);
		}
		
		NSTimer timer = null;
		int secondsRecorded = 0;
		
		UILabel writeToLabel = null;
		
		[Export("OneSecondTimer")]
		void OneSecondTimer (NSTimer timer)
		{
			secondsRecorded ++;
			TimeSpan ts = new TimeSpan (0, 0, 0, secondsRecorded);
			if (mainLabel != null)
				mainLabel.Text = ts.ToString ();
		}

		#endregion
	}
}

