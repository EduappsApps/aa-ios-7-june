using System;
using MonoTouch.UIKit;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.CoreGraphics;
using System.Drawing;

namespace MobileUtilities.Multimedia
{
	public class AudioRecordDisplayViewController : UIViewController
	{
		public AudioRecordDisplayViewController () : base()
		{
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			
			StopRecording ();
			DoRecordingDone ();
			this.DismissModalViewControllerAnimated (true);
		}
		
		private void DoRecordingDone ()
		{
			if (OnRecordingDone != null)
				OnRecordingDone (this, EventArgs.Empty);
		}
		
		public event EventHandler OnRecordingDone = null;
		
		private void StopRecording ()
		{
			if (IsRecording) {
				IsRecording = false;
				recording.Stop ();
			}
		}
		
		private AudioRecording recording = null;
		private bool IsRecording = false;

		AudioRecordDisplayView _displayView = null;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			Title = "Recording";

			_displayView = new AudioRecordDisplayView();

			_displayView.Center = new PointF(this.View.Bounds.Left + this.View.Bounds.Width / 2, 
			                                 this.View.Bounds.Top + this.View.Bounds.Height / 2);
			_displayView.Bounds = this.View.Bounds;
			this.Add (_displayView);

			StartTheTimer ();
			
			try {
				
				recording = new AudioRecording ();
				recording.StartRecording (Filename);
				IsRecording = true;
				
				NavigationItem.RightBarButtonItem = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain, 
				                                                         new EventHandler (CancelRecording));
			} catch {
				// Show a message that the recording could not occur
				DidFailToRecord = true;
			}
			
			if (DidFailToRecord) {
				NSTimer.CreateTimer (TimeSpan.FromMilliseconds (200), ShutItDown);
			}
		}
		
		public void ShutItDown ()
		{
			DismissModalViewControllerAnimated (true);	
		}
		
		public bool DidFailToRecord = false;
		
		public string Filename = "";
		
		public void CreateFileNameForFolder (string folder)
		{
			string file = "";
			foreach (char c in Guid.NewGuid().ToString())
				if (Char.IsLetterOrDigit (c))
					file += c;
			file += ".aif";
			
			Filename = System.IO.Path.Combine (folder, file);
		}
		
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			
			StopRecording ();
		}
		
		public void CancelRecording (object sender, EventArgs e)
		{
			StopRecording ();
			
			if (System.IO.File.Exists (Filename)) {
				System.IO.File.Delete (Filename);
			}
			
			if (timer != null) {
				timer.Invalidate ();
				timer = null;
			}
			
			DismissModalViewControllerAnimated (true);
		}
		
		private void StartTheTimer ()
		{
			timer = NSTimer.CreateScheduledTimer (1, this, new Selector ("OneSecondTimer"), null, true);
		}
		
		NSTimer timer = null;
		int secondsRecorded = 0;
		
		UILabel writeToLabel = null;
		
		[Export("OneSecondTimer")]
		void OneSecondTimer ()
		{
			secondsRecorded ++;
			TimeSpan ts = new TimeSpan (0, 0, 0, secondsRecorded);

			if (_displayView != null) {
				_displayView.TimeText = ts.ToString ();
				_displayView.SetNeedsDisplay();
			}
		}
	}

	public class AudioRecordDisplayView : UIView
	{
		public string TimeText = "00:00:00";

		public override void Draw (System.Drawing.RectangleF rect)
		{
			base.Draw (rect);

			//// General Declarations
			var colorSpace = CGColorSpace.CreateDeviceRGB();
			var context = UIGraphics.GetCurrentContext();
			
			//// Color Declarations
			UIColor windowFrameColor = UIColor.FromRGBA(0.667f, 0.667f, 0.667f, 1.000f);
			UIColor gradientColor = UIColor.FromRGBA(0.980f, 0.188f, 0.027f, 0.450f);
			
			//// Gradient Declarations
			var gradientColors = new CGColor [] {UIColor.Red.CGColor, UIColor.FromRGBA(0.996f, 0.160f, 0.000f, 0.725f).CGColor, gradientColor.CGColor};
			var gradientLocations = new float [] {0, 0.49f, 1};
			var gradient = new CGGradient(colorSpace, gradientColors, gradientLocations);
			var gradient2Colors = new CGColor [] {windowFrameColor.CGColor, UIColor.FromRGBA(0.374f, 0.374f, 0.374f, 1.000f).CGColor, UIColor.Black.CGColor};
			var gradient2Locations = new float [] {0, 0.12f, 0.48f};
			var gradient2 = new CGGradient(colorSpace, gradient2Colors, gradient2Locations);
			
			//// Shadow Declarations
			var shadow = UIColor.Black.CGColor;
			var shadowOffset = new SizeF(2.1f, 1.1f);
			var shadowBlurRadius = 2;
			
			//// Abstracted Attributes
			var textContent = "Stop Recording";
			var text2Content = "Recording Audio...";
			
			//// Rectangle Drawing
			var rectanglePath = UIBezierPath.FromRect(new RectangleF(1.5f, -0.5f, 319, 641));
			context.SaveState();
			rectanglePath.AddClip();
			context.DrawLinearGradient(gradient2, new PointF(161, -0.5f), new PointF(161, 640.5f), 0);
			context.RestoreState();
			UIColor.Black.SetStroke();
			rectanglePath.LineWidth = 1;
			rectanglePath.Stroke();
			
			
			//// Oval Drawing
			var ovalPath = UIBezierPath.FromOval(new RectangleF(82, 79, 160, 160));
			context.SaveState();
			context.SetShadowWithColor(shadowOffset, shadowBlurRadius, shadow);
			context.BeginTransparencyLayer(null);
			ovalPath.AddClip();
			context.DrawLinearGradient(gradient, new PointF(218.57f, 215.57f), new PointF(105.43f, 102.43f), 0);
			context.EndTransparencyLayer();
			context.RestoreState();
			
			
			
			//// Text Drawing
			var textRect = new RectangleF(101, 125, 126, 88);
			context.SaveState();
			context.SetShadowWithColor(shadowOffset, shadowBlurRadius, shadow);
			UIColor.White.SetFill();
			new NSString(textContent).DrawString(textRect, UIFont.FromName("TrebuchetMS", 24), UILineBreakMode.WordWrap, UITextAlignment.Center);
			context.RestoreState();
			
			
			
			//// Text 2 Drawing
			var text2Rect = new RectangleF(43, 277, 241, 43);
			context.SaveState();
			context.SetShadowWithColor(shadowOffset, shadowBlurRadius, shadow);
			UIColor.White.SetFill();
			new NSString(text2Content).DrawString(text2Rect, UIFont.FromName("TrebuchetMS", 20), UILineBreakMode.WordWrap, UITextAlignment.Center);
			context.RestoreState();
			
			
			//// Text 3 Drawing
			var text3Rect = new RectangleF(41, 336, 241, 43);
			context.SaveState();
			context.SetShadowWithColor(shadowOffset, shadowBlurRadius, shadow);
			UIColor.White.SetFill();
			new NSString(TimeText).DrawString(text3Rect, UIFont.FromName("TrebuchetMS", 15), UILineBreakMode.WordWrap, UITextAlignment.Center);
			context.RestoreState();
		}
	}
}

