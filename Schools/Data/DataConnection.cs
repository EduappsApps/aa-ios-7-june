using System;
using System.Collections.Generic;
using System.IO;
using SQLite;
using UIKit;
using Foundation;

namespace Schools
{
	public class DataConnection
	{
		public class LockedSQLiteConnection : SQLiteConnection
		{
			public LockedSQLiteConnection(string path) : base(path)
			{
			}

			public override void Close ()
			{
				base.Close ();

				DataConnection.ReleaseConnection ();
			}
		}

		static System.Threading.Mutex _connectionMutex = new System.Threading.Mutex(false);

		public static void ReleaseConnection()
		{
			_connectionMutex.ReleaseMutex ();
		}

		public static string GetDatabasePath ()
		{
			string DatabaseName = "SchoolsDB107.db3"; 
			string appDataStorage = "";
			if (UIDevice.CurrentDevice.CheckSystemVersion (8, 0))
			{
				var url = NSFileManager.DefaultManager.GetUrls (NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomain.User) [0];
				appDataStorage = url.Path;
			}
			else
			{
				appDataStorage = Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments);
			}

			return Path.Combine (appDataStorage, DatabaseName); 	
		}

		public static bool HasDatabaseBeenSetup = false;
		private static SQLiteConnection _connection = null;
		private static readonly object padlock = new object ();

		public static SQLiteConnection GetConnection ()
		{
			_connectionMutex.WaitOne();

			//if (_connection == null)
				_connection = new LockedSQLiteConnection (GetDatabasePath ());

			return _connection;
		}

		public static void RemoveItem (object item)
		{
			using (var conn = GetConnection ())
			{
				conn.Delete (item);
			} 
		}

		public static bool RequiresSetup = false;

		public static void SetupDatabaseIfNeeded ()
		{
			string dbPath = GetDatabasePath ();
		
			var db = new SQLiteConnection (dbPath);

			db.CreateTable<GeneralSettings> ();
			db.CreateTable<Student> ();
			db.CreateTable<StudentCourse> ();

			db.CreateTable<School> ();
			db.CreateTable<CourseGroup> ();
			db.CreateTable<Course> ();
			db.CreateTable<Assessment> ();
			db.CreateTable<Notice> ();

			// Get the number of students
			var countCmd = db.CreateCommand("select count(*) from Student");
			if (countCmd.ExecuteScalar<int>() == 0)
			{
				RequiresSetup = true;
			}

			db.Close ();
		}
	}
}
