using System;
using System.Json;
using System.Collections.Generic;
using System.Net;
using System.IO;
using System.Linq;
using System.Text;

namespace Schools
{
	public class JsonLoaders
	{
		public static Assessment LoadAssessment(JsonObject o)
		{
			Assessment entry = new Assessment();
			entry.CourseId = Convert.ToInt64(o["CourseId"].ToString());
			entry.Description = o["Description"];
			entry.DueDate = o["DueDate"];
			entry.Id = Convert.ToInt64(o["AssessmentId"].ToString());
			entry.Link = o["WebLink"];
			entry.Name = o["Title"];
			entry.SchoolId = Convert.ToInt64(o["SchoolId"].ToString());
			entry.Weighting = Convert.ToDouble(o["Weighting"].ToString());

			// Compensate for the fact the server thinks of it differently than the app
			var et = o["AssessmentType"].ToString();

			if (et.Equals("2"))
				entry.AssType = 1;
			else
				entry.AssType = 2;

			return entry;
		}

		public static AssessmentDetails LoadAssessmentDetails(JsonObject o)
		{
			AssessmentDetails result = new AssessmentDetails ();

			result.SchoolsCount = Convert.ToInt32(o["SchoolsCount"].ToString());
			result.SchoolsSubscribedCount = Convert.ToInt32(o["SchoolsSubscribedCount"].ToString());

			var list = o ["assessments"] as JsonArray;
			List<Assessment> allItems = new List<Assessment> ();
			foreach (JsonObject item in list) {
				allItems.Add (LoadAssessment (item));
			}
			result.assessments = allItems.ToArray ();

			return result;
		}

		public static SchoolDocumentDetails LoadSchoolDocumentDetails(JsonObject o)
		{
			SchoolDocumentDetails result = new SchoolDocumentDetails ();

			result.SchoolsCount = Convert.ToInt32(o["SchoolsCount"].ToString());
			result.SchoolsSubscribedCount = Convert.ToInt32(o["SchoolsSubscribedCount"].ToString());

			var list = o ["documents"] as JsonArray;
			List<SchoolDocument> allItems = new List<SchoolDocument> ();
			foreach (JsonObject item in list) {
				allItems.Add (LoadSchoolDocument (item));
			}
			result.documents = allItems.ToArray ();

			return result;
		}

		public static Course LoadCourse(JsonObject o)
		{
			Course c = new Course();
			c.Code = o["CourseCode"];
			c.Description = o["Description"];
			c.Id = Convert.ToInt64(o["CourseId"].ToString());
			c.Name = o["CourseName"];
			c.SchoolId = Convert.ToInt64(o["SchoolId"].ToString());
			c.Subject = o["Subject"];
			//c.CourseGroup = Convert.ToInt64(o["CourseGroup"].ToString());

			return c;
		}

		public static School LoadSchool(JsonObject o)
		{
			School s = new School();
			s.Address = o["Address"];
			s.Email = o["EmailAddress"];
			s.FaxNumber = o["FaxNumber"];
			s.HasLocation = false;
			s.Latitude = 0;
			s.Longtitude = 0;
			s.Id = Convert.ToInt64(o["SchoolId"].ToString());
			s.PhoneNumber = o["PhoneNumber"];
			s.SchoolName = o["SchoolName"];
			s.WebSite = o["WebSite"];
			s.RequiresSubscription = o ["IsSubscribed"].ToString ().ToLower () != "true";

			if (o.ContainsKey ("CanteenUrl"))
				s.CanteenUrl = o ["CanteenUrl"];
			else
				s.CanteenUrl = "";

			switch (Convert.ToInt32(o["SchoolType"].ToString()))
			{
				case 1:
					s.SchoolType = SchoolType.School;
					break;
				case 2:
					s.SchoolType = SchoolType.University;
					break;
				case 3:
					s.SchoolType = SchoolType.PrimarySchool;
					break;
				case 4:
					s.SchoolType = SchoolType.MiddleSchool;
					break;
				case 5:
					s.SchoolType = SchoolType.HighSchool;
					break;
			}

			return s;
		}

		public static CourseGroup LoadCourseGroup(JsonObject o)
		{
			CourseGroup g = new CourseGroup();
			g.CourseGroupName = o["CourseGroupName"];
			g.Id = Convert.ToInt64(o["CourseGroupId"].ToString());
			g.SchoolId = Convert.ToInt64(o["SchoolID"].ToString());
			return g;
		}

		public static Notice LoadNotice (JsonObject o)
		{
			Notice result = new Notice();

			result.DateOfNotice = o["NotificationDateTime"];
			result.NoticeMessage = o["message"];

			string id = o["id"];
			result.NotificationId = Convert.ToInt64(id);

			return result;
		}

		public static SchoolDocument LoadSchoolDocument(JsonObject o)
		{
			SchoolDocument result = new SchoolDocument ();

			result.Title = o ["Title"];
			result.Description = o ["Description"];
			result.DocumentAdded = o ["DocumentAdded"];
			result.DocumentExpires = o ["DocumentExpires"];
			result.DocumentLink = o ["DocumentLink"];
			result.DocumentType = Convert.ToInt32 (o ["DocumentType"].ToString());
			result.SchoolDocumentId = Convert.ToInt64 (o ["SchoolDocumentId"].ToString());
			result.SchoolId = Convert.ToInt64 (o ["SchoolId"].ToString());

			return result;
		}

		public static List<T> LoadCollectionFromWebLink<T>(string url, ProcessJsonEntry handler) where T : class
		{
			return LoadCollectionFromWebLink<T>(url, handler, null);
		}

		public static List<T> LoadCollectionFromWebLink<T>(string url, ProcessJsonEntry handler, string transactionId
		                                                   ) where T : class
		{
			List<T> result = new List<T>();

			var request = HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "GET";
			request.Timeout = 45000;

			if (transactionId != null)
			{
				request.Headers["AssessmentsTransaction"] = transactionId;
			}

			try
			{
				if (NetworkIndicator != null)
					NetworkIndicator.Start();

				using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
				{
					if (response.StatusCode != HttpStatusCode.OK)
					{
						// Present the errir details
						Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
					} else {
						using (StreamReader reader = new StreamReader(response.GetResponseStream()))
						{
							var content = reader.ReadToEnd();

							var data = JsonObject.Parse(content);
							if (data is JsonArray)
							{
								var array = data as JsonArray;
								foreach (JsonObject o in array)
								{
									T item = handler(o) as T;
									if (item != null)
										result.Add(item);
								}
							} else if (data is JsonObject) {
								T item = handler(data as JsonObject) as T;
								if (item != null)
									result.Add(item);
							}
						}
					}
				} 
			} finally {
				if (NetworkIndicator != null)
					NetworkIndicator.Stop();
			}

			return result;
		}

		public static List<T> LoadCollectionFromWebLinkWithPOST<T>(string url, string postData, ProcessJsonEntry handler, string transactionId
		                                                   ) where T : class
		{
			List<T> result = new List<T>();

			var request = HttpWebRequest.Create(url);
			request.ContentType = "application/json";
			request.Method = "POST";
			request.Timeout = 45000;
			byte[] byteArray = Encoding.UTF8.GetBytes (postData);
			request.ContentLength = byteArray.Length;

			Stream dataStream = request.GetRequestStream ();
			dataStream.Write (byteArray, 0, byteArray.Length);
			dataStream.Close ();

			if (transactionId != null)
			{
				request.Headers["AssessmentsTransaction"] = transactionId;
			}

			try
			{
				if (NetworkIndicator != null)
					NetworkIndicator.Start();

				using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
				{
					if (response.StatusCode != HttpStatusCode.OK)
					{
						// Present the errir details
						Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
					} else {
						using (StreamReader reader = new StreamReader(response.GetResponseStream()))
						{
							var content = reader.ReadToEnd();

							var data = JsonObject.Parse(content);
							if (data is JsonArray)
							{
								var array = data as JsonArray;
								foreach (JsonObject o in array)
								{
									T item = handler(o) as T;
									if (item != null)
										result.Add(item);
								}
							} else if (data is JsonObject) {
								T item = handler(data as JsonObject) as T;
								if (item != null)
									result.Add(item);
							}
						}
					}
				} 
			} finally {
				if (NetworkIndicator != null)
					NetworkIndicator.Stop();
			}

			return result;
		}

		public static List<T> LoadCollectionFromWebLinkWithIds<T>(string urlPrefix, List<string> ids, int maxBatchSize, 
		                                                         Dictionary<string, string> additionalHeaders,
		                                                         ProcessJsonEntry handler
		                                                   ) where T : class
		{
			List<T> result = new List<T>();

			try
			{
				if (NetworkIndicator != null)
					NetworkIndicator.Start();

				// Start the process of reading in the Ids
				List<string> AllIds = ids.Select(o => o.ToString()).ToList();

				while (AllIds.Count > 0)
				{
					List<string> currentBatch = new List<string>();
					int numberToRemove;

					if (AllIds.Count > maxBatchSize)
					{
						currentBatch.AddRange(AllIds.GetRange(0, maxBatchSize));
						numberToRemove = maxBatchSize;
					} else {
						currentBatch.AddRange(AllIds);
						numberToRemove = AllIds.Count;
					}

					string batchIds = String.Join(",", currentBatch.ToArray());

					string url = urlPrefix + batchIds;

					// Load the details now
					var request = HttpWebRequest.Create(url);
					request.ContentType = "application/json";
					request.Method = "GET";
					request.Timeout = 45000;

					foreach (string key in additionalHeaders.Keys)
						request.Headers[key] = additionalHeaders[key];

					using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
					{
						if (response.StatusCode != HttpStatusCode.OK)
						{
							// Present the errir details
							Console.Out.WriteLine("Error fetching data. Server returned status code: {0}", response.StatusCode);
						} else {
							using (StreamReader reader = new StreamReader(response.GetResponseStream()))
							{
								var content = reader.ReadToEnd();
								
								var data = JsonObject.Parse(content);
								if (data is JsonArray)
								{
									var array = data as JsonArray;
									foreach (JsonObject o in array)
									{
										T item = handler(o) as T;
										if (item != null)
											result.Add(item);
									}
								} else if (data is JsonObject) {
									handler(data as JsonObject);
								}
							}
						}
					}

					// Remove the items
					AllIds.RemoveRange(0, numberToRemove);
				}
			} finally {
				if (NetworkIndicator != null)	
					NetworkIndicator.Stop();
			}
			
			return result;
		}

		public static DownloadProcessingInfo NetworkIndicator = null;
	}

	public delegate object ProcessJsonEntry(JsonObject o);

	public class DownloadProcessingInfo
	{
		public virtual void Start()
		{

		}

		public virtual void Stop()
		{

		}
	}
}

