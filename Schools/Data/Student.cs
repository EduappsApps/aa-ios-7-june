using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public class Student
	{
		[PrimaryKey, AutoIncrementAttribute]
		public int Id { get; set; }

		public string Name { get; set; }
		public long SchoolId { get; set; }

		public string DisplayName()
		{
			if (Name != null && Name.Length > 0)
				return Name;
			else
				return "No Name";
		}
	}
}

