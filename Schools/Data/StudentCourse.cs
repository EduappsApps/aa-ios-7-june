using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{

	public class StudentCourse {
		[Indexed]
		public long StudentId { get; set; }

		[Indexed]
		public long CourseId { get; set; }

		[AutoIncrementAttribute, PrimaryKey]
		public int StudentCourseId { get; set; }
	}

	public delegate void StudentDetailsSave(Student s);
	
}
