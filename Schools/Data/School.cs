using System;
using SQLite;

namespace Schools
{
	public enum SchoolType : int
	{
		School = 1,
		University = 2,
		PrimarySchool = 3,
		MiddleSchool = 4,
		HighSchool = 5
	}

	public class School
	{
		[PrimaryKey, AutoIncrement]
		public int CoreId { get; set; }

		[Indexed]
		public long Id
		{
			get; set;
		}

		public SchoolType SchoolType
		{
			get; set;
		}

		[Indexed]
		public string SchoolName
		{
			get; set;
		}

		public string Address
		{
			get; set;
		}

		public bool HasLocation
		{
			get; set;
		}

		public float Latitude 
		{
			get; set;
		}

		public float Longtitude
		{
			get; set;
		}

		public string PhoneNumber
		{
			get; set;
		}

		public string FaxNumber 
		{
			get; set;
		}

		public string Email
		{
			get; set;
		}

		public string WebSite
		{
			get; set;
		}

		public bool RequiresSubscription {
			get;
			set;
		}

		public string CanteenUrl
		{
			get;
			set;
		}
	}
}

