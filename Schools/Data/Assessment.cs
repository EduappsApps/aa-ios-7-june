using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public enum AssessmentType : int
	{
		Exam = 1,
		Assignment = 2
	}
	
	public class Assessment
	{
		[AutoIncrement, PrimaryKey]
		public int CoreId { get; set; }

		[Indexed]
		public long Id { get; set; }

		public long SchoolId { get; set; }

		public long CourseId { get; set; }

		public string Name { get; set; }

		public string Description { get; set; }
		public double Weighting { get; set; }
		public string DueDate { get; set; }
		public int AssType { get; set; }
		public string Link { get; set; } // Optional

		public AssessmentType GetAssessmentType()
		{
			return (AssessmentType)AssType;
		}

		public DateTime GetLocalDueDate()
		{
			int i = DueDate.IndexOf('(');
			int j = DueDate.IndexOf(')');
			string datePart = DueDate.Substring(i+1, j - i - 1);

			int k = datePart.IndexOf('+');
			string dp = datePart.Substring(0, k);
			string tzp = datePart.Substring(k+1);

			return new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToInt64(dp));
		}

		public string GetSummaryTitle(SQLiteConnection db)
		{
			string line = EduAppsBaseAppConstants.CalendarEntryPrefix;
//			line += GetAssessmentType() == AssessmentType.Assignment ? "Assignment" : "Exam";
//			line += ": ";

			var allStudents = GetStudents(db);
			List<string> allStudentNames = new List<string>();

			if (allStudents != null)
			{
				foreach (var n in allStudents)
					allStudentNames.Add(n.DisplayName());
			}

			line += Name + " for " + GrammarString.Join(allStudentNames.ToArray(), "and") + ".";
			if (Weighting > 0.0f)
				line += " (Worth " + Weighting.ToString("P0") + ")";
			return line;
		}

		public List<Student> GetStudents(SQLiteConnection db)
		{
			return db.Query<Student>("select * from Student where Id in (select StudentId from StudentCourse where CourseId = ?)", this.CourseId);
		}

		public Course GetCourse(SQLiteConnection db)
		{
			return LocalCache.LoadCourseFromCache(db, this.CourseId);
		}

		public string GetSubject (SQLiteConnection db)
		{
			// Get the Course
			var c = GetCourse(db);
			if (c != null)
				return c.Subject;
			else
				return "";
		}

		public string GetSchoolLocation (SQLiteConnection db)
		{
			string result = "";

			School s = LocalCache.LoadSchoolFromCache (db, SchoolId);
			result = s.SchoolName;
			if (s.Address != null && s.Address.Length > 0)
				result += ": " + s.Address;

			return result;
		}

		public bool IsSoon()
		{
			var timeDiff = GetLocalDueDate() - DateTime.Now;
			return timeDiff.TotalDays <= 30;
		}

		public string GetEmailDescription(SQLiteConnection db)
		{
			string result = "";

			result += Description;

			// Add the course name
			var c = GetCourse(db);
			if (c != null)
			{
				if (!String.IsNullOrEmpty(c.Name))
				{
					if (result.Length > 0)
						result += Environment.NewLine;
					
					result += "Course: " + c.Name;
				}
			} else {
				result += "Course: Title not available";
			}

			// Add the Subject
			var s = GetSubject(db);
			if (!String.IsNullOrEmpty(s))
			{
				if (result.Length > 0)
					result += Environment.NewLine;

				result += "Subject: " + s;
			}

			return result;
		}
	}

	// Singleton that lets people know that assessments need to reload 
	public class AssessmentReloader
	{
		#region Singleton Access 
		static readonly AssessmentReloader instance = new AssessmentReloader();
		
		// Explicit static constructor to tell C# compiler
		// not to mark type as beforefieldinit
		static AssessmentReloader()
		{
		}
		
		AssessmentReloader()
		{
		}
		
		public static AssessmentReloader Instance
		{
			get
			{
				return instance;
			}
		}

		#endregion

		public event EventHandler OnReload = null;

		public void Reload()
		{
			if (OnReload != null)
				OnReload(this, EventArgs.Empty);
		}
	}

	public class AssessmentDetails
	{
		public int SchoolsCount;
		public int SchoolsSubscribedCount;
		public Assessment[] assessments;
	}
}

