using System;
using System.Collections.Generic;

namespace Schools
{
	public class LocalCache
	{
		public static void StoreSchools (List<School> list)
		{
			using (var db = DataConnection.GetConnection ())
			{
				foreach (var item in list)
					LocalCache.StoreSchoolToCache (db, item);
			}
		}

		public static void StoreSchoolToCache(SQLite.SQLiteConnection db, School s)
		{
			try
			{
				var list = db.Query<School>("select * from School where Id = ?", s.Id);
				if (list.Count == 1)
					db.Delete(list[0]);

				db.Insert(s);
			} catch {
				Console.WriteLine("There was an issue storing the school from the cache");
			}
		}

		public static School LoadSchoolFromCache(SQLite.SQLiteConnection db, long id)
		{
			try {
				var list = db.Query<School> ("select * from School where Id = ?", id);
				if (list.Count == 1)
					return list [0];
				else
					return null;
			} catch (Exception ex) {
				return null;
			}
		}

		public static void StoreCourseGroups (List<CourseGroup> list)
		{
			using (var db = DataConnection.GetConnection ())
			{
				foreach (var item in list)
					LocalCache.StoreCourseGroupToCache (db, item);
			}
		}

		public static void StoreCourseGroupToCache(SQLite.SQLiteConnection db, CourseGroup cg)
		{
			try {
				var list = db.Query<CourseGroup> ("select * from CourseGroup where Id = ?", cg.Id);
				if (list.Count == 1)
					db.Delete (list [0]);
				
				db.Insert (cg);
			} catch (Exception ex) {
				
			}
		}

		public static CourseGroup LoadCourseGroupFromCache (SQLite.SQLiteConnection db, long id)
		{
			try {
				var list = db.Query<CourseGroup> ("select * from CourseGroup where Id = ?", id);
				if (list.Count == 1)
					return list [0];
				else
					return null;
			} catch (Exception ex) {
				return null;
			}
		}

		public static void StoreCourses (List<Course> list)
		{
			using (var db = DataConnection.GetConnection ())
			{
				foreach (var item in list)
					LocalCache.StoreCourseToCache (db, item, item.CourseGroup);
			}
		}

		public static void StoreCourseToCache(SQLite.SQLiteConnection db, Course c, long courseGroupId)
		{
			try {
				var list = db.Query<Course> ("select * from Course where Id = ?", c.Id);
				if (list.Count == 1)
					db.Delete (list [0]);
				
				db.Insert (c);
			} catch (Exception ex) {
				Console.WriteLine("Could not save course: " + c.Id + "; " + ex.Message);
			}
		}

		public static Course LoadCourseFromCache(SQLite.SQLiteConnection db, long id)
		{
			try
			{
				var list = db.Query<Course>("select * from Course where Id = ?", id);
				if (list.Count == 1)
					return list[0];
				else
					return null;
			} catch {
				return null;
			}
		}

		public static void StoreAssessments (List<Assessment> list)
		{
			using (var db = DataConnection.GetConnection ())
			{
				try
				{
					foreach (var item in list)
						LocalCache.StoreAssessmentToCache (db, item);
				} catch {
					// Log the details. Todo
				} 
			}
		}

		public static void StoreAssessmentToCache(SQLite.SQLiteConnection db, Assessment a)
		{
			try
			{
				var list = db.Query<Assessment>("select * from Assessment where Id = ?", a.Id);
				if (list.Count == 1)
					db.Delete(list[0]);

				db.Insert(a);
			} catch {

			}
		}

		public static Assessment LoadAssessmentFromCache(SQLite.SQLiteConnection db, long id)
		{
			try
			{
				var list = db.Query<Assessment>("select * from Assessment where Id = ?", id);
				if (list.Count == 1)
					return list[0];
				else
					return null;
			} catch {
				return null;
			}
		}

		public static void StoreNotices (List<Notice> list)
		{
			using (var db = DataConnection.GetConnection ())
			{
				foreach (var item in list)
					LocalCache.StoreNoticeToCache (db, item);
			}
		}


		public static void StoreNoticeToCache(SQLite.SQLiteConnection db, Notice n)
		{
			try
			{
				var list = db.Query<Assessment>("select * from Notice where NotificationId = ?", n.NotificationId);
				if (list.Count == 1)
					db.Delete(list[0]);
				
				db.Insert(n);
			} catch {
				
			}
		}
	}
}

