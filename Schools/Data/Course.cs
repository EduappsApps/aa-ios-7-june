using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	// Drill Down. School > Year Group > Subject > Course 
	// School Group list
 	public class Course
	{
		[PrimaryKey, AutoIncrement]
		public int CoreId { get; set; }
		
		[Indexed]
		public long Id { get; set; }

		[Indexed]
		public long CourseGroup { get; set; }

		public string Subject { get; set; }

		[Indexed]
		public long SchoolId { get; set; }

		public string Code { get; set; }

		[Indexed]
		public string Name { get; set; }

		public string Description { get; set; }

		public string GetStudentsForCourse(SQLiteConnection db)
		{
			var students = db.Query<Student>("select * from Student where Id in (select StudentId from StudentCourse where CourseId = ?)", Id);

			List<string> studentList = new List<string>();

			foreach (var student in students)
				studentList.Add(student.DisplayName());

			return GrammarString.Join(studentList.ToArray(), "and");
		}
	}
}

