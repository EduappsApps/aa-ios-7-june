using System;
using System.Json;
using System.Collections.Generic;

namespace Schools
{
	public class AssessmentAlertDevice
	{
#if __ANDROID__
		public static int PlatformType = 2;
		public static string PlatformName = "Android";
#else
		public static int PlatformType = 1;
		public static string PlatformName = "iOS";
#endif
	}
}
