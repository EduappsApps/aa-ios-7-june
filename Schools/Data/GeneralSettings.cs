using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public class GeneralSettings
	{
		[AutoIncrement, PrimaryKeyAttribute]
		public int Id { get; set; }

		public string CalendarName { get; set; }
		public string LastPage { get; set; }
		public string PushDeviceToken { get; set; }

		public bool OneDayNotify { get; set; }
		public bool ThreeDayNotify { get; set; }
		public bool OneWeekNotify { get; set; }
		public bool TwoWeekNotify { get; set; }
		public bool ThreeWeekNotify { get; set; }

		public string SubscriptionTransaction { get; set; }

		public string CalendarSyncDetails { get; set; }

		public bool HasAskedAboutPushNotifications { get; set; }

		public static GeneralSettings GetSettings(SQLiteConnection db) 
		{
			var list = db.Query<GeneralSettings>("select * from GeneralSettings");
			if (list.Count == 0)
			{
				GeneralSettings settings = new GeneralSettings();
				settings.LastPage = "Students";
				settings.CalendarName = "Default";

				settings.OneDayNotify = true;
				settings.ThreeDayNotify = true;
				settings.OneWeekNotify = true;
				settings.TwoWeekNotify = true;
				settings.ThreeWeekNotify = true;

				db.Insert(settings);
				return settings;
			} else
				return list[0];
		}

		public string AccountDetailsText { get; set; }

		public bool HasAccount()
		{
			return !String.IsNullOrEmpty(AccountDetailsText);
		}

		public AccountDetails GetAccountDetails()
		{
			if (String.IsNullOrEmpty(AccountDetailsText))
				return null;
			else
				return AccountDetails.FromString(AccountDetailsText, true);
		}

		public void StoreUserDetails(string username, string pwHash)
		{
			AccountDetails d = new AccountDetails() {
				Username = username,
				PwdHash = pwHash
			};

			this.AccountDetailsText = d.Encrypt();
		}

		public string GetPushNotificationSummary ()
		{
			List<string> result = new List<string>();
			if (OneDayNotify)
				result.Add("1 day");
			if (ThreeDayNotify)
				result.Add("3 days");
			if (OneWeekNotify)
				result.Add("1 wk");
			if (TwoWeekNotify)
				result.Add("2 wks");
			if (ThreeWeekNotify)
				result.Add("3 wks");

			if (result.Count > 0)
				return GrammarString.Join(result.ToArray(), "and") + " before";
			else
				return "No notifications";
		}

		public bool IsSubscriptionValid()
		{
			if (SubscriptionTransaction == null)
				return false;

			if (SubscriptionTransaction.Trim ().Length == 0)
				return false;

			try
			{
				StoredSubscriptionDetails details = StoredSubscriptionDetails.FromString(SubscriptionTransaction, true);
				return details.expiryDate >= DateTime.Now;
			} catch {
				return false;
			}
		}

		public StoredSubscriptionDetails GetSubscriptionDetails()
		{
			if (String.IsNullOrEmpty(SubscriptionTransaction))
				return null;
			else
				return StoredSubscriptionDetails.FromString(SubscriptionTransaction, true);
		}

#if __ANDROID__
		public StoredGMailDetails GetCalendarSyncDetails()
		{
			if (String.IsNullOrEmpty(CalendarSyncDetails))
				return null;
			else
				return StoredGMailDetails.FromString(CalendarSyncDetails, true);
		}

		public string GetCalendarSyncSummary()
		{
			var details = GetCalendarSyncDetails();
			if (details == null)
				return "Not currently synchronising"; 
			else
			{
				if (details.IsBlank())
					return "Not currently synchronising";
				else
					return "Synchronising to Calendar '" + details.CalendarName + "'";
			}
		}

		public void StoreGoogleCalendarDetails(string username, string pwd, string calendarName)
		{
			StoredGMailDetails details = new StoredGMailDetails() {
				Username = username, 
				Password = pwd,
				CalendarName = calendarName
			};
			
			this.CalendarSyncDetails = details.Encrypt();
		}
#endif
	}
}

