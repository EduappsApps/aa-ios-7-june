using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public class DateUtils
	{
		public static DateTime FromString(string value)
		{
			int i = value.IndexOf('(');
			int j = value.IndexOf(')');
			string datePart = value.Substring(i+1, j - i - 1);

			int k = datePart.IndexOf('+');
			string dp = datePart.Substring(0, k);
			//string tzp = datePart.Substring(k+1);

			DateTime utcConverted = new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds (Convert.ToInt64 (dp)).ToLocalTime ();
			return utcConverted;
			//return new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(Convert.ToInt64(dp));
		}
	}

	public class Notice
	{
		[AutoIncrement, PrimaryKey]
		public long NotificationId;

		public string NoticeMessage;

		public string DateOfNotice;

		public DateTime GetLocalMessageDate()
		{
			return DateUtils.FromString (DateOfNotice);
		}

		public bool IsAWebLink()
		{
			Uri test = null;
			return Uri.TryCreate (NoticeMessage, UriKind.Absolute, out test) && 
				(test.Scheme == "http" || test.Scheme == "https");
		}
	}

}
