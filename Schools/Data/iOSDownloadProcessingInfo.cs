using System;
using System.Json;
using System.Collections.Generic;
using System.Net;
using System.IO;
using UIKit;

namespace Schools
{
	public class iOSDownloadProcessingInfo : DownloadProcessingInfo
	{
		public override void Start ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
		}

		public override void Stop ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}
	}
}
