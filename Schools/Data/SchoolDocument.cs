using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	public class SchoolDocument
	{
		public long SchoolDocumentId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public int DocumentType { get; set; }
		public string DocumentLink { get; set; }
		public string DocumentExpires { get; set; }
		public string DocumentAdded { get; set; }
		public long SchoolId { get; set; }

		public DateTime GetExpiresTime()
		{
			return DateUtils.FromString (DocumentExpires);
		}

		public DateTime GetAddedTime()
		{
			return DateUtils.FromString (DocumentAdded);
		}
	}

	public class SchoolDocumentDetails
	{
		public int SchoolsCount;
		public int SchoolsSubscribedCount;
		public SchoolDocument[] documents;
	}
}
