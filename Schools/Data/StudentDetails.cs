using System;
using SQLite;
using System.Collections.Generic;

namespace Schools
{
	//public delegate void StudentDetailsSave(Student s);

	public class StudentDetails 
	{
		public Student student = null;
		public bool IsNew = true;

		public School studentSchool = null;

		public List<Course> currentCourses = new List<Course>();
		public List<Course> originalCourses = new List<Course>();

		public bool HaveCoursesChanged()
		{
			return true;
		}

		public event StudentDetailsSave AfterSave = null;

		private bool IsEmpty()
		{
			return (IsNew && (student.Name == null || student.Name.Length == 0) && this.currentCourses.Count == 0);
		}

		public void SaveDetails()
		{
			if (IsEmpty())
			{
				return;
			}

			using (var db = DataConnection.GetConnection())
			{
				if (IsNew)
					db.Insert(student);
				else
					db.Update(student);

				// Add the courses 
				if (HaveCoursesChanged())
				{
					// Delete all the courses for the student
					db.Execute("delete from StudentCourse where StudentId = " + student.Id);

					foreach (var c in currentCourses)
					{
						StudentCourse sc = new StudentCourse();
						sc.CourseId = c.Id;
						sc.StudentId = student.Id;

						db.Insert(sc);
					}

					// TODO: This should be put into an AfterSave event call. Was: 
					AssessmentReload.Current.RequiresDataReload = true;
				}
			} 

			if (AfterSave != null)
			{
				AfterSave(student);
			}

			// Make the note that the courses have changed
			DeviceStudentUpdate.PushChanges();
		}

		public void AddCourse (Course item)
		{
			bool hasItem = false;

			foreach (Course c in currentCourses)
				if (c.Id == item.Id)
				{
					hasItem = true;
					break;
				}

			if (!hasItem)
				currentCourses.Add(item);
		}

		public void RemoveCourse (Course item)
		{
			Course foundCourse = null;

			foreach (Course c in currentCourses)
				if (c.Id == item.Id)
			{
				foundCourse = c;
				break;
			}
			
			if (foundCourse != null)
				currentCourses.Remove(foundCourse);
		}
	}
}
