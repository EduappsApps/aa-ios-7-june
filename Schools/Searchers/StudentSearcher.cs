using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using MBProgressHUD;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class StudentSearcher: SearchViewController<Student>
	{
		public StudentSearcher () : base("Students")
		{
			Title = "Students";
			TabBarItem.Image = UIImage.FromFile (ImageConstants.Student);
			SearchBarPlaceholder = "Search for a Student";
		}

		public static NSString cellIdent = new NSString("StudentsFlyweight");
		
		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}

		public override UITableViewCell DrawCell (Student item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);
			
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
			}

			if (item.Name.Length > 0) {
				cell.TextLabel.Text = item.Name;
				cell.TextLabel.Alpha = 1.0f;
			} else {
				cell.TextLabel.Text = "No Name";
				cell.TextLabel.Alpha = 0.5f;
			}

			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.ImageView.Image = UIImage.FromFile (ImageConstants.Student);
			
			return cell;
		}

		public override void ApplyThemeToCell (Student item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			Items.Clear ();
			using (var conn = DataConnection.GetConnection ())
			{
				var list = conn.Query<Student> ("select * from Student");
				Items.AddRange (list);
			} 
		}

		public override void DoSearchDataRetrieve ()
		{
			base.DoSearchDataRetrieve ();

			SearchItems.Clear ();
			var likeText = "%" + SearchText + "%";
			using (var conn = DataConnection.GetConnection ())
			{
				var list = conn.Query<Student> ("select * from Student where Name like ?", likeText);
				SearchItems.AddRange (list);
			} 
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Students");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			StartAsyncDataRetrieve("Loading Students...");

			NavigationItem.SetRightBarButtonItem(PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Add, DoAddStudent), false);
			EditButtonOnRight = false;
			AddEditButton();

			if (DataConnection.RequiresSetup)
			{
				// Show the Setup Wizard from the first screen
				DoAddStudent(this, EventArgs.Empty);
			}

			SetupPullToRefresh();
		}

		StudentDetails _details = null;

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
		}

		public void StartEdit (object sender, EventArgs e)
		{
			tvc.SetEditing (true, true);

			NavigationItem.SetRightBarButtonItems (new UIBarButtonItem[]{ 
				PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Add, DoAddStudent)
				,PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Done, FinishEdit) 
			}, true);
		}


		public void FinishEdit (object sender, EventArgs e)
		{
			tvc.SetEditing (false, true);
			NavigationItem.SetRightBarButtonItems (new UIBarButtonItem[]{ 
				PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Add, DoAddStudent)
				,PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Edit, StartEdit) 
			}, true);
		}


		private void DoAddStudent(object sender, EventArgs e)
		{

			navMan = new StyledNavController();

			// Add the Student Controller
			newStudent = new Student();
			_details = new StudentDetails();
			_details.student = newStudent;
			_details.AfterSave += (Student s) => {
				Items.Add(s);
				this.tvc.RefreshTableDisplay();
				ShowFinishedAddingStudents();
				//App.Current.RequiresDataReload = true;
				AssessmentReloader.Instance.Reload ();
			};

			// Add the Student screen
			//studentCtrl = new NewStudentController();
			//studentCtrl.LoadAndSetup(_details);
			//navMan.PushViewController(studentCtrl, true);

			// Add the splash screen
			if (Items.Count > 0) {
				studentCtrl = new NewStudentController ();
				studentCtrl.alreadyRegisteredStudent = Items[0];
				studentCtrl.LoadAndSetup (_details);
				navMan.PushViewController(studentCtrl, false);
			} else {
				_iab = new IntroSplashViewController();
				_iab.details = _details;
				_iab.alreadyRegisteredStudent = null;
				navMan.PushViewController(_iab, false);
			}

			PresentModalViewController(navMan, true);
		}

		NewStudentController studentCtrl;
		IntroSplashViewController _iab;

		public override void DoRowSelected (Student item)
		{
			base.DoRowSelected (item);

			// Show the summary for the student
			StudentInfo info = new StudentInfo();
			info.LoadAndSetup(item);
			info.OriginalName = item.Name;
			NavigationController.PushViewController(info, true);
		}

		public override void RemoveItem (Student item)
		{
			using (var db = DataConnection.GetConnection())
			{
				db.Execute("delete from StudentCourse where StudentId = " + item.Id);
				db.Delete(item);
			} 

			DeviceStudentUpdate.PushChanges();
			AssessmentReload.Current.RequiresDataReload = true;
			AssessmentReloader.Instance.Reload ();
		}

		UINavigationController navMan;
		Student newStudent;

		PopupNotifierController _finishedAdding;

		private void ShowFinishedAddingStudents ()
		{
		
			var hud = new MTMBProgressHUD (View) {
				LabelText = "Student Added",
				DetailsLabelText = "Click below to see the student(s) " + EduAppsBaseAppConstants.EntriesPlural,
				RemoveFromSuperViewOnHide = true,
				Mode = MBProgressHUDMode.Text
			};

			View.AddSubview (hud);

			hud.Show (animated: true);
			hud.Hide (animated: true, delay: 3);

		}
	}
}

