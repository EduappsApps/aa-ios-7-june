using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class CourseGroupSearcher : SearchViewController<CourseGroup>
	{
		public School school = null;

		public CourseGroupSearcher () : base("Course Groups")
		{
			SearchBarPlaceholder = "Search for a course group";
		}

		public StudentDetails StudentDetails = null;

		public static NSString cellIdent = new NSString("CourseGroupsFlyweight");
		
		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}
		
		public override UITableViewCell DrawCell (CourseGroup item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);
			
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
			}
			
			cell.TextLabel.Text = item.CourseGroupName;
			cell.DetailTextLabel.Text = "at " + school.SchoolName;
			
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
			cell.ImageView.Image = UIImage.FromFile ("images/admissions_48.png");
			
			return cell;
		}

		public override void ApplyThemeToCell (CourseGroup item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (school.SchoolType == SchoolType.School)
				Title = "Years";
			else
				Title = "Departments";

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Done, (sender, e) => {
				StudentDetails.SaveDetails();
				NavigationController.DismissModalViewController(true);
			});

			StartAsyncDataRetrieve("Loading Course Groups", "");

			SetupPullToRefresh();
		}

		int Data_PageNumber = 1;
		int Search_PageNumber = 1; 
		int Records_Per_Search = 30;

		#region No Network Support
		
		PopupNotifierController NoNetworkPopup;
		bool ShowNoNetworkOnFirstAppear = false;
		bool HasShown = false;
		
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			
			HasShown = true;
			
			if (ShowNoNetworkOnFirstAppear)
			{
				DoShowNoNetwork();
				ShowNoNetworkOnFirstAppear = false;
			}

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Course Groups");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());

		}
		
		void DoShowNoNetwork()
		{
			BeginInvokeOnMainThread(delegate {
				if (NoNetworkPopup == null)
				NoNetworkPopup = new PopupNotifierController (this.View, "No network connectivity", "Displaying stored information") { 
					AutomaticTiming = false,
					Location = PopupNotifierLocation.Bottom,
					BackgroundColor = UIColor.Blue,
					UseSystemFont = true
				};
				
				NoNetworkPopup.Popup ();
			});
		}
		
		void ShowNoNetwork ()
		{
			if (HasShown) {
				DoShowNoNetwork();
			} else {
				ShowNoNetworkOnFirstAppear = true;
			}
		}
		
		void HideNoNetwork()
		{
			if (NoNetworkPopup != null)
				NoNetworkPopup.Hide();
		}
		
		#endregion

		public override void DoDataRetrieve ()
		{
			Items.Clear ();
		
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					Data_PageNumber = 1;

					// Get the list of schools from the web site
					string url = String.Format(
						Host.Prefix + "/rest/CourseGroups.ashx/SchoolCourseGroups/{0}/{1}/{2}", school.Id, Records_Per_Search, 1);
					
					var list = JsonLoaders.LoadCollectionFromWebLink<CourseGroup>(
						url, (o) => { return JsonLoaders.LoadCourseGroup(o); });

					LocalCache.StoreCourseGroups(list);
					Items.AddRange (list);

					// Determine if we can load more records
					Data_PageNumber++;

					CanLoadMoreData = list.Count == Records_Per_Search;
					HideNoNetwork();

					couldConnect = true;
				}
			} catch {
				couldConnect = false;
			}

			if (!couldConnect)
			{
				Items.Clear();
				using (var db = DataConnection.GetConnection())
				{
					var courseGroups = db.Query<CourseGroup>("select * from CourseGroup where SchoolId = ?", school.Id);
					Items.AddRange(courseGroups);
				} 

				ShowNoNetwork();
			}
		}

		public override CourseGroup[] LoadMoreItems ()
		{
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					// Get the list of schools from the web site
					string url = String.Format (
						Host.Prefix + "/rest/CourseGroups.ashx/SchoolCourseGroups/{0}/{1}/{2}", school.Id, Records_Per_Search, Data_PageNumber);
				
					var list = JsonLoaders.LoadCollectionFromWebLink<CourseGroup> (
					url, (o) => {
						return JsonLoaders.LoadCourseGroup (o); });
				
					LocalCache.StoreCourseGroups(list);

					// Determine if we can load more records
					Data_PageNumber++;
					CanLoadMoreData = list.Count == Records_Per_Search;
				
					HideNoNetwork ();
					return list.ToArray ();
				}
			} catch {
				couldConnect = false;
			}

			ShowNoNetwork();
			return new CourseGroup[] {};
		}

		public override void DoSearchDataRetrieve ()
		{
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					SearchItems.Clear ();

					Search_PageNumber = 1;

					string url = String.Format (
						Host.Prefix + "/rest/CourseGroups.ashx/SearchSchoolCourseGroups/{0}/{1}/{2}?term={3}", 
						school.Id, 30, 1, SearchText);

					var list = JsonLoaders.LoadCollectionFromWebLink<CourseGroup> (
					url, (o) => {
						return JsonLoaders.LoadCourseGroup (o); });

					LocalCache.StoreCourseGroups(list);

					SearchItems.AddRange (list);

					Search_PageNumber++;
					CanLoadMoreSearch = list.Count == Records_Per_Search;
					HideNoNetwork ();

					couldConnect = true;
				} 
			} catch {
				couldConnect = false;
			}

			if (!couldConnect)
			{
				SearchItems.Clear();

				using (var db = DataConnection.GetConnection())
				{
					var courseGroups = db.Query<CourseGroup>("select * from CourseGroup where SchoolId = ? and CourseGroupName = ?", 
					                                         school.Id, '%' + SearchText + '%');
					SearchItems.AddRange(courseGroups);
				} 
				ShowNoNetwork();
			}
		}

		public override CourseGroup[] LoadMoreSearchItems ()
		{
			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					string url = String.Format (
						Host.Prefix + "/rest/CourseGroups.ashx/SearchSchoolCourseGroups/{0}/{1}/{2}?term={3}", 
					school.Id, 30, Search_PageNumber, SearchText);
				
					var list = JsonLoaders.LoadCollectionFromWebLink<CourseGroup> (
					url, (o) => {
						return JsonLoaders.LoadCourseGroup (o); });
				
					LocalCache.StoreCourseGroups(list);

					Search_PageNumber++;
					CanLoadMoreSearch = list.Count == Records_Per_Search;
				
					HideNoNetwork ();
					couldConnect = true;

					return list.ToArray ();
				} 
			} catch {
				couldConnect = false;
			}

			ShowNoNetwork();
			return new CourseGroup[] { };
		}

		private CourseSearcher _searcher = null;
		
		public override void DoRowSelected (CourseGroup item)
		{
			base.DoRowSelected (item);
			
			// Need to display the Course Groups for the selected school
			_searcher = new CourseSearcher();
			_searcher.school = this.school;
			_searcher.coursegroup = item;
			_searcher.StudentDetails = StudentDetails;
			NavigationController.PushViewController(_searcher, true);
		}
	}
}

