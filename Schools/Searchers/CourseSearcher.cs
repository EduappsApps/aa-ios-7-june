using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class CourseSearcher : SearchViewController<Course>
	{
		public School school = null;
		public CourseGroup coursegroup = null;

		public StudentDetails StudentDetails;

		public CourseSearcher () : base(EduAppsBaseAppConstants.CoursesTitle)
		{
			SearchBarPlaceholder = "Search for a " + EduAppsBaseAppConstants.CoursesSingular;
		}
		
		public static NSString cellIdent = new NSString("CoursesFlyweight");
		
		public override NSString GetCellIdentifier ()
		{
			return cellIdent;
		}

		bool HasCourse (long id)
		{
			foreach (var c in StudentDetails.currentCourses)
				if (c.Id == id)
					return true;

			return false;
		}
		
		public override UITableViewCell DrawCell (Course item, UITableView tableView, NSIndexPath indexPath)
		{
			var cell = tableView.DequeueReusableCell (cellIdent);
			
			if (cell == null) {
				cell = new UITableViewCell (UITableViewCellStyle.Subtitle, cellIdent);
			}
			
			cell.TextLabel.Text = item.Name;

			var desc = item.Description;
			if (!String.IsNullOrEmpty(item.Code))
			{
				if (!String.IsNullOrEmpty(item.Description))
				{
					desc = "(" + item.Code + ") " + desc;
				} else {
					desc = "(" + item.Code + ")";
				}
			}

			cell.DetailTextLabel.Text = desc;

			if (HasCourse(item.Id))
				cell.Accessory = UITableViewCellAccessory.Checkmark;
			else
				cell.Accessory = UITableViewCellAccessory.None;

			cell.ImageView.Image = UIImage.FromFile ("images/subject2_48.png");

			return cell;
		}

		public override void ApplyThemeToCell (Course item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		void RemoveCourse (long id)
		{
			Course foundCourse = null;

			foreach (var c in StudentDetails.currentCourses)
				if (c.Id == id) {
					foundCourse = c;
					break;
				}

			StudentDetails.currentCourses.Remove(foundCourse);
		}

		void AddCourse (Course item)
		{
			StudentDetails.currentCourses.Add(item);
			StudentDetails.student.SchoolId = item.SchoolId;
		}

		public override void DoRowSelected (Course item)
		{
			if (HasCourse(item.Id))
			{
				// Remove the course
				RemoveCourse(item.Id);
			} else {
				AddCourse(item);
			}

			this.tvc.TableView.ReloadData();
		}
		
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Done, (sender, e) => {
				StudentDetails.SaveDetails();
				NavigationController.DismissModalViewController(true);
			});
			
			StartAsyncDataRetrieve("Loading " + EduAppsBaseAppConstants.CoursesTitle + "...", "");

			IsLoadingMoreText = "Loading...";
			//AutoLoadMore = true;
			SetupPullToRefresh();
		}

		int Data_PageNumber = 1;
		int Search_PageNumber = 1; 
		int Records_Per_Search = 30;

		#region No Network Support
		
		PopupNotifierController NoNetworkPopup;
		bool ShowNoNetworkOnFirstAppear = false;
		bool HasShown = false;
		
		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			
			HasShown = true;
			
			if (ShowNoNetworkOnFirstAppear)
			{
				DoShowNoNetwork();
				ShowNoNetworkOnFirstAppear = false;
			}

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Courses");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}
		
		void DoShowNoNetwork()
		{
			BeginInvokeOnMainThread(delegate {
				if (NoNetworkPopup == null)
				NoNetworkPopup = new PopupNotifierController (this.View, "No network connectivity", "Displaying known " + EduAppsBaseAppConstants.CoursesTitle.ToLower()) { 
					AutomaticTiming = false,
					Location = PopupNotifierLocation.Bottom,
					BackgroundColor = UIColor.Blue,
					UseSystemFont = true
				};
				
				NoNetworkPopup.Popup ();
			});
		}
		
		void ShowNoNetwork ()
		{
			if (HasShown) {
				DoShowNoNetwork();
			} else {
				ShowNoNetworkOnFirstAppear = true;
			}
		}
		
		void HideNoNetwork()
		{
			if (NoNetworkPopup != null)
				NoNetworkPopup.Hide();
		}
		
		#endregion

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			bool couldConnect = false;

			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					// TODO: Load a larger number of entries in this system
					Items.Clear ();

					Data_PageNumber = 1;

					// Get the list of schools from the web site
					string url = String.Format (
						Host.Prefix + "/rest/Courses.ashx/GetCourses/{0}/{1}/{2}/{3}", coursegroup.SchoolId, coursegroup.Id, Records_Per_Search, 1);
				
					var list = JsonLoaders.LoadCollectionFromWebLink<Course> (
					url, (o) => {
						return JsonLoaders.LoadCourse (o); });

					LocalCache.StoreCourses(list);

					Items.AddRange (list);

					// Determine if we can load more records
					Data_PageNumber++;
					CanLoadMoreData = list.Count == Records_Per_Search;
					HideNoNetwork();
					couldConnect = true;
				}
			} catch {
				couldConnect = false;
			}

			if (!couldConnect)
			{
				Items.Clear();

				using (var db = DataConnection.GetConnection())
				{
					var items = db.Query<Course>("select * from Course where SchoolId = ? and CourseGroupId = ?", 
					                             school.Id, coursegroup.Id);

					Items.AddRange(items);
				} 
				ShowNoNetwork();
			}
		}

		public override Course[] LoadMoreItems ()
		{
			bool couldConnect = false;

			if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {

				string url = String.Format (
					Host.Prefix + "/rest/Courses.ashx/GetCourses/{0}/{1}/{2}/{3}", coursegroup.SchoolId, coursegroup.Id, Records_Per_Search, Data_PageNumber);

				var list = JsonLoaders.LoadCollectionFromWebLink<Course> (
					url, (o) => {
					return JsonLoaders.LoadCourse (o);	});
				
				LocalCache.StoreCourses(list);

				// Determine if we can load more records
				Data_PageNumber++;
				CanLoadMoreData = list.Count == Records_Per_Search;
				HideNoNetwork ();
				couldConnect = true;

				return list.ToArray ();
			} else {
				couldConnect = false;
			}

			ShowNoNetwork ();
			return new Course[] {};
		}

		public override void DoSearchDataRetrieve ()
		{
			bool couldConnect = false;
			
			try
			{
				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
					SearchItems.Clear ();
					
					Search_PageNumber = 1;
					
					// Get the list of schools from the web site
					string url = String.Format (
						Host.Prefix + "/rest/Courses.ashx/SearchCourses/{0}/{1}/{2}/{3}?term={4}", coursegroup.SchoolId, coursegroup.Id, Records_Per_Search, 1, SearchText);
					
					var list = JsonLoaders.LoadCollectionFromWebLink<Course> (
						url, (o) => {
						return JsonLoaders.LoadCourse (o); });
					
					LocalCache.StoreCourses(list);

					SearchItems.AddRange (list);
					
					// Determine if we can load more records
					Search_PageNumber++;
					CanLoadMoreSearch = list.Count == Records_Per_Search;
					HideNoNetwork();
					couldConnect = true;
				}
			} catch {
				couldConnect = false;
			}
			
			if (!couldConnect)
			{
				SearchItems.Clear();
				
				using (var db = DataConnection.GetConnection())
				{
					string searchText = "%" + SearchText + "%";
					var items = db.Query<Course>("select * from Course where SchoolId = ? and CourseGroupId = ? and (CourseName like ? or Description like ?)", 
					                             school.Id, coursegroup.Id, searchText, searchText);
					CanLoadMoreSearch = false;
					SearchItems.AddRange(items);
				} 
				ShowNoNetwork();
			}
		}

		public override Course[] LoadMoreSearchItems ()
		{
			if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
				// TODO: This needs to use the searchable version by page
				
				string url = String.Format (
					Host.Prefix + "/rest/Courses.ashx/SearchCourses/{0}/{1}/{2}/{3}?term={4}", coursegroup.SchoolId, coursegroup.Id, Records_Per_Search, Search_PageNumber, SearchText);

				var list = JsonLoaders.LoadCollectionFromWebLink<Course>(
					url, (o) => { return JsonLoaders.LoadCourse(o);	});
				
				LocalCache.StoreCourses(list);

				// Determine if we can load more records
				Search_PageNumber++;
				CanLoadMoreSearch = list.Count == Records_Per_Search;
				
				HideNoNetwork();
				
				return list.ToArray();
			} else {
				ShowNoNetwork();
				return new Course[] {};
			}
		}
	}
}

