using System;
using MobileUtilities.Forms;
using Foundation;
using UIKit;
using System.Collections.Generic;
using System.Linq;
using System.Json;
using MobileUtilities.Data;
using MBProgressHUD;
using AVFoundation;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class AssessmentsList : SearchViewController<Assessment>
	{
		public AssessmentsList () : base(EduAppsBaseAppConstants.EntriesPlural)
		{
			this.Title = EduAppsBaseAppConstants.EntriesTitle;
			TabBarItem.Image = UIImage.FromFile (ImageConstants.Checkmark);
			SearchBarPlaceholder = EduAppsBaseAppConstants.EntriesSearchText;
			IsGrouped = true;
			ShowGroupIndex = false;
		}

		public override NSString GetCellIdentifier ()
		{
			return AssessmentCell.ID;
		}

		public override bool CanEditItem (Assessment item)
		{
			return false;
		}

		public override UITableViewCell DrawCell (Assessment item, UITableView tableView, NSIndexPath indexPath)
		{
			//AssessmentCell cell = tableView.DequeueReusableCell (AssessmentCell.ID) as AssessmentCell;

			//if (cell == null) {
			var cell = new AssessmentCell();
			//} 

			cell.SetAssessment(item);

			var students = StudentListForCourse(item.CourseId);
			if (students.Length > 0)
				cell.TextLabel.Text = students + ": " + item.Name;
			else
				cell.TextLabel.Text = item.Name;

			using (var db = DataConnection.GetConnection())
			{
				try
				{
					var course = item.GetCourse(db);
					string cn = course.Name;
					if (!String.IsNullOrEmpty(course.Subject))
					{
						cn += "; (" + course.Subject + ")";
					}
					cell.DetailTextLabel.Text = cn;
				} catch {
					cell.DetailTextLabel.Text = "";
				}
			}

			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

//			if (item.GetAssessmentType() == AssessmentType.Assignment)
//				cell.ImageView.Image = UIImage.FromFile (ImageConstants.Assignment);
//			else
//				cell.ImageView.Image = UIImage.FromFile (ImageConstants.Exam);

			return cell;
		}

		public override void ApplyThemeToCell (Assessment item, UITableViewCell cell)
		{
			base.ApplyThemeToCell (item, cell);

			Theme.ApplyThemeToCell(cell);
		}

		Dictionary<long, Student> _studentsHash = new Dictionary<long, Student>();
		Dictionary<long, List<Student>> _coursesHash = new Dictionary<long, List<Student>>();

		void AddStudentToCourse (long courseId, long studentId)
		{
			Student s = _studentsHash[studentId];

			if (!_coursesHash.ContainsKey(courseId))
				_coursesHash[courseId] = new List<Student>();

			if (!_coursesHash[courseId].Contains(s))
				_coursesHash[courseId].Add(s);
		}

		string StudentListForCourse (long courseId)
		{
			if (_coursesHash.ContainsKey (courseId)) {
				var list = _coursesHash [courseId];

				var names = new List<string>();
				foreach (Student s in list)
					names.Add(s.DisplayName());

				return String.Join(", ", names.ToArray());
			} else {
				return "";
			}
		}

		bool IsAStudentNameMatch(long courseId, string name)
		{
			if (_coursesHash.ContainsKey (courseId)) {
				var list = _coursesHash [courseId];
				
				foreach (Student s in list)
					if (s.Name.ToLower().Contains(name))
						return true;
			} 

			return false;
		}

		#region No Network Support
		
		PopupNotifierController NoNetworkPopup;
		bool ShowNoNetworkOnFirstAppear = false;
		bool HasShown = false;

		bool IsSubscribed()
		{
			return true;
			/*
			using (var db = DataConnection.GetConnection ()) {
				var settings = GeneralSettings.GetSettings (db);
				var subscriptionDetails = settings.GetSubscriptionDetails ();
				bool hasLegacySubscription = false;
				if (subscriptionDetails != null)
					hasLegacySubscription = subscriptionDetails.expiryDate >= DateTime.Now;
				bool hasCloudSubscription = CloudSubscriptionStorage.IsSubscribed ();
				// Write the cloud settings if you have never used it before
				hasCloudSubscription = false;
				if (hasLegacySubscription && (hasCloudSubscription == false))
					CloudSubscriptionStorage.WriteSubscriptionValidUntil (subscriptionDetails.expiryDate);
				return hasCloudSubscription || hasLegacySubscription;
			}*/
		}

		bool IsSigned(){
			return true;
			/*
			using (var db = DataConnection.GetConnection ()) {
				var settings = GeneralSettings.GetSettings (db);
				var accountDetails = settings.GetAccountDetails ();
				bool isAlreadySigned = false;
				if (accountDetails != null)
					isAlreadySigned = !accountDetails.Username.Equals ("");

				return isAlreadySigned;
			}*/
		}

		void CheckSubscription ()
		{
			UIBarButtonItem refreshBtn = PlatformButtonStyle.MakeTextButton (UIBarButtonSystemItem.Refresh, 
				new EventHandler (DoReload));
			/*UIBarButtonItem actionBtn;
			if (IsSubscribed()) {

				if (IsSigned ()) {
					actionBtn = new UIBarButtonItem ("Account", UIBarButtonItemStyle.Bordered, DoManageAccount);
				} 
				else {
					actionBtn = new UIBarButtonItem ("Register", UIBarButtonItemStyle.Bordered, DoRegisterAccount);
				}
			}
			else {
				actionBtn = new UIBarButtonItem ("Purchase", UIBarButtonItemStyle.Bordered, DoPurchase);
			}*/
			NavigationItem.LeftBarButtonItem = refreshBtn;
			//NavigationItem.SetRightBarButtonItem( actionBtn,false);
			//NavigationItem.SetRightBarButtonItems (new UIBarButtonItem[]{refreshBtn, actionBtn}, true);
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			
			HasShown = true;
			
			if (ShowNoNetworkOnFirstAppear)
			{
				DoShowNoNetwork();
				ShowNoNetworkOnFirstAppear = false;
			}

			if (AssessmentReload.Current.RequiresDataReload)
			{
				// We need to reload the data
				DoReload(this, EventArgs.Empty);
			}

			CheckSubscription (); 

			SetupiCloudWatcher ();

			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Assessments");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		NSObject keyValueNotification;

		void SetupiCloudWatcher ()
		{
			keyValueNotification = 
				NSNotificationCenter.DefaultCenter.AddObserver (
				NSUbiquitousKeyValueStore.DidChangeExternallyNotification
					, delegate (NSNotification n) {
					NSDictionary userInfo = n.UserInfo;
					NSNumber reasonNumber = (NSNumber)userInfo.ObjectForKey (NSUbiquitousKeyValueStore.ChangeReasonKey);
						int reason = reasonNumber.Int32Value; // reason change was triggered
					NSArray changedKeys = (NSArray)userInfo.ObjectForKey (NSUbiquitousKeyValueStore.ChangedKeysKey);
					var changedKeysList = new List<string> ();
					for (uint i = 0; i < changedKeys.Count; i++) {
							var key = new NSString (changedKeys.ValueAt (i).ToString()); // resolve key to a string
						changedKeysList.Add (key);
					}

					// now do something with the list...
					if (changedKeysList.Contains (CloudSubscriptionStorage.subscriptionExpiresDateKey))
						CheckSubscription (); 
			});
		}

		NonSubscribedAssessmentsViewController _purchaseScreen;

		void DoPurchase(object sender, EventArgs e)
		{


			/*
			using (var systemSound = new MonoTouch.AudioToolbox.so (NSUrl.FromFilename (path))) {
				systemSound.PlaySystemSound ();
			}*/

			/*var soundObj = MonoTouch.AudioToolbox.SystemSound.FromFile ("default.caf");
			soundObj.PlaySystemSound();*/

			/*var mediaFile = NSUrl.FromFilename("default.caf");
			var audioPlayer = AVAudioPlayer.FromUrl(mediaFile);
			audioPlayer.FinishedPlaying += delegate { audioPlayer.Dispose(); };
			audioPlayer.Play();*/


			// Show the purchase subscription screen
			_purchaseScreen = new NonSubscribedAssessmentsViewController ();
			_purchaseScreen.LoadAndSetup (null);
			NavigationController.PushViewController (_purchaseScreen, true);
		}

		void DoShowNoNetwork()
		{
			BeginInvokeOnMainThread(delegate {
				if (NoNetworkPopup == null)
					NoNetworkPopup = new PopupNotifierController (this.View, "No network connectivity", "Showing known " + EduAppsBaseAppConstants.EntriesPlural) { 
					AutomaticTiming = false,
					Location = PopupNotifierLocation.Bottom,
					BackgroundColor = UIColor.Blue
				};
				NoNetworkPopup.AnimStart += (sender, e) => {
					tvc.TableView.Frame = new CoreGraphics.CGRect(tvc.TableView.Frame.Left, tvc.TableView.Frame.Top, 
					                                                    tvc.TableView.Frame.Width, tvc.TableView.Frame.Height - NoNetworkPopup.View.Frame.Height);
				};
				NoNetworkPopup.AnimStop += (sender, e) => {
					tvc.TableView.Frame = new CoreGraphics.CGRect(tvc.TableView.Frame.Left, tvc.TableView.Frame.Top, 
					                                                    tvc.TableView.Frame.Width, tvc.TableView.Frame.Height + NoNetworkPopup.View.Frame.Height);
				};
				
				NoNetworkPopup.Popup ();
			});
		}
		
		void ShowNoNetwork ()
		{
			if (HasShown) {
				DoShowNoNetwork();
			} else {
				ShowNoNetworkOnFirstAppear = true;
			}
		}
		
		void HideNoNetwork()
		{
			if (NoNetworkPopup != null)
				NoNetworkPopup.Hide();
		}
		
		#endregion

		public bool HasLoadedEver = false;

		public override void DoDataRetrieve ()
		{
			base.DoDataRetrieve ();

			HasLoadedEver = true;

			Items.Clear ();

			_studentsHash = new Dictionary<long, Student> ();
			_coursesHash = new Dictionary<long, List<Student>> ();
			
			// Get the students
			List<Student> students;
			string allCoursesList;
			List<string> allCourses = new List<string> ();

			using (var conn = DataConnection.GetConnection ())
			{
				students = conn.Query<Student> ("select * from Student");

				if (students.Count == 0)
					return;
				
				foreach (var s in students)
					_studentsHash [s.Id] = s;
				
				// Get all the courses for a Query
				List<StudentCourse> courses = conn.Query<StudentCourse> ("select * from StudentCourse");

				foreach (StudentCourse sc in courses) {
					// Add the entry for the course
					try
					{
						AddStudentToCourse (sc.CourseId, sc.StudentId);
					} catch (Exception couldNotAddStudent) {
						OutputLogFile.Log("Error Adding Student to Course: ", couldNotAddStudent);
					}

				}

				courses = conn.Query<StudentCourse> ("select * from StudentCourse group by CourseId");

				foreach (StudentCourse sc in courses) {
					allCourses.Add (sc.CourseId.ToString ());
				}

				if (allCourses.Count == 0) {
					isLoadingAssessments = false;
					return;
				}

				allCoursesList = String.Join (",", allCourses.ToArray ());

			} 

			bool didConnect = false;

			try {
				GeneralSettings settings;
				using (var db = DataConnection.GetConnection())
				{
					settings = GeneralSettings.GetSettings(db);
				}

				if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
				
					AssessmentDownloadStrategy strategy;

					if (IsSubscribed () || EduAppsBaseAppConstants.RequireSubscription == false) {
						strategy = new SubscribedAssessmentDownloadStrategy(allCourses);
					} else {
						strategy = new NonSubscribedAssessmentDownloadStrategy(allCourses);
					}
					strategy.Populate();

					Items.AddRange (strategy.Assessments);

					HideNoNetwork ();
					didConnect = true;

					if (strategy is NonSubscribedAssessmentDownloadStrategy)
					{
						NonSubscribedAssessmentDownloadStrategy info = strategy as NonSubscribedAssessmentDownloadStrategy;
						if (info.SchoolsSubscribedCount == 0)
						{
							// We have no schools that are subscribed. Show the Purchase screen
						} else if (info.SchoolsSubscribedCount < info.SchoolsCount)
						{
							// We have some schools that are not available. Pop up a message that
							// there are schools there. 
							BeginInvokeOnMainThread(delegate {
								AskUserToPay();
							});
						}
					}
				}
			} catch (Exception blah) {
				didConnect = false;
			}

			if (didConnect == false)
			{
				Items.Clear();
				using (var conn = DataConnection.GetConnection())
				{
					var foundAssessments = conn.Query<Assessment>("select * from Assessment where CourseId in (" + allCoursesList + ")");
					Items.AddRange(foundAssessments);
				} 
				ShowNoNetwork ();
			}

			for (int i = 1; i < Items.Count ; i++) {
				for (int j = 0; j < i; j++) {
					if (Items.ElementAt (j).Id == Items.ElementAt (i).Id) {
						Items.RemoveAt (i);
						i--;
						break;
					}
				}
			}

			AssessmentReload.Current.RequiresDataReload = false;
			isLoadingAssessments = false;
		}

		UIAlertView _alert;

		bool _isAskingForPayment = false;

		private void AskUserToPay()
		{
			if (_isAskingForPayment)
				return;

			_isAskingForPayment = true;

			string header = "Subscription Required";
			string subText = "To access this information please purchase a subscription. Only in cases where the school has paid " + 
				"for the subscription for their community is access permitted without a 12 month subscription." + 
				Environment.NewLine + Environment.NewLine + 
				"Would you like to purchase a subscription now?";

			// Pop up the Alert box with Yes and Not Now
			_alert = new UIAlertView (); 
			_alert.Title = header;
			_alert.Message = subText;

			_alert.AddButton("Yes"); 
			_alert.AddButton("Not now"); 
			_alert.Canceled += (sender, e) => {
				// Hide the thing
				_alert.Hidden = true;
				_isAskingForPayment = false;
			};
			_alert.Clicked += (sender, e) => {
				if (e.ButtonIndex == 0)
				{
					// Show the purchase screen
					_alert.Hidden = true;

					DoPurchase(this, EventArgs.Empty);
				}
				_isAskingForPayment = false;
			};

			_alert.Show(); 
		}

		public override void DoSearchDataRetrieve ()
		{
			base.DoSearchDataRetrieve ();

			SearchItems.Clear ();

			var lowerText = SearchText.ToLower();

			// See if we have any Students that match that name
			foreach (var item in Items)
				if (item.Description.ToLower().Contains(lowerText) ||
				    item.Name.ToLower().Contains(lowerText) ||
				    item.Link.ToLower().Contains(lowerText) || 
				    IsAStudentNameMatch(item.CourseId, lowerText))
						SearchItems.Add(item);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			isLoadingAssessments = true;

			StartAsyncDataRetrieve("Loading " + EduAppsBaseAppConstants.EntriesPlural + "...", "");

//			NavigationItem.LeftBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Refresh, 
//			                                                       new EventHandler(DoReload));

			SetupChangeMonitor();

			SetupPullToRefresh();
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated); 

		}

		bool isLoadingAssessments = false;

		private void DoReload(object sender, EventArgs e)
		{
			if (isLoadingAssessments)
				return;

			isLoadingAssessments = true;

			StartAsyncDataRetrieve("Reloading " + EduAppsBaseAppConstants.EntriesPlural, "");

			this.tvc.TableView.ReloadData();
		}

		public override string GetGroupForItem (Assessment item)
		{
			return NaturalDateTime.FormatRange(item.GetLocalDueDate());
		}

		public override void DoRowSelected (Assessment item)
		{
			base.DoRowSelected (item);

//			VisualAssessmentViewController va = new VisualAssessmentViewController(item);
//			NavigationController.PushViewController(va, true);

			ShowAssessmentViewController c = new ShowAssessmentViewController();
			c.LoadAndSetup(item);
			NavigationController.PushViewController(c, true);
		}

		void SetupChangeMonitor ()
		{
			AssessmentReloader.Instance.OnReload += (sender, e) => {
				BeginInvokeOnMainThread(delegate {
					Reload ();
				});
			};
		}

		EnterRegistrationDetailsViewController _acctForm;
		UINavigationController _regNav;

		void DoRegisterAccount(object sender, EventArgs e)
		{
			_regNav = new StyledNavController();

			// Pop up the Enter Registration Request Details
			_acctForm = new EnterRegistrationDetailsViewController ();
			_acctForm.RegistrationMode = RegistrationDetailsMode.Register;
			_acctForm.LoadAndSetup(new object());
			_acctForm.OnRegister += HandleOnRegister;

			_regNav.PushViewController(_acctForm, false);
			_regNav.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
			this.TabBarController.PresentModalViewController(_regNav, true);
		}

		// Show the screen so they can manage their account
		void DoManageAccount(object sender, EventArgs e)
		{
			_regNav = new StyledNavController();

			// Pop up the Enter Registration Request Details
			_acctForm = new EnterRegistrationDetailsViewController ();
			_acctForm.RegistrationMode = RegistrationDetailsMode.AlreadySignedIn;
			_acctForm.LoadAndSetup(new object());
			_acctForm.OnRegister += HandleOnRegister;
			
			_regNav.PushViewController(_acctForm, false);
			_regNav.ModalTransitionStyle = UIModalTransitionStyle.FlipHorizontal;
			this.TabBarController.PresentModalViewController(_regNav, true);
		}

		void HandleOnRegister (object sender, EnterRegistrationArgs args)
		{
			GeneralSettings settings = null;

			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings(db);
			} 

			// Perform the registration of the account against the server
			string url = Host.CombineSecure("/rest/Accounts.ashx/RegisterAccount");

			// Send the argument to the Web Site for processing
			string pwdHash = MD5Utility.Hash(args.Password);
			JsonObject o = new JsonObject();
			o.Add("EmailAddress", new JsonPrimitive(args.Username));
			o.Add("PwdHash", new JsonPrimitive(pwdHash));
			o.Add("TransactionId", new JsonPrimitive(settings.GetSubscriptionDetails().transactionId));
			o.Add("DeviceType", new JsonPrimitive(1));

			string jsonContent = o.ToString();

			var resultFromRegistration = MobileUtilities.Network.Http.HttpsPost2(url, jsonContent, "", "");

			var details = JsonObject.Parse(resultFromRegistration);
			int result = Convert.ToInt32(details["Result"].ToString());
			string errorMessage = (string)details["ErrorMessage"];

			if (result == 1)
			{
				// Success. Store the registration details
				SubscriptionManager.StoreRegisteredAccount(args.Username, pwdHash);
				args.Success = true;
			} else {
				// Failure. Display the error message that we have
				args.Success = false;
				args.FailureMessage = errorMessage;
			}
		}
	}
}

