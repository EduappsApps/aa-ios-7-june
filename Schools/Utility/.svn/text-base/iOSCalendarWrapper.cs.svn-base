using System;
using System.Collections.Generic;
using MonoTouch.EventKit;
using MonoTouch.Foundation;

namespace Schools
{
	public class iOSCalendarWrapper : CalendarWrapper
	{
		EKCalendar _cal;

		public iOSCalendarWrapper (EKCalendar cal)
		{
			_cal = cal;
		}

		public override void DeleteEntry (CalendarEntry ce)
		{
			iOSCalendarEntry entry = ce as iOSCalendarEntry;
			NSError e;

			App.Current.EventStore.RemoveEvents(entry.iOSEvent, EKSpan.ThisEvent, out e);
			//App.Current.EventStore.RemoveEvent (entry.iOSEvent, EKSpan.ThisEvent, true, out e);
		}

		public override List<CalendarEntry> GetAssessmentAlerts ()
		{
			DateTime startDate = DateTime.MinValue;
			DateTime endDate = DateTime.MaxValue;
			NSPredicate query = App.Current.EventStore.PredicateForEvents (DateTime.Now.AddDays(-366), 
			                                                               DateTime.Now.AddDays(366), new EKCalendar[] { _cal });

			List<CalendarEntry> result = new List<CalendarEntry> ();
			App.Current.EventStore.EnumerateEvents(query, delegate(EKEvent currentEvent, ref bool stop)
			{
				if (currentEvent.Title.StartsWith (EduAppsBaseAppConstants.CalendarEntryPrefix))
					result.Add (new iOSCalendarEntry (currentEvent));
			});

			return result;
		}

		public override CalendarEntry GetById (string id)
		{
			EKEvent foundEvent = App.Current.EventStore.EventFromIdentifier ( id );
			return new iOSCalendarEntry(foundEvent);
		}

		public override void InsertAssessment (Assessment assessment)
		{
			EKEventStore store = App.Current.EventStore;
			
			EKEvent evt = EKEvent.FromStore(store);
			evt.Calendar = _cal;
			
			evt.StartDate = assessment.GetLocalDueDate();
			evt.EndDate = assessment.GetLocalDueDate();
			evt.Title = assessment.GetSummaryTitle();
			evt.Notes = assessment.GetEmailDescription();
			
			evt.Location = assessment.GetSchoolLocation();
			evt.Availability = EKEventAvailability.Free;
			evt.AllDay = true;
			
			if (assessment.Link != null && assessment.Link.Length > 0)
				evt.Url = new NSUrl(assessment.Link); 
			
			NSError error;
			store.SaveEvent(evt, EKSpan.ThisEvent, out error);
		}

		public override string GetName ()
		{
			return _cal.Title;
		}
	}	
}
