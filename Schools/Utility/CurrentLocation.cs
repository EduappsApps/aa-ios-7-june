using System;
using CoreLocation;
using System.Web;

namespace Schools
{
	public class CurrentLocation
	{
		public static CLLocation DiscoveredLocation = null;

		public static string AsQueryString {
			get
			{
				if (DiscoveredLocation == null)
					return "na";
				else
					return DiscoveredLocation.Coordinate.Latitude + "," + DiscoveredLocation.Coordinate.Longitude;
			}
		}
	}
}

