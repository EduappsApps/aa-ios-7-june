using System;

namespace Schools
{
	public class NaturalDateTime
	{
		private DateTime _datetime;

		public NaturalDateTime(DateTime theDateTime)
		{
			_datetime = theDateTime;
		}

		public static string Format(DateTime value)
		{
			NaturalDateTime dt = new NaturalDateTime(value);
			return dt.DefaultFormat();
		}

		public static string FormatRange(DateTime value)
		{
			NaturalDateTime dt = new NaturalDateTime(value);
			return dt.AsRange();
		}

		/// <summary>
		/// * So instead of tomorrows date it comes back with "Tomorrow". 
		/// * Instead of yesterdays date it comes back as "Yesterday"
		/// * For todays date we just put a time only. 
		/// * Times are displayed using small information. 12:00pm should be returned as noon, 3:00:00 am should be condensed to 3p, 
		///   We only need to show the hours and minutes, and only display the minutes when there are minutes. PM and AM should be 
		///   replaced with a or p. (That's probably a property to set)
		/// </summary>
		/// <returns></returns>
		public string DefaultFormat()
		{
			if (_datetime.Date == DateTime.Now.Date.AddDays(1))
				return "Tomorrow";
			
			if (_datetime.Date == DateTime.Now.Date.AddDays(-1))
				return "Yesterday";
			
			// date is today - return as time only
			if (_datetime.Date == DateTime.Now.Date)
			{
				// show Noon
				if (_datetime.Hour == 12 && _datetime.Minute == 0) 
					return "Noon";
				
				// show time in shortest format. ex:2:30p, 8:01a, 12:01p
				string ampm;
				if (_datetime.ToString("tt").ToUpper() == "PM") 
					ampm = "p";
				else
					ampm = "a";
				return _datetime.ToString("h:MM" + ampm);
			}
			
			// any other date return as short date string
			return _datetime.ToShortDateString();
		}

		public string AsRange()
		{
			if (_datetime.Date == DateTime.Now.Date)
				return "Today";

			if (_datetime.Date == DateTime.Now.Date.AddDays(1))
				return "Tomorrow";
			
			if (_datetime.Date == DateTime.Now.Date.AddDays(-1))
				return "Yesterday";

			if (_datetime.Date >= DateTime.Now.Date && _datetime.Date <= DateTime.Now.Date.AddDays(7))
				return "This week";

			if (_datetime.Date >= DateTime.Now.Date && _datetime.Date <= DateTime.Now.Date.AddDays(30))
				return "Next 30 Days";

			return _datetime.ToString("MMMM yyyy");
		}
	}
}

