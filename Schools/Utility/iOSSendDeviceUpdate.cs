using System;
using System.Net;
using System.Text;
using System.Threading;
using UIKit;

namespace Schools
{
	public partial class SendDeviceUpdate
	{
		public void AfterSend (bool success)
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}

		public void BeforeSend ()
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
		}
	}	
}
