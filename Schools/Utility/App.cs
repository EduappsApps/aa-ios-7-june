using System;
using EventKit;
using UIKit;
using Foundation;

namespace Schools
{
	public class App
	{
		#region Singleton Access

		public static App Current {
			get { return current; }
		}
		private static App current;

		#endregion

		public EKEventStore EventStore {
			get { return eventStore; }
		}
		protected EKEventStore eventStore;
		
		static App ()
		{
			current = new App();
		}

		protected App () 
		{
			eventStore = new EKEventStore ( );
			Wrapper = new iOSCalendarStore(eventStore);
		}

		public CalendarStore Wrapper;

		public bool HasBeenGrantedAccess = false;

		public void RequestAccess (UIViewController c)
		{
			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () >= 6.0) {
				App.Current.EventStore.RequestAccess (EKEntityType.Event, 
			                                      (bool granted, NSError e) => {
					HasBeenGrantedAccess = granted;

					if (granted) {

					} else {
							c.BeginInvokeOnMainThread (delegate {
							new UIAlertView ("Calendar Access Denied", 
							                 "The Application has been denied access to the Calendar. Calendar Syncronisation will not be possible until it is enabled in Privacy Settings for " + EduAppsBaseAppConstants.ApplicationName, 
					                 	  null, "OK", null).Show ();
						});
					}
				});
			} else {
				HasBeenGrantedAccess = true;
			}
		}
	}
}

