using System;
using System.Security.Cryptography;
using System.Text;
using SQLite;

namespace Schools
{
	public class MD5Utility
	{
		public static string Hash(string input)
		{
			// step 1, calculate MD5 hash from input
			MD5 md5 = System.Security.Cryptography.MD5.Create();
			byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
			byte[] hash = md5.ComputeHash(inputBytes);
			
			// step 2, convert byte array to hex string
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < hash.Length; i++)
			{
				sb.Append(hash[i].ToString("X2"));
			}
			return sb.ToString();
		}

		public static string AssessmentToString (SQLiteConnection db, Assessment a)
		{
			StringBuilder b = new StringBuilder ();

			b.AppendLine (a.GetSummaryTitle(db));
			b.AppendLine (a.GetEmailDescription(db));

			if (a.Link != null && a.Link.Length > 0)
				b.AppendLine(a.Link);
			// Get the School Name for the Assessment which will be the location name
			b.Append(a.DueDate);

			b.Append(true);

			return b.ToString();
		}

		public static string HashCalendarEntry (CalendarEntry e)
		{
			StringBuilder b = new StringBuilder ();

			b.Append(e.Name);
			b.Append(e.Description);
			b.Append(e.Location);
			b.Append(e.startDate);
			b.Append(e.endDate);
			b.Append(e.AllDay);

			return Hash (b.ToString());
		}

		public static string HashAssessment(SQLiteConnection db, Assessment a)
		{
			string content = AssessmentToString(db, a);
			return Hash(content);
		}
	}
}

