using System;
using MobileUtilities.Forms;
using UIKit;
using MessageUI;
using CoreGraphics;

namespace Schools
{
	public class DisplaySchoolNiceController : UIViewController
	{
		School _item;

		public DisplaySchoolNiceController (School item) : base()
		{
			Title = "School";
			_item = item;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.BackgroundColor = UIColor.White;

			var actions = new SchoolActions (_item);

			UIImageView schoolImage = new UIImageView (new CGRect (25, 34, 53, 53));
			schoolImage.Image = UIImage.FromFile ("finalicons/298.png");
			Add (schoolImage);

			UILabel schoolName = new UILabel (new CGRect (98, 32, 195, 56));
			schoolName.Text = _item.SchoolName;
			Add (schoolName);

			UILabel address = new UILabel (new CGRect (25, 107, 268, 35));
			address.Text = _item.Address;
			address.Lines = 0;
			address.LineBreakMode = UILineBreakMode.WordWrap;
			Add (address);

			UILabel phone = new UILabel (new CGRect (27, 151, 268, 25));
			phone.Text = "Phone: " + _item.PhoneNumber;
			Add (phone);

			UILabel fax = new UILabel (new CGRect (25, 176, 268, 25));
			fax.Text = "Fax: " + _item.FaxNumber;
			Add (fax);

			UILabel webSite = new UILabel (new CGRect (25, 211, 268, 35));
			webSite.Text = "Web: " + _item.WebSite;
			Add (webSite);

			UILabel canteen = new UILabel (new CGRect (25, 246, 268, 35));
			canteen.Text = "Canteen: " + _item.CanteenUrl;
			Add (canteen);

			// Add the buttons
			UIButton btnCall = new UIButton (new CGRect (25, 280, 116, 34));
			btnCall.SetTitle ("Call", UIControlState.Normal);
			Add (btnCall);
			btnCall.TouchUpInside += (sender, e) => {
				actions.MakeCall();
			};
			btnCall.SetTitleColor (UIColor.Blue, UIControlState.Normal);

			UIButton btnEmail = new UIButton (new CGRect (177, 280, 116, 34));
			btnEmail.SetTitle ("Email", UIControlState.Normal);
			Add (btnEmail);
			btnEmail.TouchUpInside += (sender, e) => {
				actions.Email(NavigationController);
			};
			btnEmail.SetTitleColor (UIColor.Blue, UIControlState.Normal);

			UIButton btnWebSite = new UIButton (new CGRect (25, 325, 116, 34));
			btnWebSite.SetTitle ("Web Site", UIControlState.Normal);
			Add (btnWebSite);
			btnWebSite.TouchUpInside += (sender, e) => {
				actions.VisitWebSite(NavigationController);
			};
			btnWebSite.SetTitleColor (UIColor.Blue, UIControlState.Normal);

			UIButton btnCanteen = new UIButton (new CGRect (177, 325, 116, 34));
			btnCanteen.SetTitle ("Canteen Site", UIControlState.Normal);
			Add (btnCanteen);
			btnCanteen.TouchUpInside += (sender, e) => {
				actions.VisitCanteenSite(NavigationController);
			};

			btnCanteen.SetTitleColor (UIColor.Blue, UIControlState.Normal);

			schoolName.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);
			foreach (UILabel l in new UILabel[] { address, phone, fax, webSite, canteen })
				l.Font = UIFont.FromName (Theme.SubTextFontName, Theme.SubTextFontSize);
			foreach (UIButton b in new UIButton[] { btnCall, btnEmail, btnWebSite, btnCanteen })
				b.Font = UIFont.FromName (Theme.TextFontName, Theme.TextFontSize);
		}
	}

	public class SchoolActions
	{
		School _item;

		public SchoolActions(School item)
		{
			_item = item;
		}

		public void MakeCall()
		{
			string phoneLink = "tel:";
			foreach (char c in _item.PhoneNumber)
				if (Char.IsNumber (c))
					phoneLink += c;
			UIApplication.SharedApplication.OpenUrl (new Foundation.NSUrl (phoneLink));
		}

		MFMailComposeViewController _mailController;

		public void Email(UIViewController parent)
		{
			// Open up the email dialog
			_mailController = new MFMailComposeViewController ();
			_mailController.SetToRecipients (new string[]{ _item.Email });
			_mailController.SetSubject ("");
			_mailController.SetMessageBody ("", false);
			_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
				args.Controller.DismissViewController (true, null);
			};
			parent.PresentViewController (_mailController, true, null);
		}

		WebNavigateSection _webSite;

		void ShowWebLink(UINavigationController nav, string link, string title)
		{
			_webSite = new WebNavigateSection (link, title);
			nav.PushViewController (_webSite, true);
		}

		public void VisitWebSite(UINavigationController nav)
		{
			ShowWebLink (nav, _item.WebSite, "Web Site");
		}

		public void VisitCanteenSite(UINavigationController nav)
		{
			ShowWebLink (nav, _item.CanteenUrl, "Canteen");
		}
	}

	public class DisplaySchoolController : ViewEditFormController<School>
	{
		public DisplaySchoolController () : base()
		{
			Title = "School";
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			Title = _item.SchoolName;

			// Add the aspects of the school
			Section _main = new Section (_item.SchoolName);

			_main.AddInfoText ("", _item.Address);
			//var addr = _main.AddNavigate(_item)
			//            AddMap (_item.Address, false);
			//addr.OnRetrieveAddress += (object sender, AddressDetailsArgs args) => {
			//	args.Location = _item.Latitude + "," + _item.Longtitude;
			//	args.addrType = AddressType.GPS;
			//};

			if (!String.IsNullOrEmpty (_item.WebSite)) {
				if (_item.WebSite.StartsWith ("www"))
					_item.WebSite = "http://" + _item.WebSite;

				var webLink = _main.AddWebLink ("", _item.WebSite);
			}

			if (!String.IsNullOrEmpty (_item.PhoneNumber)) {
				_main.AddInfoText ("Phone: " + _item.PhoneNumber, "").OnClick += (sender, e) => {
					string phoneLink = "tel:";
					foreach (char c in _item.PhoneNumber)
						if (Char.IsNumber (c))
							phoneLink += c;
					UIApplication.SharedApplication.OpenUrl (new Foundation.NSUrl (phoneLink));
				};
			}

			if (!String.IsNullOrEmpty (_item.Email)) {
				_main.AddInfoText ("Email: " + _item.Email, "").OnClick += (sender, e) => {

					// Open up the email dialog
					_mailController = new MFMailComposeViewController ();
					_mailController.SetToRecipients (new string[]{ _item.Email });
					_mailController.SetSubject ("");
					_mailController.SetMessageBody ("", false);
					_mailController.Finished += ( object s, MFComposeResultEventArgs args) => {
						Console.WriteLine (args.Result.ToString ());
						args.Controller.DismissViewController (true, null);
					};
					this.PresentViewController (_mailController, true, null);
				};
			}

			if (!String.IsNullOrEmpty(_item.FaxNumber))
				_main.AddInfoText ("Fax: " + _item.FaxNumber, "");
			if (!String.IsNullOrEmpty(_item.CanteenUrl))
				_main.AddWebLink ("Canteen Link", _item.CanteenUrl);

			sections.Add (_main);
		}

		MFMailComposeViewController _mailController;
	}
}

