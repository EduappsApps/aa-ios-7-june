using System;
using MobileUtilities.Forms;
using System.Collections.Generic;
using UIKit;

namespace Schools
{
	public class StudentInfo : ViewEditFormController<Student>
	{
		public StudentInfo () : base()
		{

		}

		Entry _name;

		Section _coursesSection;

		public string OriginalName {
			get;
			set;
		}

		public class ComboInfo
		{
			public StudentCourse sc;
			public Course c;
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			if (_item.Name.Length > 0)
				Title = _item.Name;
			else
				Title = "Student";

			Section details = new Section("Student");
			_name = details.AddString("Name", "", "No name", false);
			_name.Value = _item.Name;
			_name.CanDelete = false;

			// List all the schools that the student is attending courses at

			var schoolList = details.AddInfoText("Schools", "");
			schoolList.CanDelete = false;

			sections.Add(details);

			// Load the courses
			_coursesSection = new Section(EduAppsBaseAppConstants.CoursesTitle);

			int courseCount = 0;

			List<long> schoolIds = new List<long>();

			using (var db = DataConnection.GetConnection())
			{
				var coursesEnrolled = db.Query<StudentCourse>("select * from StudentCourse where StudentId = " + _item.Id);
				foreach (StudentCourse c in coursesEnrolled)
				{
					courseCount++;

					var localCourse = LocalCache.LoadCourseFromCache(db, c.CourseId);
					var courseEntry = _coursesSection.AddInfoText(localCourse.Name, localCourse.Description);
					courseEntry.CanDelete = true;
					courseEntry.Tag = new ComboInfo() { sc= c, c = localCourse };

					if (!schoolIds.Contains(localCourse.SchoolId))
					{
						schoolIds.Add(localCourse.SchoolId);
					}
				}

				List<string> schoolNames = new List<string>();
				foreach (long id in schoolIds)
				{
					var school = LocalCache.LoadSchoolFromCache(db, id);
					schoolNames.Add(school.SchoolName);
				}
				schoolList.Value = GrammarString.Join(schoolNames.ToArray());
				if (schoolList.Value.Length == 0)
					schoolList.Value = "No Schools Listed";

				if (schoolNames.Count > 1)
				{
					// Add the school name to the Course Entries
					foreach (Entry e in _coursesSection.entries)
					{
						if (e.entryType == EntryType.InfoText)
						{
							Course c = (e.Tag as ComboInfo).c;

							// Get the School Name
							var school = LocalCache.LoadSchoolFromCache(db, c.SchoolId);
							e.Label = e.Label + " (" + school.SchoolName + ")";
						}
					}
				}
			} 

			var addCourseButton = _coursesSection.AddButton("Add " + EduAppsBaseAppConstants.CoursesSingular);
			addCourseButton.CanDelete = false;
			addCourseButton.OnClick += AddCourseButtonClick;

			sections.Add(_coursesSection);
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		NewStudentController studentCtrl = null;
		UINavigationController navMan = null;
		StudentDetails _details = null;
		SchoolSearcher studSearch = null;

		private CourseGroupSearcher _cgSearcher = null;
		private CourseSearcher _cSearcher = null;
		private CourseSearcher _searcher = null;

		void AddCourseButtonClick (object sender, EventArgs e)
		{
			// Show the popup dialog for the Screen
			navMan = new StyledNavController();

			_details = new StudentDetails();
			_details.student = _item;
			_details.IsNew = false;
			_details.AfterSave += (Student s) => {
				sections.Clear();
				SetupForm ();
				TableView.ReloadData();
			};

			// Need to load the original courses

			School school = null;
			CourseGroup courseGroup = null;

			using (var db = DataConnection.GetConnection())
			{
				var courses = db.Query<Course>("select * from Course where Id in (select CourseId from StudentCourse where StudentId = ?)", _item.Id);
				_details.originalCourses.AddRange(courses);
				_details.currentCourses.AddRange(courses);

				school = LocalCache.LoadSchoolFromCache (db, _item.SchoolId);
				if (_details.originalCourses != null && _details.originalCourses.Count > 0)
					courseGroup = LocalCache.LoadCourseGroupFromCache (db, _details.originalCourses [0].CourseGroup);
			} 

			studentCtrl = new NewStudentController();
			studentCtrl.LoadAndSetup (_details);
			navMan.PushViewController (studentCtrl, false);

			if (school == null) {

				// Push the School Search form
				studSearch = new SchoolSearcher ();
				studSearch.StudentDetails = _details;
				navMan.PushViewController(studSearch, false);

			} else {

				courseGroup = null;

				if (courseGroup == null) {
					if (EduAppsBaseAppConstants.ShowCourseGroups) {
						// Need to display the Course Groups for the selected school
						_cgSearcher = new CourseGroupSearcher ();
						_cgSearcher.school = school;
						_cgSearcher.StudentDetails = _details;
						navMan.PushViewController (_cgSearcher, false);
						//NavigationController.PushViewController(_cgSearcher, true);
					} else {
						// Show the Courses for the selected school
						_cSearcher = new CourseSearcher ();
						_cSearcher.school = school;
						_cSearcher.coursegroup = new CourseGroup ();
						_cSearcher.coursegroup.CourseGroupName = "Main";
						_cSearcher.coursegroup.Id = -1;
						_cSearcher.coursegroup.SchoolId = school.Id;
						_cSearcher.StudentDetails = _details;
						navMan.PushViewController (_cSearcher, false);

						//NavigationController.PushViewController(_cSearcher, true);
					}
				} else {
					_searcher = new CourseSearcher();
					_searcher.school = school;
					_searcher.coursegroup = courseGroup;
					_searcher.StudentDetails = _details;
					navMan.PushViewController (_searcher, false);
					//NavigationController.PushViewController(_searcher, true);
				}

				/**/
			}

			PresentModalViewController(navMan, true);

		}

		public override void SaveData ()
		{
			base.SaveData ();

			_item.Name = _name.AsString;
			using (var conn = DataConnection.GetConnection())
			{
				conn.Update(_item);
			}

			if (_item.Name != OriginalName)
			{
				DeviceStudentUpdate.PushChanges();
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			AddEditButton();
		}

		protected override void RemoveEntry (Entry entry)
		{
			base.RemoveEntry (entry);

			ComboInfo combo = entry.Tag as ComboInfo;
			DataConnection.RemoveItem (combo.sc);

			AssessmentReload.Current.RequiresDataReload = true;
			DeviceStudentUpdate.PushChanges();

			AssessmentReloader.Instance.Reload ();
		}
	}
}

