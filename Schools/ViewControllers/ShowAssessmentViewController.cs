using System;
using MobileUtilities.Forms;
using System.Collections.Generic;
using UIKit;
using MonoTouch.Dialog;
using CoreGraphics;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class ShowAssessmentViewController : ViewEditFormController<Assessment>
	{
		public ShowAssessmentViewController () : base()
		{
			Title = EduAppsBaseAppConstants.EntriesSingular;
		}

		public Student student = null;

		public override void SetupForm ()
		{
			base.SetupForm ();

			Section s = new Section ("Student");

			// Find the Students related to this assessments
			var courseId = _item.CourseId;

			List<string> studentList = new List<string>();

			using (var db = DataConnection.GetConnection())
			{
				var students = db.Query<Student>("select * from Student where Id in (select StudentId from StudentCourse where CourseId = ?)", courseId);

				foreach (var student in students)
				{
					studentList.Add(student.DisplayName());
				}

				var eStudent = s.AddNavigate (GrammarString.Join(studentList.ToArray(), "and"));
				eStudent.HideNavigation = true;
				eStudent.NavigateImage = ImageConstants.Student;
				eStudent.IsReadOnly = true;
				sections.Add (s);
				if (studentList.Count <= 1)
					s.Header = "Student";
				else
					s.Header = "Students";

				Section a = new Section (_item.GetAssessmentType () == AssessmentType.Assignment ? "Assessment" : "Exam");
				if (EduAppsBaseAppConstants.FixAssessmentTitle)
					a.Header = EduAppsBaseAppConstants.FixedAssessmentTitle;

				a.AddInfoText (_item.Name, _item.Description);

				Entry assessment;
				if (_item.GetAssessmentType () == AssessmentType.Assignment) {
					assessment = a.AddNameValue ("Due", NaturalDateTime.Format (_item.GetLocalDueDate ()));
					assessment.NavigateImage = ImageConstants.Assignment;
				} else {
					assessment = a.AddNameValue ("Date", NaturalDateTime.Format (_item.GetLocalDueDate ()));
					assessment.NavigateImage = ImageConstants.Exam;
				}
				if (EduAppsBaseAppConstants.FixAssessmentDateTitle)
					assessment.Label = EduAppsBaseAppConstants.FixedAssessmentDateTitle;

				assessment.HideNavigation = true;

				var course = LocalCache.LoadCourseFromCache(db, _item.CourseId);
				
				a.AddNameValue(EduAppsBaseAppConstants.SubjectTitle, course.Name);
				if (_item.Weighting > 0)
					a.AddNameValue("Weighting", _item.Weighting.ToString("P1"));

				if (_item.Link.Length > 0)
				{
					a.AddWebLink("Web Link", _item.Link);
				}
				sections.Add(a);
			} 
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}
	}

	public class VisualAssessmentViewController : UIViewController
	{
		Assessment _assessment;

		public VisualAssessmentViewController(Assessment assessment) : base()
		{
			_assessment = assessment;
		}

		UILabel _lblTitle, _lblSubject, _lblDueDate, _lblAssessmentType, _lblWeighting;
		UIImageView _imgAssessmentType;
		UITextView _tvDescription;
		GlassButton _btnMore;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			View.BackgroundColor = UIColor.White;

			UIGradientView gr = new UIGradientView(View.Bounds);
			gr.startColor = UIColor.White;
			View.Add(gr);

			_lblTitle = new UILabel (new CGRect (20, 20, 280, 32));
			_lblTitle.Font = UIFont.BoldSystemFontOfSize (20f);
			_lblTitle.TextAlignment = UITextAlignment.Center;
			_lblTitle.Text = _assessment.Name;
			_lblTitle.BackgroundColor = UIColor.Clear;
			View.Add (_lblTitle);

			_lblSubject = new UILabel (new CGRect (20, 60, 280, 21));
			_lblSubject.Font = UIFont.SystemFontOfSize (15f);
			_lblSubject.TextAlignment = UITextAlignment.Center;

			using (var db = DataConnection.GetConnection())
			{
				_lblSubject.Text = _assessment.GetSubject (db);
			} 

			_lblSubject.BackgroundColor = UIColor.Clear;
			View.Add (_lblSubject);

			_imgAssessmentType = new UIImageView (new CGRect (168, 89, 132, 123));
			View.Add (_imgAssessmentType);


			CalendarDayView cv = new CalendarDayView(new CGRect(20, 89, 132, 123));
			cv._assessment = _assessment;
			View.Add(cv);

			_lblDueDate = new UILabel (new CGRect (20, 220, 132, 21));
			_lblDueDate.Font = UIFont.BoldSystemFontOfSize (15f);
			_lblDueDate.TextAlignment = UITextAlignment.Center;
			_lblDueDate.Text = NaturalDateTime.Format(_assessment.GetLocalDueDate ());
			_lblDueDate.BackgroundColor = UIColor.Clear;
			View.Add (_lblDueDate);

			_lblAssessmentType = new UILabel (new CGRect (168, 220, 132, 21));
			_lblAssessmentType.Font = UIFont.BoldSystemFontOfSize (15f);
			_lblAssessmentType.TextAlignment = UITextAlignment.Center;
			_lblAssessmentType.BackgroundColor = UIColor.Clear;
			View.Add (_lblAssessmentType);

			if (_assessment.GetAssessmentType () == AssessmentType.Assignment) {
				_imgAssessmentType.Image = UIImage.FromFile ("finalicons/homework.png");
				_lblAssessmentType.Text = "Assignment";
			} else {
				_imgAssessmentType.Image = UIImage.FromFile ("finalicons/desk.png");
				_lblAssessmentType.Text = "Exam";
			}

			_lblWeighting = new UILabel(new CGRect(168, 249, 132, 21));
			_lblWeighting.Font = UIFont.BoldSystemFontOfSize (15f);
			_lblWeighting.TextAlignment = UITextAlignment.Center;
			_lblWeighting.Text = "Worth: " + _assessment.Weighting.ToString("P1");
			_lblWeighting.BackgroundColor = UIColor.Clear;
			View.Add(_lblWeighting);

			// Add the Description
			_tvDescription = new UITextView(new CGRect(20, 278, 280, 128));
			_tvDescription.Text = _assessment.Description;
			_tvDescription.Editable = false;
			_tvDescription.BackgroundColor = UIColor.Clear;

			View.Add(_tvDescription);

			Title = _assessment.Name;
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Assessment Detail");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

	}
}

