using System;
using MobileUtilities.Forms;
using UIKit;

namespace Schools
{
	public class SchoolViewController : ViewEditFormController<School>
	{
		public SchoolViewController () : base()
		{
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			var details = new Section("Info");
			details.AddString("Name", "", "School name", false);
			details.AddCombo("Region", "", new string[] { "Australia", "New Zealand", "Canada"}, "Region", false);
			details.AddEmail("Email", "", "Email", false);
			details.AddButton("Click me").OnClick += (sender, e) => {

			};

			sections.Add(details);
		}

		public override void SaveData ()
		{
			base.SaveData ();

			using (var db = DataConnection.GetConnection())
			{
				db.Update(_item);
			} 
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}
	}
}

