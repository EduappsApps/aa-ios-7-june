using System;
using System.Threading;
using MobileUtilities.Forms;
using UIKit;
using Foundation;
using System.Collections.Generic;
using EventKit;
using ObjCRuntime;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class PushNotificationWhenDisplay : ViewEditFormController<GeneralSettings>
	{
		public PushNotificationWhenDisplay() : base(UITableViewStyle.Plain)
		{
			Title = "Notify";
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Show the details
		}

		Entry _oneDayBefore, _threeDaysBefore, _oneWeekBefore, _twoWeeksBefore, _threeWeeksBefore;

		public override void SetupForm ()
		{
			base.SetupForm ();

			Section main = new Section("Notify of " + EduAppsBaseAppConstants.EntriesPlural, 
			                           "Notifications are sent to subscribers only");

			_oneDayBefore = main.AddBoolean("1 day before", false, false);
			_oneDayBefore.AsBoolean = _item.OneDayNotify;

			_threeDaysBefore = main.AddBoolean("3 days before", false, false);
			_threeDaysBefore.AsBoolean = _item.ThreeDayNotify;

			_oneWeekBefore = main.AddBoolean("1 week before", false, false);
			_oneWeekBefore.AsBoolean = _item.OneWeekNotify;

			_twoWeeksBefore = main.AddBoolean("2 weeks before", false, false);
			_twoWeeksBefore.AsBoolean = _item.TwoWeekNotify;

			_threeWeeksBefore = main.AddBoolean("3 weeks before", false, false);
			_threeWeeksBefore.AsBoolean = _item.ThreeWeekNotify;

			sections.Add(main);
		}

		public override void SaveData ()
		{
			base.SaveData ();

			_item.OneDayNotify = _oneDayBefore.AsBoolean;
			_item.ThreeDayNotify = _threeDaysBefore.AsBoolean;
			_item.OneWeekNotify = _oneWeekBefore.AsBoolean;
			_item.TwoWeekNotify = _twoWeeksBefore.AsBoolean;
			_item.ThreeWeekNotify = _threeWeeksBefore.AsBoolean;

			using (var db = DataConnection.GetConnection())
			{
				db.Update(_item);
			}

			DeviceStudentUpdate.PushChanges();

			if (AfterSave != null)
				AfterSave(_item, EventArgs.Empty);
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "Push Notification When Display");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		public event EventHandler AfterSave = null;
	}
	
}
