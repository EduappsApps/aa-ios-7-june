using System;
using UIKit;

namespace Schools
{
	public class StyledNavController : UINavigationController
	{
		public StyledNavController () : base()
		{
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}
	}
}

