using System;
using MobileUtilities.Forms;
using UIKit;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class NewStudentController : ViewEditFormController<StudentDetails>
	{
		public NewStudentController () : base()
		{
			Title = "Student";
		}

		Entry _name = null;

		public Student alreadyRegisteredStudent = null;
		private CourseGroupSearcher _cgSearcher = null;
		private CourseSearcher _cSearcher = null;

		public override void SaveData ()
		{
			base.SaveData ();

			if (_item != null)
				_item.student.Name = _name.AsString;
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			var main = new Section ("", "");
			_name = main.AddString ("Name", "", "Name", false);
			_name.Value = _item.student.Name;
			sections.Add (main);

			var schoolsSection = new Section ("School");
			if (_item.studentSchool == null) {
				schoolsSection.AddButton ("Select a school").OnClick += (object sender, EventArgs e) => {

				};
			} else {
				// Show the School
				schoolsSection.AddNavigate (_item.studentSchool.SchoolName).Placeholder = _item.studentSchool.Address;
			}

			var courses = new Section ("Courses");

			// Load the courses that the student has enrolled in
			foreach (var c in _item.currentCourses) {
				// Add the list of subscribed courses
				var courseLink = courses.AddNavigate (c.Name);
				courseLink.Placeholder = c.Subject;
				courseLink.OnClick += (object sender, EventArgs e) => {
					// View the details of the course

				};
			}

			var btnCourses = courses.AddButton(EduAppsBaseAppConstants.SchoolSearchForText);

			btnCourses.OnClick += (sender, e) => {
				// Show the School List
				ss = new SchoolSearcher();
				ss.StudentDetails = this._item;
				NavigationController.PushViewController(ss, true);
			};

			if (alreadyRegisteredStudent != null) {
				School school = null;

				using (var db = DataConnection.GetConnection())
				{
					school = LocalCache.LoadSchoolFromCache (db, alreadyRegisteredStudent.SchoolId);
				} 

				if (school != null) {
					var btnSelectPrevious = courses.AddButton (EduAppsBaseAppConstants.SelectPreviousSchoolForText);

					btnSelectPrevious.OnClick += (object sender, EventArgs e) => {
						if (EduAppsBaseAppConstants.ShowCourseGroups)
						{
							// Need to display the Course Groups for the selected school
							_cgSearcher = new CourseGroupSearcher();
							_cgSearcher.school = school;
							_cgSearcher.StudentDetails = this._item;
							NavigationController.PushViewController(_cgSearcher, true);
							//NavigationController.PushViewController(_cgSearcher, true);
						} else {
							// Show the Courses for the selected school
							_cSearcher = new CourseSearcher();
							_cSearcher.school = school;
							_cSearcher.coursegroup = new CourseGroup();
							_cSearcher.coursegroup.CourseGroupName = "Main";
							_cSearcher.coursegroup.Id = -1;
							_cSearcher.coursegroup.SchoolId = school.Id;
							_cSearcher.StudentDetails = this._item;
							NavigationController.PushViewController(_cSearcher, true);

							//NavigationController.PushViewController(_cSearcher, true);
						}
					};
				} else {

				}
			}

			sections.Add(courses);
		}

		private void DoSaveDetails(object sender, EventArgs e)
		{
			_item.student.Name = _name.AsString;
			_item.SaveDetails();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Done, (sender, e) => {
				DoSaveDetails(this, EventArgs.Empty);
				NavigationController.DismissModalViewController(true);
			});
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "New Student");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		SchoolSearcher ss;
	}


}

