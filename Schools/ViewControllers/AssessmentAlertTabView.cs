using System;
using MobileUtilities.Forms;
using UIKit;

namespace Schools
{
	public class AssessmentAlertTabView : ButtonTabViewController
	{
		public override UIKit.UIImage GetBackgroundImage ()
		{
			return UIImage.FromFile("theme/tabbar-center.png");
		}

		public override UIKit.UIImage GetButtonImage ()
		{
			return UIImage.FromFile("theme/Pinwheel-Logo.png");
		}

		public override string GetTitle ()
		{
			return EduAppsBaseAppConstants.EntriesPlural;
		}
	}
}

