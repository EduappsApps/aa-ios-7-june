using System;
using MobileUtilities.Forms;
using System.Collections.Generic;
using MonoTouch.UIKit;

namespace Schools
{
	public class StudentInfo : ViewEditFormController<Student>
	{
		public StudentInfo () : base()
		{

		}

		Entry _name;

		Section _coursesSection;

		public string OriginalName {
			get;
			set;
		}

		public override void SetupForm ()
		{
			base.SetupForm ();


			if (_item.Name.Length > 0)
				Title = _item.Name;
			else
				Title = "Student";

			Section details = new Section("Student");
			_name = details.AddString("Name", "", "No name", false);
			_name.Value = _item.Name;
			_name.CanDelete = false;

			// List all the schools that the student is attending courses at

			var schoolList = details.AddInfoText("Schools", "");
			schoolList.CanDelete = false;

			sections.Add(details);

			// Load the courses
			_coursesSection = new Section(EduAppsBaseAppConstants.CoursesTitle);

			int courseCount = 0;

			List<long> schoolIds = new List<long>();

			var db = DataConnection.GetConnection();
			try
			{
				var coursesEnrolled = db.Query<StudentCourse>("select * from StudentCourse where StudentId = " + _item.Id);
				foreach (StudentCourse c in coursesEnrolled)
				{
					courseCount++;

					var localCourse = LocalCache.LoadCourseFromCache(db, c.CourseId);
					var courseEntry = _coursesSection.AddInfoText(localCourse.Name, localCourse.Description);
					courseEntry.CanDelete = true;
					courseEntry.Tag = localCourse;

					if (!schoolIds.Contains(localCourse.SchoolId))
					{
						schoolIds.Add(localCourse.SchoolId);
					}
				}

				List<string> schoolNames = new List<string>();
				foreach (long id in schoolIds)
				{
					var school = LocalCache.LoadSchoolFromCache(db, id);
					schoolNames.Add(school.SchoolName);
				}
				schoolList.Value = GrammarString.Join(schoolNames.ToArray());
				if (schoolList.Value.Length == 0)
					schoolList.Value = "No Schools Listed";

				if (schoolNames.Count > 1)
				{
					// Add the school name to the Course Entries
					foreach (Entry e in _coursesSection.entries)
					{
						if (e.entryType == EntryType.InfoText)
						{
							Course c = e.Tag as Course;

							// Get the School Name
							var school = LocalCache.LoadSchoolFromCache(db, c.SchoolId);
							e.Label = e.Label + " (" + school.SchoolName + ")";
						}
					}
				}
			} finally {
				DataConnection.ReleaseConnection();
			}

			var addCourseButton = _coursesSection.AddButton("Add " + EduAppsBaseAppConstants.CoursesSingular);
			addCourseButton.CanDelete = false;
			addCourseButton.OnClick += AddCourseButtonClick;

			sections.Add(_coursesSection);
		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		NewStudentController studentCtrl = null;
		UINavigationController navMan = null;
		StudentDetails _details = null;
		SchoolSearcher studSearch = null;

		void AddCourseButtonClick (object sender, EventArgs e)
		{
			// Show the popup dialog for the Screen
			studentCtrl = new NewStudentController();
			
			navMan = new UINavigationController();

			_details = new StudentDetails();
			_details.student = _item;
			_details.IsNew = false;
			_details.AfterSave += (Student s) => {
				sections.Clear();
				SetupForm ();
				TableView.ReloadData();
			};

			// Need to load the original courses
			var db = DataConnection.GetConnection();
			try
			{
				var courses = db.Query<Course>("select * from Course where Id in (select CourseId from StudentCourse where StudentId = ?)", _item.Id);
				_details.originalCourses.AddRange(courses);
				_details.currentCourses.AddRange(courses);
			} finally {
				DataConnection.ReleaseConnection();
			}

			studentCtrl.LoadAndSetup(_details);
			navMan.PushViewController(studentCtrl, false);

			// Push the School Search form
			studSearch = new SchoolSearcher();
			studSearch.StudentDetails = _details;
			navMan.PushViewController(studSearch, false);

			PresentModalViewController(navMan, true);
		}

		public override void SaveData ()
		{
			base.SaveData ();

			_item.Name = _name.AsString;
			var conn = DataConnection.GetConnection();
			try
			{
				conn.Update(_item);
			} finally {
				DataConnection.ReleaseConnection();
			}

			if (_item.Name != OriginalName)
			{
				DeviceStudentUpdate.PushChanges();
			}
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			AddEditButton();
		}

		protected override void RemoveEntry (Entry entry)
		{
			// Find the course
			Course c = entry.Tag as Course;

			// Remove it from the Student List
			long studentId = _item.Id;

			// Remove it from the database
			var db = DataConnection.GetConnection ();
			try
			{
				List<StudentCourse> sc = db.Query<StudentCourse> ("select * from StudentCourse where StudentId = ? and CourseId = ?",
				                                                 studentId, c.Id);
				if (sc.Count == 1) {
					db.Delete (sc [0]);
					_coursesSection.entries.Remove(entry);

					AssessmentReload.Current.RequiresDataReload = true;

					DeviceStudentUpdate.PushChanges();
				}
			} finally {
				DataConnection.ReleaseConnection();
			}
		}
	}
}

