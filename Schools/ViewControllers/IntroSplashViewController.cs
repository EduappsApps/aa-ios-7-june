using System;
using MobileUtilities.Forms;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class IntroSplashViewController : ImageAndButtonViewController
	{
		public IntroSplashViewController () : base("finalicons/IntroScreen.png", "Next", 320f, 326f)
		{
			Title = "Welcome";
		}

		public StudentDetails details;
		public Student alreadyRegisteredStudent;

		NewStudentController studentCtrl;

		protected override void DoButtonClick ()
		{
			studentCtrl = new NewStudentController();
			studentCtrl.alreadyRegisteredStudent = alreadyRegisteredStudent;
			studentCtrl.LoadAndSetup(details);
			NavigationController.PushViewController(studentCtrl, true);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			/*NavigationItem.RightBarButtonItem = PlatformButtonStyle.MakeTextButton(UIKit.UIBarButtonSystemItem.Done, 
			                                                                        DoFinish);*/
		}

		public override void ViewDidAppear(bool animated){
			base.ViewDidAppear (animated);
			GAI.SharedInstance.DefaultTracker.Set (GAIConstants.ScreenName, "IntroSplash");
			GAI.SharedInstance.DefaultTracker.Send (GAIDictionaryBuilder.CreateScreenView ().Build ());
		}

		void DoFinish(object sender, EventArgs e)
		{
			details.SaveDetails();
			DismissModalViewController (true);
		}
	}
}

