using System;
using MobileUtilities.Forms;
using UIKit;

namespace Schools
{
	public enum RegistrationDetailsMode
	{
		Register,
		SignIn,
		AlreadySignedIn
	}

	public class EnterRegistrationArgs : EventArgs
	{
		public EnterRegistrationArgs() : base()
		{

		}

		public string Username;
		public string Password;
		public bool Success;
		public string FailureMessage;
	}

	public delegate void EnterRegistrationHandler(object sender, EnterRegistrationArgs args);

	public class EnterRegistrationDetailsViewController : ViewEditFormController<object>
	{
		public EnterRegistrationDetailsViewController () : base()
		{
			Title = "Account";
		}

		Section _main;
		Entry _username, _password, _btnRegister;

		void ShowMessage(string msg)
		{
			//new UIAlertView ("Debug", msg, null, "OK").Show ();
		}

		public override void SetupForm ()
		{
			base.SetupForm ();

			GeneralSettings settings;
			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings(db);
			}

			sections.Clear ();

			ShowMessage ("Sections");

			var sSubscription = new Section ("Subscription");
			var expiryDate = sSubscription.AddNavigate ("Expires");
			expiryDate.HideNavigation = true;

			DateTime expiryDateTime = DateTime.MinValue;
			expiryDateTime = CloudSubscriptionStorage.GetExpiryDate();
			if (expiryDateTime == DateTime.MinValue)
			{
				expiryDateTime = settings.GetSubscriptionDetails ().expiryDate;
			}

			expiryDate.Value = expiryDateTime.ToString ("dddd dd MMMM yyyy");
			expiryDate.NavigateImage = ImageConstants.Calendar;
			expiryDate.NoSelect = true;
			sections.Add (sSubscription);

			if (RegistrationMode == RegistrationDetailsMode.Register) {
				sSubscription.AddInfoText ("", "By registering you will be able to use " + EduAppsBaseAppConstants.ApplicationName + " on your other iOS devices. ");

			} else if (RegistrationMode == RegistrationDetailsMode.AlreadySignedIn) {
				// We need to have 
				//sSubscription.AddInfoText ("", "You are registered on this device with the following credentials. ");
			} else if (RegistrationMode == RegistrationDetailsMode.SignIn) {
				//sSubscription.AddInfoText ("", "Sign in to see the details of the account.");
			}

			ShowMessage ("Account Details");

			_main = new Section ("Account Details");
			_username = _main.AddString ("Username", "", "Username", RegistrationMode == RegistrationDetailsMode.AlreadySignedIn);
			_password = _main.AddPassword ("Password", "", "Password", RegistrationMode == RegistrationDetailsMode.AlreadySignedIn);

			if (RegistrationMode == RegistrationDetailsMode.AlreadySignedIn) {
				var accountDetails = settings.GetAccountDetails();
				_username.Value = accountDetails != null ? accountDetails.Username : "";
				_password.Value = accountDetails != null ? accountDetails.PwdHash : "";
			}

			if (RegistrationMode == RegistrationDetailsMode.Register) {
				_btnRegister = _main.AddButton ("Register");
				_btnRegister.OnClick += DoRegister;
			}

			ShowMessage ("Done");

			sections.Add(_main);
		}

		public RegistrationDetailsMode RegistrationMode = RegistrationDetailsMode.SignIn;

		private ShowProcessingAlertView _registeringAlert;

		void DoRegister (object sender, EventArgs e)
		{
			_btnRegister.DisableButton();

			_storedUsername = _username.Value;
			_storedPassword = _password.Value;

			// Show the Alert that we are registering
			_registeringAlert = new ShowProcessingAlertView("Registering the User Account", "");
			_registeringAlert.Show();

			// Start the thread that registers the account
			System.Threading.Thread t = new System.Threading.Thread(InternalRegister);
			t.Start();
		}

		string _storedUsername, _storedPassword;

		private void InternalRegister()
		{
			var args = new EnterRegistrationArgs();

			// Register the account with the system
			if (OnRegister != null)
			{
				args.Username = _storedUsername;
				args.Password = _storedPassword;

				OnRegister(this, args);
			}

			// Hide the 
			this.BeginInvokeOnMainThread(delegate {
				_registeringAlert.Close();

				// Based on the result we will perform the storage or display
				if (!args.Success) {
					_btnRegister.EnableButton();
					DialogHelpers.ShowMessage("There was a problem registering", args.FailureMessage);
				} else {
					DismissModalViewController(true);
				}
			});
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			//NavigationItem.RightBarButtonItem = PlatformButtonStyle. MakeTextButton (UIBarButtonSystemItem.Done, DoCancel);

			NavigationItem.LeftBarButtonItem = PlatformButtonStyle.MakeTextButton(UIBarButtonSystemItem.Cancel, DoCancel);

			if (RegistrationMode == RegistrationDetailsMode.AlreadySignedIn)
				NavigationItem.SetRightBarButtonItem(new UIBarButtonItem("Sign out", PlatformButtonStyle.Bordered, DoSignout), true);
			else if (RegistrationMode == RegistrationDetailsMode.Register)
			{
				// Don't need to do anything

			}
		}

		void DoCancel(object sender, EventArgs e)
		{
			DismissModalViewController(true);
		}

		void DoSignout(object sender, EventArgs e)
		{
			using (var db = DataConnection.GetConnection())
			{
				var settings = GeneralSettings.GetSettings(db);
				settings.AccountDetailsText = "";
				settings.SubscriptionTransaction = "";

				db.Update(settings);
			}

			DismissModalViewController(true);

			// Remove the User Account Details
			/*this.RegistrationMode = RegistrationDetailsMode.Register;
			SetupForm();
			TableView.ReloadData();*/
			NavigationController.PopViewController (false);
			NonSubscribedAssessmentsViewController _purchaseScreen;
			_purchaseScreen = new NonSubscribedAssessmentsViewController ();
			_purchaseScreen.LoadAndSetup (null);
			NavigationController.PushViewController (_purchaseScreen, true);

		}

		protected override void ApplyThemeToCell (UITableViewCell cell, Entry e)
		{
			base.ApplyThemeToCell (cell, e);

			Theme.ApplyThemeToCell(cell);
			Theme.ApplyThemeToEntryControl(e);
		}

		public event EnterRegistrationHandler OnRegister = null;
	}
}

