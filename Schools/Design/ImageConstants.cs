using System;

namespace Schools
{
	public class ImageConstants
	{
		public static string Checkmark = "finalicons/checkmark.png";

		public static string School = "finalicons/school.png";
		public static string University = "finalicons/university.png";
		public static string CourseGroup = "";
		public static string Course = "";
		public static string Assessment = "";
		public static string Calendar = "finalicons/calendar.png";
		public static string Student = "finalicons/user.png";
		public static string Notices = "finalicons/bullhorn.png";

		public static string Assignment = "finalicons/homework.png";
		public static string Exam = "finalicons/exam.png";

		public static string TabAssessments = "AppIcon29x29.png";
		public static string TabStudents = "finalicons/friends.png";
		public static string TabCalendar = "finalicons/bar-icon-cal-white.png";
	}
}

