using System;
using Foundation;
using System.Text;
using System.Net;
using System.IO;
using MobileUtilities.Data;
using System.Json;

namespace Schools {
	public static class SubscriptionManager {

		static SubscriptionManager ()
		{
		}

		public static bool AddYear (string transactionId)
		{
			StoredSubscriptionDetails details;

			GeneralSettings settings = null;

			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings(db);
			}

			details = settings.GetSubscriptionDetails();

			if (details == null)
			{
				details = new StoredSubscriptionDetails();
				details.expiryDate = DateTime.Now.AddYears(1);
			} else {
				details.expiryDate = details.expiryDate.AddYears(1);
			}
			details.platformType = "iOS";
			details.transactionId = transactionId;

			// Subscribe using the cloud as well
			string fullDateString = details.expiryDate.ToString ("yyyy-M-d");

			var store = NSUbiquitousKeyValueStore.DefaultStore;

			store.SetString ("subscriptionExpiresDate", fullDateString); 
			store.Synchronize();

			// Submit the Transaction to the web site
			return SendAndStoreSubscriptionDetails(details);
		}

		public static bool AddYearFromDate (string transactionIdentifier, DateTime transactionDate)
		{
			StoredSubscriptionDetails details = new StoredSubscriptionDetails();
			details.expiryDate = transactionDate.AddYears(1);
			details.platformType = "iOS";
			details.transactionId = transactionIdentifier;

			return SendAndStoreSubscriptionDetails(details);
		}

		static bool SendAndStoreSubscriptionDetails (StoredSubscriptionDetails details)
		{
			string normalDetails = details.ToString();
			var o = new JsonObject();
			o.Add("payload", new JsonPrimitive(normalDetails));
			string toPost = o.ToString();

			using (var db = DataConnection.GetConnection ())
			{
				var settings = GeneralSettings.GetSettings (db);

				var originalSubscription = details.transactionId;

				var subscribeUrl = Host.CombineSecure("/rest/Devices.ashx/Subscribe2");
				try
				{
					// Post the details to the web site
					if (MobileUtilities.Network.Reachability.IsHostReachable (Host.Name)) {
						using (var client = new WebClient())
						{
							byte[] dataToPost = Encoding.Default.GetBytes(toPost);
							byte[] responseData = client.UploadData(subscribeUrl, "POST", dataToPost);
							var result = System.Text.Encoding.UTF8.GetString(responseData);

							if (result.Equals("true"))
							{
								settings.SubscriptionTransaction = details.Encrypt();
								db.Update(settings);
								return true;
							} else {
								Console.WriteLine("Could not Store the subscription for the system");
								return false;
							}
						}
					} else {
						Console.WriteLine("Could not reach the host for subscription");
						return false;		
					}
				} catch (Exception e) {
					Console.WriteLine("There was a fundamental issue subscribing: "+ e.Message);
					return false;
				}
			}
		}

		public static void StoreRegisteredAccount (string username, string pwdHash)
		{
			using (var db = DataConnection.GetConnection())
			{
				var settings = GeneralSettings.GetSettings(db);
				settings.StoreUserDetails(username, pwdHash);
				db.Update(settings);
			}
		}
	}
}