using System;
using System.Text;
using System.Net;
using System.IO;
using MobileUtilities.Data;
using System.Json;
using System.Collections.Generic;

namespace Schools {

	public class StoredGMailDetails : BaseEncryptedData
	{
		public string Username;
		public string Password;
		public string CalendarName;

		public bool IsBlank ()
		{
			return String.IsNullOrWhiteSpace(Username) && String.IsNullOrWhiteSpace(Password) && String.IsNullOrWhiteSpace(CalendarName);
		}

		public override string ToString ()
		{
			return Salt(6) + GetAsJson() + Salt (5);
		}

		private string GetAsJson()
		{
			JsonObject o = new JsonObject (new KeyValuePair<string, JsonValue>[] {
				new KeyValuePair<string, JsonValue> ("Username", new JsonPrimitive (Username)),
				new KeyValuePair<string, JsonValue> ("Pwd", new JsonPrimitive (Password)),
				new KeyValuePair<string, JsonValue> ("Calendar", new JsonPrimitive (CalendarName))
			});

			return o.ToString();
		}

		public static StoredGMailDetails FromString(string value, bool isEncrypted)
		{
			if (isEncrypted)
				value = EncryptionHelper.DecryptString(value, "FdwtRyk!vtYma2d");

			// Get rid of the salt
			value = value.Substring(6);
			value = value.Substring(0, value.Length - 5);
			
			// Process the JSON that we are dealing with

			var o = JsonObject.Parse(value);

			StoredGMailDetails result = new StoredGMailDetails();

			result.Username = o["Username"];
			result.Password = o["Pwd"];
			result.CalendarName = o["Calendar"];

			return result;
		}
	}
	
}