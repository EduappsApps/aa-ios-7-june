using System;
using System.Collections.Generic;
using System.Linq;

namespace Schools
{
	/// <summary>
	/// This is the AppDelegate for Assessment Alert
	/// </summary>
	public class EduAppsBaseAppConstants
	{
		public const string ApplicationName = "Assessment Alert";
		public const string EntriesTitle = "Assessments";
		public const string EntriesPlural = "Assessments";
		public const string EntriesSingular = "Assessment";
		public const string EntriesSearchText = "Search for an assessment";
		public const string NoticesPlural = "Notices";
		public const string NoticesSearchForText = "Search for a notice";
		public const string SchoolTitle = "School/University";
		public const string SchoolSearchForText = "Choose School/University";
		public const string SelectPreviousSchoolForText = "Select Previous School/University";
		public const string CoursesTitle = "Courses";
		public const string CoursesSingular = "Course";
		public const bool ShowCourseGroups = true;
		public const string CalendarEntryPrefix = "[Assessment Alert] ";

		public const string HostName = "rest.assessmentalert.com";
		public const string HostPrefix = "http://rest.assessmentalert.com";
		public const string HostSecurePrefix = "https://rest.assessmentalert.com";

		public const bool FixAssessmentTitle = false;
		public const string FixedAssessmentTitle = "Assessment";
		public const string SubjectTitle = "Course";
		public const bool FixAssessmentDateTitle = false;
		public const string FixedAssessmentDateTitle = "Date";

		public const string SchoolDocumentsPlural = "Documents";
		public const string SchoolDocumentsSearchText = "Search for Document";

		public const bool RequireSubscription = false; 

		public const string GATrackingId = "UA-40066392-3";
	}
}
