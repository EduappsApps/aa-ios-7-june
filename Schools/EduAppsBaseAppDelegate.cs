using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

using Foundation;
using UIKit;
using MobileUtilities.Forms;
using MobileUtilities.Mapping;
using System.Threading;
using System.Net;
using MobileUtilities.Network;
using AVFoundation;
using GoogleAnalytics.iOS;

namespace Schools
{
	public class EduAppsBaseAppDelegate : UIApplicationDelegate
	{

		protected CoreLocation.CLLocationManager	locationManager;

		// class-level declarations
		protected UIWindow window;
		protected UINavigationController assessmentsNav;
		protected SchoolSearcher searcher;
		
		protected UITabBarController tabBar;
		
		protected UINavigationController studentNav;
		protected StudentSearcher students;
		protected AssessmentsList assList;
		protected UINavigationController ctrlCalendar;
		protected CalendarSyncController cs;
		protected NonSubscribedAssessmentsViewController nonSubscribed;

		protected DocumentsTypeSearcher schoolForms;
		protected DocumentsTypeSearcher schoolNewsletters;

		protected DiagnosticsViewController diagnosis;

		public IGAITracker Tracker;

		#region Push Notifications Support

		[DllImport(ObjCRuntime.Constants.ObjectiveCLibrary, EntryPoint="objc_msgSend")]
		static extern IntPtr IntPtr_objc_msgSend_IntPtr_IntPtr (IntPtr deviceHandle, IntPtr setterHandle, IntPtr setterHandle1, IntPtr setterHandle2);

		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
			var oldDeviceToken = NSUserDefaults.StandardUserDefaults.StringForKey("PushDeviceToken");
			
			//There's probably a better way to do this
			var strFormat = new NSString("%@");

			//var dt = new NSString(IntPtr_objc_msgSend_IntPtr_IntPtr(new ObjCRuntime.Class("NSString").Handle, new ObjCRuntime.Selector("stringWithFormat:").Handle, strFormat.Handle, deviceToken.Handle).ToString());
			//var newDeviceToken = dt.ToString().Replace("<", "").Replace(">", "").Replace(" ", "");

			var newDeviceToken = deviceToken.Description;
			if (!string.IsNullOrWhiteSpace(newDeviceToken))
				newDeviceToken = newDeviceToken.Replace("<", "").Replace(">", "").Replace(" ", "");

			if (string.IsNullOrEmpty(oldDeviceToken) || !deviceToken.Equals(newDeviceToken))
			{
				// TODO: Upload the notification to the server
				RegisterDevice(newDeviceToken);
			}

			Console.WriteLine("DeviceToken ", newDeviceToken);

			using (var db = DataConnection.GetConnection())
			{
				var settings = GeneralSettings.GetSettings(db);
				settings.PushDeviceToken = newDeviceToken;
				db.Update(settings);
			}
			
			NSUserDefaults.StandardUserDefaults.SetString(newDeviceToken, "PushDeviceToken");
		}
		
		string _RegisterURL;
		Thread _RegisterThread;
		
		void RegisterDevice (string deviceToken)
		{
			_RegisterURL = Host.CombineSecure("/rest/Devices.ashx/Add?platform=iOS&device=" + Uri.EscapeUriString (deviceToken));
			
			_RegisterThread = new System.Threading.Thread (PerformRegister);
			_RegisterThread.Start ();
		}
		
		void PerformRegister (object arg)
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			using (var ns = new NSAutoreleasePool ()){
				// Make the request to the system and get it happening
				var result = Http.HttpsPost(_RegisterURL, "", "", "");
				
				//TODO: Ensure that it is done ok for here
				if (result.Equals("true"))
				{
					// We are good
				} else {
					// We could not unregister the device
					
					// TODO: Make a note that it could not be registered
				}
			}
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}
		
		string _unRegisterURL;
		Thread _unRegisterThread;
		
		void UnRegisterDevice(string deviceToken)
		{
			_unRegisterURL = Host.CombineSecure("/rest/Devices.ashx/Remove?platform=iOS&device=" + Uri.EscapeUriString(deviceToken));
			
			_unRegisterThread = new System.Threading.Thread(PerformUnRegister);
			_unRegisterThread.Start();
		}

		void PerformUnRegister (object arg)
		{
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
			using (var ns = new NSAutoreleasePool ()){
				// Make the request to the system and get it happening
				var result = Http.HttpsPost(_unRegisterURL, "", "", "");
				
				//TODO: Ensure that it is done ok for here
				if (result.Equals("true"))
				{
					// We are good
				} else {
					// We could not unregister the device
					
					// TODO: Make a note that it could not be registered
					
				}
			}
			UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
		}
		
		public override void FailedToRegisterForRemoteNotifications (UIApplication application, NSError error)
		{
			Console.WriteLine("Failed to register for notifications");
		}
		
		public override void ReceivedRemoteNotification (UIApplication application, NSDictionary userInfo)
		{
			//This method gets called whenever the app is already running and receives a push notification
			// YOU MUST HANDLE the notifications in this case.  Apple assumes if the app is running, it takes care of everything
			// this includes setting the badge, playing a sound, etc.
			processNotification (userInfo, false);
		}

		// Check the comments from http://roycornelissen.wordpress.com/2011/05/12/push-notifications-in-ios-with-monotouch/
		// There were fixes in the system for this. 
		void processNotification(NSDictionary options, bool fromFinishedLaunching)
		{
			//Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
			if (null != options && options.ContainsKey(new NSString("aps")))
			{
				//Get the aps dictionary
				NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;
				
				string alert = string.Empty;
				string sound = string.Empty;
				
				int badge = -1;
				
				//Extract the alert text
				//NOTE: If you're using the simple alert by just specifying "  aps:{alert:"alert msg here"}  "
				//      this will work fine.  But if you're using a complex alert with Localization keys, etc., your "alert" object from the aps dictionary
				//      will be another NSDictionary... Basically the json gets dumped right into a NSDictionary, so keep that in mind
				if (aps.ContainsKey(new NSString("alert")))
					alert = (aps[new NSString("alert")] as NSString).ToString();
				
				//Extract the sound string
				if (aps.ContainsKey(new NSString("sound")))
					sound = (aps[new NSString("sound")] as NSString).ToString();
				
				//Extract the badge
				if (aps.ContainsKey(new NSString("badge")))
				{
					string badgeStr = (aps[new NSString("badge")] as NSObject).ToString();
					int.TryParse(badgeStr, out badge);
				}

				//If this came from the ReceivedRemoteNotification while the app was running,
				// we of course need to manually process things like the sound, badge, and alert.
				if (!fromFinishedLaunching)
				{
					//Manually set the badge in case this came from a remote notification sent while the app was open
					if (badge >= 0) {
						/*UIApplication.SharedApplication.ApplicationIconBadgeNumber = badge;

						if (UIApplication.SharedApplication.ApplicationIconBadgeNumber > 0)
							tabBar.TabBar.Items [2].BadgeValue = string.Format ("{0}", UIApplication.SharedApplication.ApplicationIconBadgeNumber);
						else
							tabBar.TabBar.Items [2].BadgeValue = null;*/
					}

					try
					{
						var path = NSBundle.PathForResourceAbsolute ("Notification", "aiff", NSBundle.MainBundle.ResourcePath);

						AVPlayer player = new AVPlayer(NSUrl.FromFilename(path));
						player.Play();
						/*
						//Manually play the sound
						if (!string.IsNullOrEmpty(sound))
						{
							//This assumes that in your json payload you sent the sound filename (like sound.caf)
							// and that you've included it in your project directory as a Content Build type.
							var soundObj = MonoTouch.AudioToolbox.SystemSound.FromFile(sound);
							soundObj.PlaySystemSound();
						} else {
							var soundObj = MonoTouch.AudioToolbox.SystemSound.FromFile ("default.caf");
							//var soundObj = MonoTouch.AudioToolbox.SystemSound.FromFile(UILocalNotification.DefaultSoundName);
							soundObj.PlaySystemSound();
						}*/
					} catch {
						// Log this error
					}
					
					// Manually show an alert
					if (!string.IsNullOrEmpty(alert))
					{
						UIAlertView avAlert = new UIAlertView("Notification", alert, null, "OK", null);
						avAlert.Show();
					}
				}
			}
			
			//You can also get the custom key/value pairs you may have sent in your aps (outside of the aps payload in the json)
			// This could be something like the ID of a new message that a user has seen, so you'd find the ID here and then skip displaying
			// the usual screen that shows up when the app is started, and go right to viewing the message, or something like that.
			try
			{
				if (null != options && options.ContainsKey(new NSString("assessmentAlertDetails")))
				{
					var launchWithCustomKeyValue = (options[new NSString("assessmentAlertDetails")] as NSString).ToString();
					
					//You could do something with your customData that was passed in here
					if (launchWithCustomKeyValue == "Course Refresh")
					{
						// Reload the assessments and re-sync the calendar
						AssessmentReload.Current.RequiresDataReload = true;
					} else {
						// Check to see if it is a notification for a newsletter or form
						if (launchWithCustomKeyValue.StartsWith("form:"))
						{
							// Get the URL with the form details
							string url = launchWithCustomKeyValue.Substring (5);
							OpenSchoolDocumentPageAtUrl (schoolFormNav, schoolForms, url, "Form");
						} else if (launchWithCustomKeyValue.StartsWith("newsletter:"))
						{
							string url = launchWithCustomKeyValue.Substring (11);
							OpenSchoolDocumentPageAtUrl (schoolNewslettersNav, schoolNewsletters, url, "Newsletter");
						}
					}
				}
			} catch {
				//TODO: Log this. At the very least let it go through
			}
		}

		void OpenSchoolDocumentPageAtUrl (UINavigationController nav, DocumentsTypeSearcher searcher, string url, string title)
		{
			// Open up the tab at the appropriate point
			tabBar.SelectedViewController = nav;
			nav.PopToViewController (searcher, true);
			searcher.Reload ();

			// Create the new form and display it
			WebNavigateSection webDisplay = new WebNavigateSection (url, title);
			nav.PushViewController (webDisplay, true);
		}
		
		#endregion

		protected UINavigationController ctrlNotices;
		protected NoticesSearcher noticesList;

		protected virtual void BeforeSetup()
		{

		}

		protected UINavigationController schoolFormNav;
		protected UINavigationController schoolNewslettersNav;

		protected UINavigationController enrolledSchoolNav;

		EnrolledSchoolSearcher enrolledSchools;

		//
		// This method is invoked when the application has loaded and is ready to run. In this 
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			SetupConstants();

			DataConnection.SetupDatabaseIfNeeded ();
			
			JsonLoaders.NetworkIndicator = new iOSDownloadProcessingInfo();

			GeneralSettings settings = null;
			using (var db = DataConnection.GetConnection())
			{
				settings = GeneralSettings.GetSettings (db);
			} 
			
			Util.RequestLocation (newLocation => {
				CurrentLocation.DiscoveredLocation = newLocation;
			});
			
			window = new UIWindow (UIScreen.MainScreen.Bounds);
			
			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () >= 5.0f)
				SetupTheme ();
			
			// Setup the Paged Controller (Students, Assessments, Sync)
			//tabBar = new AssessmentAlertTabView();
			tabBar = new UITabBarController ();
			
			// Student Searcher
			studentNav = new StyledNavController ();
			students = new StudentSearcher ();
			studentNav.PushViewController (students, false);
			
			// Assessment Searcher
			assessmentsNav = new StyledNavController ();
			
			assList = new AssessmentsList ();
			assessmentsNav.PushViewController (assList, false);

			//if (settings.IsSubscriptionValid () || EduAppsBaseAppConstants.RequireSubscription == false) {
			//	assessmentsNav.PushViewController (assList, false);
			//} else {
			//	nonSubscribed = new NonSubscribedAssessmentsViewController();
			//	nonSubscribed.AssessmentsViewController = assList;
			//	nonSubscribed.LoadAndSetup(new object());
			//	assessmentsNav.PushViewController(nonSubscribed, false);
			//}

			ctrlNotices = new StyledNavController ();
			noticesList = new NoticesSearcher ();
			ctrlNotices.PushViewController (noticesList, false);

			schoolFormNav = new StyledNavController ();
			schoolForms = new DocumentsTypeSearcher (1, "Forms", "finalicons/105.png", "Search for a form");
			schoolFormNav.PushViewController (schoolForms, false);

			schoolNewslettersNav = new StyledNavController ();
			schoolNewsletters = new DocumentsTypeSearcher (2, "Newsletters", "finalicons/354-newspaper.png", "Search for a newsletter");
			schoolNewslettersNav.PushViewController (schoolNewsletters, false);

			enrolledSchoolNav = new StyledNavController ();
			enrolledSchools = new EnrolledSchoolSearcher ();
			enrolledSchoolNav.PushViewController (enrolledSchools, false);

			ctrlCalendar = new StyledNavController();
			cs = new CalendarSyncController();
			cs.LoadAndSetup(new object());
			ctrlCalendar.PushViewController(cs, false);

			diagnosis = new DiagnosticsViewController ();
			diagnosis.LoadAndSetup (null);

			tabBar.ViewControllers = GetTabViewControllers ();
			
			if (!DataConnection.RequiresSetup)
			{
				tabBar.SelectedViewController = assessmentsNav;
			} else {
				tabBar.SelectedViewController = studentNav;

			}

			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

			/*
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 1;

			if (UIApplication.SharedApplication.ApplicationIconBadgeNumber > 0)
				tabBar.TabBar.Items [2].BadgeValue = string.Format ("{0}", UIApplication.SharedApplication.ApplicationIconBadgeNumber);
			else
				tabBar.TabBar.Items [2].BadgeValue = null;
*/
			//window.AddSubview (studentNav.View);
			//window.RootViewController = studentNav;

			window.AddSubview (tabBar.View);
			window.RootViewController = tabBar;
			
			window.MakeKeyAndVisible ();

			if (MobileUtilities.Multimedia.DeviceHardware.GetOSVersion () > 8.0) {
				var pushSettings = UIUserNotificationSettings.GetSettingsForTypes (UIUserNotificationType.Alert |
					UIUserNotificationType.Badge | UIUserNotificationType.Sound, new NSSet());
				UIApplication.SharedApplication.RegisterUserNotificationSettings (pushSettings);
				UIApplication.SharedApplication.RegisterForRemoteNotifications ();
			} else {
				UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
				UIApplication.SharedApplication.RegisterForRemoteNotificationTypes (notificationTypes);
			}

			//Setup Google Analytics 
			GAI.SharedInstance.DispatchInterval = 20;
			GAI.SharedInstance.TrackUncaughtExceptions = true;

			Tracker = GAI.SharedInstance.GetTracker (EduAppsBaseAppConstants.GATrackingId);

			/*#if DEBUG

			#else
			UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(UIRemoteNotificationType.Alert
			                                                                   | UIRemoteNotificationType.Badge
			                                                                   | UIRemoteNotificationType.Sound);
			#endif*/
			return true;
		}

		public override void WillEnterForeground (UIApplication application)
		{
			// NOTE: Don't call the base implementation on a Model class
			// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events
			//throw new NotImplementedException ();
		}

		protected virtual UIViewController[] GetTabViewControllers ()
		{
			return new UIViewController[] { 
				studentNav, assessmentsNav, ctrlNotices, schoolNewslettersNav, 
				schoolFormNav, enrolledSchoolNav, ctrlCalendar 
			};
		}

		public virtual void SetupTheme()
		{
			// Subclasses to override
		}

		public virtual void SetupConstants()
		{
			// Subclasses to override
		}
	}

	// The UIApplicationDelegate for the application. This class is responsible for launching the 
	// User Interface of the application, as well as listening (and optionally responding) to 
	// application events from iOS.
	
}
