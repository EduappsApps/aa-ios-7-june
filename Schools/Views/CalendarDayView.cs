using System;
using UIKit;
using CoreGraphics;
using Foundation;

namespace Schools
{
	public class CalendarDayView : UIView
	{
		public CalendarDayView (CGRect r) : base(r)
		{
			BackgroundColor = UIColor.Clear;
		}

		public Assessment _assessment;

		public override void Draw (CGRect rect)
		{
			base.Draw (rect);

			// Need to draw the Rectangle on the left hand side first
			//// General Declarations
			var colorSpace = CGColorSpace.CreateDeviceRGB();
			var context = UIGraphics.GetCurrentContext();
			
			//// Color Declarations
			UIColor dateRed = UIColor.FromRGBA(0.800f, 0.109f, 0.000f, 1.000f);
			UIColor dateBlue = UIColor.FromRGBA(0.0f, 0.0f, 1.0f, 1.0f);
			
			//// Gradient Declarations
			var greyGradientColors = new CGColor [] {UIColor.White.CGColor, UIColor.FromRGBA(0.572f, 0.572f, 0.572f, 1.000f).CGColor, UIColor.Black.CGColor};
			var greyGradientLocations = new nfloat [] {0.65f, 0.75f, 0.75f};
			var greyGradient = new CGGradient(colorSpace, greyGradientColors, greyGradientLocations);
			
			UIColor dropShadowColor = UIColor.FromRGBA(0.476f, 0.476f, 0.476f, 0.380f);

			//// Shadow Declarations
			var dropShadow = dropShadowColor.CGColor;
			var dropShadowOffset = new CGSize(2.1f, 2.1f);
			var dropShadowBlurRadius = 1;

			//// Frames
			var frame = rect;
			
			var textContent = _assessment.GetLocalDueDate().Day.ToString();
			var text2Content = _assessment.GetLocalDueDate().ToString("MMM").ToUpper();


			//// Rounded Rectangle Drawing
			var roundedRectangleRect = new CGRect(frame.GetMinX() + 0.5f, frame.GetMinY() + 0.5f, frame.Width - 3, frame.Height - 3);
			var roundedRectanglePath = UIBezierPath.FromRoundedRect(roundedRectangleRect, 4);
			context.SaveState();
			context.SetShadow(dropShadowOffset, dropShadowBlurRadius, dropShadow);
			context.BeginTransparencyLayer(null);
			roundedRectanglePath.AddClip();
			context.DrawLinearGradient(greyGradient,
			                           new CGPoint(roundedRectangleRect.GetMidX(), roundedRectangleRect.GetMinY()),
			                           new CGPoint(roundedRectangleRect.GetMidX(), roundedRectangleRect.GetMaxY()),
			                           0);
			context.EndTransparencyLayer();
			context.RestoreState();
			
			UIColor.Black.SetStroke();
			roundedRectanglePath.LineWidth = 1;
			roundedRectanglePath.Stroke();
			
			
			//// Rounded Rectangle 2 Drawing
			var roundedRectangle2Path = UIBezierPath.FromRoundedRect(new CGRect(frame.GetMinX() + 1, frame.GetMinY() + (float)Math.Floor((frame.Height - 3) * 0.71795f + 0.5f), frame.Width - 4, frame.Height - 3 - (float)Math.Floor((frame.Height - 3) * 0.71795f + 0.5f)), UIRectCorner.BottomLeft | UIRectCorner.BottomRight, new CGSize(4, 4));

			if (_assessment.GetLocalDueDate() <= DateTime.Now.Date.AddDays(30))
				dateRed.SetFill();
			else
				dateBlue.SetFill();
			roundedRectangle2Path.Fill();

			//// Text Drawing
			var textRect = new CGRect(frame.GetMinX() + 1, frame.GetMinY() - 1, frame.Width - 4, frame.Height - 13);
			UIColor.Black.SetFill();
			new NSString(textContent).DrawString(textRect, UIFont.FromName("Helvetica-Bold", 56), UILineBreakMode.WordWrap, UITextAlignment.Center);
			
			//// Text 2 Drawing
			var text2Rect = new CGRect(frame.GetMinX() + 1, frame.GetMinY() + 26, frame.Width - 4, frame.Height - 26);
			UIColor.White.SetFill();
			new NSString(text2Content).DrawString(text2Rect, UIFont.FromName("HelveticaNeue-Bold", 9), UILineBreakMode.WordWrap, UITextAlignment.Center);

		}
	}
}

